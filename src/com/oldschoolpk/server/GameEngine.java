package com.oldschoolpk.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import com.oldschoolpk.server.core.cycle.CycleEventHandler;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;

/**
 * @author lare96 <http://github.org/lare96>
 */
public final class GameEngine implements Runnable {

	private static final AtomicBoolean LOCK = new AtomicBoolean();
	private static final ScheduledExecutorService GAME_SERVICE = Executors.newSingleThreadScheduledExecutor();
	private static final ExecutorService EXECUTOR_SERVICE = Executors.newCachedThreadPool();

	private static State currentState = State.GAME;

	public static void initialize() {
		if (LOCK.compareAndSet(false, true)) {
			GameEngine gameEngine = new GameEngine();
			GAME_SERVICE.scheduleAtFixedRate(gameEngine, 600, 300, TimeUnit.MILLISECONDS);
		}
	}

	@Override
	public void run() {
		try {
			if (currentState == State.GAME) {
				cycle();
				currentState = State.IO;
			} else if (currentState == State.IO) {
				subCycle();
				currentState = State.GAME;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void cycle() {
		World.process();
		Server.npcHandler.process();
		Server.objectManager.process();
		Server.itemHandler.process();
		CycleEventHandler.getSingleton().process();
	}

	private static void subCycle() {
		for (int i = 1; i < World.lastIndex + 1; i++) {
			Player player = World.players[i];
			if (player == null) {
				continue;
			}
//			player.preProcessing();
//			player.getClient().processQueuedPackets();
//			player.postProcessing();
		}
	}

	public static void execute(Runnable task) {
		EXECUTOR_SERVICE.execute(task);
	}

	private enum State {
		IO, GAME
	}

	private GameEngine() {
	}
}
