package com.oldschoolpk.server.constants;

public class Magic {

	public static final int MODERN_SPELLBOOK = 0;
	public static final int ANCIENT_SPELLBOOK = 1;
	public static final int LUNAR_SPELLBOOK = 2;
	
}