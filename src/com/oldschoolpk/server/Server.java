package com.oldschoolpk.server;

import java.util.logging.Logger;

import com.oldschoolpk.server.core.cycle.CycleEventHandler;
import com.oldschoolpk.server.core.cycle.impl.GlobalHelpEvent;
import com.oldschoolpk.server.core.cycle.impl.InterfaceTextEvent;
import com.oldschoolpk.server.core.cycle.impl.RestoreStatsEvent;
import com.oldschoolpk.server.core.cycle.impl.SpecialRestoreEvent;
import com.oldschoolpk.server.core.tools.XStreamUtil;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.model.npc.drop.Drops;
import com.oldschoolpk.server.net.InboundChannelHandler;
import com.oldschoolpk.server.net.codec.RS2LoginProtocolDecoder;
import com.oldschoolpk.server.net.codec.RS2PacketEncoder;
import com.oldschoolpk.server.sanction.SanctionHandler;
import com.oldschoolpk.server.sanction.StarterHandler;
import com.oldschoolpk.server.util.Checksum;
import com.oldschoolpk.server.util.cache.defs.ItemDef;
import com.oldschoolpk.server.util.cache.defs.NPCDef;
import com.oldschoolpk.server.util.cache.defs.ObjectDef;
import com.oldschoolpk.server.util.cache.region.Region;
import com.oldschoolpk.server.world.ClanManager;
import com.oldschoolpk.server.world.ItemHandler;
import com.oldschoolpk.server.world.ObjectHandler;
import com.oldschoolpk.server.world.ObjectManager;
import com.oldschoolpk.server.world.RegionSystem;
import com.oldschoolpk.server.world.ShopHandler;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * Server.java
 *
 * @author Sanity
 * @author Graham
 * @author Blake
 * @author Ryan Lmctruck30
 */

public class Server {

	public static boolean UpdateServer = false;
	public static ItemHandler itemHandler = new ItemHandler();
	public static NPCHandler npcHandler = new NPCHandler();
	public static ObjectHandler objectHandler = new ObjectHandler();
	public static ObjectManager objectManager = new ObjectManager();
	public static ShopHandler shopHandler = new ShopHandler();
	public static ClanManager clanManager = new ClanManager();
	public static RegionSystem region;// = new RegionSystem();
	private static Logger logger = Logger.getLogger(Server.class.getName());
	
	public static void main(String[] args) throws Exception {
		logger.info("Initializing " + Config.SERVER_NAME + "...");

		Checksum.getClientChecksum();

		ObjectDef.loadConfig();
		Region.load();
		ItemDef.unpackConfig();
		NPCDef.unpackConfig();
		Drops.parse();
		// new Motivote(new RewardHandler(), "",
		// "").start();
		SanctionHandler.loadSanctionList();
		StarterHandler.initialize();
		XStreamUtil.loadAllFiles();
		GameEngine.initialize();

		bind(43594);
		CycleEventHandler.getSingleton().addEvent(new InterfaceTextEvent(), 1);
		CycleEventHandler.getSingleton().addEvent(new RestoreStatsEvent(), 100);
		CycleEventHandler.getSingleton().addEvent(new SpecialRestoreEvent(), 29);
		CycleEventHandler.getSingleton().addEvent(new GlobalHelpEvent(), 200);

		logger.info(Config.SERVER_NAME + " is now online!");
	}

	private static void bind(int servicePort) {
		try {
			final ServerBootstrap bootstrap = new ServerBootstrap();

			bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {

				@Override
				protected void initChannel(SocketChannel channel) throws Exception {
					channel.pipeline().addLast("encoder", new RS2PacketEncoder());
					channel.pipeline().addLast("decoder", new RS2LoginProtocolDecoder());
					channel.pipeline().addLast("handler", new InboundChannelHandler());
				}
			});

			bootstrap.channel(NioServerSocketChannel.class);
			bootstrap.group(new NioEventLoopGroup());
			bootstrap.bind(servicePort);
			System.out.println("Success! Listening at port {}..." + servicePort);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(0);
		}
	}

	public static RegionSystem getRegion() {
		return region;
	}
}
