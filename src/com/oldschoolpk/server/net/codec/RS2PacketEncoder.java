package com.oldschoolpk.server.net.codec;

import java.util.List;

import com.oldschoolpk.server.net.NettyPacket;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

public final class RS2PacketEncoder extends MessageToMessageEncoder<NettyPacket> {

	@Override
	protected void encode(ChannelHandlerContext ctx, NettyPacket msg, List<Object> out) throws Exception {
		out.add(msg.getPayload());
	}

}
