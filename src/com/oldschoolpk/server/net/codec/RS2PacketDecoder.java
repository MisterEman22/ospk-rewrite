package com.oldschoolpk.server.net.codec;

import java.util.List;

import com.oldschoolpk.server.model.Client;
import com.oldschoolpk.server.net.NettyPacket;
import com.oldschoolpk.server.util.ISAACRandomGen;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

class RS2PacketDecoder extends ByteToMessageDecoder {

	private ISAACRandomGen isaac;
	private int opcode = -1;
	private int length = -1;

	RS2PacketDecoder(ISAACRandomGen isaac) {
		this.isaac = isaac;
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf buffer, List<Object> out) {
		if (opcode == -1) {
			if (buffer.readableBytes() < 1) {
				return;
			}

			opcode = buffer.readUnsignedByte();
			opcode = (opcode - isaac.getNextKey()) & 0xFF;
			length = Client.PACKET_SIZES[opcode];
		}

		if (length == -1) {
			if (buffer.readableBytes() < 1) {
				return;
			}

			length = buffer.readUnsignedByte();
		}

		if (buffer.readableBytes() < length) {
			return;
		}

		try {
			out.add(new NettyPacket(opcode, length, buffer.readBytes(length)));
		} finally {
			opcode = length = -1;
		}
	}

}
