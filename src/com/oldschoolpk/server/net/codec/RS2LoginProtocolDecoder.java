package com.oldschoolpk.server.net.codec;

import java.net.InetSocketAddress;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.Client;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerSave;
import com.oldschoolpk.server.net.NettyPacket;
import com.oldschoolpk.server.net.NetworkConstants;
import com.oldschoolpk.server.sanction.SanctionHandler;
import com.oldschoolpk.server.util.Checksum;
import com.oldschoolpk.server.util.ISAACRandomGen;
import com.oldschoolpk.server.util.Misc;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public final class RS2LoginProtocolDecoder extends ByteToMessageDecoder {

	/**
	 * An enumeration with the different states the
	 * {@link RS2LoginProtocolDecoder} can be in.
	 *
	 * @author Ryley Kimmel <ryley.kimmel@live.com>
	 */
	public enum LoginDecoderState {

		/**
		 * The login handshake state will wait for the username hash to be
		 * received. Once it is, a server session key will be sent to the client
		 * and the state will be set to the login header state.
		 */
		LOGIN_HANDSHAKE,

		/**
		 * The login header state will wait for the login type and payload
		 * length to be received. These are saved, and then the state will be
		 * set to the login payload state.
		 */
		LOGIN_HEADER,

		/**
		 * The login payload state will wait for all login information (such as
		 * client version, username and password).
		 */
		LOGIN_PAYLOAD

	}

	private static final Pattern ACCEPTED_USERNAME_PATTERN = Pattern.compile("[A-Za-z0-9 ]+");
	private static final Random random = new SecureRandom();

	private LoginDecoderState state = LoginDecoderState.LOGIN_HANDSHAKE;

	private int loginPacketSize;

	private int loginEncryptPacketSize = -1;

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

		switch (state) {
		case LOGIN_HANDSHAKE:
			if (in.isReadable()) {
				int protocol = in.readUnsignedByte();
				int nameHash = in.readUnsignedByte();
				if (protocol == 14) {
					long serverSessionKey = random.nextLong();
					ByteBuf handshakeResponse = Unpooled.buffer(1 + 8);
					handshakeResponse.writeBytes(new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 });
					handshakeResponse.writeByte(0);
					handshakeResponse.writeLong(serverSessionKey);
					ctx.channel().writeAndFlush(new NettyPacket(handshakeResponse));
				}
				state = LoginDecoderState.LOGIN_HEADER;
			}
			break;

		case LOGIN_HEADER:
			if (in.readableBytes() < 2) {
				return;
			}

			int loginType = in.readUnsignedByte();

			if ((loginType != 16) && (loginType != 18)) {
				ctx.close();
				return;
			}

			loginPacketSize = in.readUnsignedByte();

			loginEncryptPacketSize = loginPacketSize - (36 + 1 + 1 + 2);
			if (loginPacketSize <= 0 || loginEncryptPacketSize <= 0) {
				System.out.println("Zero or negative login size.");
				return;
			}

			state = LoginDecoderState.LOGIN_PAYLOAD;
			break;

		case LOGIN_PAYLOAD:

			if (in.readableBytes() < loginPacketSize) {
				return;
			}

			int magic = in.readByte() & 0xFF;
			int version = in.readShort();
			if (magic != 255) {
				System.out.println("Wrong magic id. " + magic);
				ctx.close();
				return;
			}

			@SuppressWarnings("unused")
			int lowMem = in.readUnsignedByte();
			for (int i = 0; i < 9; i++) {
				in.readInt();
			}

			loginEncryptPacketSize--;

			if (loginEncryptPacketSize != (in.readUnsignedByte())) {
				System.out.println("Encrypted size mismatch.");
				ctx.close();
				return;
			}

			if ((in.readUnsignedByte()) != 10) {
				System.out.println("Encrypted id != 10.");
				ctx.close();
				return;
			}

			long clientSessionKey = in.readLong();
			long serverSessionKey = in.readLong();

			int uid = in.readInt();

			String name = readString(in);
			String pass = readString(in);
			String checksum = readString(in);

			int sessionKey[] = new int[4];
			sessionKey[0] = (int) (clientSessionKey >> 32);
			sessionKey[1] = (int) clientSessionKey;
			sessionKey[2] = (int) (serverSessionKey >> 32);
			sessionKey[3] = (int) serverSessionKey;

			ISAACRandomGen inC = new ISAACRandomGen(sessionKey);
			for (int i = 0; i < 4; i++)
				sessionKey[i] += 50;
			ISAACRandomGen outC = new ISAACRandomGen(sessionKey);

			ctx.channel().pipeline().replace("decoder", "decoder", new RS2PacketDecoder(inC));
			load(ctx, uid, name, pass, inC, outC, version, checksum);
		}
	}

	/*
	 * Test whether the supplied char is valid.
	 */
	private boolean isPermitted(char ch) {
		return (ch >= '@') || (ch == ' ') || ((ch >= '0') && (ch <= '9')) || ((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z'));
	}

	private void load(final ChannelHandlerContext ctx, long clientHash, String username, String password, final ISAACRandomGen inC, final ISAACRandomGen outC, int version, String checksum) {
		byte returnCode = 2;
		@SuppressWarnings("unused")
		boolean close = false;
		username = username.trim().replaceAll(" +", " ");
		username = username.toLowerCase();
		password = password.trim().toLowerCase();

		char[] passwordChars = password.toCharArray();

		if (!ACCEPTED_USERNAME_PATTERN.matcher(username).matches()) {
			// Invalid username
			returnCode = 4;
		}

		for (char ch : passwordChars) {
			if (!isPermitted(ch)) {
				// Invalid password
				returnCode = 4;
				break;
			}
		}

		if (username.length() > 12) {
			returnCode = 8;
		}

		Client client = new Client(ctx.channel());
		Player player = new Player(client);
		player.playerName = username;
		player.playerName2 = player.playerName;
		player.playerPass = password;
		player.setInStreamDecryption(inC);
		player.setOutStreamDecryption(outC);
		player.getClient().getOutStream().packetEncryption = outC;
		player.setUsernameHash(Misc.playerNameToInt64(username));
		ctx.attr(NetworkConstants.SESSION).set(client);
		String address = ((InetSocketAddress) player.getClient().getChannel().remoteAddress()).getAddress().getHostAddress();

		player.saveCharacter = false;

		char first = username.charAt(0);
		player.properName = Character.toUpperCase(first) + username.substring(1, username.length());

		if (SanctionHandler.isBanned(player) || SanctionHandler.isIPBanned(address)) {
			returnCode = 4;
		}

		if (World.isPlayerOn(username)) {
			returnCode = 5;
		}

//		if (!checksum.equals("none") && !checksum.equals(Checksum.getClientChecksum())) {
//			 returnCode = 6;
//		}

		if (World.getPlayerCount() >= Config.MAX_PLAYERS) {
			returnCode = 7;
		}

		// Login Limit Exceeded
		// if() {
		// returnCode = 9;
		// }

		if (Server.UpdateServer) {
			returnCode = 14;
		}
		// if(Connection.checkLoginList(loginIp)) {
		// returnCode = 16;
		// }

		// Just Left World Login Delay Included
		// if() {
		// returnCode = 21;
		// }
		
//		if (!player.getUsername().equalsIgnoreCase("fallout")) {
//			returnCode = 14;
//		}

		if (returnCode == 2) {
			int load = PlayerSave.loadGame(player, player.playerName, player.playerPass);
			if (load == 0) {
				player.addStarter = true;
				if (player.playerName.toLowerCase().startsWith("mod ") || player.playerName.toLowerCase().startsWith("m0d ")) {
					returnCode = 3;
				}
			}
			if (load == 3) {
				returnCode = 3;
				player.saveFile = false;
			} else {
				for (int i = 0; i < player.getEquipment().length; i++) {
					if (player.getEquipment()[i] == 0) {
						player.getEquipment()[i] = -1;
						player.playerEquipmentN[i] = 0;
					}
				}
				if (!World.register(player)) {
					returnCode = 7;
					player.saveFile = false;
				} else {
					player.saveFile = true;
				}
			}
		}

		client.packetType = -1;
		client.packetSize = 0;
		ByteBuf buf = Unpooled.buffer(3);
		buf.writeByte(returnCode);

		if (returnCode == 2) {
			player.saveCharacter = true;
			buf.writeByte((byte) player.getChatIcon());
		} else {
			buf.writeByte((byte) 0);
		}

		buf.writeByte((byte) 0);
		NettyPacket pkt = new NettyPacket(buf);
		ctx.channel().writeAndFlush(pkt);

		if (returnCode == 2) {
			player.isActive = true;
		} else {
			ctx.channel().close();
		}
	}

	private static String readString(ByteBuf buffer) {
		buffer.markReaderIndex();
		int len = 0;

		while (buffer.readByte() != '\n') {
			len++;
		}

		buffer.resetReaderIndex();
		byte[] bytes = new byte[len];
		buffer.readBytes(bytes);
		buffer.readerIndex(buffer.readerIndex() + 1);
		return new String(bytes);
	}
}
