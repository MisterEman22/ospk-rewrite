package com.oldschoolpk.server.net;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

public final class ConnectionSet {

	private static final int MAXIMUM_CONNECTIONS = 3;

	private static final Multiset<String> connections = HashMultiset.create();

	public static void add(String address) {
		// synchronized (connections) {
		connections.add(address);
		// }
	}

	public static void remove(String address) {
		// synchronized (connections) {
		connections.remove(address);
		// }
	}

	public static boolean check(String address) {
		// no synchronized block necessary, concurrent read operations are
		// thread-safe.
		return connections.count(address) >= MAXIMUM_CONNECTIONS;
	}

}
