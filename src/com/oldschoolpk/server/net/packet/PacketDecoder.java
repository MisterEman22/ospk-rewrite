package com.oldschoolpk.server.net.packet;

import com.oldschoolpk.server.model.player.Player;

public interface PacketDecoder {
    public void processPacket(Player c, int packetType, int packetSize);
}
