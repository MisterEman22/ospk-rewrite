package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.Animation;
import com.oldschoolpk.server.model.Client;
import com.oldschoolpk.server.model.Graphic;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.model.player.command.CommandExecutors;
import com.oldschoolpk.server.net.packet.PacketDecoder;
import com.oldschoolpk.server.sanction.SanctionHandler;
import com.oldschoolpk.server.util.Misc;

/**
 * Commands reconfigured by Jack
 */
public class CommandPacketDecoder implements PacketDecoder {

	private static final String[] invalidInput = { "\n", "\r" }; // thats why
	// lol

	public static int amountVoted = 0;

	// u using a new class fr cmmands,

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {
		String playerCommand = c.getClient().getInStream().readString();
		
		if (!c.getAttribute("verified_login", false)) {
			return;
		}
		for (String characters : invalidInput) {
			if (playerCommand.toLowerCase().contains(characters.toLowerCase())) {
				c.getClient().sendMessage("You can't type this.");
				return;
			}
		}
		
		if (playerCommand.startsWith("/")) {
			if (c.clan != null) {
				if (SanctionHandler.isMuted(c) || SanctionHandler.isIPMuted(c)) {
					c.sendMessage("You are muted and can't speak.");
					return;
				}
				String message = playerCommand.substring(1);
				c.clan.sendChat(c, message);
			} else {
				c.sendMessage("You can only do this in a clan chat..");
			}
		}
		
		CommandExecutors.submit(c, playerCommand);
//		Misc.println(c.playerName + " playerCommand: " + playerCommand);
		if (c.getRights().isSuperior(PlayerRights.ADMINISTRATOR)) {
			ownerCommands(c, playerCommand);
		}
		if (c.getRights().isSuperior(PlayerRights.MODERATOR)) {
			adminCommands(c, playerCommand);
		}
		if (c.getRights() != PlayerRights.PLAYER || c.getDonatorRights().isDonator()) {
			customCommands(c, playerCommand);
		}
	}

	public static void customCommands(Player c, String playerCommand) {

		if (playerCommand.startsWith("yell")) {
			
			if (SanctionHandler.isMuted(c)) {
				c.sendMessage("You are muted and cannot yell.");
				return;
			}
			String msg = playerCommand.substring(5);
			if (msg.length() > 0) {

				if (c.getRights() == PlayerRights.PLAYER) {
					switch (c.getDonatorRights()) {
					case PREMIUM:
						c.getPA().globalYell("<img=" + (c.getChatIcon() - 1) + ">" + "[<col=fb311c>Donator@bla@] " + Misc.capitalize(c.playerName) + "@bla@: @bla@" + Misc.capitalize(msg));
						break;
					case SUPER_PREMIUM:
						c.getPA().globalYell("<img=" + (c.getChatIcon() - 1) + ">" + "[<col=00da00>Super Donator@bla@] " + Misc.capitalize(c.playerName) + "@bla@: @bla@" + Misc.capitalize(msg));
						break;
					case EXTREME_PREMIUM:
						c.getPA().globalYell("<img=" + (c.getChatIcon() - 1) + ">" + "[<col=fff60c>Extreme Donator@bla@]" + Misc.capitalize(c.playerName) + "@bla@: @bla@" + Misc.capitalize(msg));
						break;
					default:
						break;
					}
				} else {
					switch (c.getRights()) {
					case SUPPORT:
						c.getPA().globalYell("<img=" + (c.getChatIcon() - 1) + ">" + "[@blu@Supporter@bla@] @blu@" + Misc.capitalize(c.playerName) + "@bla@: @bla@" + Misc.capitalize(msg));
						break;
					case MODERATOR:
						c.getPA().globalYell("<img=" + (c.getChatIcon() - 1) + ">" + "[<col=5a6a6e>Moderator@bla@]@bla@" + Misc.capitalize(c.playerName) + "@bla@: @bla@" + Misc.capitalize(msg));
						break;
					case ADMINISTRATOR:
						c.getPA().globalYell("<img=" + (c.getChatIcon() - 1) + ">" + "[@yel@Administrator@bla@]@bla@" + Misc.capitalize(c.playerName) + "@bla@: @bla@" + Misc.capitalize(msg));
						break;
					case DEVELOPER:
						c.getPA().globalYell("<img=" + (c.getChatIcon() - 1) + ">" + "[<col=6366bb>Developer@bla@] @bla@" + Misc.capitalize(c.playerName) + "@bla@: @bla@" + Misc.capitalize(msg));
						break;
					case DICER:
						c.getPA().globalYell("<img=" + (c.getChatIcon() - 1) + ">" + "[@bla@MM@bla@] @bla@" + Misc.capitalize(c.playerName) + "@bla@: @bla@" + Misc.capitalize(msg));
						break;
					case VETERAN:
						c.getPA().globalYell("<img=" + (c.getChatIcon() - 1) + ">" + "[<col=C98700>Veteran@bla@] @bla@" + Misc.capitalize(c.playerName) + "@bla@: @bla@" + Misc.capitalize(msg));
						break;
					default:
						break;
					}
				}
			}
		}
	}

	public static void ownerCommands(Player c, String playerCommand) {
		testCommands(c, playerCommand);
		/*
		 * Owner commands
		 */
		
		
		if (playerCommand.startsWith("drop")) {
			String name = playerCommand.substring(5);
			
			if (name.length() < 3) {
				return;
			}
			
		}
		
		
	}

	public static void adminCommands(Player c, String playerCommand) {

	}

	public static void moderatorCommands(Player c, String playerCommand) {

	}

	public static void playerCommands(Player c, String playerCommand) {
	}

	public static void testCommands(Player c, String playerCommand) {
		
		if (playerCommand.startsWith("interface")) {
			String[] args = playerCommand.split(" ");
			c.getPA().showInterface(Integer.parseInt(args[1]));
		}
		if (playerCommand.startsWith("gfx")) {
			String[] args = playerCommand.split(" ");
			c.playGraphic(Graphic.create(Integer.parseInt(args[1]), 0, 100));
		}
		if (playerCommand.startsWith("anim")) {
			String[] args = playerCommand.split(" ");
			c.playAnimation(Animation.create(Integer.parseInt(args[1])));
			c.getPA().requestUpdates();
		}
		if (playerCommand.equalsIgnoreCase("mypos")) {
			c.sendMessage("X: " + c.absX);
			c.sendMessage("Y: " + c.absY);
			c.sendMessage("H: " + c.heightLevel);
		}
		if (playerCommand.startsWith("head")) {
			String[] args = playerCommand.split(" ");
			c.sendMessage("new head = " + Integer.parseInt(args[1]));
			c.headIcon = Integer.parseInt(args[1]);
			c.getPA().requestUpdates();
		}
		if (playerCommand.startsWith("spec")) {
			c.specAmount = 10;
			c.getItems().updateSpecialBar();
		}
		if (playerCommand.startsWith("seth")) {
			try {
				String[] args = playerCommand.split(" ");
				c.heightLevel = Integer.parseInt(args[1]);
				c.getPA().requestUpdates();
			} catch (Exception e) {
				c.sendMessage("fail");
			}
		}

		if (playerCommand.startsWith("npc")) {
			try {
				int newNPC = Integer.parseInt(playerCommand.substring(4));
				if (newNPC > 0) {
					//Server.npcHandler.spawnNpc(c, newNPC, c.absX, c.absY, c.heightLevel, 0, 120, 7, 70, 70, false, false);
					c.sendMessage("You spawn a Npc.");
				} else {
					c.sendMessage("No such NPC.");
				}
			} catch (Exception e) {

			}
		}
		if (playerCommand.startsWith("interface")) {
			try {
				String[] args = playerCommand.split(" ");
				int a = Integer.parseInt(args[1]);
				c.getPA().showInterface(a);
			} catch (Exception e) {
				c.sendMessage("::interface ####");
			}
		}
	}
}