package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Slient Packet
 **/
public class SilentPacket implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {

    }
}
