package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.item.bank.BankItem;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

public class BankAllButOne implements PacketDecoder {

	@Override
	public void processPacket(Player player, int packetType, int packetSize) {
		int interfaceId = player.getClient().getInStream().readSignedWordBigEndianA();
		int itemId = player.getClient().getInStream().readSignedWordBigEndianA();
		int itemSlot = player.getClient().getInStream().readSignedWordBigEndian();

		switch (interfaceId) {
		case 5382:
			int amount = player.getBank().getCurrentBankTab().getItemAmount(new BankItem(itemId + 1));
			if (amount < 1)
				return;
			if (amount == 1) {
				player.sendMessage("Your bank only contains one of this item.");
				return;
			}
			if (player.getBank().getBankSearch().isSearching()) {
				player.getBank().getBankSearch().removeItem(itemId, amount - 1);
				return;
			}
			if ((player.getBank().getCurrentBankTab().getItemAmount(new BankItem(itemId + 1)) - 1) > 1)
				player.getBankHandler().removeFromBank(itemId, amount - 1, true);
			break;
		}
	}

}
