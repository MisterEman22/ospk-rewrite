package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.content.dialogue.impl.PinDialogue;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Walking packet
 **/
public class MovementPacketDecoder implements PacketDecoder {

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {

		if (!c.getAttribute("verified_login", false)) {
			c.getDialogue().start(new PinDialogue(2));
			return;
		}
		if (packetType == 248 || packetType == 164) {
			c.faceUpdate(0);
			c.playerIndex = c.npcIndex = 0;
			c.followingX = c.followingY = 0;
			if (c.followId > 0 || c.followId2 > 0) {
				c.getPA().resetFollow();
			}
		}
		
		c.clickObjectType = c.clickNpcType = 0;

		if (c.isBusy()) {
			return;
		}


		if (c.getTradeHandler().getCurrentTrade() != null) {
			if (c.getTradeHandler().getCurrentTrade().isOpen()) {
				c.getTradeHandler().cancelCurrentTrade();
			}
		}

		if (c.duelStatus > 0 && c.duelStatus < 4) {
			Player o = World.players[c.duelingWith];
			if (o == null) {
				c.getDuel().declineDuel();
				return;
			}
			c.getDuel().declineDuel();
			o.getDuel().declineDuel();
			return;
		}

		c.getPA().removeAllWindows();

		if (c.freezeTimer > 0) {
			if (c.playerIndex == -1) {
				return;
			}
			if (World.players[c.playerIndex] != null) {
				if (c.goodDistance(c.getX(), c.getY(), World.players[c.playerIndex].getX(), World.players[c.playerIndex].getY(), 1) && packetType != 98) {
					c.playerIndex = 0;
					return;
				}
			}
			if (packetType != 98) {
				c.getClient().sendMessage("A magical force stops you from moving.");
				c.playerIndex = 0;
			}
			return;
		}

		if (c.duelRule[1] && c.duelStatus == 5) {
			if (World.players[c.duelingWith] != null && (!c.goodDistance(c.getX(), c.getY(), World.players[c.duelingWith].getX(), World.players[c.duelingWith].getY(), 1) || c.attackTimer == 0)) {
				c.getClient().sendMessage("Walking has been disabled in this duel.");
			}
			c.playerIndex = 0;
			return;
		}

		if ((c.duelStatus >= 1 && c.duelStatus <= 4) || c.duelStatus == 6) {
			if (c.duelStatus == 6) {
				c.getDuel().claimStakedItems();
			}
			return;
		}

		if (System.currentTimeMillis() - c.lastSpear < 4000) {
			c.sendMessage("You have been stunned.");
			c.playerIndex = 0;
			return;
		}
		if (packetType == 98) {
			c.mageAllowed = true;
		}
		if (c.respawnTimer > 3) {
			return;
		}
		if (packetType == 248) {
			packetSize -= 14;
		}

		int newWalkCmdSteps = (packetSize - 5) / 2;
		if (++newWalkCmdSteps > c.walkingQueueSize) {
			newWalkCmdSteps = 0;
			return;
		}
		
		c.newWalkCmdSteps = newWalkCmdSteps;

		c.getNewWalkCmdX()[0] = c.getNewWalkCmdY()[0] = 0;

		int firstStepX = c.getClient().getInStream().readSignedWordBigEndianA() - c.getMapRegionX() * 8;
		for (int i = 1; i < newWalkCmdSteps; i++) {
			c.getNewWalkCmdX()[i] = c.getClient().getInStream().readSignedByte();
			c.getNewWalkCmdY()[i] = c.getClient().getInStream().readSignedByte();
		}

		int firstStepY = c.getClient().getInStream().readSignedWordBigEndian() - c.getMapRegionY() * 8;
		c.setNewWalkCmdIsRunning(c.getClient().getInStream().readSignedByteC() == 1);
		for (int i1 = 0; i1 < newWalkCmdSteps; i1++) {
			c.getNewWalkCmdX()[i1] += firstStepX;
			c.getNewWalkCmdY()[i1] += firstStepY;
		}
		
		c.pathX = c.getNewWalkCmdX()[newWalkCmdSteps - 1] + c.mapRegionX * 8;
		c.pathY = c.getNewWalkCmdY()[newWalkCmdSteps - 1] + c.mapRegionY * 8;

//		c.getPA().playerWalk(c.pathX, c.pathY, false);
	}

}
