package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Connection;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;
import com.oldschoolpk.server.util.Misc;

/**
 * Private messaging, friends etc
 **/
public class PrivateMessagingPacketDecoder implements PacketDecoder {

	public final int ADD_FRIEND = 188, SEND_PM = 126, REMOVE_FRIEND = 215, CHANGE_PM_STATUS = 95, REMOVE_IGNORE = 59, ADD_IGNORE = 133;

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {
		switch (packetType) {

		case ADD_FRIEND:
			c.friendUpdate = true;
			long friendToAdd = c.getClient().getInStream().readQWord();

			if (friendToAdd < 0 || friendToAdd > Long.MAX_VALUE) {
				return;
			}

			Player target = World.getByUsernameHash(friendToAdd);

			for (int i1 = 0; i1 < c.friends.length; i1++) {
				if (c.friends[i1] != 0 && c.friends[i1] == friendToAdd) {
					c.sendMessage(friendToAdd + " is already on your friends list.");
					break;
				}
			}
			
			for (int i1 = 0; i1 < c.friends.length; i1++) {
				if (c.friends[i1] == 0) {
					c.friends[i1] = friendToAdd;
					if (target != null) {
						if (target.privateChat == 0 || (target.privateChat == 1 && target.getPA().isInPM(c.getUsernameHash()))) {
							c.getPA().loadPM(friendToAdd, 1);
							break;
						}
					}
					break;
				}
			}
			break;

		case SEND_PM:
			long sendMessageToFriendId = c.getClient().getInStream().readQWord();

			if (sendMessageToFriendId < 0 || sendMessageToFriendId > Long.MAX_VALUE) {
				return;
			}
			byte pmchatText[] = new byte[100];
			int pmchatTextSize = (byte) (packetSize - 8);
			c.getClient().getInStream().readBytes(pmchatText, pmchatTextSize, 0);
			if (Connection.isMuted(c))
				break;
			for (int i1 = 0; i1 < c.friends.length; i1++) {
				if (c.friends[i1] == sendMessageToFriendId) {
					boolean pmSent = false;

					for (int i2 = 1; i2 < Config.MAX_PLAYERS; i2++) {
						if (World.players[i2] != null && World.players[i2].isActive && Misc.playerNameToInt64(World.players[i2].playerName) == sendMessageToFriendId) {
							Player o = World.players[i2];
							if (o != null) {
								if (World.players[i2].privateChat == 0 || (World.players[i2].privateChat == 1 && o.getPA().isInPM(Misc.playerNameToInt64(c.playerName)))) {
									o.getPA().sendPM(Misc.playerNameToInt64(c.playerName), c.getChatIcon(), pmchatText, pmchatTextSize);
									pmSent = true;
								}
							}
							break;
						}
					}
					if (!pmSent) {
						c.sendMessage("That player is currently offline.");
						break;
					}
				}
			}
			break;

		case REMOVE_FRIEND:
			c.friendUpdate = true;
			long friendToRemove = c.getClient().getInStream().readQWord();

			if (friendToRemove < 0 || friendToRemove > Long.MAX_VALUE) {
				return;
			}

			for (int i1 = 0; i1 < c.friends.length; i1++) {
				if (c.friends[i1] == friendToRemove) {
					for (int i2 = 1; i2 < Config.MAX_PLAYERS; i2++) {
						Player o = World.players[i2];
						if (o != null) {
							if (c.friends[i1] == Misc.playerNameToInt64(World.players[i2].playerName)) {
								o.getPA().updatePM(c.playerId, 0);
								break;
							}
						}
					}
					c.friends[i1] = 0;
					break;
				}
			}
			break;

		case REMOVE_IGNORE:
			long usernameHash = c.getClient().getInStream().readQWord();
			
			if (usernameHash < 0 || usernameHash >= Long.MAX_VALUE) {
				return;
			}
			
			target = World.getByUsernameHash(usernameHash);
			
			for (int i = 0; i < c.ignores.length; i++) {
				if (c.ignores[i] == usernameHash) {
					c.ignores[i] = 0;
					if (target != null) {
						if (target.privateChat == 0 || (target.privateChat == 1 && target.getPA().isInPM(c.getUsernameHash()))) {
							c.getPA().loadPM(usernameHash, 1);
							break;
						}
					}
					break;
				}
			}
			
			
			break;

		case CHANGE_PM_STATUS:
			c.getClient().getInStream().readUnsignedByte();
			c.privateChat = c.getClient().getInStream().readUnsignedByte();
			c.getClient().getInStream().readUnsignedByte();
			for (int i1 = 1; i1 < Config.MAX_PLAYERS; i1++) {
				if (World.players[i1] != null && World.players[i1].isActive == true) {
					Player o = World.players[i1];
					if (o != null) {
						o.getPA().updatePM(c.playerId, 1);
					}
				}
			}
			break;

		case ADD_IGNORE:
			// TODO: fix this so it works :)
			break;

		}

	}
}
