package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Follow Player
 **/
public class FollowPlayer implements PacketDecoder {

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {
		int followPlayer = c.getClient().getInStream().readUnsignedWordBigEndian();
		if (World.players[followPlayer] == null) {
			return;
		}

		if (!c.getAttribute("verified_login", false)) {
			return;
		}

		c.playerIndex = 0;
		c.npcIndex = 0;
		c.mageFollow = false;
		c.usingBow = false;
		c.usingRangeWeapon = false;
		c.followDistance = 1;
		c.followId = followPlayer;
		c.getPA().normalFollow(c.followId);
	}
}
