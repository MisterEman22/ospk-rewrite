package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.model.Animation;
import com.oldschoolpk.server.model.Graphic;
import com.oldschoolpk.server.model.Position;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.content.SpawnTab;
import com.oldschoolpk.server.model.content.combat.SpellData;
import com.oldschoolpk.server.model.content.dialogue.impl.teleport.BossTeleportDialogue;
import com.oldschoolpk.server.model.content.dialogue.impl.teleport.CityTeleportDialogue;
import com.oldschoolpk.server.model.content.dialogue.impl.teleport.MinigameTeleportDialogue;
import com.oldschoolpk.server.model.content.dialogue.impl.teleport.PkingTeleportDialogue;
import com.oldschoolpk.server.model.content.dialogue.impl.teleport.SkillingTeleportDialogue;
import com.oldschoolpk.server.model.content.teleport.TeleportExecutor;
import com.oldschoolpk.server.model.item.ItemAssistant;
import com.oldschoolpk.server.model.item.bank.BankItem;
import com.oldschoolpk.server.model.item.bank.BankPin;
import com.oldschoolpk.server.model.item.bank.BankTab;
import com.oldschoolpk.server.model.item.equipment.EquipmentBonuses;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerConstants;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.net.packet.PacketDecoder;
import com.oldschoolpk.server.util.Misc;

/**
 * Clicking most buttons
 **/
public class ActionButtonPacketDecoder implements PacketDecoder {

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {
		int actionButtonId = Misc.hexToInt(c.getClient().getInStream().buffer, 0, packetSize);
		if (actionButtonId != 9154 && !c.getAttribute("verified_login", Boolean.FALSE)) {
			
			switch (actionButtonId) {
			case 9178:
			case 9190: // 1st tele option
				if (c.getDialogue().select(1)) {
					break;
				}

				break;

			case 9179:
			case 9191: // 2nd tele option
				if (c.getDialogue().select(2)) {
					break;
				}

				break;

			case 9180:
			case 9192: // 3rd tele option
				if (c.getDialogue().select(3)) {
					break;
				}
				break;
			// 4th tele option
			case 9193:
			case 9181:
				if (c.getDialogue().select(4)) {
					break;
				}
				break;
			// 5th tele option
			case 9194:
				if (c.getDialogue().select(5)) {
					break;
				}
				break;
			}
			return;
		}
		
		if (c.getRights().isSuperior(PlayerRights.ADMINISTRATOR)) {
			c.sendMessage("ActionButtonId: " + actionButtonId);
		}
		if (c.isDead || c.getLevel()[3] <= 0) {
			return;
		}
		String teleportType = c.playerMagicBook == 1 ? "ancient" : "modern";
		switch (actionButtonId) {
		
		case 226186:
		case 226198:
		case 226209:
		case 226220:
		case 226231:
		case 226242:
		case 226253:
		case 227008:
		case 227019:
			if (!c.isBanking && c.getAttribute("viewing_other_bank", null) == null) {
				c.getPA().removeAllWindows();
				return;
			}
			if (c.getBankPin().requiresUnlock()) {
				c.isBanking = false;
				c.getBankPin().open(2);
				return;
			}
			Player target = c.getAttribute("viewing_other_bank", null);
			int tabId = actionButtonId == 226186 ? 0 : actionButtonId == 226198 ? 1 : actionButtonId == 226209 ? 2 : actionButtonId == 226220 ? 3
					: actionButtonId == 226231 ? 4 : actionButtonId == 226242 ? 5 : actionButtonId == 226253 ? 6 : actionButtonId == 227008 ? 7
							: actionButtonId == 227019 ? 8 : -1;
			if (tabId <= -1 || tabId > 8)
				return;
			if (c.getBank().getBankSearch().isSearching()) {
				c.getBank().getBankSearch().reset(tabId);
				return;
			}
			if (c.getBank().getBankSearch().isSearching()) {
				c.getBank().getBankSearch().reset();
				return;
			}
			BankTab tab = (target != null ? target : c).getBank().getBankTab(tabId);
			if (tab.getTabId() == (target != null ? target : c).getBank().getCurrentBankTab().getTabId())
				return;
			if (tab.size() <= 0 && tab.getTabId() != 0) {
				c.sendMessage("Drag an item into the new tab slot to create a tab.");
				return;
			}
			(target != null ? target : c).getBank().setCurrentBankTab(tab);
			c.getPA().openUpBank();
			break;
			
		case 121164:
//			c.getPA().sendFrame36(509, 2);
			c.getPA().sendFrame36(510, 0);
			c.getPA().sendFrame36(511, 0);
			c.getPA().sendFrame36(512, 0);
			break;
			
		case 121141:
			c.getPA().sendFrame36(166, 4);
			break;
			
		case 121137:
			c.getPA().sendFrame36(166, 3);
			break;
			
		case 121133:
			c.getPA().sendFrame36(166, 2);
			break;
		case 121130:
			c.getPA().sendFrame36(166, 1);
			break;
			
		case 20174:
			c.getPA().closeAllWindows();
			BankPin pin = c.getBankPin();
			if (pin.getPin().length() <= 0)
				c.getBankPin().open(1);
			else if (!pin.getPin().isEmpty() && !pin.isAppendingCancellation())
				c.getBankPin().open(3);
			else if (!pin.getPin().isEmpty() && pin.isAppendingCancellation())
				c.getBankPin().open(4);
			break;

		case 226162:
			if (!c.isBanking || c.getAttribute("viewing_other_bank", null) != null)
				return;
			if (System.currentTimeMillis() - c.lastBankDeposit < 3000)
				return;
			if (c.getBank().getBankSearch().isSearching()) {
				c.getBank().getBankSearch().reset();
				return;
			}
			c.lastBankDeposit = System.currentTimeMillis();
			for (int slot = 0; slot < c.playerItems.length; slot++) {
				if (c.playerItems[slot] > 0 && c.playerItemsN[slot] > 0) {
					c.getBankHandler().addToBank(c.playerItems[slot] - 1, c.playerItemsN[slot], false);
				}
			}
			c.getItems().resetItems(3214);
			c.getItems().resetBank();
			c.getItems().resetTempItems();
			break;

		case 226170:
			
			if (!c.isBanking || c.getAttribute("viewing_other_bank", null) != null)
				return;
			if (System.currentTimeMillis() - c.lastBankDeposit < 3000)
				return;
			if (c.getBank().getBankSearch().isSearching()) {
				c.getBank().getBankSearch().reset();
				return;
			}
			if (c.getBankPin().requiresUnlock()) {
				c.isBanking = false;
				c.getBankPin().open(2);
				return;
			}
			c.lastBankDeposit = System.currentTimeMillis();
			for (int slot = 0; slot < c.playerEquipment.length; slot++) {
				if (c.playerEquipment[slot] > 0 && c.playerEquipmentN[slot] > 0) {
					c.getBankHandler().addEquipmentToBank(c.playerEquipment[slot], slot, c.playerEquipmentN[slot], false);
					c.getItems().wearItem(-1, 0, slot);
				}
			}
			c.getItems().resetItems(3214);
			c.getItems().resetBank();
			c.getItems().resetTempItems();
			break;

		case 226197:
		case 226208:
		case 226219:
		case 226230:
		case 226241:
		case 226252:
		case 227007:
		case 227018:
			
			if (!c.isBanking) {
				c.getPA().removeAllWindows();
				return;
			}
			if (c.getBankPin().requiresUnlock()) {
				c.isBanking = false;
				c.getBankPin().open(2);
				return;
			}
			tabId = actionButtonId == 226197 ? 1 : actionButtonId == 226208 ? 2 : actionButtonId == 226219 ? 3 : actionButtonId == 226230 ? 4
					: actionButtonId == 226241 ? 5 : actionButtonId == 226252 ? 6 : actionButtonId == 227007 ? 7 : actionButtonId == 227018 ? 8 : -1;
			tab = c.getBank().getBankTab(tabId);
			if (tab == null || tab.getTabId() == 0 || tab.size() == 0) {
				c.sendMessage("You cannot collapse this tab.");
				return;
			}
			if (tab.size() + c.getBank().getBankTab()[0].size() >= Config.BANK_SIZE) {
				c.sendMessage("You cannot collapse this tab. The contents of this tab and your");
				c.sendMessage("main tab are greater than " + Config.BANK_SIZE + " unique items.");
				return;
			}
			if (c.getBank().getBankSearch().isSearching()) {
				c.getBank().getBankSearch().reset();
				return;
			}
			for (BankItem item : tab.getItems()) {
				c.getBank().getBankTab()[0].add(item);
			}
			tab.getItems().clear();
			if (tab.size() == 0) {
				c.getBank().setCurrentBankTab(c.getBank().getBankTab(0));
			}
			c.getPA().openUpBank();
			break;

		case 226185:
		case 226196:
		case 226207:
		case 226218:
		case 226229:
		case 226240:
		case 226251:
		case 227006:
		case 227017:
			
			if (!c.isBanking) {
				c.getPA().removeAllWindows();
				return;
			}
			if (c.getBankPin().requiresUnlock()) {
				c.isBanking = false;
				c.getBankPin().open(2);
				return;
			}
			if (c.getBank().getBankSearch().isSearching()) {
				c.getBank().getBankSearch().reset();
				return;
			}
			tabId = actionButtonId == 226185 ? 0 : actionButtonId == 226196 ? 1 : actionButtonId == 226207 ? 2 : actionButtonId == 226218 ? 3
					: actionButtonId == 226229 ? 4 : actionButtonId == 226240 ? 5 : actionButtonId == 226251 ? 6 : actionButtonId == 227006 ? 7
							: actionButtonId == 227017 ? 8 : -1;
			tab = c.getBank().getBankTab(tabId);
			long value = 0;
			if (tab == null || tab.size() == 0)
				return;
			for (BankItem item : tab.getItems()) {
				long tempValue = item.getId() - 1 == 995 ? 1 : c.getShops().getItemShopValue(item.getId() - 1);
				value += tempValue * item.getAmount();
			}
			c.sendMessage("<col=255>The total networth of tab " + tab.getTabId() + " is </col><col=600000>" + Long.toString(value) + " gp</col>.");
			break;

		case 22024:
		case 86008:
			c.getPA().openUpBank();
			break;
		
		case 152090:
			c.getPA().removeAllWindows();
			break;
		case 160168:
			c.getPA().showInterface(39000);
			break;

		case 33206:
			c.getClient().getOutStream().createFrame(27);
			c.skillToChange = 0;
			break;

		case 33212:
			c.getClient().getOutStream().createFrame(27);
			c.skillToChange = 1;
			break;

		case 33209:
			c.getClient().getOutStream().createFrame(27);
			c.skillToChange = 2;
			break;

		case 33207:
			c.getClient().getOutStream().createFrame(27);
			c.skillToChange = 3;
			break;

		case 33215:
			c.getClient().getOutStream().createFrame(27);
			c.skillToChange = 4;
			break;

		case 33218:
			c.getClient().getOutStream().createFrame(27);
			c.skillToChange = 5;
			break;

		case 33221:
			c.getClient().getOutStream().createFrame(27);
			c.skillToChange = 6;
			break;
			

		case 108006:
			c.getPA().openKeptOnDeathInterface();
			break;

		case 62165:
			c.getPA().sendFrame126("http://oldschoolpk.com", 12000);
		case 62166:
		case 62167:
			c.getPA().sendFrame126("http://oldschoolpk.com", 12000);
			break;

		case 62168:
			c.getPA().sendFrame126("itemdb.biz", 12000);
			break;

		case 144142:
		case 144145:
		case 144148:
		case 144151:
		case 144154:
		case 144157:
		case 144160:
		case 144163:
		case 144166:
			SpawnTab.handleActionButtons(c, actionButtonId);
			break;

		/**
		 * Teleports
		 */
		case 109157: // edgeville
			if (!c.inSafeZone()) {
				c.sendMessage("Please run to safety first.");
				return;
			}
			TeleportExecutor.teleport(c, new Position(Config.LUMBY_X + Misc.random(2), Config.LUMBY_Y + Misc.random(2), 0));
			break;

		case 109159: // falador
			if (!c.inSafeZone()) {
				c.sendMessage("Please run to safety first.");
				return;
			}
			TeleportExecutor.teleport(c, new Position(2945 + Misc.random(2), 3370 + Misc.random(2), 0));
			break;

		case 109161: // east dragons
			if (!c.inSafeZone()) {
				c.sendMessage("Please run to safety first.");
				return;
			}
			TeleportExecutor.teleport(c, new Position(3339, 3659, 0));
			break;

		case 109163: // east dragons
			if (!c.inSafeZone()) {
				c.sendMessage("Please run to safety first.");
				return;
			}
			TeleportExecutor.teleport(c, new Position(Config.MAGEBANK_X, Config.MAGEBANK_Y, 0));
			break;

		case 118098:
			c.getPA().castVeng();
			break;
		// crafting + fletching interface:
		case 150:
			if (c.autoRet == 0)
				c.autoRet = 1;
			else
				c.autoRet = 0;
			break;
		// 1st tele option
		case 9190:
		case 9167:
		case 9178:
		case 9157:
			if (c.getDialogue().isActive()) {
				c.getDialogue().select(1);
				break;
			}
			break;
		// mining - 3046,9779,0
		// smithing - 3079,9502,0

		// 2nd tele option
		case 9191:
		case 9168:
		case 9179:
		case 9158:
			if (c.getDialogue().isActive()) {
				c.getDialogue().select(2);
				break;
			}
			break;
		// 3rd tele option

		case 9192:
		case 9169:
		case 9180:
			if (c.getDialogue().isActive()) {
				c.getDialogue().select(3);
				break;
			}
			break;
		// 4th tele option
		case 9193:
		case 9181:
			if (c.getDialogue().isActive()) {
				c.getDialogue().select(4);
				break;
			}
			break;
		// 5th tele option
		case 9194:
			if (c.getDialogue().isActive()) {
				c.getDialogue().select(5);
				break;
			}
			break;

		case 58253:
			EquipmentBonuses.writeItemBonuses(c);
			break;

		case 59004:
			c.getPA().removeAllWindows();
			break;

		case 121042:
		case 74214:
		case 152:
		case 153:
			c.isRunning2 = !c.isRunning2;
			int frame = c.isRunning2 == true ? 1 : 0;
			c.getPA().sendFrame36(173, frame);
			break;

		/* Player Options */

		/**
		 * Mouse Button
		 */
		case 74176:
		case 121108:
			if (!c.mouseButton) {
				c.mouseButton = true;
				c.getPA().sendFrame36(500, 1);
				c.getPA().sendFrame36(170, 1);
			} else if (c.mouseButton) {
				c.mouseButton = false;
				c.getPA().sendFrame36(500, 0);
				c.getPA().sendFrame36(170, 0);
			}
			break;

		/**
		 * Split Chat
		 */
		case 74184:
		case 121092:
			if (!c.isSplitChat()) {
				c.setSplitChat(true);
				c.getPA().sendFrame36(502, 1);
				c.getPA().sendFrame36(287, 1);
			} else {
				c.setSplitChat(false);
				c.getPA().sendFrame36(502, 0);
				c.getPA().sendFrame36(287, 0);
			}
			break;

		/**
		 * Chat Effects
		 */
		case 121088:
		case 74180:
			if (!c.chatEffects) {
				c.chatEffects = true;
				c.getPA().sendFrame36(501, 1);
				c.getPA().sendFrame36(171, 0);
			} else {
				c.chatEffects = false;
				c.getPA().sendFrame36(501, 0);
				c.getPA().sendFrame36(171, 1);
			}
			break;

		/**
		 * Accept aid
		 */
		case 74188:
		case 121038:
			if (!c.acceptAid) {
				c.acceptAid = true;
				c.getPA().sendFrame36(503, 1);
				c.getPA().sendFrame36(427, 1);
			} else {
				c.acceptAid = false;
				c.getPA().sendFrame36(503, 0);
				c.getPA().sendFrame36(427, 0);
			}
			break;
		/**
		 * Attack option Priority
		 */
		case 121117:
			c.getPA().sendFrame36(315, 0);
			c.getPA().sendFrame36(316, 0);
			c.getPA().sendFrame36(317, 0);
			break;

		case 121121:
			c.getPA().sendFrame36(315, 1);
			c.getPA().sendFrame36(316, 1);
			c.getPA().sendFrame36(317, 0);
			break;

		case 121125:
			c.getPA().sendFrame36(315, 1);
			c.getPA().sendFrame36(316, 0);
			c.getPA().sendFrame36(317, 1);
			break;

		case 157058:// brightness1
			c.getPA().sendFrame36(505, 1);
			c.getPA().sendFrame36(506, 0);
			c.getPA().sendFrame36(507, 0);
			c.getPA().sendFrame36(508, 0);
			c.getPA().sendFrame36(166, 1);
			break;
		case 157062:// brightness2
			c.getPA().sendFrame36(505, 0);
			c.getPA().sendFrame36(506, 1);
			c.getPA().sendFrame36(507, 0);
			c.getPA().sendFrame36(508, 0);
			c.getPA().sendFrame36(166, 2);
			break;

		case 157066:// brightness3
			c.getPA().sendFrame36(505, 0);
			c.getPA().sendFrame36(506, 0);
			c.getPA().sendFrame36(507, 1);
			c.getPA().sendFrame36(508, 0);
			c.getPA().sendFrame36(166, 3);
			break;

		case 157070:// brightness4
			c.getPA().sendFrame36(505, 0);
			c.getPA().sendFrame36(506, 0);
			c.getPA().sendFrame36(507, 0);
			c.getPA().sendFrame36(508, 1);
			c.getPA().sendFrame36(166, 4);
			break;
		case 121029:
			c.getClient().setSidebarInterface(11, 31040); // wrench tab
			break;
		case 121032:
			c.getClient().setSidebarInterface(11, 31060); // wrench tab
			break;

		case 121035:
			c.getClient().setSidebarInterface(11, 31080); // wrench tab
			break;

		case 121065:
			c.getClient().setSidebarInterface(11, 904); // wrench tab
			break;


		case 1093:
		case 1094:
		case 1097:
			if (c.autocastId > 0) {
				c.getPA().resetAutocast();
			} else {
				if (c.playerMagicBook == 1) {
					if (c.getEquipment()[PlayerConstants.PLAYER_WEAPON] == 4675)
						c.getClient().setSidebarInterface(0, 1689);
					else
						c.sendMessage("You can't autocast ancients without an ancient staff.");
				} else if (c.playerMagicBook == 0) {
					if (c.getEquipment()[PlayerConstants.PLAYER_WEAPON] == 4170) {
						c.getClient().setSidebarInterface(0, 12050);
					} else {
						c.getClient().setSidebarInterface(0, 1829);
					}
				}

			}
			break;

		/** Specials **/
		case 29188:
			c.specBarId = 7636; // the special attack text - sendframe126(S P E
			// C I A L A T T A C K, c.specBarId);
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 29163:
			c.specBarId = 7611;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 33033:
			if (c.getEquipment()[3] == 11791) {
				if (c.getCombat().checkSpecAmount(c.getEquipment()[3])) {
					c.playAnimation(Animation.create(7083));
					c.playGraphic(Graphic.create(1228, 0, 400));
					c.stofdDelay = System.currentTimeMillis();
					c.getItems().updateSpecialBar();
				} else {
					c.sendMessage("You have run out of special power.");
				}
			} else {
				c.specBarId = 8505;
				c.usingSpecial = !c.usingSpecial;
				c.getItems().updateSpecialBar();
			}
			break;

		case 29038:
			c.specBarId = 7486;
			c.getCombat().handleGmaulPlayer();
			c.getItems().updateSpecialBar();
			break;

		case 29063:
			if (c.getCombat().checkSpecAmount(c.getEquipment()[PlayerConstants.PLAYER_WEAPON])) {
				c.playGraphic(Graphic.create(246, 0, 0));
				c.forcedChat("Raarrrrrgggggghhhhhhh!");
				c.playAnimation(Animation.create(1056));
				c.getLevel()[2] = c.getLevelForXP(c.getExperience()[2]) + (c.getLevelForXP(c.getExperience()[2]) * 15 / 100);
				c.getPA().refreshSkill(2);
				c.getItems().updateSpecialBar();
			} else {
				c.sendMessage("You don't have the required special energy to use this attack.");
			}
			break;

		case 48023:
			c.specBarId = 12335;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 29138:
			c.specBarId = 7586;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 29113:
			c.specBarId = 7561;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 29238:
			c.specBarId = 7686;
			c.usingSpecial = !c.usingSpecial;
			c.getItems().updateSpecialBar();
			break;

		case 4169: // god spell charge
			c.usingMagic = true;
			if (!c.getCombat().checkMagicReqs(48)) {
				break;
			}

			if (System.currentTimeMillis() - c.godSpellDelay < Config.GOD_SPELL_CHARGE) {
				c.sendMessage("You still feel the charge in your body!");
				break;
			}
			c.godSpellDelay = System.currentTimeMillis();
			c.sendMessage("You feel charged with a magical power!");
			c.playGraphic(Graphic.create(SpellData.MAGIC_SPELLS[48][3], 0, 100));
			c.playAnimation(Animation.create(SpellData.MAGIC_SPELLS[48][2]));
			c.usingMagic = false;
			break;

		case 28164: // item kept on death
			break;

		case 9154:
			c.getClient().logout();
			break;

		/**
		 * Duel arena
		 */
		case 26065: // no forfeit
		case 26040:
			c.duelSlot = -1;
			c.getDuel().selectRule(0);
			break;

		case 26066: // no movement
		case 26048:
			c.duelSlot = -1;
			c.getDuel().selectRule(1);
			break;

		case 26069: // no range
		case 26042:
			c.duelSlot = -1;
			c.getDuel().selectRule(2);
			break;

		case 26070: // no melee
		case 26043:
			c.duelSlot = -1;
			c.getDuel().selectRule(3);
			break;

		case 26071: // no mage
		case 26041:
			c.duelSlot = -1;
			c.getDuel().selectRule(4);
			break;

		case 26072: // no drinks
		case 26045:
			c.duelSlot = -1;
			c.getDuel().selectRule(5);
			break;

		case 26073: // no food
		case 26046:
			c.duelSlot = -1;
			c.getDuel().selectRule(6);
			break;

		case 26074: // no prayer
		case 26047:
			c.duelSlot = -1;
			c.getDuel().selectRule(7);
			break;

		case 26076: // obsticals
		case 26075:
			c.duelSlot = -1;
			c.getDuel().selectRule(8);
			break;

		case 2158: // fun weapons
		case 2157:
			c.sendMessage("This rule is temporarily disabled.");
			// c.duelSlot = -1;
			// c.getDuel().selectRule(9);
			break;

		case 30136: // sp attack
		case 30137:
			c.duelSlot = -1;
			c.getDuel().selectRule(10);
			break;

		case 53245: // no helm
			c.duelSlot = 0;
			c.getDuel().selectRule(11);
			break;

		case 53246: // no cape
			c.duelSlot = 1;
			c.getDuel().selectRule(12);
			break;

		case 53247: // no ammy
			c.duelSlot = 2;
			c.getDuel().selectRule(13);
			break;

		case 53249: // no weapon.
			c.duelSlot = 3;
			c.getDuel().selectRule(14);
			break;

		case 53250: // no body
			c.duelSlot = 4;
			c.getDuel().selectRule(15);
			break;

		case 53251: // no shield
			c.duelSlot = 5;
			c.getDuel().selectRule(16);
			break;

		case 53252: // no legs
			c.duelSlot = 7;
			c.getDuel().selectRule(17);
			break;

		case 53255: // no gloves
			c.duelSlot = 9;
			c.getDuel().selectRule(18);
			break;

		case 53254: // no boots
			c.duelSlot = 10;
			c.getDuel().selectRule(19);
			break;

		case 53253: // no rings
			c.duelSlot = 12;
			c.getDuel().selectRule(20);
			break;

		case 53248: // no arrows
			c.duelSlot = 13;
			c.getDuel().selectRule(21);
			break;

		case 26018:
			Player o = World.players[c.duelingWith];
			if (o == null) {
				c.getDuel().declineDuel();
				return;
			}
			if (!c.inDuelArena() || !o.inDuelArena()) {
				c.getDuel().declineDuel();
				o.getDuel().declineDuel();
				return;
			}
			if (System.currentTimeMillis() - c.lastRuleSelected <= 5000) {
				c.sendMessage("A new rule has been selected, please wait 5 seconds before accepting.");
				return;
			}
			if (c.duelRule[2] && c.duelRule[3] && c.duelRule[4]) {
				c.sendMessage("You won't be able to attack the player with the rules you have set.");
				break;
			}
			c.duelStatus = 2;
			if (c.duelStatus == 2) {
				c.getPA().sendFrame126("Waiting for other player...", 6684);
				o.getPA().sendFrame126("Other player has accepted.", 6684);
			}
			if (o.duelStatus == 2) {
				o.getPA().sendFrame126("Waiting for other player...", 6684);
				c.getPA().sendFrame126("Other player has accepted.", 6684);
			}

			if (c.duelStatus == 2 && o.duelStatus == 2) {
				c.canOffer = false;
				o.canOffer = false;
				c.duelStatus = 3;
				o.duelStatus = 3;
				c.getDuel().confirmDuel();
				o.getDuel().confirmDuel();
			}
			break;

		case 25120:
			if (c.duelStatus == 5) {
				break;
			}
			Player o1 = World.players[c.duelingWith];
			if (o1 == null) {
				c.getDuel().declineDuel();
				return;
			}
			if (!c.inDuelArena() || !o1.inDuelArena()) {
				c.getDuel().declineDuel();
				o1.getDuel().declineDuel();
				return;
			}
			c.duelStatus = 4;
			if (o1.duelStatus == 4 && c.duelStatus == 4) {
				c.getDuel().startDuel();
				o1.getDuel().startDuel();
				o1.duelCount = 4;
				c.duelCount = 4;
				c.duelDelay = System.currentTimeMillis();
				o1.duelDelay = System.currentTimeMillis();
			} else {
				c.getPA().sendFrame126("Waiting for other player...", 6571);
				o1.getPA().sendFrame126("Other player has accepted", 6571);
			}
			break;

		case 13092:
			c.getTradeHandler().acceptStage1();
			break;

		case 13218:
			c.getTradeHandler().acceptStage2();
			break;

		case 21010:
			c.takeAsNote = true;
			break;

		case 21011:
			c.takeAsNote = false;
			break;

		// home teleports
		case 4171:
		case 50056:
		case 117048:
			TeleportExecutor.teleport(c, new Position(3086 + Misc.random(2), 3499 + Misc.random(2), 0));
			break;
			
		case 117112:
		case 4140:
		case 50235:
			c.getDialogue().start(new BossTeleportDialogue());
			break;
			
		case 117123:
		case 4143:
		case 50245:
			c.getDialogue().start(new MinigameTeleportDialogue());
			break;
			
		case 117131:
		case 4146:
		case 50253:
			c.getDialogue().start(new PkingTeleportDialogue());
			break;
			
		case 117154:
		case 4150:
		case 51005:
			c.getDialogue().start(new CityTeleportDialogue());
			break;
			
		case 117162:
		case 6004:
		case 51013:
			c.getDialogue().start(new SkillingTeleportDialogue());
			break;

		case 6005:
			TeleportExecutor.teleport(c, new Position(Config.WATCHTOWER_X, Config.WATCHTOWER_Y, 0));
			break;

		case 9125: // Accurate
		case 6221: // range accurate
		case 22228: // punch (unarmed)
		case 48010: // flick (whip)
		case 21200: // spike (pickaxe)
		case 1080: // bash (staff)
		case 6168: // chop (axe)
		case 6236: // accurate (long bow)
		case 17102: // accurate (darts)
		case 8234: // stab (dagger)
			c.fightMode = 0;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;

		case 9126: // Defensive
		case 48008: // deflect (whip)
		case 22229: // block (unarmed)
		case 21201: // block (pickaxe)
		case 1078: // focus - block (staff)
		case 6169: // block (axe)
		case 33019: // fend (hally)
		case 18078: // block (spear)
		case 8235: // block (dagger)
			c.fightMode = 1;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;

		case 9127: // Controlled
		case 48009: // lash (whip)
		case 33018: // jab (hally)
		case 6234: // longrange (long bow)
		case 6219: // longrange
		case 18077: // lunge (spear)
		case 18080: // swipe (spear)
		case 18079: // pound (spear)
		case 17100: // longrange (darts)
			c.fightMode = 3;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;

		case 9128: // Aggressive
		case 6220: // range rapid
		case 22230: // kick (unarmed)
		case 21203: // impale (pickaxe)
		case 21202: // smash (pickaxe)
		case 1079: // pound (staff)
		case 6171: // hack (axe)
		case 6170: // smash (axe)
		case 33020: // swipe (hally)
		case 6235: // rapid (long bow)
		case 17101: // repid (darts)
		case 8237: // lunge (dagger)
		case 8236: // slash (dagger)
			c.fightMode = 2;
			if (c.autocasting)
				c.getPA().resetAutocast();
			break;

		/** Prayers **/
		case 21233: // thick skin
			c.getCombat().activatePrayer(0);
			break;
		case 21234: // burst of str
			c.getCombat().activatePrayer(1);
			break;
		case 21235: // charity of thought
			c.getCombat().activatePrayer(2);
			break;
		case 70080: // range
			c.getCombat().activatePrayer(3);
			break;
		case 70082: // mage
			c.getCombat().activatePrayer(4);
			break;
		case 21236: // rockskin
			c.getCombat().activatePrayer(5);
			break;
		case 21237: // super human
			c.getCombat().activatePrayer(6);
			break;
		case 21238: // improved reflexes
			c.getCombat().activatePrayer(7);
			break;
		case 21239: // hawk eye
			c.getCombat().activatePrayer(8);
			break;
		case 21240:
			c.getCombat().activatePrayer(9);
			break;
		case 21241: // protect Item
			c.getCombat().activatePrayer(10);
			break;
		case 70084: // 26 range
			c.getCombat().activatePrayer(11);
			break;
		case 70086: // 27 mage
			c.getCombat().activatePrayer(12);
			break;
		case 21242: // steel skin
			c.getCombat().activatePrayer(13);
			break;
		case 21243: // ultimate str
			c.getCombat().activatePrayer(14);
			break;
		case 21244: // incredible reflex
			c.getCombat().activatePrayer(15);
			break;
		case 21245: // protect from magic
			c.getCombat().activatePrayer(16);
			break;
		case 21246: // protect from range
			c.getCombat().activatePrayer(17);
			break;
		case 21247: // protect from melee
			c.getCombat().activatePrayer(18);
			break;
		case 70088: // 44 range
			c.getCombat().activatePrayer(19);
			break;
		case 70090: // 45 mystic
			c.getCombat().activatePrayer(20);
			break;
		case 2171: // retrui
			c.getCombat().activatePrayer(21);
			break;
		case 2172: // redem
			c.getCombat().activatePrayer(22);
			break;
		case 2173: // smite
			c.getCombat().activatePrayer(23);
			break;
		case 70092: // chiv
			c.getCombat().activatePrayer(24);
			break;
		case 70094: // piety
			c.getCombat().activatePrayer(25);
			break;

		/* Rules Interface Buttons */
		case 125011: // Click agree
			if (!c.ruleAgreeButton) {
				c.ruleAgreeButton = true;
				c.getPA().sendFrame36(701, 1);
			} else {
				c.ruleAgreeButton = false;
				c.getPA().sendFrame36(701, 0);
			}
			break;
		case 125003:// Accept
			if (c.ruleAgreeButton) {
				c.getPA().showInterface(3559);
				c.newPlayer = false;
			} else if (!c.ruleAgreeButton) {
				c.sendMessage("You need to click on you agree before you can continue on.");
			}
			break;
		case 125006:// Decline
			c.sendMessage("You have chosen to decline, Client will be disconnected from the server.");
			break;
		/* End Rules Interface Buttons */
		case 74206:// area1
			c.getPA().sendFrame36(509, 1);
			c.getPA().sendFrame36(510, 0);
			c.getPA().sendFrame36(511, 0);
			c.getPA().sendFrame36(512, 0);
			break;
		case 74207:// area2
			c.getPA().sendFrame36(509, 0);
			c.getPA().sendFrame36(510, 1);
			c.getPA().sendFrame36(511, 0);
			c.getPA().sendFrame36(512, 0);
			break;
		case 74208:// area3
			c.getPA().sendFrame36(509, 0);
			c.getPA().sendFrame36(510, 0);
			c.getPA().sendFrame36(511, 1);
			c.getPA().sendFrame36(512, 0);
			break;
		case 74209:// area4
			c.getPA().sendFrame36(509, 0);
			c.getPA().sendFrame36(510, 0);
			c.getPA().sendFrame36(511, 0);
			c.getPA().sendFrame36(512, 1);
			break;
		case 168:
			c.playAnimation(Animation.create(855));
			break;
		case 169:
			c.playAnimation(Animation.create(856));
			break;
		case 162:
			c.playAnimation(Animation.create(857));
			break;
		case 164:
			c.playAnimation(Animation.create(858));
			break;
		case 165:
			c.playAnimation(Animation.create(859));
			break;
		case 161:
			c.playAnimation(Animation.create(860));
			break;
		case 170:
			c.playAnimation(Animation.create(861));
			break;
		case 171:
			c.playAnimation(Animation.create(862));
			break;
		case 163:
			c.playAnimation(Animation.create(863));
			break;
		case 167:
			c.playAnimation(Animation.create(864));
			break;
		case 172:
			c.playAnimation(Animation.create(865));
			break;
		case 166:
			c.playAnimation(Animation.create(866));
			break;
		case 52050:
			c.playAnimation(Animation.create(2105));
			break;
		case 52051:
			c.playAnimation(Animation.create(2106));
			break;
		case 52052:
			c.playAnimation(Animation.create(2107));
			break;
		case 52053:
			c.playAnimation(Animation.create(2108));
			break;
		case 52054:
			c.playAnimation(Animation.create(2109));
			break;
		case 52055:
			c.playAnimation(Animation.create(2110));
			break;
		case 52056:
			c.playAnimation(Animation.create(2111));
			break;
		case 52057:
			c.playAnimation(Animation.create(2112));
			break;
		case 52058:
			c.playAnimation(Animation.create(2113));
			break;
		case 43092:
			c.playAnimation(Animation.create(0x558));
			break;
		case 2155:
			c.playAnimation(Animation.create(0x46B));
			break;
		case 25103:
			c.playAnimation(Animation.create(0x46A));
			break;
		case 25106:
			c.playAnimation(Animation.create(0x469));
			break;
		case 2154:
			c.playAnimation(Animation.create(0x468));
			break;
		case 52071:
			c.playAnimation(Animation.create(0x84F));
			break;
		case 52072:
			c.playAnimation(Animation.create(0x850));
			break;
		case 59062:
			c.playAnimation(Animation.create(2836));
			break;
		case 72032:
			c.playAnimation(Animation.create(3544));
			break;
		case 72033:
			c.playAnimation(Animation.create(3543));
			break;
		case 72254:
			c.playAnimation(Animation.create(3866));
			break;
		/* END OF EMOTES */

		case 24017:
			c.getPA().resetAutocast();
			c.getItems().sendWeapon(c.getEquipment()[PlayerConstants.PLAYER_WEAPON], ItemAssistant.getItemName(c.getEquipment()[PlayerConstants.PLAYER_WEAPON]));
			break;
		}
		if (SpellData.isAutoButton(actionButtonId))
			c.assignAutocast(actionButtonId);
	}
}
