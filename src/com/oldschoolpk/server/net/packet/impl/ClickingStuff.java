package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Clicking stuff (interfaces)
 **/
public class ClickingStuff implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        if (c.isBanking) {
            c.isBanking = false;
        }
        if (c.isShopping) {
            c.isShopping = false;
        }
        if (c.getTradeHandler().getCurrentTrade() != null) {
            if (c.getTradeHandler().getCurrentTrade().isOpen()) {
                c.getTradeHandler().decline();
            }
        }
        if (c.inDuelArena()) {
            if (c.duelStatus == 6) {
                c.getDuel().claimStakedItems();
            }
            if (c.duelStatus > 4) {
                return;
            }
            Player o = World.players[c.duelingWith];
            if (o == null) {
                c.getDuel().declineDuel();
                return;
            }
            if (o.duelStatus > 4 || c.duelStatus > 4) {
                return;
            }
            c.getDuel().declineDuel();
            o.getDuel().declineDuel();
        }
    }

}
