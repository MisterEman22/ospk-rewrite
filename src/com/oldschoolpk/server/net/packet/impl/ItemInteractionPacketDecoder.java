package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.model.Animation;
import com.oldschoolpk.server.model.Position;
import com.oldschoolpk.server.model.content.FlowerGame;
import com.oldschoolpk.server.model.content.FoodHandler;
import com.oldschoolpk.server.model.content.MysteryBox;
import com.oldschoolpk.server.model.content.PotionHandler;
import com.oldschoolpk.server.model.content.teleport.Teleport;
import com.oldschoolpk.server.model.content.teleport.Teleport.TeleportType;
import com.oldschoolpk.server.model.content.teleport.TeleportExecutor;
import com.oldschoolpk.server.model.player.DonatorRights;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.net.packet.PacketDecoder;
import com.oldschoolpk.server.util.cache.defs.ItemDef;

/**
 * Handles all item click interaction
 * 
 * @author Arithium
 **/
public class ItemInteractionPacketDecoder implements PacketDecoder {

	/**
	 * The opcode for first click of an item
	 */
	public static final int FIRST_CLICK_ITEM = 122;

	/**
	 * The opcode for the second click of an item
	 */
	public static final int SECOND_CLICK_ITEM = 16;

	/**
	 * The opcode for the third click of an item
	 */
	public static final int THIRD_CLICK_ITEM = 75;

	@Override
	public void processPacket(Player player, int packetType, int packetSize) {

		switch (packetType) {
		case FIRST_CLICK_ITEM:
			handleFirstItemClick(player);
			break;
		case SECOND_CLICK_ITEM:
			handleSecondItemClick(player);
			break;
		case THIRD_CLICK_ITEM:
			handleThirdItemClick(player);
			break;
		}
	}

	/**
	 * Handles first clicking an item
	 * 
	 * @param player
	 *            The {@link Player} clicking on the item
	 */
	private void handleFirstItemClick(Player player) {
		int interfaceId = player.getClient().getInStream().readSignedWordBigEndianA();
		int itemSlot = player.getClient().getInStream().readUnsignedWordA();
		int itemId = player.getClient().getInStream().readUnsignedWordBigEndian();

		if (!player.getAttribute("verified_login", false)) {
			return;
		}

		if (itemSlot < 0 || itemSlot > 27 || interfaceId != 3214 || itemId != player.playerItems[itemSlot] - 1 || player.playerItemsN[itemSlot] <= 0) {
			player.getItems().resetItems(3214);
			return;
		}

		if (itemId == 8013) {
			player.getItems().deleteItem(itemId, itemSlot, 1);
			TeleportExecutor.teleport(player, new Teleport(new Position(Config.RESPAWN_X, Config.RESPAWN_Y, 0), TeleportType.TABLET));
			// player.getPA().teletab(Config.RESPAWN_X, Config.RESPAWN_Y, 0);
		}
		if (itemId == 2677 && !player.getDonatorRights().isDonator()) {
			player.getItems().deleteItem(itemId, itemSlot, 1);
			player.setDonatorRights(DonatorRights.PREMIUM);
			player.disconnected = true;
		}
		if (itemId == 2678 && player.getDonatorRights().isInferior(DonatorRights.SUPER_PREMIUM)) {
			player.getItems().deleteItem(itemId, itemSlot, 1);
			player.setDonatorRights(DonatorRights.SUPER_PREMIUM);
			player.disconnected = true;
		}
		if (itemId == 2679 && player.getDonatorRights().isInferior(DonatorRights.EXTREME_PREMIUM)) {
			player.getItems().deleteItem(itemId, itemSlot, 1);
			player.setDonatorRights(DonatorRights.EXTREME_PREMIUM);
			player.disconnected = true;
		}
		if (itemId == 3144) {
			if (player.duelRule[6]) {
				player.sendMessage("You may not eat in this duel.");
				return;
			}
			player.sendMessage("You eat the karambwan.");
			player.getCombat().resetPlayerAttack();
			player.playAnimation(Animation.create(829));
			player.attackTimer += 1;
			player.getItems().deleteItem(itemId, itemSlot, 1);
			if (player.getLevel()[3] < player.getPA().getLevelForXP(player.getExperience()[3])) {
				player.getLevel()[3] += 18;
				player.sendMessage("It heals some health.");
				if (player.getLevel()[3] > player.getPA().getLevelForXP(player.getExperience()[3])) {
					player.getLevel()[3] = player.getPA().getLevelForXP(player.getExperience()[3]);
				}
			}
			player.getPA().refreshSkill(3);
		}
		if (itemId == 299) {
			FlowerGame.planFlower(player);
			return;
		}
		if (itemId == 6199) {
			MysteryBox.openBox(player, itemId, itemSlot);
		}
		if (itemId == 4067) {
			player.sendMessage("You claimed @blu@" + player.getItems().getItemAmount(4067) + "@bla@ PVP Points.");
			player.pkPoints += player.getItems().getItemAmount(4067);
			player.getItems().deleteItem(4067, itemSlot, player.getItems().getItemAmount(4067));
		}
		ItemDef def = ItemDef.forId(itemId);

		if (def != null && def.getItemAction() != null) {
			if (def.getItemAction()[0].equals("Drink")) {
				PotionHandler.drinkPotion(player, itemId, itemSlot);
			} else if (def.getItemAction()[0].equals("Eat")) {
				FoodHandler.eatFood(player, itemId, itemSlot);
			}
		}
		if (itemId == 952) {
			if (player.inArea(3553, 3301, 3561, 3294)) {
				player.teleTimer = 3;
				player.newLocation = 1;
			} else if (player.inArea(3550, 3287, 3557, 3278)) {
				player.teleTimer = 3;
				player.newLocation = 2;
			} else if (player.inArea(3561, 3292, 3568, 3285)) {
				player.teleTimer = 3;
				player.newLocation = 3;
			} else if (player.inArea(3570, 3302, 3579, 3293)) {
				player.teleTimer = 3;
				player.newLocation = 4;
			} else if (player.inArea(3571, 3285, 3582, 3278)) {
				player.teleTimer = 3;
				player.newLocation = 5;
			} else if (player.inArea(3562, 3279, 3569, 3273)) {
				player.teleTimer = 3;
				player.newLocation = 6;
			}
		}
	}

	/**
	 * Handles second clicking on an item
	 * 
	 * @param player
	 *            The {@link Player} clicking on the item
	 */
	private void handleSecondItemClick(Player player) {
		final int itemId = player.getClient().getInStream().readSignedWordA();
		final int slot = player.getClient().getInStream().readSignedWordBigEndianA();
		final int interfaceId = player.getClient().getInStream().readSignedWordBigEndianA();

		if (!player.getAttribute("verified_login", false)) {
			return;
		}

		if (slot < 0 || slot > 27 || interfaceId != 3214 || player.playerItems[slot] - 1 != itemId || player.playerItemsN[slot] <= 0) {
			player.getItems().resetItems(3214);
			return;
		}

		switch (itemId) {

		default:
			if (player.getRights().isSuperior(PlayerRights.ADMINISTRATOR))
				System.out.println(player.playerName + " - Item3rdOption: " + itemId);
			break;
		}
	}

	/**
	 * Handles third clicking an item
	 * 
	 * @param player
	 *            The {@link Player} clicking on the item
	 */
	private void handleThirdItemClick(Player player) {
		int interfaceId = player.getClient().getInStream().readSignedWordBigEndianA();
		int slot = player.getClient().getInStream().readSignedWordBigEndian();
		int itemId = player.getClient().getInStream().readSignedWordA();

		if (!player.getAttribute("verified_login", false)) {
			return;
		}

		if (slot < 0 || slot > 27 || interfaceId != 3214 || player.playerItems[slot] - 1 != itemId || player.playerItemsN[slot] <= 0) {
			return;
		}

		switch (itemId) {

		case 1712:
			// player.getPA().handleGlory(itemId);
			break;

		default:
			if (player.getRights().isSuperior(PlayerRights.ADMINISTRATOR))
				System.out.println(player.playerName + " - Item3rdOption: " + itemId + " : " + interfaceId + " : " + slot);
			break;
		}
	}

}
