package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Challenge Player
 **/
public class ChallengePlayer implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        switch (packetType) {
        case 128:
            int answerPlayer = c.getClient().getInStream().readUnsignedWord();
            if (World.players[answerPlayer] == null) {
                return;
            }
            break;
        }
    }
}
