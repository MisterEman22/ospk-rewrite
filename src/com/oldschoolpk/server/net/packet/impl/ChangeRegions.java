package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Change Regions
 */
public class ChangeRegions implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        // Server.objectHandler.updateObjects(c);
        Server.itemHandler.reloadItems(c);
        Server.objectManager.loadObjects(c);
        c.getPA().castleWarsObjects();
        c.getPA().removeObjects();
        //c.getPA().Deletewalls(c);
        c.saveFile = true;

        if (c.skullTimer > 0) {
            c.isSkulled = true;
            c.headIconPk = 0;
            c.getPA().requestUpdates();
        }

    }

}
