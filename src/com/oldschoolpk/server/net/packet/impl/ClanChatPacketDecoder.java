package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;
import com.oldschoolpk.server.util.Misc;

/**
 * Chat
 **/
public class ClanChatPacketDecoder implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        String textSent = Misc.longToPlayerName2(c.getClient().getInStream().readQWord());
        textSent = textSent.replaceAll("_", " ");
    }
}
