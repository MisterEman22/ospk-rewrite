package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Move Items
 **/
public class MoveItems implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        int interfaceId = c.getClient().getInStream().readSignedWordBigEndianA();
        boolean insertMode = c.getClient().getInStream().readSignedByteC() == 1;
        int from = c.getClient().getInStream().readSignedWordBigEndianA();
        int to = c.getClient().getInStream().readSignedWordBigEndian();
        
        c.getItems().moveItems(from, to, interfaceId, insertMode);
    }
}