package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.content.combat.SpellData;
import com.oldschoolpk.server.model.item.ItemAssistant;
import com.oldschoolpk.server.model.item.equipment.RangeWeaponConstants;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerConstants;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Attack Player
 **/
public class AttackPlayer implements PacketDecoder {

	public static final int ATTACK_PLAYER = 128, MAGE_PLAYER = 249;

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {

		if (!c.getAttribute("verified_login", false)) {
			return;
		}
		c.playerIndex = 0;
		c.npcIndex = 0;
		switch (packetType) {

		/**
		 * Attack player
		 **/
		case ATTACK_PLAYER:
			c.playerIndex = c.getClient().getInStream().readSignedWord();
			if (c.playerIndex < 0)
				break;
			if (World.players[c.playerIndex] == null) {
				break;
			}

			if (c.respawnTimer > 0 || c.getLevel()[3] <= 0 || c.isDead) {
				break;
			}

			if (c.inDuelArena() && c.duelStatus != 5) {
				if (c.arenas()) {
					c.sendMessage("You can't challenge inside the arena.");
					return;
				}
				c.getDuel().requestDuel(c.playerIndex);
				c.playerIndex = 0;
				return;
			}

			if (c.duelStatus == 5 && c.duelCount > 0) {
				c.sendMessage("The duel hasn't started yet!");
				c.playerIndex = 0;
				return;
			}

			if (c.autocastId > 0)
				c.autocasting = true;

			if (!c.autocasting && c.spellId > 0) {
				c.spellId = 0;
			}
			c.mageFollow = false;
			c.spellId = 0;
			c.usingMagic = false;
			c.usingBow = false;
			c.usingRangeWeapon = false;
			boolean usingArrows = false;
			for (int bowId : RangeWeaponConstants.BOWS) {
				if (c.getEquipment()[PlayerConstants.PLAYER_WEAPON] == bowId) {
					c.usingBow = true;
					for (int arrowId : RangeWeaponConstants.ARROWS) {
						if (c.getEquipment()[PlayerConstants.PLAYER_ARROWS] == arrowId) {
							usingArrows = true;
						}
					}
				}
			}
			for (int otherRangeId : RangeWeaponConstants.OTHER_RANGE_WEAPONS) {
				if (c.getEquipment()[PlayerConstants.PLAYER_WEAPON] == otherRangeId) {
					c.usingRangeWeapon = true;
				}
			}

			if ((c.usingBow || c.autocasting) && c.goodDistance(c.getX(), c.getY(), World.players[c.playerIndex].getX(), World.players[c.playerIndex].getY(), 6)) {
				c.stopMovement();
			}

			if (c.usingRangeWeapon && c.goodDistance(c.getX(), c.getY(), World.players[c.playerIndex].getX(), World.players[c.playerIndex].getY(), 3)) {
				c.usingRangeWeapon = true;
				c.stopMovement();
			}
			if (!c.usingRangeWeapon)
				c.usingRangeWeapon = false;
			if (!c.getCombat().usingCorrectArrows() && Config.CORRECT_ARROWS && c.usingBow && !c.getCombat().usingCrystalBow() && !c.usingMagic) {
				c.sendMessage("You can't use " + ItemAssistant.getItemName(c.getEquipment()[PlayerConstants.PLAYER_ARROWS]).toLowerCase() + "s with a " + ItemAssistant.getItemName(c.getEquipment()[PlayerConstants.PLAYER_WEAPON]).toLowerCase() + ".");
				c.stopMovement();
				c.getCombat().resetPlayerAttack();
				return;
			}
			if (c.getCombat().checkReqs()) {
				c.followId = c.playerIndex;
				c.getPA().combatFollowPlayer(c.playerIndex);
				if (!c.usingMagic && !c.usingBow && !c.usingRangeWeapon) {
					c.followDistance = 1;
				}
				if (c.attackTimer <= 0) {
				}
			}
			break;

		/**
		 * Attack player with magic
		 **/
		case MAGE_PLAYER:
			if (!c.mageAllowed) {
				c.mageAllowed = true;
				break;
			}
			// c.usingSpecial = false;
			// c.getItems().updateSpecialBar();

			c.playerIndex = c.getClient().getInStream().readSignedWordA();
			int castingSpellId = c.getClient().getInStream().readSignedWordBigEndian();
			c.usingMagic = false;
			if (World.players[c.playerIndex] == null) {
				break;
			}

			if (c.respawnTimer > 0) {
				break;
			}

			if (c.duelStatus == 5 && c.duelCount > 0) {
				c.sendMessage("The duel hasn't started yet!");
				c.playerIndex = 0;
				return;
			}

			for (int i = 0; i < SpellData.MAGIC_SPELLS.length; i++) {
				if (castingSpellId == SpellData.MAGIC_SPELLS[i][0]) {
					if (SpellData.MAGIC_SPELLS[i][16] != c.playerMagicBook) {
						c.getCombat().resetPlayerAttack();
						return;
					}
					c.spellId = i;
					c.usingMagic = true;
					break;
				}
			}

			if (c.autocasting) {
				c.getPA().resetAutocast();
			}

			if (!c.getCombat().checkReqs()) {
				break;
			}

			for (int r = 0; r < c.REDUCE_SPELLS.length; r++) { // reducing
				// spells,
				// confuse etc
				if (World.players[c.playerIndex].REDUCE_SPELLS[r] == SpellData.MAGIC_SPELLS[c.spellId][0]) {
					if ((System.currentTimeMillis() - World.players[c.playerIndex].reduceSpellDelay[r]) < World.players[c.playerIndex].REDUCE_SPELL_TIME[r]) {
						c.sendMessage("That player is currently immune to this spell.");
						c.usingMagic = false;
						c.stopMovement();
						c.getCombat().resetPlayerAttack();
					}
					break;
				}
			}

			if (System.currentTimeMillis() - World.players[c.playerIndex].teleBlockDelay < World.players[c.playerIndex].teleBlockLength && SpellData.MAGIC_SPELLS[c.spellId][0] == 12445) {
				c.sendMessage("That player is already affected by this spell.");
				c.usingMagic = false;
				c.stopMovement();
				c.getCombat().resetPlayerAttack();
			}

			if (c.usingMagic) {
				if (c.goodDistance(c.getX(), c.getY(), World.players[c.playerIndex].getX(), World.players[c.playerIndex].getY(), 7)) {
					c.stopMovement();
				}
				c.followId = c.playerIndex;
				c.mageFollow = true;
				c.faceUpdate(c.followId + 32768);
				c.getPA().combatFollowPlayer(c.followId);
			}
			break;

		}

	}

}
