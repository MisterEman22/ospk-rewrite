package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.core.cycle.CycleEvent;
import com.oldschoolpk.server.core.cycle.CycleEventContainer;
import com.oldschoolpk.server.core.cycle.CycleEventHandler;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Pickup Item
 **/
public class PickupItem implements PacketDecoder {

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {
		c.pItemY = c.getClient().getInStream().readSignedWordBigEndian();
		c.pItemId = c.getClient().getInStream().readUnsignedWord();
		c.pItemX = c.getClient().getInStream().readSignedWordBigEndian();
		if (Math.abs(c.getX() - c.pItemX) > 25 || Math.abs(c.getY() - c.pItemY) > 25) {
			c.resetWalkingQueue();
			return;
		}

		if (!c.getAttribute("verified_login", false)) {
			return;
		}

		if (!Server.itemHandler.itemExists(c.pItemId, c.pItemX, c.pItemY)) {
			return;
		}
		c.getCombat().resetPlayerAttack();
		if (c.getX() == c.pItemX && c.getY() == c.pItemY) {
			Server.itemHandler.removeGroundItem(c, c.pItemId, c.pItemX, c.pItemY, true);
		} else {
			c.walkingToItem = true;
			CycleEventHandler.getSingleton().addEvent(c, 1, new CycleEvent() {

				@Override
				public void execute(CycleEventContainer container) {

					if (!c.walkingToItem || !Server.itemHandler.itemExists(c.pItemId, c.pItemX, c.pItemY)) {
						container.stop();
						return;
					}
					if (c.getX() == c.pItemX && c.getY() == c.pItemY) {
						Server.itemHandler.removeGroundItem(c, c.pItemId, c.pItemX, c.pItemY, true);
						container.stop();
					}
				}

				@Override
				public void stop() {
					c.walkingToItem = false;
				}

			});
		}

	}

}
