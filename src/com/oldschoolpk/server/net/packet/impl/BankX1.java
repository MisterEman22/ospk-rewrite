package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Bank X Items
 **/
public class BankX1 implements PacketDecoder {

    public static final int PART1 = 135;
    public static final int PART2 = 208;
    public int XremoveSlot, XinterfaceID, XremoveID, Xamount;

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        if (packetType == 135) {
            c.xRemoveSlot = c.getClient().getInStream().readSignedWordBigEndian();
            c.xInterfaceId = c.getClient().getInStream().readUnsignedWordA();
            c.xRemoveId = c.getClient().getInStream().readSignedWordBigEndian();
        }
        if (c.xInterfaceId == 3900) {
            c.getShops().buyItem(c.xRemoveId, c.xRemoveSlot, 20);// buy 20
            c.xRemoveSlot = 0;
            c.xInterfaceId = 0;
            c.xRemoveId = 0;
            return;
        }

        if (packetType == PART1) {
            //synchronized (c) {
            c.getClient().getOutStream().createFrame(27);
            // }
        }

    }
}
