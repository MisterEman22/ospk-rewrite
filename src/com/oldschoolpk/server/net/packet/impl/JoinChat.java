package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.world.Clan;

public class JoinChat implements PacketDecoder {

	@Override
	public void processPacket(Player player, int packetType, int packetSize) {
		long value = player.getClient().getInStream().readQWord();
		String text = Misc.longToPlayerName2(value);
		String owner = text.replaceAll("_", " ");

		if (player.getDialogue().isActive()) {
			player.getDialogue().input(text);
			return;
		}
		if (owner != null && owner.length() > 0) {
			if (player.clan == null) {
				Clan clan = Server.clanManager.getClan(owner);
				if (clan != null) {
					clan.addMember(player);
				} else if (owner.equalsIgnoreCase(player.playerName)) {
					Server.clanManager.create(player);
				} else {
					player.sendMessage(Misc.formatPlayerName(owner) + " has not created a clan yet.");
				}
				player.getPA().refreshSkill(21);
				player.getPA().refreshSkill(22);
				player.getPA().refreshSkill(23);
			}
		}
	}

}
