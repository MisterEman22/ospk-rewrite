package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Remove Item
 **/
public class RemoveItem implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        int interfaceId = c.getClient().getInStream().readUnsignedWordA();
        int removeSlot = c.getClient().getInStream().readUnsignedWordA();
        int removeId = c.getClient().getInStream().readUnsignedWordA();
        switch (interfaceId) {

        case 1688:
            c.getItems().removeItem(removeSlot);
            break;
            
        case 7423:
			c.getBankHandler().addToBank(removeId, 1, false);
			c.getItems().resetItems(7423);
			break;

        case 5064:
        	if (c.isBanking) {
				c.getBankHandler().addToBank(removeId, 1, true);
			}
            break;

        case 5382:
        	if(c.getBank().getBankSearch().isSearching()) {
        		c.getBank().getBankSearch().removeItem(removeId, 1);
        		return;
        	}
            c.getBankHandler().removeFromBank(removeId, 1, true);
            break;

        case 3900:
            c.getShops().buyFromShopPrice(removeId, removeSlot);
            break;

        case 3823:
            c.getShops().sellToShopPrice(removeId, removeSlot);
            break;

        case 3322:
            if (c.duelStatus > 0) {
                c.getDuel().stakeItem(removeId, removeSlot, 1);
                return;
            }
            c.getTradeHandler().tradeItem(removeId, removeSlot, 1);
            break;

        case 3415:
            if (c.duelStatus > 0) {
                return;
            }
            c.getTradeHandler().fromTrade(removeId, removeSlot, 1);
            break;

        case 6669:
            c.getDuel().fromDuel(removeId, removeSlot, 1);
            break;
        }
    }

}
