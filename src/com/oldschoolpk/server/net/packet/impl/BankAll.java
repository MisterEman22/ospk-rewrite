package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.item.GameItem;
import com.oldschoolpk.server.model.item.ItemDefinitions;
import com.oldschoolpk.server.model.item.Items;
import com.oldschoolpk.server.model.item.bank.BankItem;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Bank All Items
 **/
public class BankAll implements PacketDecoder {

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {
		int removeSlot = c.getClient().getInStream().readUnsignedWordA();
		int interfaceId = c.getClient().getInStream().readUnsignedWord();
		int removeId = c.getClient().getInStream().readUnsignedWordA();

		switch (interfaceId) {
		case 3900:
			c.getShops().buyItem(removeId, removeSlot, 10);
			break;

		case 3823:
			c.getShops().sellItem(removeId, removeSlot, 10);
			break;

		case 5064:
			// if (ItemDefinitions.forId(removeId).isStackable()) {
			// c.getBankHandler().bankItem(removeId, removeSlot,
			// c.playerItemsN[removeSlot]);
			// } else {
			// c.getBankHandler().bankItem(c.playerItems[removeSlot],
			// removeSlot, c.getItems().itemAmount(c.playerItems[removeSlot]));
			// }
			if (c.isBanking) {
				c.getBankHandler().addToBank(removeId, c.getItems().getItemAmount(removeId), true);
			}
			break;

		case 5382:
			// c.getBankHandler().fromBank(removeId, removeSlot,
			// c.bankItemsN[removeSlot]);
			if (!c.isBanking) {
				return;
			}
			if (c.getBank().getBankSearch().isSearching()) {
				c.getBank().getBankSearch().removeItem(removeId, c.getBank().getCurrentBankTab().getItemAmount(new BankItem(removeId + 1)));
				return;
			}
			c.getBankHandler().removeFromBank(removeId, c.getBank().getCurrentBankTab().getItemAmount(new BankItem(removeId + 1)), true);
			break;

		case 3322:
			if (c.duelStatus > 0) {
				if (Items.itemStackable[removeId] || Items.itemIsNote[removeId]) {
					c.getDuel().stakeItem(removeId, removeSlot, c.playerItemsN[removeSlot]);
				} else {
					c.getDuel().stakeItem(removeId, removeSlot, c.getItems().itemAmount(c.playerItems[removeSlot]));
				}
				return;
			}
			if (ItemDefinitions.forId(removeId).isStackable()) {
				c.getTradeHandler().tradeItem(c.playerItems[removeSlot] - 1, removeSlot, c.playerItemsN[removeSlot]);
			} else {
				c.getTradeHandler().tradeItem(removeId, removeSlot, 28);
			}
			break;

		case 6669:
			if (Items.itemStackable[removeId] || Items.itemIsNote[removeId]) {
				for (GameItem item : c.getDuel().stakedItems) {
					if (item.id == removeId) {
						c.getDuel().fromDuel(removeId, removeSlot, c.getDuel().stakedItems.get(removeSlot).amount);
					}
				}

			} else {
				c.getDuel().fromDuel(removeId, removeSlot, 28);
			}
			break;

		case 3415:
			if (removeSlot < 0 || removeSlot > 27) {
				return;
			}
			int amount = c.getTradeHandler().getOfferN()[removeSlot];
			int itemId = c.getTradeHandler().getOffer()[removeSlot] - 1;
			if (!ItemDefinitions.forId(itemId).isStackable() && !ItemDefinitions.forId(itemId).isNoteable()) {
				amount = 28;
			}
			c.getTradeHandler().fromTrade(removeId, removeSlot, amount);
			break;

		}
	}
}
