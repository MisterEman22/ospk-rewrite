package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.item.ItemAssistant;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Wear Item
 **/
public class WearItem implements PacketDecoder {

	@Override
	public void processPacket(Player client, int packetType, int packetSize) {
		client.wearId = client.getClient().getInStream().readUnsignedWord();
		client.wearSlot = client.getClient().getInStream().readUnsignedWordA();
		client.interfaceId = client.getClient().getInStream().readUnsignedWordA();
		if (client.wearId <= 0) {
			System.out.println("[WARNING]: " + client.playerName + " is trying to send negative values.");
			return;
		}

		if (client.playerItems[client.wearSlot] - 1 != client.wearId) {
			client.getItems().resetItems(3214);
			return;
		}
		
		if (client.getLevel()[3] <= 0 || client.isDead) {
			return;
		}
		if (client.playerIndex > 0 || client.npcIndex > 0) {
			if (client.wearId != 4153) {
				client.getCombat().resetPlayerAttack();
				client.stopFollowing();
			}
			
		}
		ItemAssistant.wearItem(client, client.wearId, client.wearSlot);
	}
}
