package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Bank 10 Items
 **/
public class Bank10 implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        int interfaceId = c.getClient().getInStream().readUnsignedWordBigEndian();
        int removeId = c.getClient().getInStream().readUnsignedWordA();
        int removeSlot = c.getClient().getInStream().readUnsignedWordA();

        switch (interfaceId) {
        case 1688:
            c.getPA().useOperate(removeId);
            break;
        case 3900:
            c.getShops().buyItem(removeId, removeSlot, 5);
            break;

        case 3823:
            c.getShops().sellItem(removeId, removeSlot, 5);
            break;

        case 5064:
//            c.getBankHandler().bankItem(removeId, removeSlot, 10);
        	if (c.isBanking) {
				c.getBankHandler().addToBank(removeId, 10, true);
			}
            break;

        case 5382:
//            c.getBankHandler().fromBank(removeId, removeSlot, 10);
        	if(c.getBank().getBankSearch().isSearching()) {
        		c.getBank().getBankSearch().removeItem(removeId, 10);
        		return;
        	}
			c.getBankHandler().removeFromBank(removeId, 10, true);
            break;
        case 3322:
            if (c.duelStatus > 0) {
                c.getDuel().stakeItem(removeId, removeSlot, 10);
                return;
            }
            c.getTradeHandler().tradeItem(removeId, removeSlot, 10);
            break;

        case 3415:
            if (c.duelStatus > 0) {
                return;
            }
            c.getTradeHandler().fromTrade(removeId, removeSlot, 10);
            break;

        case 6669:
            c.getDuel().fromDuel(removeId, removeSlot, 10);
            break;
        }
    }

}
