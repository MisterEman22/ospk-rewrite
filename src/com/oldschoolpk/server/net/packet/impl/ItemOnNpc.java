package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.item.UseItem;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

public class ItemOnNpc implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        int itemId = c.getClient().getInStream().readSignedWordA();
        int index = c.getClient().getInStream().readSignedWordA();
        int slot = c.getClient().getInStream().readSignedWordBigEndian();
        
        if (index < 0 || index >= NPCHandler.npcs.length || slot < 0 || slot > 27 || c.playerItems[slot] - 1 != itemId || c.playerItemsN[slot] <= 0) {
        	c.getItems().resetItems(3214);
        	return;
        }
        
        if (NPCHandler.npcs[index] == null) {
        	return;
        }
        
        int npcId = NPCHandler.npcs[index].getNPCType();

        UseItem.ItemonNpc(c, itemId, npcId, slot);
    }
}
