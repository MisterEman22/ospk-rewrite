package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.item.UseItem;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

public class ItemOnItem implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        int usedWithSlot = c.getClient().getInStream().readUnsignedWord();
        int itemUsedSlot = c.getClient().getInStream().readUnsignedWordA();
        
        if (usedWithSlot < 0 || usedWithSlot > 27 || itemUsedSlot < 0 || itemUsedSlot > 27) {
        	c.getItems().resetItems(3214);
        	return;
        }
        
        int useWith = c.playerItems[usedWithSlot] - 1;
        int itemUsed = c.playerItems[itemUsedSlot] - 1;
        UseItem.ItemonItem(c, itemUsed, useWith);
    }

}
