package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

public class BankModifiableX implements PacketDecoder {

	@Override
	public void processPacket(Player player, int packetType, int packetSize) {
		int slot = player.getClient().getInStream().readUnsignedWordA();
		int component = player.getClient().getInStream().readUnsignedWord();
		int item = player.getClient().getInStream().readUnsignedWordA();
		int amount = player.getClient().getInStream().readDWord();
		
		if(amount <= 0)
			return;
		switch(component) {
			case 5382:
            	if(player.getBank().getBankSearch().isSearching()) {
            		player.getBank().getBankSearch().removeItem(item, amount);
            		return;
            	}
                player.getBankHandler().removeFromBank(item, amount, true);
                break;
		}
	}

}
