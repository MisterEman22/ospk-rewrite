package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Bank 5 Items
 **/
public class Bank5 implements PacketDecoder {

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {
		int interfaceId = c.getClient().getInStream().readSignedWordBigEndianA();
		int removeId = c.getClient().getInStream().readSignedWordBigEndianA();
		int removeSlot = c.getClient().getInStream().readSignedWordBigEndian();

		switch (interfaceId) {

		case 3900:
			c.getShops().buyItem(removeId, removeSlot, 1);
			break;

		case 3823:
			c.getShops().sellItem(removeId, removeSlot, 1);
			break;

		case 5064:
			if (c.isBanking) {
				c.getBankHandler().addToBank(removeId, 5, true);
			}
			// c.getBankHandler().bankItem(removeId, removeSlot, 5);
			break;

		case 5382:
//			c.getBankHandler().fromBank(removeId, removeSlot, 5);
			if(c.getBank().getBankSearch().isSearching()) {
        		c.getBank().getBankSearch().removeItem(removeId, 5);
        		return;
        	}
			c.getBankHandler().removeFromBank(removeId, 5, true);
			break;

		case 3322:
			if (c.duelStatus > 0) {
				c.getDuel().stakeItem(removeId, removeSlot, 5);
				return;
			}
			c.getTradeHandler().tradeItem(removeId, removeSlot, 5);
			break;

		case 3415:
			if (c.duelStatus > 0) {
				return;
			}
			c.getTradeHandler().fromTrade(c.getTradeHandler().getOffer()[removeSlot] - 1, removeSlot, 5);
			break;

		case 6669:
			c.getDuel().fromDuel(removeId, removeSlot, 5);
			break;
		}
	}

}
