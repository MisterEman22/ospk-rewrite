package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Dialogue
 **/
public class DialoguePacketDecoder implements PacketDecoder {

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {

		if (c.getDialogue().isActive()) {
			c.getDialogue().next();
		}

	}

}
