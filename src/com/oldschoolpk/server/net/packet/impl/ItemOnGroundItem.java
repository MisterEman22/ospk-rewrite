package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.net.packet.PacketDecoder;
import com.oldschoolpk.server.util.Misc;

public class ItemOnGroundItem implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        int interfaceId = c.getClient().getInStream().readSignedWordBigEndian();
        int itemUsed = c.getClient().getInStream().readSignedWordA();
        int groundItem = c.getClient().getInStream().readUnsignedWord();
        int gItemY = c.getClient().getInStream().readSignedWordA();
        int itemUsedSlot = c.getClient().getInStream().readSignedWordBigEndianA();
        int gItemX = c.getClient().getInStream().readUnsignedWord();
        
        if (interfaceId != 3214 || itemUsedSlot < 0 || itemUsedSlot > 27 || c.playerItems[itemUsedSlot] - 1 != itemUsed || c.playerItemsN[itemUsedSlot] <= 0) {
        	c.getItems().resetItems(3214);
        	return;
        }
        
        if (!Server.itemHandler.itemExists(groundItem, gItemX, gItemY)) {
            return;
        }

        switch (itemUsed) {

        default:
            if (c.getRights().isSuperior(PlayerRights.ADMINISTRATOR))
                Misc.println("ItemUsed " + itemUsed + " on Ground Item " + groundItem);
            break;
        }
    }

}
