package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.core.cycle.CycleEvent;
import com.oldschoolpk.server.core.cycle.CycleEventContainer;
import com.oldschoolpk.server.core.cycle.CycleEventHandler;
import com.oldschoolpk.server.model.content.combat.SpellData;
import com.oldschoolpk.server.model.item.equipment.RangeWeaponConstants;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerConstants;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Click NPC
 */
public class ClickNPC implements PacketDecoder {
    public static final int ATTACK_NPC = 72, MAGE_NPC = 131, FIRST_CLICK = 155, SECOND_CLICK = 17, THIRD_CLICK = 21;

    @Override
    public void processPacket(Player client, int packetType, int packetSize) {
    	
    	if (!client.getAttribute("verified_login", false)) {
			return;
		}
        client.npcIndex = 0;
        client.npcClickIndex = 0;
        client.playerIndex = 0;
        client.clickNpcType = 0;
        client.getPA().resetFollow();
        switch (packetType) {

        /**
         * Attack npc melee or range
         **/
        case ATTACK_NPC:
            if (!client.mageAllowed) {
                client.mageAllowed = true;
                client.sendMessage("I can't reach that.");
                break;
            }
            client.npcIndex = client.getClient().getInStream().readUnsignedWordA();
            if (NPCHandler.npcs[client.npcIndex] == null) {
                client.npcIndex = 0;
                break;
            }
            
            if (NPCHandler.npcs[client.npcIndex].getMaxHP() == 0) {
                client.npcIndex = 0;
                break;
            }
            
            if (NPCHandler.npcs[client.npcIndex] == null) {
                break;
            }
            
            if (client.autocastId > 0)
                client.autocasting = true;
            if (!client.autocasting && client.spellId > 0) {
                client.spellId = 0;
            }
            client.faceUpdate(client.npcIndex);
            client.usingMagic = false;
            client.usingBow = false;
            client.usingRangeWeapon = false;
            boolean usingArrows = false;
            if (client.getEquipment()[PlayerConstants.PLAYER_WEAPON] >= 4214 && client
                .getEquipment()[PlayerConstants.PLAYER_WEAPON] <= 4223)
                client.usingBow = true;
            for (int bowId : RangeWeaponConstants.BOWS) {
                if (client.getEquipment()[PlayerConstants.PLAYER_WEAPON] == bowId) {
                    client.usingBow = true;
                    for (int arrowId : RangeWeaponConstants.ARROWS) {
                        if (client.getEquipment()[PlayerConstants.PLAYER_ARROWS] == arrowId) {
                            usingArrows = true;
                        }
                    }
                }
            }
            for (int otherRangeId : RangeWeaponConstants.OTHER_RANGE_WEAPONS) {
                if (client.getEquipment()[PlayerConstants.PLAYER_WEAPON] == otherRangeId) {
                    client.usingRangeWeapon = true;
                }
            }
            if ((client.usingBow || client.autocasting) && client
                .goodDistance(client.getX(), client.getY(), NPCHandler.npcs[client.npcIndex].getX(), NPCHandler.npcs[client.npcIndex].getY(),
                    7)) {
                client.stopMovement();
            }

            if (client.usingRangeWeapon && client
                .goodDistance(client.getX(), client.getY(), NPCHandler.npcs[client.npcIndex].getX(), NPCHandler.npcs[client.npcIndex].getY(),
                    4)) {
                client.stopMovement();
            }
            if (!client.getCombat().usingCorrectArrows() && Config.CORRECT_ARROWS && client.usingBow && !client.getCombat()
                .usingCrystalBow()) {
                client.sendMessage("You can't use " + client.getItems().getItemName(client.getEquipment()[PlayerConstants.PLAYER_ARROWS])
                    .toLowerCase() + "s with a " + client.getItems().getItemName(client.getEquipment()[PlayerConstants.PLAYER_WEAPON])
                    .toLowerCase() + ".");
                client.stopMovement();
                client.getCombat().resetPlayerAttack();
                return;
            }
            if (client.getEquipment()[PlayerConstants.PLAYER_WEAPON] == 9185 && !client.getCombat().properBolts()) {
                client.sendMessage("You must use bolts with a crossbow.");
                client.stopMovement();
                client.getCombat().resetPlayerAttack();
                return;
            }

            if (client.followId > 0) {
                client.getPA().resetFollow();
            }
            if (client.attackTimer <= 0) {
                client.getCombat().attackNpc(client.npcIndex);
                client.attackTimer++;
				if (client.npcIndex > 0 ) { 
					client.getPA().removeAllWindows();
					client.getPA().sendFrame126("true:" + client.npcIndex + ":" + NPCHandler.npcs[client.npcIndex].HP + ":" + NPCHandler.npcs[client.npcIndex].getMaxHP(), 35000);
				} // Try now
            }

            break;

        /**
         * Attack npc with magic
         **/
        case MAGE_NPC:
            if (!client.mageAllowed) {
                client.mageAllowed = true;
                client.sendMessage("I can't reach that.");
                break;
            }
            // c.usingSpecial = false;
            // c.getItems().updateSpecialBar();

            client.npcIndex = client.getClient().getInStream().readSignedWordBigEndianA();
            int castingSpellId = client.getClient().getInStream().readSignedWordA();
            client.usingMagic = false;

            if (NPCHandler.npcs[client.npcIndex] == null) {
                break;
            }

            if (NPCHandler.npcs[client.npcIndex].getMaxHP() == 0 || NPCHandler.npcs[client.npcIndex].getNPCType() == 944) {
                client.sendMessage("You can't attack this npc.");
                break;
            }

            for (int i = 0; i < SpellData.MAGIC_SPELLS.length; i++) {
                if (castingSpellId == SpellData.MAGIC_SPELLS[i][0]) {
                    client.spellId = i;
                    client.usingMagic = true;
                    break;
                }
            }
            if (castingSpellId == 1171) { // crumble undead
                for (int npc : Config.UNDEAD_NPCS) {
                    if (NPCHandler.npcs[client.npcIndex].getNPCType() != npc) {
                        client.sendMessage("You can only attack undead monsters with this spell.");
                        client.usingMagic = false;
                        client.stopMovement();
                        break;
                    }
                }
            }
            /*
             * if(!c.getCombat().checkMagicReqs(c.spellId)) { c.stopMovement();
			 * break; }
			 */

            if (client.autocasting)
                client.autocasting = false;

            if (client.usingMagic) {
                if (client
                    .goodDistance(client.getX(), client.getY(), NPCHandler.npcs[client.npcIndex].getX(), NPCHandler.npcs[client.npcIndex].getY(),
                        6)) {
                    client.stopMovement();
                }
                if (client.attackTimer <= 0) {
                    client.getCombat().attackNpc(client.npcIndex);
                    client.attackTimer++;
                }
            }

            break;

        case FIRST_CLICK:
            client.npcClickIndex = client.getClient().getInStream().readSignedWordBigEndian();
            client.npcType = NPCHandler.npcs[client.npcClickIndex].getNPCType();
            if (client.goodDistance(NPCHandler.npcs[client.npcClickIndex].getX(), NPCHandler.npcs[client.npcClickIndex].getY(), client.getX(),
                client.getY(), 1)) {
                client.turnPlayerTo(NPCHandler.npcs[client.npcClickIndex].getX(), NPCHandler.npcs[client.npcClickIndex].getY());
                NPCHandler.npcs[client.npcClickIndex].facePlayer(client.playerId);
                client.getActions().firstClickNpc(client.npcType);
            } else {
				client.clickNpcType = 1;
				client.followId2 = client.npcClickIndex;
				CycleEventHandler.getSingleton().addEvent(client, 1, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						
						if (client.clickNpcType != 1 || NPCHandler.npcs[client.npcClickIndex] == null || NPCHandler.npcs[client.npcClickIndex].isDead) {
							container.stop();
							return;
						}
						if (client.goodDistance(client.getX(), client.getY(), NPCHandler.npcs[client.npcClickIndex].getX(), NPCHandler.npcs[client.npcClickIndex].getY(), 1)) {
							client.getActions().firstClickNpc(client.npcType);
							container.stop();
						}
					}
					
					@Override
					public void stop() {
						client.clickNpcType = 0;
						client.stopFollowing();
					}
					
				});
			}
            break;

        case SECOND_CLICK:
            client.npcClickIndex = client.getClient().getInStream().readUnsignedWordBigEndianA();
            if (client.npcClickIndex < 0)
                return;
            client.npcType = NPCHandler.npcs[client.npcClickIndex].getNPCType();
            if (client.goodDistance(NPCHandler.npcs[client.npcClickIndex].getX(), NPCHandler.npcs[client.npcClickIndex].getY(), client.getX(),
                client.getY(), 1)) {
                client.turnPlayerTo(NPCHandler.npcs[client.npcClickIndex].getX(), NPCHandler.npcs[client.npcClickIndex].getY());
                NPCHandler.npcs[client.npcClickIndex].facePlayer(client.playerId);
                client.getActions().secondClickNpc(client.npcType);
            } else {
				client.clickNpcType = 2;
				client.followId2 = client.npcClickIndex;
				CycleEventHandler.getSingleton().addEvent(client, 1, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						
						if (client.clickNpcType != 2 || NPCHandler.npcs[client.npcClickIndex] == null || NPCHandler.npcs[client.npcClickIndex].isDead) {
							container.stop();
							return;
						}
						if (client.goodDistance(client.getX(), client.getY(), NPCHandler.npcs[client.npcClickIndex].getX(), NPCHandler.npcs[client.npcClickIndex].getY(), 1)) {
							client.getActions().secondClickNpc(client.npcType);
							container.stop();
						}
					}
					
					@Override
					public void stop() {
						client.clickNpcType = 0;
						client.stopFollowing();
					}
					
				});
			}
            break;

        case THIRD_CLICK:
            client.npcClickIndex = client.getClient().getInStream().readSignedWord();
            if (client.npcClickIndex < 0)
                return;
            client.npcType = NPCHandler.npcs[client.npcClickIndex].getNPCType();
            if (client.goodDistance(NPCHandler.npcs[client.npcClickIndex].getX(), NPCHandler.npcs[client.npcClickIndex].getY(), client.getX(),
                client.getY(), 1)) {
                client.turnPlayerTo(NPCHandler.npcs[client.npcClickIndex].getX(), NPCHandler.npcs[client.npcClickIndex].getY());
                NPCHandler.npcs[client.npcClickIndex].facePlayer(client.playerId);
                client.getActions().thirdClickNpc(client.npcType);
            } else {
				client.clickNpcType = 3;
				client.followId2 = client.npcClickIndex;
				CycleEventHandler.getSingleton().addEvent(client, 1, new CycleEvent() {

					@Override
					public void execute(CycleEventContainer container) {
						
						if (client.clickNpcType != 3 || NPCHandler.npcs[client.npcClickIndex] == null || NPCHandler.npcs[client.npcClickIndex].isDead) {
							container.stop();
							return;
						}
						if (client.goodDistance(client.getX(), client.getY(), NPCHandler.npcs[client.npcClickIndex].getX(), NPCHandler.npcs[client.npcClickIndex].getY(), 1)) {
							client.getActions().thirdClickNpc(client.npcType);
							container.stop();
						}
					}
					
					@Override
					public void stop() {
						client.clickNpcType = 0;
						client.stopFollowing();
					}
					
				});
			}
            break;
        }

    }
}
