package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Bank X Items
 **/
public class BankX2 implements PacketDecoder {
    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        int Xamount = c.getClient().getInStream().readDWord();
        if (Xamount == 0)
            Xamount = 1;
        
        if (c.getDialogue().isActive()) {
        	c.getDialogue().input(Xamount);
        	return;
        }
        switch (c.xInterfaceId) {

        case 5064:
            if (c.inTrade) {
                c.sendMessage("You can't bank items while trading!");
                return;
            }
//            c.getBankHandler().bankItem(c.playerItems[c.XremoveSlot], c.XremoveSlot, Xamount);
            c.getBankHandler().addToBank(c.playerItems[c.xRemoveSlot] - 1, Xamount, true);
            break;

        case 5382:
            if (c.inTrade) {
                c.sendMessage("You can't store items while trading!");
                return;
            }
//            c.getBankHandler().fromBank(c.bankItems[c.XremoveSlot], c.XremoveSlot, Xamount);
        	c.getBankHandler().removeFromBank(c.getBank().getCurrentBankTab().getItem(c.xRemoveSlot).getId() - 1, Xamount, true);
            break;

        case 3322:
            c.getTradeHandler().tradeItem(c.playerItems[c.XremoveSlot] - 1, c.XremoveSlot, c.Xamount);
            break;

        case 3415:
            c.getTradeHandler().fromTrade(c.getTradeHandler().getOffer()[c.XremoveSlot] - 1, c.XremoveSlot, c.Xamount);
            break;
        }
    }
}