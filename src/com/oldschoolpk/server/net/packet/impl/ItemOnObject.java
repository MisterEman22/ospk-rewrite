package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.item.UseItem;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

public class ItemOnObject implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        /*
         * a = ? b = ?
		 */

        int interfaceId = c.getClient().getInStream().readUnsignedWord();
        int objectId = c.getClient().getInStream().readSignedWordBigEndian();
        int objectY = c.getClient().getInStream().readSignedWordBigEndianA();
        int slot = c.getClient().getInStream().readUnsignedWord();
        int objectX = c.getClient().getInStream().readSignedWordBigEndianA();
        int itemId = c.getClient().getInStream().readUnsignedWord();
       
        if (slot < 0 || slot > 27 || interfaceId != 3214 || c.playerItems[slot] - 1 != itemId || c.playerItemsN[slot] <= 0) {
        	c.getItems().resetItems(3214);
        	return;
        }
        
        UseItem.ItemonObject(c, objectId, objectX, objectY, itemId);

    }

}
