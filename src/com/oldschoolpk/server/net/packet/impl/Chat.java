package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;
import com.oldschoolpk.server.sanction.SanctionHandler;

/**
 * Chat
 **/
public class Chat implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        c.setChatTextEffects(c.getClient().getInStream().readUnsignedByteS());
        c.setChatTextColor(c.getClient().getInStream().readUnsignedByteS());
        c.setChatTextSize((byte) (c.getClient().packetSize - 2));
        c.getClient().getInStream().readBytes_reverseA(c.getChatText(), c.getChatTextSize(), 0);
        
        if (!c.getAttribute("verified_login", false)) {
			return;
		}
        
        if (SanctionHandler.isMuted(c) || SanctionHandler.isIPMuted(c)) {
            if (SanctionHandler.isMuted(c)) {
                c.sendMessage("Your account is temporarily muted.");
                c.sendMessage("You can appeal your mute at http://oldschoolpk.com.");
            } else if (SanctionHandler.isIPMuted(c)) {
                c.sendMessage("Your account is permanently muted.");
                c.sendMessage("This mute cannot be appealed.");
            }
            return;
        }
        c.setChatTextUpdateRequired(true);
    }
}
