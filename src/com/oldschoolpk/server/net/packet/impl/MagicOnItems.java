package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Magic on items
 **/
public class MagicOnItems implements PacketDecoder {

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {
		int slot = c.getClient().getInStream().readSignedWord();
		int itemId = c.getClient().getInStream().readSignedWordA();
		int interfaceId = c.getClient().getInStream().readSignedWord();
		int spellId = c.getClient().getInStream().readSignedWordA();

		if (slot < 0 || slot > 27 || interfaceId != 3214 || spellId < 0 || spellId > 55 || c.playerItems[slot] - 1 != itemId || c.playerItemsN[slot] <= 0) {
			c.getItems().resetItems(3214);
			return;
		}

		c.usingMagic = true;
		c.getPA().magicOnItems(slot, itemId, spellId);
		c.usingMagic = false;

	}

}
