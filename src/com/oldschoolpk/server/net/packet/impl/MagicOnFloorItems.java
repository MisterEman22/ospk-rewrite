package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.Animation;
import com.oldschoolpk.server.model.Graphic;
import com.oldschoolpk.server.model.content.combat.SpellData;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Magic on floor items
 **/
public class MagicOnFloorItems implements PacketDecoder {

    @Override
    public void processPacket(Player c, int packetType, int packetSize) {
        int itemY = c.getClient().getInStream().readSignedWordBigEndian();
        int itemId = c.getClient().getInStream().readUnsignedWord();
        int itemX = c.getClient().getInStream().readSignedWordBigEndian();
        c.getClient().getInStream().readUnsignedWordA();

        if (!Server.itemHandler.itemExists(itemId, itemX, itemY)) {
            c.stopMovement();
            return;
        }
        c.usingMagic = true;
        if (!c.getCombat().checkMagicReqs(51)) {
            c.stopMovement();
            return;
        }

        if (c.goodDistance(c.getX(), c.getY(), itemX, itemY, 12)) {
            int offY = (c.getX() - itemX) * -1;
            int offX = (c.getY() - itemY) * -1;
            c.teleGrabX = itemX;
            c.teleGrabY = itemY;
            c.teleGrabItem = itemId;
            c.turnPlayerTo(itemX, itemY);
            c.teleGrabDelay = System.currentTimeMillis();
            c.playAnimation(Animation.create(SpellData.MAGIC_SPELLS[51][2]));
            c.playGraphic(Graphic.create(SpellData.MAGIC_SPELLS[51][3], 0, 100));
            c.getPA().createPlayersStillGfx(144, itemX, itemY, 0, 72);
            c.getPA().createPlayersProjectile(c.getX(), c.getY(), offX, offY, 50, 70, SpellData.MAGIC_SPELLS[51][4], 50, 10, 0, 50);
            c.getPA().addSkillXP(SpellData.MAGIC_SPELLS[51][7], 6);
            c.getPA().refreshSkill(6);
            c.stopMovement();
        }
    }

}
