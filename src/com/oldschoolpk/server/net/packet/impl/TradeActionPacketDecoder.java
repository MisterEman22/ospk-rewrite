package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Trading
 */
public class TradeActionPacketDecoder implements PacketDecoder {

    public static final int REQUEST = 73, ANSWER = 139;

    @Override
    public void processPacket(Player client, int packetType, int packetSize) {
    	
    	if (!client.getAttribute("verified_login", false)) {
			return;
		}
    	
        if (client.isBanking || client.isShopping || client.duelStatus > 0) {
            client.sendMessage("You are far too busy right now.");
            return;
        }
        if (packetType == REQUEST) {
            int trade = client.getClient().getInStream().readSignedWordBigEndian();
            if (trade < 0 || trade >= Config.MAX_PLAYERS)
                return;
            if (!client.goodDistance(client.getX(), client.getY(), World.players[trade].getX(),
                World.players[trade].getY(), 1)) {
                client.sendMessage("Please move closer to the person you are trying to trade.");
                return;
            }
            if (World.players[trade] != null) {
                Player c = World.players[trade];
                client.getTradeHandler().requestTrade(c);
            }
        } else if (packetType == ANSWER) {
            int trade = client.getClient().getInStream().readSignedWordBigEndian();

            if (trade < 0 || trade >= Config.MAX_PLAYERS)
                return;
            if (!client.goodDistance(client.getX(), client.getY(), World.players[trade].getX(),
                World.players[trade].getY(), 1)) {
                client.sendMessage("Please move closer to the person you are trying to trade.");
                return;
            }
            if (World.players[trade] != null) {
                Player c = World.players[trade];
                client.getTradeHandler().answerTrade(c);
            }
        }
    }

}
