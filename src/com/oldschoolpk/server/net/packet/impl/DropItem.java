package com.oldschoolpk.server.net.packet.impl;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.npc.NPCFollowing;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerSave;
import com.oldschoolpk.server.net.packet.PacketDecoder;

/**
 * Drop Item
 **/
public class DropItem implements PacketDecoder {

	@Override
	public void processPacket(Player c, int packetType, int packetSize) {
		int itemId = c.getClient().getInStream().readUnsignedWordA();
		int interfaceId = c.getClient().getInStream().readUnsignedWord();
		int slot = c.getClient().getInStream().readUnsignedWordA();
		
		if (!c.getAttribute("verified_login", false)) {
			return;
		}

		if (interfaceId != 3214 || slot < 0 || slot > 27 || itemId < 0 || itemId > Config.ITEM_LIMIT || (c.playerItems[slot] - 1 != itemId) || c.playerItemsN[slot] <= 0) {
			c.getItems().resetItems(3214);
			return;
		}

		if (System.currentTimeMillis() - c.alchDelay < 1800) {
			return;
		}

		if (c.getLevel()[3] <= 0 || c.isDead) {
			return;
		}

		if (c.getTradeHandler().getCurrentTrade() != null) {
			if (c.getTradeHandler().getCurrentTrade().isOpen()) {
				c.getTradeHandler().decline();
			}
		}

		if (c.getPets().dropPet(itemId)) {
			return;
		}
		
		boolean droppable = true;
		for (int i : Config.UNDROPPABLE_ITEMS) {
			if (i == itemId) {
				droppable = false;
				break;
			}
		}

		if (droppable) {
			c.getCombat().resetPlayerAttack();
			Server.itemHandler.createGroundItem(c, itemId, c.getX(), c.getY(), c.playerItemsN[slot], c.getId());
			c.getItems().deleteItem(itemId, slot, c.playerItemsN[slot]);
			PlayerSave.saveGame(c);
		} else {
			c.sendMessage("This items cannot be dropped.");
		}

	}
}
