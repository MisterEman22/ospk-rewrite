package com.oldschoolpk.server.net.packet;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.packet.impl.ActionButtonPacketDecoder;
import com.oldschoolpk.server.net.packet.impl.AttackPlayer;
import com.oldschoolpk.server.net.packet.impl.Bank10;
import com.oldschoolpk.server.net.packet.impl.Bank5;
import com.oldschoolpk.server.net.packet.impl.BankAll;
import com.oldschoolpk.server.net.packet.impl.BankAllButOne;
import com.oldschoolpk.server.net.packet.impl.BankModifiableX;
import com.oldschoolpk.server.net.packet.impl.BankX;
import com.oldschoolpk.server.net.packet.impl.ChangeAppearance;
import com.oldschoolpk.server.net.packet.impl.ChangeRegions;
import com.oldschoolpk.server.net.packet.impl.Chat;
import com.oldschoolpk.server.net.packet.impl.ClickNPC;
import com.oldschoolpk.server.net.packet.impl.ClickingInGame;
import com.oldschoolpk.server.net.packet.impl.ClickingStuff;
import com.oldschoolpk.server.net.packet.impl.CommandPacketDecoder;
import com.oldschoolpk.server.net.packet.impl.DialoguePacketDecoder;
import com.oldschoolpk.server.net.packet.impl.DropItem;
import com.oldschoolpk.server.net.packet.impl.FollowPlayer;
import com.oldschoolpk.server.net.packet.impl.IdleLogoutPacketDecoder;
import com.oldschoolpk.server.net.packet.impl.InterfaceAction;
import com.oldschoolpk.server.net.packet.impl.ItemInteractionPacketDecoder;
import com.oldschoolpk.server.net.packet.impl.ItemOnGroundItem;
import com.oldschoolpk.server.net.packet.impl.ItemOnItem;
import com.oldschoolpk.server.net.packet.impl.ItemOnNpc;
import com.oldschoolpk.server.net.packet.impl.ItemOnObject;
import com.oldschoolpk.server.net.packet.impl.JoinChat;
import com.oldschoolpk.server.net.packet.impl.MagicOnFloorItems;
import com.oldschoolpk.server.net.packet.impl.MagicOnItems;
import com.oldschoolpk.server.net.packet.impl.MoveItems;
import com.oldschoolpk.server.net.packet.impl.MovementPacketDecoder;
import com.oldschoolpk.server.net.packet.impl.ObjectInterationPacketDecoder;
import com.oldschoolpk.server.net.packet.impl.PickupItem;
import com.oldschoolpk.server.net.packet.impl.PrivateMessagingPacketDecoder;
import com.oldschoolpk.server.net.packet.impl.ReceiveString;
import com.oldschoolpk.server.net.packet.impl.RemoveItem;
import com.oldschoolpk.server.net.packet.impl.SilentPacket;
import com.oldschoolpk.server.net.packet.impl.TradeActionPacketDecoder;
import com.oldschoolpk.server.net.packet.impl.WearItem;

public class PacketHandler {

	private static PacketDecoder packetId[] = new PacketDecoder[256];

	static {
		SilentPacket u = new SilentPacket();
		packetId[3] = u;
		packetId[202] = u;
		packetId[77] = u;
		packetId[86] = u;
		packetId[78] = u;
		packetId[36] = u;
		packetId[226] = u;
		packetId[246] = u;
		packetId[148] = u;
		packetId[183] = u;
		packetId[230] = u;
		packetId[136] = u;
		packetId[189] = u;
		packetId[152] = u;
		packetId[200] = u;
		packetId[85] = u;
		packetId[165] = u;
		packetId[238] = u;
		packetId[150] = u;
		packetId[40] = new DialoguePacketDecoder();
		ObjectInterationPacketDecoder co = new ObjectInterationPacketDecoder();
		packetId[132] = co;
		packetId[252] = co;
		packetId[70] = co;
		packetId[57] = new ItemOnNpc();
		ClickNPC cn = new ClickNPC();
		packetId[72] = cn;
		packetId[131] = cn;
		packetId[155] = cn;
		packetId[17] = cn;
		packetId[21] = cn;
		
		ItemInteractionPacketDecoder itemInteraction = new ItemInteractionPacketDecoder();
		packetId[ItemInteractionPacketDecoder.FIRST_CLICK_ITEM] = itemInteraction;
		packetId[ItemInteractionPacketDecoder.SECOND_CLICK_ITEM] = itemInteraction;
		packetId[ItemInteractionPacketDecoder.THIRD_CLICK_ITEM] = itemInteraction;
		
		packetId[241] = new ClickingInGame();
		packetId[4] = new Chat();
		packetId[236] = new PickupItem();
		packetId[87] = new DropItem();
		packetId[185] = new ActionButtonPacketDecoder();
		packetId[130] = new ClickingStuff();
		packetId[103] = new CommandPacketDecoder();
		packetId[214] = new MoveItems();
		packetId[237] = new MagicOnItems();
		packetId[181] = new MagicOnFloorItems();
		packetId[202] = new IdleLogoutPacketDecoder();
		AttackPlayer ap = new AttackPlayer();
		packetId[249] = ap;
		packetId[128] = ap;
		packetId[73] = new TradeActionPacketDecoder();
		packetId[139] = new TradeActionPacketDecoder();
		packetId[153] = new FollowPlayer();
		packetId[41] = new WearItem();
		packetId[145] = new RemoveItem();
		packetId[117] = new Bank5();
		packetId[43] = new Bank10();
		packetId[129] = new BankAll();
		packetId[140] = new BankAllButOne();
		packetId[141] = new BankModifiableX();
		packetId[101] = new ChangeAppearance();
		PrivateMessagingPacketDecoder pm = new PrivateMessagingPacketDecoder();
		packetId[188] = pm;
		packetId[126] = pm;
		packetId[215] = pm;
		packetId[59] = pm;
		packetId[95] = pm;
		packetId[133] = pm;
		BankX bx = new BankX();
		packetId[135] = bx;
		packetId[208] = bx;
		MovementPacketDecoder w = new MovementPacketDecoder();
		packetId[98] = w;
		packetId[164] = w;
		packetId[248] = w;
		packetId[53] = new ItemOnItem();
		packetId[192] = new ItemOnObject();
		packetId[25] = new ItemOnGroundItem();
		ChangeRegions cr = new ChangeRegions();
		packetId[121] = cr;
		packetId[210] = cr;
		packetId[60] = new JoinChat();
		packetId[127] = new ReceiveString();
		packetId[213] = new InterfaceAction();
	}

	public static void processPacket(Player c, int packetType, int packetSize) {
		if (packetType == -1) { // this
			return;
		}
		PacketDecoder p = packetId[packetType];

		if (p != null) {
			try {
				p.processPacket(c, packetType, packetSize);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			c.disconnected = true;
			System.out.println(c.playerName + "is sending invalid PacketType: " + packetType + ". PacketSize: " + packetSize);
		}
	}

}
