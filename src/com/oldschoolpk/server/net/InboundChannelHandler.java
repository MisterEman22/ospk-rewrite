package com.oldschoolpk.server.net;

import java.net.InetSocketAddress;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.model.Client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * An sharable implementation of {@link ChannelInboundHandlerAdapter} which
 * handles incoming upstream channel events using Netty.
 *
 * @author Ryley Kimmel <ryley.kimmel@live.com>
 */
@Sharable
public final class InboundChannelHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
		Client session = ctx.attr(NetworkConstants.SESSION).getAndRemove();

		if (session != null) {
			session.getPlayer().disconnected = true;
		}
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		// session will be null at this stage, don't bother trying to get it
		Channel channel = ctx.channel();

		// get the connected address
		String address = ((InetSocketAddress) channel.remoteAddress()).getAddress().getHostAddress();

		// check if the connected address can continue connecting
		if (ConnectionSet.check(address)) {
			// if not, write a single byte with the too many connections
			// response code
			ByteBuf buf = ctx.alloc().buffer(1).writeByte(9);

			// flush the buffer
			channel.writeAndFlush(buf);

			// close the channel
			channel.close();
		}
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		Client session = ctx.attr(NetworkConstants.SESSION).get();

		if (session == null) {
			return;
		}
		
		if (msg instanceof NettyPacket) {
			if (session.getPacketsReceived().get() < Config.MAX_INCOMONG_PACKETS_PER_CYCLE) {
				if (!session.getPlayer().disconnected) {
					session.queueMessage((NettyPacket) msg);
				}
			}
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable e) {
		Channel channel = ctx.channel();
		channel.close();
	}

}
