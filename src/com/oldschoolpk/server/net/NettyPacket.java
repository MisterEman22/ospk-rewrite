package com.oldschoolpk.server.net;

import io.netty.buffer.ByteBuf;

/**
 * Represents a payload of bytes that can be transmitting through streams.
 *
 * @author Ryley Kimmel <ryley.kimmel@live.com>
 */
public final class NettyPacket {

  private final int opcode;
  private final int length;
  private final ByteBuf payload;

  public NettyPacket(ByteBuf payload) {
    this(-1, payload);
  }

  public NettyPacket(int opcode, ByteBuf payload) {
    this.opcode = opcode;
    length = payload.writableBytes();
    this.payload = payload;
  }

  public NettyPacket(int opcode, int length, ByteBuf payload) {
    this.opcode = opcode;
    this.length = length;
    this.payload = payload;
  }

  public int getOpcode() {
    return opcode;
  }

  public int getLength() {
    return length;
  }

  public ByteBuf getPayload() {
    return payload;
  }

  public byte[] array() {
    return payload.array();
  }

}
