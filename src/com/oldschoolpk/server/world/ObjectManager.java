package com.oldschoolpk.server.world;

import java.util.ArrayList;

import com.oldschoolpk.server.model.Position;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.content.teleport.Teleport;
import com.oldschoolpk.server.model.content.teleport.Teleport.TeleportType;
import com.oldschoolpk.server.model.content.teleport.TeleportExecutor;
import com.oldschoolpk.server.model.object.GameObject;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.util.Misc;

/**
 * @author Sanity
 */

public class ObjectManager {

    public ArrayList<GameObject> objects = new ArrayList<GameObject>();
    private ArrayList<GameObject> toRemove = new ArrayList<GameObject>();

    public boolean objectExist(int objectX, int objectY) {
        for (GameObject o : objects) {
            if (o.objectX == objectX && o.objectY == objectY) {
                return true;
            }
        }
        return false;
    }

    public void process() {
        for (GameObject o : objects) {
            if (o.tick > 0)
                o.tick--;
            else {
                updateObject(o);
                toRemove.add(o);
            }
        }
        for (GameObject o : toRemove) {
            if (isObelisk(o.newId)) {
                int index = getObeliskIndex(o.newId);
                if (activated[index]) {
                    activated[index] = false;
                    teleportObelisk(index);
                }
            }
            objects.remove(o);
        }
        toRemove.clear();
    }

    public void removeObject(int x, int y) {
        for (int j = 0; j < World.players.length; j++) {
            if (World.players[j] != null) {
                Player c = World.players[j];
                c.getPA().object(-1, x, y, 0, 10);
            }
        }
    }

    public void updateObject(GameObject o) {
        for (int j = 0; j < World.players.length; j++) {
            if (World.players[j] != null) {
                Player c = World.players[j];
                c.getPA().object(o.newId, o.objectX, o.objectY, o.face, o.type);
            }
        }
    }

    public void placeObject(GameObject o) {
        for (int j = 0; j < World.players.length; j++) {
            if (World.players[j] != null) {
                Player c = World.players[j];
                if (c.distanceToPoint(o.objectX, o.objectY) <= 60)
                    c.getPA().object(o.objectId, o.objectX, o.objectY, o.face, o.type);
            }
        }
    }

    public GameObject getObject(int x, int y, int height) {
        for (GameObject o : objects) {
            if (o.objectX == x && o.objectY == y && o.height == height)
                return o;
        }
        return null;
    }

    public void loadObjects(Player c) {
        if (c == null)
            return;
        for (GameObject o : objects) {
            if (loadForPlayer(o, c))
                c.getPA().object(o.objectId, o.objectX, o.objectY, o.face, o.type);
        }
        loadCustomSpawns(c);
    }

    public void loadCustomSpawns(Player client) {
//        client.getPA().checkObjectSpawn(409, 3085, 3508, 0, 1, 10); // prayer altar
//        client.getPA().checkObjectSpawn(6552, 3085, 3510, 0, 1, 10); // ancients altar
//        client.getPA().checkObjectSpawn(-1, 3090, 3503, 0, 1, 10); // tree
//        client.getPA().checkObjectSpawn(-1, 3088, 3509, 0, 1, 10);
    }

    public final int IN_USE_ID = 14825;

    public boolean isObelisk(int id) {
        for (int j = 0; j < obeliskIds.length; j++) {
            if (obeliskIds[j] == id)
                return true;
        }
        return false;
    }

    public int[] obeliskIds = { 14829, 14830, 14827, 14828, 14826, 14831 };
    public int[][] obeliskCoords = { { 3154, 3618 }, { 3225, 3665 }, { 3033, 3730 }, { 3104, 3792 }, { 2978, 3864 },
        { 3305, 3914 } };
    public boolean[] activated = { false, false, false, false, false, false };

    public void startObelisk(int obeliskId) {
        int index = getObeliskIndex(obeliskId);
        if (index >= 0) {
            if (!activated[index]) {
                activated[index] = true;
                addObject(new GameObject(14825, obeliskCoords[index][0], obeliskCoords[index][1], 0, -1, 10, obeliskId, 16));
                addObject(new GameObject(14825, obeliskCoords[index][0] + 4, obeliskCoords[index][1], 0, -1, 10, obeliskId, 16));
                addObject(new GameObject(14825, obeliskCoords[index][0], obeliskCoords[index][1] + 4, 0, -1, 10, obeliskId, 16));
                addObject(
                    new GameObject(14825, obeliskCoords[index][0] + 4, obeliskCoords[index][1] + 4, 0, -1, 10, obeliskId, 16));
            }
        }
    }

    public int getObeliskIndex(int id) {
        for (int j = 0; j < obeliskIds.length; j++) {
            if (obeliskIds[j] == id)
                return j;
        }
        return -1;
    }

    public void teleportObelisk(int port) {
        int random = Misc.random(5);
        while (random == port) {
            random = Misc.random(5);
        }
        for (int j = 0; j < World.players.length; j++) {
            if (World.players[j] != null) {
                Player c = World.players[j];
                int xOffset = c.absX - obeliskCoords[port][0];
                int yOffset = c.absY - obeliskCoords[port][1];
                if (c.goodDistance(c.getX(), c.getY(), obeliskCoords[port][0] + 2, obeliskCoords[port][1] + 2, 1)) {
                    TeleportExecutor.teleport(c, new Teleport(new Position(obeliskCoords[random][0] + xOffset, obeliskCoords[random][1] + yOffset, 0), TeleportType.OBELISK), false);
                }
            }
        }
    }

    public boolean loadForPlayer(GameObject o, Player c) {
        if (o == null || c == null)
            return false;
        return c.distanceToPoint(o.objectX, o.objectY) <= 60 && c.heightLevel == o.height;
    }

    public void addObject(GameObject o) {
        if (getObject(o.objectX, o.objectY, o.height) == null) {
            objects.add(o);
            placeObject(o);
        }
    }

}