package com.oldschoolpk.server.sql;

import java.util.List;

import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.data.Row;

import com.oldschoolpk.server.model.content.QuestTab;
import com.oldschoolpk.server.model.player.DonatorRights;
import com.oldschoolpk.server.model.player.Player;

public class DonationSQL {

	private static final String SELECT_QUERY = "SELECT * FROM `donations` WHERE `username` = :user AND `fulfilled` = 0";
	private static final String UPDATE_QUERY = "UPDATE `donations` SET `fulfilled` = 1 WHERE `transaction_id` = :id";

	public static void claimDonationRewards(Player client) {
		try (Connection con = Database.getDonationsDatabase().open()) {
			/*
			 * Construct and execute query.
			 */
			Query selectQuery = con.createQuery(SELECT_QUERY);
			selectQuery.addParameter("user", client.playerName);
			List<Row> results = selectQuery.executeAndFetchTable().rows();

			/*
			 * Return if there are no pending donations.
			 */
			if (results.isEmpty()) {
				client.sendMessage("There are no pending donations available.");
				return;
			}

			/*
			 * Give donation rewards.
			 */
			double totalTokens = 0;
			double totalPrice = 0;
			for (Row row : results) {
				totalTokens += row.getDouble("tokens");
				totalPrice += row.getDouble("price");
			}
			client.donationPoints += totalTokens;
			client.donationAmount += totalPrice;
			QuestTab.updateInterface(client);

			if (client.donationAmount >= 100 && client.getDonatorRights().isInferior(DonatorRights.EXTREME_PREMIUM)) {
				client.setDonatorRights(DonatorRights.EXTREME_PREMIUM);
				client.sendMessage("Re-log to receive your Extreme Premium status!");
			} else if (client.donationAmount >= 50 && client.getDonatorRights().isInferior(DonatorRights.SUPER_PREMIUM)) {
				client.setDonatorRights(DonatorRights.SUPER_PREMIUM);
				client.sendMessage("Re-log to receive your Super Premium status!");
			} else if (client.donationAmount >= 10 && client.getDonatorRights().isInferior(DonatorRights.PREMIUM)) {
				client.setDonatorRights(DonatorRights.PREMIUM);
				client.sendMessage("Re-log to receive your Premium status!");
			}

			client.sendMessage("Thank you for your generosity!");

			/*
			 * Update donation status.
			 */
			for (Row row : results) {
				Query updateQuery = con.createQuery(UPDATE_QUERY);
				updateQuery.addParameter("id", row.getLong("transaction_id"));
				updateQuery.executeUpdate();
			}
		}
	}

}