package com.oldschoolpk.server.sql;

import java.util.List;

import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.data.Row;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.content.QuestTab;
import com.oldschoolpk.server.model.player.Player;

public class VoteSQL {

	private static final String SELECT_QUERY = "SELECT * FROM  `mv_votes` WHERE `user` = :user AND `fulfilled` = 0 AND `ready` = 1";
	private static final String UPDATE_QUERY = "UPDATE `mv_votes` SET `fulfilled` = 1 WHERE `user` = :user AND `ready` = 1";
	
	public static void claimVoteRewards(Player client) {
		try(Connection con = Database.getVoteDatabase().open()) {
			/*
			 * Construct and execute query.
			 */
			Query selectQuery = con.createQuery(SELECT_QUERY);
			selectQuery.addParameter("user", client.playerName);
			List<Row> results = selectQuery.executeAndFetchTable().rows();
			
			/*
			 * Return if there are no pending votes.
			 */
			if (results.isEmpty()) {
				client.sendMessage("There are no pending votes available.");
				return;
			}
			
			/*
			 * Give vote rewards.
			 */
			int voteAmount = results.size();
			int coinsAmount = voteAmount * 150_000;
			if (!client.getItems().addItem(995, coinsAmount)) {
				Server.itemHandler.createGroundItem(client, 995, client.getX(), client.getY(), coinsAmount, client.playerId);
				client.sendMessage("You have no space to carry your reward. The reward has been dropped on the ground!");
			}
			client.votePoints += voteAmount;
			QuestTab.updateInterface(client);
			client.sendMessage("Thank you for voting!");
			
			/*
			 * Update votes status. 
			 */
			Query updateQuery = con.createQuery(UPDATE_QUERY);
			updateQuery.addParameter("user", client.playerName);
			updateQuery.executeUpdate();
		}
	}
	
}
