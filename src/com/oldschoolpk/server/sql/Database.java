package com.oldschoolpk.server.sql;

import org.sql2o.Sql2o;

public class Database {

	private static final Sql2o voteDatabase = new Sql2o("jdbc:mysql://localhost:3306/game_server", "root", "");
	private static final Sql2o donationsDb = new Sql2o("jdbc:mysql://localhost:3306/game_server", "root", "");
	
	public static Sql2o getVoteDatabase() {
		return voteDatabase;
	}

	public static Sql2o getDonationsDatabase() {
		return donationsDb;
	}
	
}