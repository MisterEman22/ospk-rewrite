package com.oldschoolpk.server.model.item.equipment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Logger;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.core.tools.XStreamUtil;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerConstants;

public class EquipmentBonuses {

	public static void resetPlayerBonuses(Player client) {
		for (int index = 0; index < client.playerBonus.length; index++) {
			client.playerBonus[index] = 0;
		}
	}

	public static void getItemBonuses(Player client) {
		for (int index = 0; index < client.getEquipment().length; index++) {
			if (client.getEquipment()[index] > 0) {
				if (EquipmentBonuses.forId(client.getEquipment()[index]) == null) {
					continue;
				}
				if (client.getEquipment()[index] == EquipmentBonuses.forId(
						client.getEquipment()[index]).getId()) {
					client.playerBonus[PlayerConstants.STAB_ATTACK] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getStabAttack();
					client.playerBonus[PlayerConstants.SLASH_ATTACK] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getSlashAttack();
					client.playerBonus[PlayerConstants.CRUSH_ATTACK] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getCrushAttack();
					client.playerBonus[PlayerConstants.MAGIC_ATTACK] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getMagicAttack();
					client.playerBonus[PlayerConstants.RANGE_ATTACK] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getRangeAttack();
					client.playerBonus[PlayerConstants.STAB_DEFENSIVE] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getStabDefence();
					client.playerBonus[PlayerConstants.SLASH_DEFENSIVE] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getSlashDefence();
					client.playerBonus[PlayerConstants.CRUSH_DEFENSIVE] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getCrushDefence();
					client.playerBonus[PlayerConstants.MAGIC_DEFENSIVE] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getMagicDefence();
					client.playerBonus[PlayerConstants.RANGE_DEFENSIVE] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getRangeDefence();
					client.playerBonus[PlayerConstants.MELEE_STRENGTH] += EquipmentBonuses
							.forId(client.getEquipment()[index]).getStrength();
					client.playerBonus[PlayerConstants.RANGE_STRENGTH] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getRangeStrength();
					client.playerBonus[PlayerConstants.MAGIC_DAMAGE_BONUS] += EquipmentBonuses
							.forId(client.getEquipment()[index])
							.getMagicStrength();
					client.playerBonus[PlayerConstants.PRAYER_BONUS] += EquipmentBonuses
							.forId(client.getEquipment()[index]).getPrayer();
				}
			}
		}
	}

	public static final String[] BONUS_NAMES = { "Stab", "Slash", "Crush",
			"Magic", "Range", "Stab", "Slash", "Crush", "Magic", "Range",
			"Strength", "Range Strength", "Magic Damage", "Prayer" };

	public static void writeItemBonuses(Player client) {
		int offset = 0;
		String send = "";
		for (int i = 0; i < client.playerBonus.length; i++) {
			if (client.playerBonus[i] >= 0) {
				send = BONUS_NAMES[i] + ": +" + client.playerBonus[i];
			} else {
				send = BONUS_NAMES[i] + ": -"
						+ java.lang.Math.abs(client.playerBonus[i]);
			}

			if (i == 10) {
				offset = 1;
			}
			client.getPA().sendFrame126(send, (1675 + i + offset));
		}
	}

	/**
	 * Java logger
	 */
	private static final Logger logger = Logger
			.getLogger(EquipmentBonuses.class.getName());

	private static EquipmentBonuses[] def = null;

	public static EquipmentBonuses forId(int id) {
		return def[id];
	}

	public static void init() throws FileNotFoundException {
		logger.info("Loading item bonuses...");
		List<EquipmentBonuses> bonusList = (List<EquipmentBonuses>) XStreamUtil
				.getxStream()
				.fromXML(new FileInputStream("./data/bonuses.xml"));
		def = new EquipmentBonuses[Config.ITEM_LIMIT];
		for (EquipmentBonuses defs : bonusList) {
			def[defs.getId()] = defs;
		}
		logger.info(def.length + " item bonuses has been unpacked...");
	}

	private int id;
	private int stabAttack;
	private int slashAttack;
	private int crushAttack;
	private int magicAttack;
	private int rangeAttack;

	private int stabDefence;
	private int slashDefence;
	private int crushDefence;
	private int magicDefence;
	private int rangeDefence;

	private int strength;
	private int rangeStrength;
	private int magicStrength;
	private int prayer;

	public int getId() {
		return id;
	}

	public int getStabAttack() {
		return stabAttack;
	}

	public int getSlashAttack() {
		return slashAttack;
	}

	public int getCrushAttack() {
		return crushAttack;
	}

	public int getMagicAttack() {
		return magicAttack;
	}

	public int getRangeAttack() {
		return rangeAttack;
	}

	public int getStabDefence() {
		return stabDefence;
	}

	public int getSlashDefence() {
		return slashDefence;
	}

	public int getCrushDefence() {
		return crushDefence;
	}

	public int getMagicDefence() {
		return magicDefence;
	}

	public int getRangeDefence() {
		return rangeDefence;
	}

	public int getStrength() {
		return strength;
	}

	public int getRangeStrength() {
		return rangeStrength;
	}

	public int getMagicStrength() {
		return magicStrength;
	}

	public int getPrayer() {
		return prayer;
	}
}
