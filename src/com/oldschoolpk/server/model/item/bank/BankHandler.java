package com.oldschoolpk.server.model.item.bank;

import java.util.Arrays;
import java.util.Iterator;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.item.ItemAssistant;
import com.oldschoolpk.server.model.item.ItemDefinitions;
import com.oldschoolpk.server.model.item.Items;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerConstants;

public class BankHandler {

	Player client;

	public BankHandler(Player client) {
		this.client = client;
	}

	/**
	 * Sends an item to the bank in any tab possible.
	 * 
	 * @param itemId
	 *            the item id
	 * @param amount
	 *            the item amount
	 */
	public void sendItemToAnyTab(int itemId, int amount) {
		BankItem item = new BankItem(itemId, amount);
		for (BankTab tab : client.getBank().getBankTab()) {
			if (tab.freeSlots() > 0 || tab.contains(item)) {
				client.getBank().setCurrentBankTab(tab);
				addItemToBank(itemId, amount);
				return;
			}
		}
		addItemToBank(itemId, amount);
	}

	/**
	 * Adds an item to the players inventory, bank, or drops it. It will do this
	 * under any circumstance so if it cannot be added to the inventory it will
	 * next try to send it to the bank and if it cannot, it will drop it.
	 * 
	 * @param itemId
	 *            the item
	 * @param amount
	 *            the amount of said item
	 */
	public void addItemUnderAnyCircumstance(int itemId, int amount) {
		if (!client.getItems().addItem(itemId, amount)) {
			sendItemToAnyTabOrDrop(new BankItem(itemId, amount), client.getX(), client.getY());
		}
	}

	/**
	 * The x and y represents the possible x and y location of the dropped item
	 * if in fact it cannot be added to the bank.
	 * 
	 * @param item
	 * @param x
	 * @param y
	 */
	public void sendItemToAnyTabOrDrop(BankItem item, int x, int y) {
		item = new BankItem(item.getId() + 1, item.getAmount());
		if (bankContains(item.getId() - 2)) {
			if (isBankSpaceAvailable(item)) {
				sendItemToAnyTab(item.getId() - 1, item.getAmount());
			} else {
				Server.itemHandler.createGroundItem(client, item.getId() - 1, x, y, client.heightLevel, item.getAmount(), client.playerId);
			}
		} else {
			sendItemToAnyTab(item.getId() - 1, item.getAmount());
		}
	}

	public boolean isNotable(int itemId) {
		return ItemDefinitions.forId(itemId) != null && ItemDefinitions.forId(itemId).isNoteable() || Items.itemIsNote[itemId];
	}

	public void addItemToBank(int itemId, int amount) {
		BankTab tab = client.getBank().getCurrentBankTab();
		BankItem item = new BankItem(itemId + 1, amount);
		Iterator<BankTab> iterator = Arrays.asList(client.getBank().getBankTab()).iterator();
		while (iterator.hasNext()) {
			BankTab t = iterator.next();
			if (t != null && t.size() > 0) {
				Iterator<BankItem> iterator2 = t.getItems().iterator();
				while (iterator2.hasNext()) {
					BankItem i = iterator2.next();
					if (i.getId() == item.getId() && !isNotable(itemId)) {
						if (t.getTabId() != tab.getTabId()) {
							tab = t;
						}
					} else {
						if (isNotable(itemId) && i.getId() == item.getId() - 1) {
							item = new BankItem(itemId, amount);
							if (t.getTabId() != tab.getTabId()) {
								tab = t;
							}
						}
					}
				}
			}
		}
		if (isNotable(itemId))
			item = new BankItem(itemId, amount);
		if (tab.freeSlots() == 0) {
			client.sendMessage("The item has been dropped on the floor.");
			Server.itemHandler.createGroundItem(client, itemId, client.absX, client.absY, client.heightLevel, amount, client.playerId);
			return;
		}
		long totalAmount = ((long) tab.getItemAmount(item) + (long) item.getAmount());
		if (totalAmount >= Integer.MAX_VALUE) {
			client.sendMessage("The item has been dropped on the floor.");
			Server.itemHandler.createGroundItem(client, itemId, client.absX, client.absY, client.heightLevel, amount, client.playerId);
			return;
		}
		tab.add(item);
		client.getItems().resetTempItems();
		if (client.isBanking)
			client.getItems().resetBank();
		client.sendMessage(client.getItems().getItemName(itemId) + " x" + item.getAmount() + " has been added to your bank.");
	}

	/**
	 * Banking your item.
	 * 
	 * @param itemID
	 * @param fromSlot
	 * @param amount
	 * @return
	 */

	public boolean addToBank(int itemID, int amount, boolean updateView) {

		if (client.getAttribute("viewing_other_bank", null) != null) {
			return false;
		}
		if (!client.isBanking)
			return false;
		if (!client.getItems().playerHasItem(itemID))
			return false;
		if (client.getBank().getBankSearch().isSearching()) {
			client.getBank().getBankSearch().reset();
			return false;
		}
		if (client.getBankPin().requiresUnlock()) {
			client.getItems().resetBank();
			client.getBankPin().open(2);
			return false;
		}
		BankTab tab = client.getBank().getCurrentBankTab();
		BankItem item = new BankItem(itemID + 1, amount);
		Iterator<BankTab> iterator = Arrays.asList(client.getBank().getBankTab()).iterator();
		while (iterator.hasNext()) {
			BankTab t = iterator.next();
			if (t != null && t.size() > 0) {
				Iterator<BankItem> iterator2 = t.getItems().iterator();
				while (iterator2.hasNext()) {
					BankItem i = iterator2.next();
					if (i.getId() == item.getId() && !isNotable(itemID)) {
						if (t.getTabId() != tab.getTabId()) {
							tab = t;
							// client.getBank().setCurrentBankTab(tab);
							// resetBank();
						}
					} else {
						if (isNotable(itemID) && i.getId() == item.getId() - 1) {
							item = new BankItem(itemID, amount);
							if (t.getTabId() != tab.getTabId()) {
								tab = t;
								// client.getBank().setCurrentBankTab(tab);
								// resetBank();
							}
						}
					}
				}
			}
		}
		if (isNotable(itemID)) {
			item = new BankItem(itemID, amount);
		}
		if (item.getAmount() > client.getItems().getItemAmount(itemID))
			item.setAmount(client.getItems().getItemAmount(itemID));
		if (tab.getItemAmount(item) == Integer.MAX_VALUE) {
			client.sendMessage("Your bank is already holding the maximum amount of " + client.getItems().getItemName(itemID).toLowerCase() + " possible.");
			return false;
		}
		if (tab.freeSlots() == 0 && !tab.contains(item)) {
			client.sendMessage("Your current bank tab is full.");
			return false;
		}
		long totalAmount = ((long) tab.getItemAmount(item) + (long) item.getAmount());
		if (totalAmount >= Integer.MAX_VALUE) {
			int difference = Integer.MAX_VALUE - tab.getItemAmount(item);
			item.setAmount(difference);
			client.getItems().deleteItem2(itemID, difference);
		} else {
			client.getItems().deleteItem2(itemID, item.getAmount());
		}
		tab.add(item);
		if (updateView) {
			client.getItems().resetTempItems();
			client.getItems().resetBank();
		}
		return true;
	}

	public boolean bankContains(int itemId) {
		for (BankTab tab : client.getBank().getBankTab())
			if (tab.contains(new BankItem(itemId + 1)))
				return true;
		return false;
	}

	public boolean bankContains(int itemId, int itemAmount) {
		for (BankTab tab : client.getBank().getBankTab()) {
			if (tab.containsAmount(new BankItem(itemId + 1, itemAmount))) {
				return true;
			}
		}
		return false;
	}

	public boolean isBankSpaceAvailable(BankItem item) {
		for (BankTab tab : client.getBank().getBankTab()) {
			if (tab.contains(item)) {
				return tab.spaceAvailable(item);
			}
		}
		return false;
	}

	public boolean removeFromAnyTabWithoutAdding(int itemId, int itemAmount, boolean updateView) {

		BankTab tab = null;
		BankItem item = new BankItem(itemId + 1, itemAmount);
		for (BankTab searchedTab : client.getBank().getBankTab()) {
			if (searchedTab.contains(item)) {
				tab = searchedTab;
				break;
			}
		}
		if (tab == null) {
			return false;
		}
		if (itemAmount <= 0)
			return false;
		if (!tab.contains(item))
			return false;
		if (tab.getItemAmount(item) < itemAmount) {
			item.setAmount(tab.getItemAmount(item));
		}
		if (item.getAmount() < 0)
			item.setAmount(0);
		tab.remove(item);
		if (tab.size() == 0) {
			client.getBank().setCurrentBankTab(client.getBank().getBankTab(0));
		}
		if (updateView) {
			client.getItems().resetBank();
		}
		client.getItems().resetItems(5064);
		return true;
	}

	public void removeFromBank(int itemId, int itemAmount, boolean updateView) {
		BankTab tab = client.getBank().getCurrentBankTab();
		BankItem item = new BankItem(itemId + 1, itemAmount);
		boolean noted = false;
		if (!client.isBanking)
			return;
		
		if (client.getAttribute("viewing_other_bank", null) != null) {
			return;
		}
		
		if (itemAmount <= 0)
			return;
		if (client.getBankPin().requiresUnlock()) {
			client.getItems().resetBank();
			client.getBankPin().open(2);
			return;
		}
		// if (System.currentTimeMillis() - c.lastBankDeposit < 3000)
		// return;

		if (!tab.contains(item))
			return;
		if (client.takeAsNote) {
			if (client.getItems().getItemName(itemId).trim().equalsIgnoreCase(client.getItems().getItemName(itemId + 1).trim()) && isNotable(itemId + 1)) {
				noted = true;
			} else
				client.sendMessage("This item cannot be taken out as noted.");
		}
		/**
		 * Update and remove the proceeding snippet
		 */
		int[] buggedItems = {

		};
		if (client.getItems().freeSlots() == 0 && !client.getItems().playerHasItem(itemId)) {
			client.sendMessage("There is not enough space in your inventory.");
			return;
		}
		if (client.getItems().getItemAmount(itemId) == Integer.MAX_VALUE) {
			client.sendMessage("Your inventory is already holding the maximum amount of " + ItemAssistant.getItemName(itemId).toLowerCase() + " possible.");
			return;
		}
		if (isStackable(item.getId() - 1) || noted) {
			long totalAmount = (long) client.getItems().getItemAmount(itemId) + (long) itemAmount;
			if (totalAmount > Integer.MAX_VALUE)
				item.setAmount(tab.getItemAmount(item) - client.getItems().getItemAmount(itemId));
		}
		if (tab.getItemAmount(item) < itemAmount) {
			item.setAmount(tab.getItemAmount(item));
		}
		if (!isStackable(item.getId() - 1) && !noted) {
			if (client.getItems().freeSlots() < item.getAmount())
				item.setAmount(client.getItems().freeSlots());
		}
		if (item.getAmount() < 0)
			item.setAmount(0);
		if (!noted)
			client.getItems().addItem(item.getId() - 1, item.getAmount());
		else
			client.getItems().addItem(item.getId(), item.getAmount());
		tab.remove(item);
		if (tab.size() == 0) {
			client.getBank().setCurrentBankTab(client.getBank().getBankTab(0));
		}
		if (updateView) {
			client.getItems().resetBank();
		}
		client.getItems().resetItems(5064);
	}

	/**
	 * Checks if the item is stackable.
	 * 
	 * @param itemID
	 * @return
	 */
	public boolean isStackable(int itemID) {
		return Items.itemStackable[itemID];
	}

	public boolean addEquipmentToBank(int itemID, int slot, int amount, boolean updateView) {
		if (!client.isBanking)
			return false;
		if (client.playerEquipment[slot] != itemID || client.playerEquipmentN[slot] <= 0)
			return false;
		BankTab tab = client.getBank().getCurrentBankTab();
		BankItem item = new BankItem(itemID + 1, amount);
		Iterator<BankTab> iterator = Arrays.asList(client.getBank().getBankTab()).iterator();
		while (iterator.hasNext()) {
			BankTab t = iterator.next();
			if (t != null && t.size() > 0) {
				Iterator<BankItem> iterator2 = t.getItems().iterator();
				while (iterator2.hasNext()) {
					BankItem i = iterator2.next();
					if (i.getId() == item.getId() && !isNotable(itemID)) {
						if (t.getTabId() != tab.getTabId()) {
							tab = t;
							client.getBank().setCurrentBankTab(tab);
							client.getItems().resetBank();
						}
					} else {
						if (isNotable(itemID) && i.getId() == item.getId() - 1) {
							item = new BankItem(itemID, amount);
							if (t.getTabId() != tab.getTabId()) {
								tab = t;
								client.getBank().setCurrentBankTab(tab);
								client.getItems().resetBank();
							}
						}
					}
				}
			}
		}
		if (isNotable(itemID))
			item = new BankItem(itemID, amount);
		if (item.getAmount() > client.playerEquipmentN[slot])
			item.setAmount(client.playerEquipmentN[slot]);
		if (tab.getItemAmount(item) == Integer.MAX_VALUE) {
			client.sendMessage("Your bank is already holding the maximum amount of " + client.getItems().getItemName(itemID).toLowerCase() + " possible.");
			return false;
		}
		if (tab.freeSlots() == 0 && !tab.contains(item)) {
			client.sendMessage("Your current bank tab is full.");
			return false;
		}
		long totalAmount = ((long) tab.getItemAmount(item) + (long) item.getAmount());
		if (totalAmount >= Integer.MAX_VALUE) {
			client.sendMessage("Your bank is already holding the maximum amount of this item.");
			return false;
		} else
			client.playerEquipmentN[slot] -= item.getAmount();
		if (client.playerEquipmentN[slot] <= 0) {
			client.playerEquipmentN[slot] = -1;
			client.playerEquipment[slot] = -1;
		}
		tab.add(item);
		if (updateView) {
			client.getItems().resetTempItems();
			client.getItems().resetBank();
			client.getItems().updateSlot(slot);
		}
		client.setAttribute("full_body", Items.isFullBody(client.getEquipment()[PlayerConstants.PLAYER_BODY]));
		client.setAttribute("full_mask", Items.isFullMask(client.getEquipment()[PlayerConstants.PLAYER_HAT]));
		client.setAttribute("full_helm", Items.isFullMask(client.getEquipment()[PlayerConstants.PLAYER_HAT]));
		return true;
	}

	public void sendReplacementOfTempItem() {
		int itemCount = 0;
		for (int i = 0; i < client.playerItems.length; i++) {
			if (client.playerItems[i] > -1) {
				itemCount = i;
			}
		}
		client.getClient().getOutStream().createFrameVarSizeWord(53);
		client.getClient().getOutStream().writeWord(5064); // inventory
		client.getClient().getOutStream().writeWord(itemCount + 1); // number of items
		for (int i = 0; i < itemCount + 1; i++) {
			if (client.playerItemsN[i] > 254) {
				client.getClient().getOutStream().writeByte(255);
				client.getClient().getOutStream().writeDWord_v2(client.playerItemsN[i]);
			} else {
				client.getClient().getOutStream().writeByte(client.playerItemsN[i]);
			}
			if (client.playerItems[i] > Config.ITEM_LIMIT || client.playerItems[i] < 0) {
				client.playerItems[i] = Config.ITEM_LIMIT;
			}
			client.getClient().getOutStream().writeWordBigEndianA(client.playerItems[i]); // item
		}

		client.getClient().getOutStream().endFrameVarSizeWord();
		client.getClient().flushOutStream();
	}

	public void swapBankItem(int from, int to) {
		BankItem item = client.getBank().getCurrentBankTab().getItem(from);
		client.getBank().getCurrentBankTab().setItem(from, client.getBank().getCurrentBankTab().getItem(to));
		client.getBank().getCurrentBankTab().setItem(to, item);
	}

}
