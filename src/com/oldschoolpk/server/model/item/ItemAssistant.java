package com.oldschoolpk.server.model.item;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.content.RewardHandler;
import com.oldschoolpk.server.model.item.bank.BankItem;
import com.oldschoolpk.server.model.item.bank.BankTab;
import com.oldschoolpk.server.model.item.equipment.EquipmentBonuses;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerConstants;
import com.oldschoolpk.server.util.Misc;

public class ItemAssistant {

	private Player c;

	public ItemAssistant(Player client) {
		this.c = client;
	}

	/**
	 * Items
	 **/

	public int[][] brokenBarrows = { { 4708, 4860 }, { 4710, 4866 }, { 4712, 4872 }, { 4714, 4878 }, { 4716, 4884 }, { 4720, 4896 }, { 4718, 4890 }, { 4720, 4896 }, { 4722, 4902 }, { 4732, 4932 }, { 4734, 4938 }, { 4736, 4944 }, { 4738, 4950 },
			{ 4724, 4908 }, { 4726, 4914 }, { 4728, 4920 }, { 4730, 4926 }, { 4745, 4956 }, { 4747, 4926 }, { 4749, 4968 }, { 4751, 4794 }, { 4753, 4980 }, { 4755, 4986 }, { 4757, 4992 }, { 4759, 4998 } };

	public void resetItems(int WriteFrame) {
		// synchronized(c) {

		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrameVarSizeWord(53);
			c.getClient().getOutStream().writeWord(WriteFrame);
			c.getClient().getOutStream().writeWord(c.playerItems.length);
			for (int i = 0; i < c.playerItems.length; i++) {
				if (c.playerItemsN[i] > 254) {
					c.getClient().getOutStream().writeByte(255);
					c.getClient().getOutStream().writeDWord_v2(c.playerItemsN[i]);
				} else {
					c.getClient().getOutStream().writeByte(c.playerItemsN[i]);
				}
				c.getClient().getOutStream().writeWordBigEndianA(c.playerItems[i]);
			}
			c.getClient().getOutStream().endFrameVarSizeWord();
			c.getClient().flushOutStream();
		}
	}

	public int getItemCount(int itemID) {
		int count = 0;
		for (int j = 0; j < c.playerItems.length; j++) {
			if (c.playerItems[j] == itemID + 1) {
				count += c.playerItemsN[j];
			}
		}
		return count;
	}

	public int getTotalCount(int itemID) {
		int count = 0;
		for (int j = 0; j < c.playerItems.length; j++) {
			if (Items.itemIsNote[itemID + 1]) {
				if (itemID + 2 == c.playerItems[j])
					count += c.playerItemsN[j];
			}
			if (!Items.itemIsNote[itemID + 1]) {
				if (itemID + 1 == c.playerItems[j]) {
					count += c.playerItemsN[j];
				}
			}
		}

		return count;
	}

	public long getTotalWealth() {
		long wealth = 0;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] > 0) {
				wealth += c.getShops().getItemShopValue(c.playerItems[i] - 1) * c.playerItemsN[i];
			}
		}
		for (int i = 0; i < c.playerEquipment.length; i++) {
			if (c.playerEquipment[i] > 0) {
				wealth += c.getShops().getItemShopValue(c.playerEquipment[i]) * c.playerEquipmentN[i];
			}
		}
		for (int i = 0; i < c.getBank().getBankTab().length; i++) {
			BankTab tab = c.getBank().getBankTab(i);
			for (BankItem item : tab.getItems()) {
				if (item != null && item.getId() > 0) {
					wealth += c.getShops().getItemShopValue(item.getId() - 1) * item.getAmount();
				}
			}
		}
		return wealth;
	}

	public void sendItemsKept() {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrameVarSizeWord(53);
			c.getClient().getOutStream().writeWord(6963);
			c.getClient().getOutStream().writeWord(c.itemKeptId.length);
			for (int i = 0; i < c.itemKeptId.length; i++) {
				if (c.playerItemsN[i] > 254) {
					c.getClient().getOutStream().writeByte(255);
					c.getClient().getOutStream().writeDWord_v2(1);
				} else {
					c.getClient().getOutStream().writeByte(1);
				}
				if (c.itemKeptId[i] > 0) {
					c.getClient().getOutStream().writeWordBigEndianA(c.itemKeptId[i] + 1);
				} else {
					c.getClient().getOutStream().writeWordBigEndianA(0);
				}
			}
			c.getClient().getOutStream().endFrameVarSizeWord();
			c.getClient().flushOutStream();
		}
	}

	/**
	 * Item kept on death
	 **/

	public void keepItem(int keepItem, boolean deleteItem) {
		int value = 0;
		int item = 0;
		int slotId = 0;
		boolean itemInInventory = false;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] - 1 > 0) {
				int inventoryItemValue = c.getShops().getItemShopValue(c.playerItems[i] - 1);
				if (inventoryItemValue > value && (!c.invSlot[i])) {
					value = inventoryItemValue;
					item = c.playerItems[i] - 1;
					slotId = i;
					itemInInventory = true;
				}
			}
		}
		for (int i1 = 0; i1 < c.getEquipment().length; i1++) {
			if (c.getEquipment()[i1] > 0) {
				int equipmentItemValue = c.getShops().getItemShopValue(c.getEquipment()[i1]);
				if (equipmentItemValue > value && (!c.equipSlot[i1])) {
					value = equipmentItemValue;
					item = c.getEquipment()[i1];
					slotId = i1;
					itemInInventory = false;
				}
			}
		}
		if (itemInInventory) {
			c.invSlot[slotId] = true;
			if (deleteItem) {
				deleteItem(c.playerItems[slotId] - 1, getItemSlot(c.playerItems[slotId] - 1), 1);
			}
		} else {
			c.equipSlot[slotId] = true;
			if (deleteItem) {
				deleteEquipment(item, slotId);
			}
		}
		c.itemKeptId[keepItem] = item;
	}

	/**
	 * Reset items kept on death
	 **/

	public void resetKeepItems() {
		for (int i = 0; i < c.itemKeptId.length; i++) {
			c.itemKeptId[i] = -1;
		}
		for (int i1 = 0; i1 < c.invSlot.length; i1++) {
			c.invSlot[i1] = false;
		}
		for (int i2 = 0; i2 < c.equipSlot.length; i2++) {
			c.equipSlot[i2] = false;
		}
	}

	/**
	 * delete all items
	 **/

	public void deleteAllItems() {
		for (int i1 = 0; i1 < c.getEquipment().length; i1++) {
			deleteEquipment(c.getEquipment()[i1], i1);
		}
		for (int i = 0; i < c.playerItems.length; i++) {
			deleteItem(c.playerItems[i] - 1, getItemSlot(c.playerItems[i] - 1), c.playerItemsN[i]);
		}
	}

	/**
	 * Drop all items for your killer
	 **/

	public void dropAllItems() {
		Player o = World.players[c.killerId];
		int totalWealth = 0;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (o != null) {
				if (isTradeable(c.playerItems[i] - 1)) {
					totalWealth += c.getShops().getItemShopValue(c.playerItems[i] - 1);
					Server.itemHandler.createGroundItem(o, c.playerItems[i] - 1, c.getX(), c.getY(), c.playerItemsN[i], c.killerId);
				} else {
					if (specialCase(c.playerItems[i] - 1))
						Server.itemHandler.createGroundItem(o, 995, c.getX(), c.getY(), getUntradePrice(c.playerItems[i] - 1), c.killerId);
					Server.itemHandler.createGroundItem(c, c.playerItems[i] - 1, c.getX(), c.getY(), c.playerItemsN[i], c.playerId);
				}
			} else {
				Server.itemHandler.createGroundItem(c, c.playerItems[i] - 1, c.getX(), c.getY(), c.playerItemsN[i], c.playerId);
			}
		}
		for (int e = 0; e < c.getEquipment().length; e++) {
			if (o != null) {
				if (isTradeable(c.getEquipment()[e])) {
					totalWealth += c.getShops().getItemShopValue(c.getEquipment()[e]);
					Server.itemHandler.createGroundItem(o, c.getEquipment()[e], c.getX(), c.getY(), c.playerEquipmentN[e], c.killerId);
				} else {
					if (specialCase(c.getEquipment()[e]))
						Server.itemHandler.createGroundItem(o, 995, c.getX(), c.getY(), getUntradePrice(c.getEquipment()[e]), c.killerId);
					Server.itemHandler.createGroundItem(c, c.getEquipment()[e], c.getX(), c.getY(), c.playerEquipmentN[e], c.playerId);
				}
			} else {
				Server.itemHandler.createGroundItem(c, c.getEquipment()[e], c.getX(), c.getY(), c.playerEquipmentN[e], c.playerId);
			}
		}
		if (o != null) {
			System.out.println("total wealth: " + totalWealth);
			RewardHandler.giveWealth(o, totalWealth, c);
			Server.itemHandler.createGroundItem(o, 526, c.getX(), c.getY(), 1, c.killerId);
		}
	}

	public int getUntradePrice(int item) {
		switch (item) {
		case 2518:
		case 2524:
		case 2526:
			return 100000;
		case 2520:
		case 2522:
			return 150000;
		}
		return 0;
	}

	public boolean specialCase(int itemId) {
		switch (itemId) {
		case 2518:
		case 2520:
		case 2522:
		case 2524:
		case 2526:
			return true;
		}
		return false;
	}

	public void handleSpecialPickup(int itemId) {
		// c.sendMessage("My " + getItemName(itemId) +
		// " has been recovered. I should talk to the void knights to get it
		// back.");
		// c.getItems().addToVoidList(itemId);
	}

	public void addToVoidList(int itemId) {
		switch (itemId) {
		case 2518:
			c.voidStatus[0]++;
			break;
		case 2520:
			c.voidStatus[1]++;
			break;
		case 2522:
			c.voidStatus[2]++;
			break;
		case 2524:
			c.voidStatus[3]++;
			break;
		case 2526:
			c.voidStatus[4]++;
			break;
		}
	}

	public boolean isTradeable(int itemId) {
		for (int j = 0; j < Config.ITEM_TRADEABLE.length; j++) {
			if (itemId == Config.ITEM_TRADEABLE[j]) {

				return false;
			}
		}
		return true;
	}

	/**
	 * Add Item
	 **/
	public boolean addItem(int item, int amount) {
		// synchronized(c) {
		if (amount < 1) {
			amount = 1;
		}
		if (item <= 0) {
			return false;
		}
		if ((((freeSlots() >= 1) || playerHasItem(item, 1)) && Items.itemStackable[item]) || ((freeSlots() > 0) && !Items.itemStackable[item])) {
			for (int i = 0; i < c.playerItems.length; i++) {
				if ((c.playerItems[i] == (item + 1)) && Items.itemStackable[item] && (c.playerItems[i] > 0)) {
					c.playerItems[i] = (item + 1);
					if (((c.playerItemsN[i] + amount) < Config.MAXITEM_AMOUNT) && ((c.playerItemsN[i] + amount) > -1)) {
						c.playerItemsN[i] += amount;
					} else {
						c.playerItemsN[i] = Config.MAXITEM_AMOUNT;
					}
					if (c.getClient().getOutStream() != null && c != null) {
						c.getClient().getOutStream().createFrameVarSizeWord(34);
						c.getClient().getOutStream().writeWord(3214);
						c.getClient().getOutStream().writeByte(i);
						c.getClient().getOutStream().writeWord(c.playerItems[i]);
						if (c.playerItemsN[i] > 254) {
							c.getClient().getOutStream().writeByte(255);
							c.getClient().getOutStream().writeDWord(c.playerItemsN[i]);
						} else {
							c.getClient().getOutStream().writeByte(c.playerItemsN[i]);
						}
						c.getClient().getOutStream().endFrameVarSizeWord();
						c.getClient().flushOutStream();
					}
					i = 30;
					resetItems(3214);
					return true;
				}
			}
			for (int i = 0; i < c.playerItems.length; i++) {
				if (c.playerItems[i] <= 0) {
					c.playerItems[i] = item + 1;
					if ((amount < Config.MAXITEM_AMOUNT) && (amount > -1)) {
						c.playerItemsN[i] = 1;
						if (amount > 1) {
							c.getItems().addItem(item, amount - 1);
							return true;
						}
					} else {
						c.playerItemsN[i] = Config.MAXITEM_AMOUNT;
					}
					i = 30;
					resetItems(3214);
					return true;
				}
			}
			return false;
		} else {
			c.sendMessage("Not enough space in your inventory.");
			resetItems(3214);
			return false;
		}
	}

	public boolean addItem(int item, int itemSlot, int amount) {
		// synchronized(c) {
		if (amount < 1) {
			amount = 1;
		}
		if (item <= 0) {
			return false;
		}
		if ((((freeSlots() >= 1) || playerHasItem(item, 1)) && ItemDefinitions.forId(item).isStackable()) || ((freeSlots() > 0) && !ItemDefinitions.forId(item).isStackable())) {
			if ((c.playerItems[itemSlot] == (item + 1)) && ItemDefinitions.forId(item).isStackable() && (c.playerItems[itemSlot] > 0)) {
				c.playerItems[itemSlot] = (item + 1);
				if (((c.playerItemsN[itemSlot] + amount) < Config.MAXITEM_AMOUNT) && ((c.playerItemsN[itemSlot] + amount) > -1)) {
					c.playerItemsN[itemSlot] += amount;
				} else {
					c.playerItemsN[itemSlot] = Config.MAXITEM_AMOUNT;
				}
				if (c.getClient().getOutStream() != null && c != null) {
					c.getClient().getOutStream().createFrameVarSizeWord(34);
					c.getClient().getOutStream().writeWord(3214);
					c.getClient().getOutStream().writeByte(itemSlot);
					c.getClient().getOutStream().writeWord(c.playerItems[itemSlot]);
					if (c.playerItemsN[itemSlot] > 254) {
						c.getClient().getOutStream().writeByte(255);
						c.getClient().getOutStream().writeDWord(c.playerItemsN[itemSlot]);
					} else {
						c.getClient().getOutStream().writeByte(c.playerItemsN[itemSlot]);
					}
					c.getClient().getOutStream().endFrameVarSizeWord();
					c.getClient().flushOutStream();
				}
				resetItems(3214);
				return true;
			}
			if (c.playerItems[itemSlot] <= 0) {
				c.playerItems[itemSlot] = item + 1;
				if ((amount < Config.MAXITEM_AMOUNT) && (amount > -1)) {
					c.playerItemsN[itemSlot] = 1;
					if (amount > 1) {
						c.getItems().addItem(item, itemSlot, amount - 1);
						resetItems(3214);
						return true;
					}
				} else {
					c.playerItemsN[itemSlot] = Config.MAXITEM_AMOUNT;
				}
				resetItems(3214);
				return true;
			}
			return false;
		} else {
			c.sendMessage("Not enough space in your inventory.");
			resetItems(3214);
			return false;
		}
	}

	public String itemType(int item) {
		for (int i = 0; i < Items.capes.length; i++) {
			if (item == Items.capes[i])
				return "cape";
		}
		for (int i = 0; i < Items.hats.length; i++) {
			if (item == Items.hats[i])
				return "hat";
		}
		for (int i = 0; i < Items.boots.length; i++) {
			if (item == Items.boots[i])
				return "boots";
		}
		for (int i = 0; i < Items.gloves.length; i++) {
			if (item == Items.gloves[i])
				return "gloves";
		}
		for (int i = 0; i < Items.shields.length; i++) {
			if (item == Items.shields[i])
				return "shield";
		}
		for (int i = 0; i < Items.amulets.length; i++) {
			if (item == Items.amulets[i])
				return "amulet";
		}
		for (int i = 0; i < Items.arrows.length; i++) {
			if (item == Items.arrows[i])
				return "arrows";
		}
		for (int i = 0; i < Items.rings.length; i++) {
			if (item == Items.rings[i])
				return "ring";
		}
		for (int i = 0; i < Items.body.length; i++) {
			if (item == Items.body[i])
				return "body";
		}
		for (int i = 0; i < Items.legs.length; i++) {
			if (item == Items.legs[i])
				return "legs";
		}
		return "weapon";
	}

	/**
	 * Wear Item
	 **/

	public void sendWeapon(int Weapon, String WeaponName) {
		String WeaponName2 = WeaponName.replaceAll("Bronze", "");
		WeaponName2 = WeaponName2.replaceAll("Iron", "");
		WeaponName2 = WeaponName2.replaceAll("Steel", "");
		WeaponName2 = WeaponName2.replaceAll("Black", "");
		WeaponName2 = WeaponName2.replaceAll("Mithril", "");
		WeaponName2 = WeaponName2.replaceAll("Adamant", "");
		WeaponName2 = WeaponName2.replaceAll("Rune", "");
		WeaponName2 = WeaponName2.replaceAll("Granite", "");
		WeaponName2 = WeaponName2.replaceAll("Dragon", "");
		WeaponName2 = WeaponName2.replaceAll("Drag", "");
		WeaponName2 = WeaponName2.replaceAll("Crystal", "");
		WeaponName2 = WeaponName2.trim();
		if (WeaponName.equals("Unarmed")) {
			c.getClient().setSidebarInterface(0, 5855); // punch, kick, block
			c.getPA().sendFrame126(WeaponName, 5857);
		} else if (WeaponName2.startsWith("halberd") || WeaponName2.endsWith("of the dead")) {
			c.getClient().setSidebarInterface(0, 8460); // jab, swipe, fend
			c.getPA().sendFrame246(8461, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 8463);
		} else if (WeaponName.endsWith("whip") || WeaponName.endsWith("tentacle")) {
			c.getClient().setSidebarInterface(0, 12290); // flick, lash, deflect
			c.getPA().sendFrame246(12291, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 12293);
		} else if (WeaponName.endsWith("bow") || WeaponName.endsWith("10") || WeaponName.endsWith("full") || WeaponName.startsWith("seercull")) {
			c.getClient().setSidebarInterface(0, 1764); // accurate, rapid,
														// longrange
			c.getPA().sendFrame246(1765, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 1767);
		} else if (WeaponName.startsWith("Staff") || WeaponName.endsWith("staff") || WeaponName.endsWith("wand")) {
			c.getClient().setSidebarInterface(0, 328); // spike, impale, smash,
														// block
			c.getPA().sendFrame246(329, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 331);
		} else if (WeaponName2.startsWith("dart") || WeaponName2.startsWith("knife") || WeaponName2.startsWith("javelin") || WeaponName.equalsIgnoreCase("toktz-xil-ul") || WeaponName.contains("blowpipe")) {
			c.getClient().setSidebarInterface(0, 4446); // accurate, rapid,
														// longrange
			c.getPA().sendFrame246(4447, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 4449);
		} else if (WeaponName2.contains("dagger") || WeaponName2.contains("sword")) {
			c.getClient().setSidebarInterface(0, 2276); // stab, lunge, slash,
														// block
			c.getPA().sendFrame246(2277, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 2279);
		} else if (WeaponName2.startsWith("pickaxe")) {
			c.getClient().setSidebarInterface(0, 5570); // spike, impale, smash,
														// block
			c.getPA().sendFrame246(5571, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 5573);
		} else if (WeaponName2.startsWith("axe") || WeaponName2.startsWith("battleaxe") || WeaponName.endsWith("axe")) {
			c.getClient().setSidebarInterface(0, 1698); // chop, hack, smash,
														// block
			c.getPA().sendFrame246(1699, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 1701);
		} else if (WeaponName2.startsWith("Scythe")) {
			c.getClient().setSidebarInterface(0, 8460); // jab, swipe, fend
			c.getPA().sendFrame246(8461, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 8463);
		} else if (WeaponName2.startsWith("spear")) {
			c.getClient().setSidebarInterface(0, 4679); // lunge, swipe, pound,
														// block
			c.getPA().sendFrame246(4680, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 4682);
		} else if (WeaponName2.toLowerCase().contains("mace")) {
			c.getClient().setSidebarInterface(0, 3796);
			c.getPA().sendFrame246(3797, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 3799);

		} else if (c.getEquipment()[PlayerConstants.PLAYER_WEAPON] == 4153) {
			c.getClient().setSidebarInterface(0, 425); // war hamer equip.
			c.getPA().sendFrame246(426, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 428);
		} else {
			c.getClient().setSidebarInterface(0, 2423); // chop, slash, lunge,
														// block
			c.getPA().sendFrame246(2424, 200, Weapon);
			c.getPA().sendFrame126(WeaponName, 2426);
		}

	}

	/**
	 * Weapon Requirements
	 **/

	public void getRequirements(int itemId) {
		c.attackLevelReq = c.defenceLevelReq = c.strengthLevelReq = c.prayerLevelReq = c.rangeLevelReq = c.magicLevelReq = 0;
		String name = getItemName(itemId).toLowerCase();
		if (name.contains("defender")) {
			if (name.contains("steel")) {
				c.defenceLevelReq = 5;
			}
			if (name.contains("black")) {
				c.defenceLevelReq = 10;
			}
			if (name.contains("mithril")) {
				c.defenceLevelReq = 20;
			}
			if (name.contains("adamant")) {
				c.defenceLevelReq = 30;
			}
			if (name.contains("rune")) {
				c.defenceLevelReq = 40;
			}
			if (name.contains("dragon")) {
				c.defenceLevelReq = 60;
			}
		}
		if (name.contains("steel")) {
			if (name.contains("plate") || name.contains("chain") || name.contains("boots") || name.contains("helm") || name.contains("shield")) {
				c.defenceLevelReq = 5;
			}
			if (name.contains("scimitar") || name.contains("dagger") || name.contains("sword")) {
				c.attackLevelReq = 5;
			}
			if (name.contains("knife") || name.contains("thrownaxe") || name.contains("javelin")) {
				c.rangeLevelReq = 5;
			}
		}
		if (name.contains("black")) {
			if (name.contains("d'hide body")) {
				c.defenceLevelReq = 40;
				c.rangeLevelReq = 70;
			}
			if (name.contains("d'hide chaps") || name.contains("d'hide vamb")) {
				c.rangeLevelReq = 70;
			}
			if (name.contains("plate") || name.contains("chain") || name.contains("boots") || name.contains("helm") || name.contains("shield")) {
				c.defenceLevelReq = 10;
			}
			if (name.contains("scimitar") || name.contains("dagger") || name.contains("sword")) {
				c.attackLevelReq = 10;
			}
			if (name.contains("knife") || name.contains("thrownaxe") || name.contains("javelin")) {
				c.rangeLevelReq = 10;
			}
		}
		if (name.contains("mithril")) {
			if (name.contains("plate") || name.contains("chain") || name.contains("boots") || name.contains("helm") || name.contains("shield")) {
				c.defenceLevelReq = 20;
			}
			if (name.contains("scimitar") || name.contains("dagger") || name.contains("sword")) {
				c.attackLevelReq = 20;
			}
			if (name.contains("knife") || name.contains("thrownaxe") || name.contains("javelin")) {
				c.rangeLevelReq = 20;
			}
		}
		if (name.contains("adamant")) {
			if (name.contains("plate") || name.contains("chain") || name.contains("boots") || name.contains("helm") || name.contains("shield")) {
				c.defenceLevelReq = 30;
			}
			if (name.contains("scimitar") || name.contains("dagger") || name.contains("sword")) {
				c.attackLevelReq = 30;
			}
			if (name.contains("knife") || name.contains("thrownaxe") || name.contains("javelin")) {
				c.rangeLevelReq = 30;
			}
		}
		if (name.contains("maple shortbow")) {
			c.rangeLevelReq = 30;
		}
		if (name.contains("yew shortbow")) {
			c.rangeLevelReq = 40;
		}
		if (name.contains("Willow Shortbow")) {
			c.rangeLevelReq = 20;
		}
		if (name.contains("rune")) {
			if (name.contains("plate") || name.contains("chain") || name.contains("boots") || name.contains("helm") || name.contains("shield")) {
				c.defenceLevelReq = 40;
			}
			if (name.contains("scimitar") || name.contains("dagger") || name.contains("sword")) {
				c.attackLevelReq = 40;
			}
			if (name.contains("knife") || name.contains("thrownaxe") || name.contains("javelin") || name.contains("yew shortbow")) {
				c.rangeLevelReq = 40;
			}
		}
		if (name.contains("dragon")) {
			if (name.contains("plate") || name.contains("chain") || name.contains("boots") || name.contains("helm") || name.contains("shield")) {
				c.defenceLevelReq = 60;
			}
			if (name.contains("scimitar") || name.contains("dagger") || name.contains("sword")) {
				c.attackLevelReq = 60;
			}
			if (name.contains("knife") || name.contains("thrownaxe") || name.contains("javelin")) {
				c.rangeLevelReq = 60;
			}
		}
		if (name.contains("initiate")) {
			c.defenceLevelReq = 20;
			c.prayerLevelReq = 10;
		}
		if (name.contains("green")) {
			if (name.contains("d'hide")) {
				if (name.contains("d'hide body")) {
					c.rangeLevelReq = 40;
					c.defenceLevelReq = 40;
				} else {
					c.rangeLevelReq = 40;
				}
			}
		}
		if (name.contains("blue")) {
			if (name.contains("d'hide")) {
				if (name.contains("body")) {
					c.rangeLevelReq = 50;
					c.defenceLevelReq = 40;
				} else {
					c.rangeLevelReq = 50;
				}
			}
		}
		if (name.contains("red")) {
			if (name.contains("d'hide")) {
				if (name.contains("body")) {
					c.rangeLevelReq = 60;
					c.defenceLevelReq = 40;
				} else {
					c.rangeLevelReq = 60;
				}
			}
		}
		if (name.contains("saradomin") || name.contains("zamorak")) {
			if (name.contains("d'hide")) {
				if (name.contains("body")) {
					c.rangeLevelReq = 70;
					c.defenceLevelReq = 40;
				} else {
					c.rangeLevelReq = 70;
				}
			}
		}
		if (name.contains("crossbow")) {
			if (name.contains("rune")) {
				c.rangeLevelReq = 61;
			}
			if (name.contains("karil's") || name.contains("armadyl")) {
				c.rangeLevelReq = 70;
			}
		}
		if (name.contains("dharok's") || name.contains("torag's") || name.contains("guthan's") || name.contains("verac's")) {
			if (name.contains("plate") || name.contains("helm") || name.contains("chain") || name.contains("brassard")) {
				c.defenceLevelReq = 70;
			}
			if (name.contains("greataxe") || name.contains("hammers") || name.contains("warspear") || name.contains("flail")) {
				c.attackLevelReq = 70;
				c.strengthLevelReq = 70;
			}
		}
		if (name.contains("karil's")) {
			if (name.contains("leathertop") || name.contains("leatherskirt")) {
				c.rangeLevelReq = 70;
				c.defenceLevelReq = 70;
			}
		}
		if (name.contains("trident of the seas") || name.contains("trident of the swamp")) {
			c.magicLevelReq = 75;
		}
		if (name.contains("3rd age bow") || name.contains("	3rd age longsword") || name.contains("3rd age wand")) {
			c.rangeLevelReq = 65;
			c.magicLevelReq = 65;
			c.attackLevelReq = 65;
		}
		if (name.contains("abyssal dagger")) {
			c.attackLevelReq = 70;
		}
		if (name.contains("rangers' tunic") || name.contains("ranger boots")) {
			c.rangeLevelReq = 40;
		}
		if (name.contains("ancient staf")) {
			c.attackLevelReq = 50;
			c.magicLevelReq = 50;
		}
		if (name.contains("armadyl crossbow")) {
			c.rangeLevelReq = 75;
		}
		if (name.equals("dragon warhammer")) {
			c.attackLevelReq = 60;
		}
		if (name.contains("dragon spear") || name.contains("dragon halberd") || name.contains("dragon mace")) {
			c.attackLevelReq = 60;
		}
		if (name.contains("primordial boots")) {
			c.defenceLevelReq = 75;
			c.strengthLevelReq = 75;
		}
		if (name.contains("eternal boots")) {
			c.defenceLevelReq = 75;
			c.magicLevelReq = 75;
		}
		if (name.contains("pegasian boots")) {
			c.defenceLevelReq = 75;
			c.rangeLevelReq = 75;
		}
		if (name.contains("master wand") || name.contains("mage's book")) {
			c.magicLevelReq = 60;
		}
		if (name.contains("elysian spirit shield")) {
			c.prayerLevelReq = 75;
			c.defenceLevelReq = 75;
		}
		if (name.contains("spectral spirit shield")) {
			c.defenceLevelReq = 75;
			c.prayerLevelReq = 75;
			c.magicLevelReq = 65;
		}
		if (name.contains("arcane spirit shield")) {
			c.defenceLevelReq = 75;
			c.prayerLevelReq = 70;
			c.magicLevelReq = 65;
		}
		if (name.contains("blessd spirit shield")) {
			c.defenceLevelReq = 70;
			c.prayerLevelReq = 60;
		}

		if (name.contains("abyssal whip")) {
			c.attackLevelReq = 70;
		}
		if (name.equals("staff of the dead") || name.contains("toxic staff of the dead")) {
			c.attackLevelReq = 75;
			c.magicLevelReq = 75;
		}
		if (name.contains("a byssal tentacle")) {
			c.attackLevelReq = 75;
		}
		if (name.contains("ahrim's")) {
			if (name.contains("robe") || name.contains("hood")) {
				c.magicLevelReq = 70;
				c.defenceLevelReq = 70;
			}
			if (name.contains("staff")) {
				c.magicLevelReq = 70;
				c.attackLevelReq = 70;
			}
		}
		if (name.contains("dragonfire")) {
			c.defenceLevelReq = 75;
		}
		if (name.contains("godsword")) {
			c.attackLevelReq = 75;
		}
		if (name.contains("bandos")) {
			if (name.contains("chest") || name.contains("tasset") || name.contains("boots")) {
				c.defenceLevelReq = 65;
			}
		}
		if (name.contains("armadyl")) {
			if (name.contains("helmet") || name.contains("chestplate") || name.contains("chainskirt")) {
				c.defenceLevelReq = 70;
				c.rangeLevelReq = 70;
			}
		}
		if (name.contains("void knight") || name.contains("void mage") || name.contains("void ranger") || name.contains("void melee") || name.contains("elite void")) {
			c.defenceLevelReq = 42;
			c.attackLevelReq = 42;
			c.strengthLevelReq = 42;
			c.rangeLevelReq = 42;
			c.magicLevelReq = 42;
		}
		if (name.equals("granite maul")) {
			c.attackLevelReq = 50;
			c.strengthLevelReq = 50;
		}
		if (name.contains("archer helm")) {
			c.defenceLevelReq = 45;
		}
		if (name.equals("farseer helm")) {
			c.defenceLevelReq = 45;
		}
		if (name.equals("helm of neitiznot")) {
			c.defenceLevelReq = 55;
		}
		if (name.equals("zerker Helm")) {
			c.defenceLevelReq = 45;
		}
		if (name.equals("tzhaar-ket-em")) {
			c.attackLevelReq = 60;
		}
		if (name.equals("tzhaar-ket-om")) {
			c.strengthLevelReq = 60;
		}
		if (name.contains("mystic")) {
			if (name.contains("hat") || name.contains("robe top") || name.contains("robe bottom") || name.contains("gloves") || name.contains("boots")) {
				c.magicLevelReq = 40;
				c.defenceLevelReq = 20;
			}
		}
		if (name.contains("splitbark")) {
			if (name.contains("helm") || name.contains("body") || name.contains("legs") || name.contains("gauntlets") || name.contains("boots")) {
				c.magicLevelReq = 40;
				c.defenceLevelReq = 40;
			}
		}
		if (name.equalsIgnoreCase("magic shortbow")) {
			c.rangeLevelReq = 50;
		}
		if (name.equals("toktz-xil-ul") || name.equals("dark bow")) {
			c.rangeLevelReq = 60;
		}
		if (name.contains("snakeskin")) {
			c.rangeLevelReq = 30;
			c.defenceLevelReq = 30;
		}
		if (name.contains("infinity")) {
			c.magicLevelReq = 50;
			c.defenceLevelReq = 25;
		}
		if (name.equals("master wand") || name.contains("mage's book") || name.contains("malediction ward")) {
			c.magicLevelReq = 60;
		}
		if (name.equals("ancient staff")) {
			c.magicLevelReq = 50;
			c.defenceLevelReq = 50;
		}
	}

	/**
	 * two handed weapon check
	 **/
	public boolean is2handed(String itemName, int itemId) {
		if (itemName.contains("ahrim") || itemName.contains("karil") || itemName.contains("verac") || itemName.contains("guthan") || itemName.contains("dharok") || itemName.contains("torag") || itemName.contains("halberd")
				|| itemName.contains("blowpipe")) {
			return true;
		}
		if (itemName.contains("longbow") || itemName.contains("shortbow") || itemName.contains("ark bow")) {
			return true;
		}
		if (itemName.contains("crystal")) {
			return true;
		}
		if (itemName.contains("godsword") || itemName.contains("aradomin sword") || itemName.contains("2h") || itemName.contains("spear") || itemName.contains("blessed sword")) {
			return true;
		}
		switch (itemId) {
		case 6724: // seercull
		case 11730:
		case 4153:
		case 6528:
		case 14484:
			return true;
		}
		return false;
	}

	/**
	 * Weapons special bar, adds the spec bars to weapons that require them and
	 * removes the spec bars from weapons which don't require them
	 **/

	public void addSpecialBar(int weapon) {
		switch (weapon) {

		case 4151: // whip
		case 12006:
			c.getPA().sendFrame171(0, 12323);
			specialAmount(weapon, c.specAmount, 12335);
			break;

		case 859: // magic bows
		case 861:
		case 11235:
		case 11785:
			c.getPA().sendFrame171(0, 7549);
			specialAmount(weapon, c.specAmount, 7561);
			break;

		case 4587: // dscimmy
			c.getPA().sendFrame171(0, 7599);
			specialAmount(weapon, c.specAmount, 7611);
			break;

		case 3204: // d hally
		case 13092:
		case 11791:
			c.getPA().sendFrame171(0, 8493);
			specialAmount(weapon, c.specAmount, 8505);
			break;

		case 1377: // d battleaxe
			c.getPA().sendFrame171(0, 7499);
			specialAmount(weapon, c.specAmount, 7511);
			break;

		case 4153: // gmaul
			c.getPA().sendFrame171(0, 7474);
			specialAmount(weapon, c.specAmount, 7486);
			break;

		case 1249: // dspear
			c.getPA().sendFrame171(0, 7674);
			specialAmount(weapon, c.specAmount, 7686);
			break;

		case 1215:// dragon dagger
		case 1231:
		case 5680:
		case 5698:
		case 1305: // dragon long
		case 11802:
		case 11698:
		case 11700:
		case 11730:
		case 11696:
		case 13205:
		case 13265:
		case 13267:
		case 13269:
		case 13271:
			c.getPA().sendFrame171(0, 7574);
			specialAmount(weapon, c.specAmount, 7586);
			break;

		case 1434: // dragon mace
			c.getPA().sendFrame171(0, 7624);
			specialAmount(weapon, c.specAmount, 7636);
			break;

		default:
			c.getPA().sendFrame171(1, 7624); // mace interface
			c.getPA().sendFrame171(1, 7474); // hammer, gmaul
			c.getPA().sendFrame171(1, 7499); // axe
			c.getPA().sendFrame171(1, 7549); // bow interface
			c.getPA().sendFrame171(1, 7574); // sword interface
			c.getPA().sendFrame171(1, 7599); // scimmy sword interface, for most
			// swords
			c.getPA().sendFrame171(1, 8493);
			c.getPA().sendFrame171(1, 12323); // whip interface
			break;
		}
	}

	/**
	 * Specials bar filling amount
	 **/

	public void specialAmount(int weapon, double specAmount, int barId) {
		c.specBarId = barId;
		c.getPA().sendFrame70(specAmount >= 10 ? 500 : 0, 0, (--barId));
		c.getPA().sendFrame70(specAmount >= 9 ? 500 : 0, 0, (--barId));
		c.getPA().sendFrame70(specAmount >= 8 ? 500 : 0, 0, (--barId));
		c.getPA().sendFrame70(specAmount >= 7 ? 500 : 0, 0, (--barId));
		c.getPA().sendFrame70(specAmount >= 6 ? 500 : 0, 0, (--barId));
		c.getPA().sendFrame70(specAmount >= 5 ? 500 : 0, 0, (--barId));
		c.getPA().sendFrame70(specAmount >= 4 ? 500 : 0, 0, (--barId));
		c.getPA().sendFrame70(specAmount >= 3 ? 500 : 0, 0, (--barId));
		c.getPA().sendFrame70(specAmount >= 2 ? 500 : 0, 0, (--barId));
		c.getPA().sendFrame70(specAmount >= 1 ? 500 : 0, 0, (--barId));
		updateSpecialBar();
		sendWeapon(weapon, getItemName(weapon));
		c.getPA().sendFrame126((int) (c.specAmount * 10) + "", 155);

	}

	/**
	 * Special attack text and what to highlight or blackout
	 **/

	public void updateSpecialBar() {
		if (c.usingSpecial) {
			c.getPA()
					.sendFrame126("" + (c.specAmount >= 2 ? "@yel@S P" : "@bla@S P") + "" + (c.specAmount >= 3 ? "@yel@ E" : "@bla@ E") + "" + (c.specAmount >= 4 ? "@yel@ C I" : "@bla@ C I") + "" + (c.specAmount >= 5 ? "@yel@ A L" : "@bla@ A L") + ""
							+ (c.specAmount >= 6 ? "@yel@  A" : "@bla@  A") + "" + (c.specAmount >= 7 ? "@yel@ T T" : "@bla@ T T") + "" + (c.specAmount >= 8 ? "@yel@ A" : "@bla@ A") + "" + (c.specAmount >= 9 ? "@yel@ C" : "@bla@ C") + ""
							+ (c.specAmount >= 10 ? "@yel@ K" : "@bla@ K"), c.specBarId);
		} else {
			c.getPA().sendFrame126("@bla@S P E C I A L  A T T A C K", c.specBarId);
		}
	}

	/**
	 * Wear Item
	 **/
	public static boolean wearItem(Player c, int wearID, int slot) {
		if (!c.getItems().playerHasItem(wearID, 1, slot)) {
			// add a method here for logging cheaters(If you want)
			return false;
		}
		// synchronized(c) {
		int targetSlot = 0;
		boolean canWearItem = true;
		if (c.playerItems[slot] == (wearID + 1)) {
			c.getItems().getRequirements(wearID);
			String name = getItemName(wearID).toLowerCase();
			if (wearID == 542 || wearID == 1033 || wearID == 6108) {
				targetSlot = PlayerConstants.PLAYER_LEGS;
			} else if (wearID == 1035 || wearID == 6107) {
				targetSlot = PlayerConstants.PLAYER_BODY;
			} else if (name.contains("helm") || name.contains("coif") || name.contains("cowl") || name.contains("hat") || name.contains("mask") || name.contains("hood") || name.equals("afro") || name.contains("headband") || name.contains("sallet")
					|| name.contains("crown") || name.contains("mitre")) {
				targetSlot = PlayerConstants.PLAYER_HAT;
			} else if (name.contains("legs") || name.contains("tasset") || name.contains("bottom") || name.contains("robeskirt") || name.contains("skirt") || name.contains("chaps") || name.contains("knight robe") || name.contains("elite void robe")
					|| name.contains("cuisse") || name.contains("pantaloons") || name.contains("trouser") || name.contains("skeleton leggings") || name.contains("greaves")) {
				targetSlot = PlayerConstants.PLAYER_LEGS;
			} else if (name.contains("shield") || name.contains("defender") || name.contains("defender") || name.contains("ward") || name.contains("book")) {
				targetSlot = PlayerConstants.PLAYER_SHIELD;
			} else if (name.contains("body") || name.contains("torso") || name.contains("top") || name.contains("brassard") || name.contains("hauberk") || name.contains("jacket") || name.contains("tunic") || name.contains("chest")
					|| name.contains("domin d'hide") || name.contains("orak d'hide") || name.contains("skeleton shirt") || name.contains("wizard robe")) {
				targetSlot = PlayerConstants.PLAYER_BODY;
			} else if (name.contains("cloak") || name.contains("cape") || name.contains("accumulator") || name.contains("attractor")) {
				targetSlot = PlayerConstants.PLAYER_CAPE;
			} else if (name.contains("boots") || name.contains("shoes") || name.contains("feet") || name.contains("flippers")) {
				targetSlot = PlayerConstants.PLAYER_FEET;
			} else if (name.contains("glove") || name.contains("bracelet") || name.contains("vamb") || name.contains("bracers")) {
				targetSlot = PlayerConstants.PLAYER_GLOVES;
			} else if (wearID == 7462) {
				targetSlot = PlayerConstants.PLAYER_GLOVES;
			} else if (name.contains("arrow") || name.contains("bolt")) {
				targetSlot = PlayerConstants.PLAYER_ARROWS;
			} else if (name.contains("ring")) {
				targetSlot = PlayerConstants.PLAYER_RING;
			} else if (name.contains("amulet") || name.contains("necklace") || name.contains("stole")) {
				targetSlot = PlayerConstants.PLAYER_AMULET;
			} else {
				targetSlot = 3;
			}
			if (wearID == 12006 && c.getHits()[0] <= 0) {
				c.getHits()[0] = 5000;
			}
			if (c.duelRule[11] && targetSlot == 0) {
				c.sendMessage("Wearing hats has been disabled in this duel!");
				return false;
			}
			if (c.duelRule[12] && targetSlot == 1) {
				c.sendMessage("Wearing capes has been disabled in this duel!");
				return false;
			}
			if (c.duelRule[13] && targetSlot == 2) {
				c.sendMessage("Wearing amulets has been disabled in this duel!");
				return false;
			}
			if (c.duelRule[14] && targetSlot == 3) {
				c.sendMessage("Wielding weapons has been disabled in this duel!");
				return false;
			}
			if (c.duelRule[15] && targetSlot == 4) {
				c.sendMessage("Wearing bodies has been disabled in this duel!");
				return false;
			}
			if ((c.duelRule[16] && targetSlot == 5) || (c.duelRule[16] && c.getItems().is2handed(getItemName(wearID).toLowerCase(), wearID))) {
				c.sendMessage("Wearing shield has been disabled in this duel!");
				return false;
			}
			if (c.duelRule[17] && targetSlot == 7) {
				c.sendMessage("Wearing legs has been disabled in this duel!");
				return false;
			}
			if (c.duelRule[18] && targetSlot == 9) {
				c.sendMessage("Wearing gloves has been disabled in this duel!");
				return false;
			}
			if (c.duelRule[19] && targetSlot == 10) {
				c.sendMessage("Wearing boots has been disabled in this duel!");
				return false;
			}
			if (c.duelRule[20] && targetSlot == 12) {
				c.sendMessage("Wearing rings has been disabled in this duel!");
				return false;
			}
			if (c.duelRule[21] && targetSlot == 13) {
				c.sendMessage("Wearing arrows has been disabled in this duel!");
				return false;
			}

			if (targetSlot == 10 || targetSlot == 7 || targetSlot == 5 || targetSlot == 4 || targetSlot == 0 || targetSlot == 9 || targetSlot == 10) {
				if (c.prayerLevelReq > 0) {
					if (c.getPA().getLevelForXP(c.getExperience()[5]) < c.prayerLevelReq) {
						c.sendMessage("You need a prayer level of " + c.prayerLevelReq + " to wear this item.");
						canWearItem = false;
					}
				}
				if (c.defenceLevelReq > 0) {
					if (c.getPA().getLevelForXP(c.getExperience()[1]) < c.defenceLevelReq) {
						c.sendMessage("You need a defence level of " + c.defenceLevelReq + " to wear this item.");
						canWearItem = false;
					}
				}
				if (c.rangeLevelReq > 0) {
					if (c.getPA().getLevelForXP(c.getExperience()[4]) < c.rangeLevelReq) {
						c.sendMessage("You need a range level of " + c.rangeLevelReq + " to wear this item.");
						canWearItem = false;
					}
				}
				if (c.magicLevelReq > 0) {
					if (c.getPA().getLevelForXP(c.getExperience()[6]) < c.magicLevelReq) {
						c.sendMessage("You need a magic level of " + c.magicLevelReq + " to wear this item.");
						canWearItem = false;
					}
				}
			}
			if (targetSlot == 3) {
				if (c.strengthLevelReq > 0) {
					if (c.getPA().getLevelForXP(c.getExperience()[2]) < c.strengthLevelReq) {
						c.sendMessage("You need a strength level of " + c.strengthLevelReq + " to wield this weapon.");
						canWearItem = false;
					}
				}
				if (c.attackLevelReq > 0) {
					if (c.getPA().getLevelForXP(c.getExperience()[0]) < c.attackLevelReq) {
						c.sendMessage("You need an attack level of " + c.attackLevelReq + " to wield this weapon.");
						canWearItem = false;
					}
				}
				if (c.rangeLevelReq > 0) {
					if (c.getPA().getLevelForXP(c.getExperience()[4]) < c.rangeLevelReq) {
						c.sendMessage("You need a range level of " + c.rangeLevelReq + " to wield this weapon.");
						canWearItem = false;
					}
				}
				if (c.magicLevelReq > 0) {
					if (c.getPA().getLevelForXP(c.getExperience()[6]) < c.magicLevelReq) {
						c.sendMessage("You need a magic level of " + c.magicLevelReq + " to wield this weapon.");
						canWearItem = false;
					}
				}
			}

			if (!canWearItem) {
				return false;
			}

			int wearAmount = c.playerItemsN[slot];
			if (wearAmount < 1) {
				return false;
			}

			if (targetSlot == PlayerConstants.PLAYER_WEAPON) {
				c.autocasting = false;
				c.autocastId = 0;
				c.getPA().sendFrame36(108, 0);
			}

			if (slot >= 0 && wearID >= 0) {
				int toEquip = c.playerItems[slot];
				int toEquipN = c.playerItemsN[slot];
				int toRemove = c.getEquipment()[targetSlot];
				int toRemoveN = c.playerEquipmentN[targetSlot];
				boolean stackable = false;
				if (getItemName(toRemove).contains("javelin") || getItemName(toRemove).contains("dart") || getItemName(toRemove).contains("knife") || getItemName(toRemove).contains("bolt") || getItemName(toRemove).contains("arrow")
						|| getItemName(toRemove).contains("Bolt") || getItemName(toRemove).contains("bolts") || getItemName(toRemove).contains("thrownaxe") || getItemName(toRemove).contains("throwing"))
					stackable = true;
				else
					stackable = false;
				if (toEquip == toRemove + 1 && ItemDefinitions.forId(toRemove).isStackable()) {
					c.getItems().deleteItem(toRemove, c.getItems().getItemSlot(toRemove), toEquipN);
					c.playerEquipmentN[targetSlot] += toEquipN;
				} else if (targetSlot != 5 && targetSlot != 3) {
					if (c.getItems().playerHasItem(toRemove, 1) && stackable == true) {
						c.playerItems[slot] = 0;// c.playerItems[slot] =
						// toRemove + 1;
						c.playerItemsN[slot] = 0;// c.playerItemsN[slot] =
						// toRemoveN;
						if (toRemove > 0 && toRemoveN > 0)// c.getEquipment()[targetSlot]
							// = toEquip - 1;
							c.getItems().addItem(toRemove, toRemoveN);// c.playerEquipmentN[targetSlot]
						// = toEquipN;
					} else {
						c.playerItems[slot] = toRemove + 1;
						c.playerItemsN[slot] = toRemoveN;
					}
					c.getEquipment()[targetSlot] = toEquip - 1;
					c.playerEquipmentN[targetSlot] = toEquipN;
				} else if (targetSlot == 5) {
					boolean wearing2h = c.getItems().is2handed(getItemName(c.getEquipment()[3]).toLowerCase(), c.getEquipment()[3]);
					if (wearing2h) {
						toRemove = c.getEquipment()[3];
						toRemoveN = c.playerEquipmentN[3];
						c.getEquipment()[3] = -1;
						c.playerEquipmentN[3] = 0;
						c.getItems().updateSlot(3);
					}
					c.playerItems[slot] = toRemove + 1;
					c.playerItemsN[slot] = toRemoveN;
					c.getEquipment()[targetSlot] = toEquip - 1;
					c.playerEquipmentN[targetSlot] = toEquipN;
				} else if (targetSlot == 3) {
					boolean is2h = c.getItems().is2handed(getItemName(wearID).toLowerCase(), wearID);
					boolean wearingShield = c.getEquipment()[PlayerConstants.PLAYER_SHIELD] > 0;
					boolean wearingWeapon = c.getEquipment()[3] > 0;
					if (is2h) {
						if (wearingShield && wearingWeapon) {
							if (c.getItems().freeSlots() > 0) {
								if (c.getItems().playerHasItem(toRemove, 1) && stackable == true) {
									c.playerItems[slot] = 0;// c.playerItems[slot]
									// = toRemove + 1;
									c.playerItemsN[slot] = 0;// c.playerItemsN[slot]
									// = toRemoveN;
									if (toRemove > 0 && toRemoveN > 0)// c.getEquipment()[targetSlot]
										// =
										// toEquip
										// - 1;
										c.getItems().addItem(toRemove, toRemoveN);// c.playerEquipmentN[targetSlot]
									// =
									// toEquipN;
								} else {
									c.playerItems[slot] = toRemove + 1;
									c.playerItemsN[slot] = toRemoveN;
								}
								c.getEquipment()[targetSlot] = toEquip - 1;
								c.playerEquipmentN[targetSlot] = toEquipN;
								c.getItems().removeItem(PlayerConstants.PLAYER_SHIELD);
							} else {
								c.sendMessage("You do not have enough inventory space to do this.");
								return false;
							}
						} else if (wearingShield && !wearingWeapon) {
							c.playerItems[slot] = c.getEquipment()[PlayerConstants.PLAYER_SHIELD] + 1;
							c.playerItemsN[slot] = c.playerEquipmentN[PlayerConstants.PLAYER_SHIELD];
							c.getEquipment()[targetSlot] = toEquip - 1;
							c.playerEquipmentN[targetSlot] = toEquipN;
							c.getEquipment()[PlayerConstants.PLAYER_SHIELD] = -1;
							c.playerEquipmentN[PlayerConstants.PLAYER_SHIELD] = 0;
							c.getItems().updateSlot(PlayerConstants.PLAYER_SHIELD);
						} else {
							if (c.getItems().playerHasItem(toRemove, 1) && stackable == true) {
								c.playerItems[slot] = 0;// c.playerItems[slot] =
								// toRemove + 1;
								c.playerItemsN[slot] = 0;// c.playerItemsN[slot]
								// = toRemoveN;
								if (toRemove > 0 && toRemoveN > 0)// c.getEquipment()[targetSlot]
									// = toEquip
									// - 1;
									c.getItems().addItem(toRemove, toRemoveN);// c.playerEquipmentN[targetSlot]
								// =
								// toEquipN;
							} else {
								c.playerItems[slot] = toRemove + 1;
								c.playerItemsN[slot] = toRemoveN;
							}
							c.getEquipment()[targetSlot] = toEquip - 1;
							c.playerEquipmentN[targetSlot] = toEquipN;
						}
					} else {
						if (c.getItems().playerHasItem(toRemove, 1) && stackable == true) {
							c.playerItems[slot] = 0;// c.playerItems[slot] =
							// toRemove + 1;
							c.playerItemsN[slot] = 0;// c.playerItemsN[slot] =
							// toRemoveN;
							if (toRemove > 0 && toRemoveN > 0)// c.getEquipment()[targetSlot]
								// = toEquip -
								// 1;
								c.getItems().addItem(toRemove, toRemoveN);// c.playerEquipmentN[targetSlot]
							// = toEquipN;
						} else {
							c.playerItems[slot] = toRemove + 1;
							c.playerItemsN[slot] = toRemoveN;
						}
						c.getEquipment()[targetSlot] = toEquip - 1;
						c.playerEquipmentN[targetSlot] = toEquipN;
					}
				}
				c.getItems().resetItems(3214);
			}
			if (targetSlot == 3) {
				c.usingSpecial = false;
				c.getItems().addSpecialBar(wearID);
			}
			if (c.getClient().getOutStream() != null && c != null) {
				c.getClient().getOutStream().createFrameVarSizeWord(34);
				c.getClient().getOutStream().writeWord(1688);
				c.getClient().getOutStream().writeByte(targetSlot);
				c.getClient().getOutStream().writeWord(wearID + 1);

				if (c.playerEquipmentN[targetSlot] > 254) {
					c.getClient().getOutStream().writeByte(255);
					c.getClient().getOutStream().writeDWord(c.playerEquipmentN[targetSlot]);
				} else {
					c.getClient().getOutStream().writeByte(c.playerEquipmentN[targetSlot]);
				}

				c.getClient().getOutStream().endFrameVarSizeWord();
				c.getClient().flushOutStream();
			}
			if (targetSlot == PlayerConstants.PLAYER_BODY) {
				c.setAttribute("full_body", Items.isFullBody(c.getEquipment()[PlayerConstants.PLAYER_BODY]));
			} else if (targetSlot == PlayerConstants.PLAYER_HAT) {
				c.setAttribute("full_mask", Items.isFullMask(c.getEquipment()[PlayerConstants.PLAYER_HAT]));
				c.setAttribute("full_helm", Items.isFullMask(c.getEquipment()[PlayerConstants.PLAYER_HAT]));
			}
			c.getItems().sendWeapon(c.getEquipment()[3], getItemName(c.getEquipment()[3]));
			EquipmentBonuses.resetPlayerBonuses(c);
			EquipmentBonuses.getItemBonuses(c);
			EquipmentBonuses.writeItemBonuses(c);
			c.getCombat().getPlayerAnimIndex();
			c.getPA().requestUpdates();
			return true;
		} else {
			return false;
		}
	}

	public void wearItem(int wearID, int wearAmount, int targetSlot) {
		// synchronized (c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrameVarSizeWord(34);
			c.getClient().getOutStream().writeWord(1688);
			c.getClient().getOutStream().writeByte(targetSlot);
			c.getClient().getOutStream().writeWord(wearID + 1);

			if (wearAmount > 254) {
				c.getClient().getOutStream().writeByte(255);
				c.getClient().getOutStream().writeDWord(wearAmount);
			} else {
				c.getClient().getOutStream().writeByte(wearAmount);
			}
			c.getClient().getOutStream().endFrameVarSizeWord();
			c.getClient().flushOutStream();
			c.getEquipment()[targetSlot] = wearID;
			c.playerEquipmentN[targetSlot] = wearAmount;
			c.getItems().sendWeapon(c.getEquipment()[PlayerConstants.PLAYER_WEAPON], getItemName(c.getEquipment()[PlayerConstants.PLAYER_WEAPON]));
			EquipmentBonuses.resetPlayerBonuses(c);
			EquipmentBonuses.getItemBonuses(c);
			EquipmentBonuses.writeItemBonuses(c);
			c.getCombat().getPlayerAnimIndex();
			c.updateRequired = true;
			c.setAppearanceUpdateRequired(true);
		}
		// }
	}

	public void updateSlot(int slot) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrameVarSizeWord(34);
			c.getClient().getOutStream().writeWord(1688);
			c.getClient().getOutStream().writeByte(slot);
			c.getClient().getOutStream().writeWord(c.getEquipment()[slot] + 1);
			if (c.playerEquipmentN[slot] > 254) {
				c.getClient().getOutStream().writeByte(255);
				c.getClient().getOutStream().writeDWord(c.playerEquipmentN[slot]);
			} else {
				c.getClient().getOutStream().writeByte(c.playerEquipmentN[slot]);
			}
			c.getClient().getOutStream().endFrameVarSizeWord();
			c.getClient().flushOutStream();
		}

	}

	/**
	 * Remove Item
	 **/
	public void removeItem(int slot) {
		if (c.getClient().getOutStream() != null && c != null) {
			if (c.getEquipment()[slot] > -1) {
				if (addItem(c.getEquipment()[slot], c.playerEquipmentN[slot])) {
					c.getEquipment()[slot] = -1;
					c.playerEquipmentN[slot] = 0;
					sendWeapon(c.getEquipment()[PlayerConstants.PLAYER_WEAPON], getItemName(c.getEquipment()[PlayerConstants.PLAYER_WEAPON]));
					EquipmentBonuses.resetPlayerBonuses(c);
					EquipmentBonuses.getItemBonuses(c);
					EquipmentBonuses.writeItemBonuses(c);
					c.getCombat().getPlayerAnimIndex();
					c.getClient().getOutStream().createFrame(34);
					c.getClient().getOutStream().writeWord(6);
					c.getClient().getOutStream().writeWord(1688);
					c.getClient().getOutStream().writeByte(slot);
					c.getClient().getOutStream().writeWord(0);
					c.getClient().getOutStream().writeByte(0);
					c.getClient().flushOutStream();
					c.updateRequired = true;
					c.setAppearanceUpdateRequired(true);
					if (slot == PlayerConstants.PLAYER_BODY) {
						c.setAttribute("full_body", Items.isFullBody(c.getEquipment()[PlayerConstants.PLAYER_BODY]));
					} else if (slot == PlayerConstants.PLAYER_HAT) {
						c.setAttribute("full_mask", Items.isFullMask(c.getEquipment()[PlayerConstants.PLAYER_HAT]));
						c.setAttribute("full_helm", Items.isFullMask(c.getEquipment()[PlayerConstants.PLAYER_HAT]));
					}
				}
			}
		}
	}

	public void resetBank() {

		Player target = c.getAttribute("viewing_other_bank", null);

		if (target != null) {

			int tabId = target.getBank().getCurrentBankTab().getTabId();
			for (int i = 0; i < target.getBank().getBankTab().length; i++) {
				if (i == 0)
					continue;
				if (i != target.getBank().getBankTab().length - 1 && target.getBank().getBankTab()[i].size() == 0 && target.getBank().getBankTab()[i + 1].size() > 0) {
					for (BankItem item : target.getBank().getBankTab()[i + 1].getItems()) {
						target.getBank().getBankTab()[i].add(item);
					}
					target.getBank().getBankTab()[i + 1].getItems().clear();
				}
			}
			c.getPA().sendFrame36(700, 0);
			c.getPA().sendFrame34a(58040, -1, 0, 0);
			int newSlot = -1;
			for (int i = 0; i < target.getBank().getBankTab().length; i++) {
				BankTab tab = target.getBank().getBankTab()[i];
				if (i == tabId) {
					c.getPA().sendFrame36(700 + i, 1);
				} else {
					c.getPA().sendFrame36(700 + i, 0);
				}
				if (tab.getTabId() != 0 && tab.size() > 0 && tab.getItem(0) != null) {
					c.getPA().sendFrame171(0, 58050 + i);
					c.getPA().sendFrame34a(58040 + i, target.getBank().getBankTab()[i].getItem(0).getId() - 1, 0, target.getBank().getBankTab()[i].getItem(0).getAmount());
				} else if (i != 0) {
					if (newSlot == -1) {
						newSlot = i;
						c.getPA().sendFrame34a(58040 + i, -1, 0, 0);
						c.getPA().sendFrame171(0, 58050 + i);
						continue;
					}
					c.getPA().sendFrame34a(58040 + i, -1, 0, 0);
					c.getPA().sendFrame171(1, 58050 + i);
				}
			}
			c.getClient().getOutStream().createFrameVarSizeWord(53);
			c.getClient().getOutStream().writeWord(5382); // bank
			c.getClient().getOutStream().writeWord(Config.BANK_SIZE);
			BankTab tab = target.getBank().getCurrentBankTab();
			for (int i = 0; i < Config.BANK_SIZE; i++) {
				if (i > tab.size() - 1) {
					c.getClient().getOutStream().writeByte(0);
					c.getClient().getOutStream().writeWordBigEndianA(0);
					continue;
				} else {
					BankItem item = tab.getItem(i);
					if (item == null)
						item = new BankItem(-1, 0);
					if (item.getAmount() > 254) {
						c.getClient().getOutStream().writeByte(255);
						c.getClient().getOutStream().writeDWord_v2(item.getAmount());
					} else {
						c.getClient().getOutStream().writeByte(item.getAmount());
					}
					if (item.getAmount() < 1)
						item.setAmount(0);
					if (item.getId() > Config.ITEM_LIMIT || item.getId() < 0)
						item.setId(-1);
					c.getClient().getOutStream().writeWordBigEndianA(item.getId());
				}
			}
			c.getClient().getOutStream().endFrameVarSizeWord();
			c.getClient().flushOutStream();
			c.getPA().sendFrame126("" + target.getBank().getCurrentBankTab().size(), 58061);
			c.getPA().sendFrame126(Integer.toString(tabId), 5292);
			c.getPA().sendFrame126(Misc.capitalize(target.playerName.toLowerCase()) + "'s Bank.", 58064);
		} else {
			int tabId = c.getBank().getCurrentBankTab().getTabId();
			for (int i = 0; i < c.getBank().getBankTab().length; i++) {
				if (i == 0)
					continue;
				if (i != c.getBank().getBankTab().length - 1 && c.getBank().getBankTab()[i].size() == 0 && c.getBank().getBankTab()[i + 1].size() > 0) {
					for (BankItem item : c.getBank().getBankTab()[i + 1].getItems()) {
						c.getBank().getBankTab()[i].add(item);
					}
					c.getBank().getBankTab()[i + 1].getItems().clear();
				}
			}
			c.getPA().sendFrame36(700, 0);
			c.getPA().sendFrame34a(58040, -1, 0, 0);
			int newSlot = -1;
			for (int i = 0; i < c.getBank().getBankTab().length; i++) {
				BankTab tab = c.getBank().getBankTab()[i];
				if (i == tabId) {
					c.getPA().sendFrame36(700 + i, 1);
				} else {
					c.getPA().sendFrame36(700 + i, 0);
				}
				if (tab.getTabId() != 0 && tab.size() > 0 && tab.getItem(0) != null) {
					c.getPA().sendFrame171(0, 58050 + i);
					c.getPA().sendFrame34a(58040 + i, c.getBank().getBankTab()[i].getItem(0).getId() - 1, 0, c.getBank().getBankTab()[i].getItem(0).getAmount());
				} else if (i != 0) {
					if (newSlot == -1) {
						newSlot = i;
						c.getPA().sendFrame34a(58040 + i, -1, 0, 0);
						c.getPA().sendFrame171(0, 58050 + i);
						continue;
					}
					c.getPA().sendFrame34a(58040 + i, -1, 0, 0);
					c.getPA().sendFrame171(1, 58050 + i);
				}
			}
			c.getClient().getOutStream().createFrameVarSizeWord(53);
			c.getClient().getOutStream().writeWord(5382); // bank
			c.getClient().getOutStream().writeWord(Config.BANK_SIZE);
			BankTab tab = c.getBank().getCurrentBankTab();
			for (int i = 0; i < Config.BANK_SIZE; i++) {
				if (i > tab.size() - 1) {
					c.getClient().getOutStream().writeByte(0);
					c.getClient().getOutStream().writeWordBigEndianA(0);
					continue;
				} else {
					BankItem item = tab.getItem(i);
					if (item == null)
						item = new BankItem(-1, 0);
					if (item.getAmount() > 254) {
						c.getClient().getOutStream().writeByte(255);
						c.getClient().getOutStream().writeDWord_v2(item.getAmount());
					} else {
						c.getClient().getOutStream().writeByte(item.getAmount());
					}
					if (item.getAmount() < 1)
						item.setAmount(0);
					if (item.getId() > Config.ITEM_LIMIT || item.getId() < 0)
						item.setId(-1);
					c.getClient().getOutStream().writeWordBigEndianA(item.getId());
				}
			}
			c.getClient().getOutStream().endFrameVarSizeWord();
			c.getClient().flushOutStream();
			c.getPA().sendFrame126("" + c.getBank().getCurrentBankTab().size(), 58061);
			c.getPA().sendFrame126(Integer.toString(tabId), 5292);
			c.getPA().sendFrame126(Misc.capitalize(c.playerName.toLowerCase()) + "'s Bank.", 58064);
		}
	}

	public void resetTempItems() {
		
		Player target = c.getAttribute("viewing_other_bank", null);

		if (target != null) {
			int itemCount = 0;
			for (int i = 0; i < c.playerItems.length; i++) {
				if (c.playerItems[i] > -1) {
					itemCount = i;
				}
			}

			c.getClient().getOutStream().createFrameVarSizeWord(53);
			c.getClient().getOutStream().writeWord(5064);
			c.getClient().getOutStream().writeWord(itemCount + 1);
			for (int i = 0; i < target.playerItems.length; i++) {
				if (target.playerItemsN[i] > 254) {
					c.getClient().getOutStream().writeByte(255);
					c.getClient().getOutStream().writeDWord_v2(target.playerItemsN[i]);
				} else {
					c.getClient().getOutStream().writeByte(target.playerItemsN[i]);
				}
				if (c.playerItems[i] > Config.ITEM_LIMIT || c.playerItems[i] < 0) {
					c.playerItems[i] = Config.ITEM_LIMIT;
				}
				c.getClient().getOutStream().writeWordBigEndianA(target.playerItems[i]);
			}
		} else {
			int itemCount = 0;
			for (int i = 0; i < c.playerItems.length; i++) {
				if (c.playerItems[i] > -1) {
					itemCount = i;
				}
			}
			c.getClient().getOutStream().createFrameVarSizeWord(53);
			c.getClient().getOutStream().writeWord(5064);
			c.getClient().getOutStream().writeWord(itemCount + 1);
			for (int i = 0; i < itemCount + 1; i++) {
				if (c.playerItemsN[i] > 254) {
					c.getClient().getOutStream().writeByte(255);
					c.getClient().getOutStream().writeDWord_v2(c.playerItemsN[i]);
				} else {
					c.getClient().getOutStream().writeByte(c.playerItemsN[i]);
				}
				if (c.playerItems[i] > Config.ITEM_LIMIT || c.playerItems[i] < 0) {
					c.playerItems[i] = Config.ITEM_LIMIT;
				}
				c.getClient().getOutStream().writeWordBigEndianA(c.playerItems[i]);
			}
		}
		c.getClient().getOutStream().endFrameVarSizeWord();
		c.getClient().flushOutStream();
	}

	public int itemAmount(int itemID) {
		int tempAmount = 0;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] == itemID) {
				tempAmount += c.playerItemsN[i];
			}
		}
		return tempAmount;
	}

	public boolean isStackable(int itemID) {
		return Items.itemStackable[itemID];
	}

	/**
	 * Update Equip tab
	 **/

	public void setEquipment(int wearID, int amount, int targetSlot) {
		// synchronized(c) {
		c.getClient().getOutStream().createFrameVarSizeWord(34);
		c.getClient().getOutStream().writeWord(1688);
		c.getClient().getOutStream().writeByte(targetSlot);
		c.getClient().getOutStream().writeWord(wearID + 1);
		if (amount > 254) {
			c.getClient().getOutStream().writeByte(255);
			c.getClient().getOutStream().writeDWord(amount);
		} else {
			c.getClient().getOutStream().writeByte(amount);
		}
		c.getClient().getOutStream().endFrameVarSizeWord();
		c.getClient().flushOutStream();
		c.getEquipment()[targetSlot] = wearID;
		c.playerEquipmentN[targetSlot] = amount;
		c.updateRequired = true;
		c.setAppearanceUpdateRequired(true);
	}

	/**
	 * Move Items
	 **/

	public void moveItems(int from, int to, int moveWindow, boolean insertMode) {
		if (moveWindow == 3214) {
			int tempI;
			int tempN;
			tempI = c.playerItems[from];
			tempN = c.playerItemsN[from];

			c.playerItems[from] = c.playerItems[to];
			c.playerItemsN[from] = c.playerItemsN[to];
			c.playerItems[to] = tempI;
			c.playerItemsN[to] = tempN;
			resetItems(3214);
		}
		if (moveWindow == 5382) {
			if (!c.isBanking) {
				c.getPA().removeAllWindows();
				resetBank();
				return;
			}

			if (c.getBank().getBankSearch().isSearching()) {
				c.getBank().getBankSearch().reset();
				return;
			}
			if (c.getBankPin().requiresUnlock()) {
				resetBank();
				c.isBanking = false;
				c.getBankPin().open(2);
				return;
			}
			if (to > 999) {
				int tabId = to - 1000;
				if (tabId < 0)
					tabId = 0;
				if (tabId == c.getBank().getCurrentBankTab().getTabId()) {
					c.sendMessage("You cannot add an item from it's tab to the same tab.");
					resetBank();
					return;
				}
				if (from > c.getBank().getCurrentBankTab().size()) {
					resetBank();
					return;
				}
				BankItem item = c.getBank().getCurrentBankTab().getItem(from);
				if (item == null) {
					resetBank();
					return;
				}
				c.getBank().getCurrentBankTab().remove(item);
				c.getBank().getBankTab()[tabId].add(item);
			} else {
				if (from > c.getBank().getCurrentBankTab().size() - 1 || to > c.getBank().getCurrentBankTab().size() - 1) {
					resetBank();
					return;
				}
				if (!insertMode) {
					BankItem item = c.getBank().getCurrentBankTab().getItem(from);
					c.getBank().getCurrentBankTab().setItem(from, c.getBank().getCurrentBankTab().getItem(to));
					c.getBank().getCurrentBankTab().setItem(to, item);
				} else {
					int tempFrom = from;
					for (int tempTo = to; tempFrom != tempTo;)
						if (tempFrom > tempTo) {
							c.getBankHandler().swapBankItem(tempFrom, tempFrom - 1);
							tempFrom--;
						} else if (tempFrom < tempTo) {
							c.getBankHandler().swapBankItem(tempFrom, tempFrom + 1);
							tempFrom++;
						}
				}
			}
		}
		if (moveWindow == 5382) {
			c.getItems().resetBank();
		}
		if (moveWindow == 5064) {
			int tempI;
			int tempN;
			tempI = c.playerItems[from];
			tempN = c.playerItemsN[from];

			c.playerItems[from] = c.playerItems[to];
			c.playerItemsN[from] = c.playerItemsN[to];
			c.playerItems[to] = tempI;
			c.playerItemsN[to] = tempN;
		}
		resetTempItems();

	}

	/**
	 * delete Item
	 **/

	public void deleteEquipment(int i, int j) {
		// synchronized(c) {
		if (World.players[c.playerId] == null) {
			return;
		}
		if (i < 0) {
			return;
		}

		c.getEquipment()[j] = -1;
		c.playerEquipmentN[j] = c.playerEquipmentN[j] - 1;
		c.getClient().getOutStream().createFrame(34);
		c.getClient().getOutStream().writeWord(6);
		c.getClient().getOutStream().writeWord(1688);
		c.getClient().getOutStream().writeByte(j);
		c.getClient().getOutStream().writeWord(0);
		c.getClient().getOutStream().writeByte(0);
		if (j == PlayerConstants.PLAYER_WEAPON) {
			sendWeapon(-1, "Unarmed");
		}
		EquipmentBonuses.resetPlayerBonuses(c);
		EquipmentBonuses.getItemBonuses(c);
		EquipmentBonuses.writeItemBonuses(c);
		c.updateRequired = true;
		c.setAppearanceUpdateRequired(true);
	}

	public void deleteItem(int id, int amount) {
		if (id <= 0)
			return;
		for (int j = 0; j < c.playerItems.length; j++) {
			if (amount <= 0)
				break;
			if (c.playerItems[j] == id + 1) {
				c.playerItems[j] = 0;
				c.playerItemsN[j] = 0;
				amount--;
			}
		}
		resetItems(3214);
	}

	public void deleteItem(int id, int slot, int amount) {
		if (id <= 0 || slot < 0) {
			return;
		}
		if (c.playerItems[slot] == (id + 1)) {
			if (c.playerItemsN[slot] > amount) {
				c.playerItemsN[slot] -= amount;
			} else {
				c.playerItemsN[slot] = 0;
				c.playerItems[slot] = 0;
			}
			resetItems(3214);
		}
	}

	public void deleteItem2(int id, int amount) {
		int am = amount;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (am == 0) {
				break;
			}
			if (c.playerItems[i] == (id + 1)) {
				if (c.playerItemsN[i] > amount) {
					c.playerItemsN[i] -= amount;
					break;
				} else {
					c.playerItems[i] = 0;
					c.playerItemsN[i] = 0;
					am--;
				}
			}
		}
		resetItems(3214);
	}

	/**
	 * Delete Arrows
	 **/
	public void deleteArrow() {
		// synchronized(c) {
		if (c.getEquipment()[PlayerConstants.PLAYER_CAPE] == 10499 && Misc.random(100) < 70 && c.getEquipment()[PlayerConstants.PLAYER_ARROWS] != 4740)
			return;
		if (c.playerEquipmentN[PlayerConstants.PLAYER_ARROWS] == 1) {
			c.getItems().deleteEquipment(c.getEquipment()[PlayerConstants.PLAYER_ARROWS], PlayerConstants.PLAYER_ARROWS);
		}
		if (c.playerEquipmentN[PlayerConstants.PLAYER_ARROWS] != 0) {
			c.getClient().getOutStream().createFrameVarSizeWord(34);
			c.getClient().getOutStream().writeWord(1688);
			c.getClient().getOutStream().writeByte(PlayerConstants.PLAYER_ARROWS);
			c.getClient().getOutStream().writeWord(c.getEquipment()[PlayerConstants.PLAYER_ARROWS] + 1);
			if (c.playerEquipmentN[PlayerConstants.PLAYER_ARROWS] - 1 > 254) {
				c.getClient().getOutStream().writeByte(255);
				c.getClient().getOutStream().writeDWord(c.playerEquipmentN[PlayerConstants.PLAYER_ARROWS] - 1);
			} else {
				c.getClient().getOutStream().writeByte(c.playerEquipmentN[PlayerConstants.PLAYER_ARROWS] - 1);
			}
			c.getClient().getOutStream().endFrameVarSizeWord();
			c.getClient().flushOutStream();
			c.playerEquipmentN[PlayerConstants.PLAYER_ARROWS] -= 1;
		}
		c.updateRequired = true;
		c.setAppearanceUpdateRequired(true);
	}

	public void deleteEquipment() {
		// synchronized(c) {
		if (c.playerEquipmentN[PlayerConstants.PLAYER_WEAPON] == 1) {
			c.getItems().deleteEquipment(c.getEquipment()[PlayerConstants.PLAYER_WEAPON], PlayerConstants.PLAYER_WEAPON);
		}
		if (c.playerEquipmentN[PlayerConstants.PLAYER_WEAPON] != 0) {
			c.getClient().getOutStream().createFrameVarSizeWord(34);
			c.getClient().getOutStream().writeWord(1688);
			c.getClient().getOutStream().writeByte(PlayerConstants.PLAYER_WEAPON);
			c.getClient().getOutStream().writeWord(c.getEquipment()[PlayerConstants.PLAYER_WEAPON] + 1);
			if (c.playerEquipmentN[PlayerConstants.PLAYER_WEAPON] - 1 > 254) {
				c.getClient().getOutStream().writeByte(255);
				c.getClient().getOutStream().writeDWord(c.playerEquipmentN[PlayerConstants.PLAYER_WEAPON] - 1);
			} else {
				c.getClient().getOutStream().writeByte(c.playerEquipmentN[PlayerConstants.PLAYER_WEAPON] - 1);
			}
			c.getClient().getOutStream().endFrameVarSizeWord();
			c.getClient().flushOutStream();
			c.playerEquipmentN[PlayerConstants.PLAYER_WEAPON] -= 1;
		}
		c.updateRequired = true;
		c.setAppearanceUpdateRequired(true);
	}

	/**
	 * Dropping Arrows
	 **/

	public void dropArrowNpc() {
		if (c.getEquipment()[PlayerConstants.PLAYER_CAPE] == 10499)
			return;
		int enemyX = NPCHandler.npcs[c.oldNpcIndex].getX();
		int enemyY = NPCHandler.npcs[c.oldNpcIndex].getY();
		if (Misc.random(10) >= 4) {
			if (Server.itemHandler.itemAmount(c.getUsername(), c.rangeItemUsed, enemyX, enemyY) == 0) {
				Server.itemHandler.createGroundItem(c, c.rangeItemUsed, enemyX, enemyY, 1, c.getId());
			} else if (Server.itemHandler.itemAmount(c.getUsername(), c.rangeItemUsed, enemyX, enemyY) != 0) {
				int amount = Server.itemHandler.itemAmount(c.getUsername(), c.rangeItemUsed, enemyX, enemyY);
				Server.itemHandler.removeGroundItem(c, c.rangeItemUsed, enemyX, enemyY, false);
				Server.itemHandler.createGroundItem(c, c.rangeItemUsed, enemyX, enemyY, amount + 1, c.getId());
			}
		}
	}

	public void dropArrowPlayer() {
		int enemyX = World.players[c.oldPlayerIndex].getX();
		int enemyY = World.players[c.oldPlayerIndex].getY();
		if (c.getEquipment()[PlayerConstants.PLAYER_CAPE] == 10499)
			return;
		if (Misc.random(10) >= 4) {
			if (Server.itemHandler.itemAmount(c.getUsername(), c.rangeItemUsed, enemyX, enemyY) == 0) {
				Server.itemHandler.createGroundItem(c, c.rangeItemUsed, enemyX, enemyY, 1, c.getId());
			} else if (Server.itemHandler.itemAmount(c.getUsername(), c.rangeItemUsed, enemyX, enemyY) != 0) {
				int amount = Server.itemHandler.itemAmount(c.getUsername(), c.rangeItemUsed, enemyX, enemyY);
				Server.itemHandler.removeGroundItem(c, c.rangeItemUsed, enemyX, enemyY, false);
				Server.itemHandler.createGroundItem(c, c.rangeItemUsed, enemyX, enemyY, amount + 1, c.getId());
			}
		}
	}

	public void removeAllItems() {
		for (int i = 0; i < c.playerItems.length; i++) {
			c.playerItems[i] = 0;
			c.playerItemsN[i] = 0;
		}
		resetItems(3214);
	}

	public int freeSlots() {
		int freeS = 0;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] <= 0) {
				freeS++;
			}
		}
		return freeS;
	}

	public int findItem(int id, int[] items, int[] amounts) {
		for (int i = 0; i < c.playerItems.length; i++) {
			if (((items[i] - 1) == id) && (amounts[i] > 0)) {
				return i;
			}
		}
		return -1;
	}

	public static String getItemName(int itemId) {
		if (itemId > -1) {
			if (ItemDefinitions.forId(itemId) != null) {
				if (ItemDefinitions.forId(itemId).getName() != null) {
					return ItemDefinitions.forId(itemId).getName();
				}
			}
		}
		return "Unarmed";
	}

	public int getItemSlot(int ItemID) {
		for (int i = 0; i < c.playerItems.length; i++) {
			if ((c.playerItems[i] - 1) == ItemID) {
				return i;
			}
		}
		return -1;
	}

	public void removeEquipment(int slot) {
		boolean godArmourChanged = false;

		if (c.getClient().getOutStream() == null || c == null) {
			return;
		}
		if (c.getEquipment()[slot] < 0) {
			return;
		}
		c.getEquipment()[slot] = -1;
		c.playerEquipmentN[slot] = 0;
		sendWeapon(c.getEquipment()[PlayerConstants.PLAYER_WEAPON], getItemName(c.getEquipment()[PlayerConstants.PLAYER_WEAPON]));
		EquipmentBonuses.writeItemBonuses(c);
		c.getCombat().getPlayerAnimIndex();
		c.getClient().getOutStream().createFrame(34);
		c.getClient().getOutStream().writeWord(6);
		c.getClient().getOutStream().writeWord(1688);
		c.getClient().getOutStream().writeByte(slot);
		c.getClient().getOutStream().writeWord(0);
		c.getClient().getOutStream().writeByte(0);
		c.updateRequired = true;
		c.setAppearanceUpdateRequired(true);
		if (godArmourChanged && c.getLevel()[3] > c.getPA().getLevelForXP(c.getExperience()[3])) {
			c.getLevel()[3] = c.getPA().getLevelForXP(c.getExperience()[3]);
			c.getPA().refreshSkill(3);
		}
	}

	public int getItemAmount(int ItemID) {
		int itemCount = 0;
		for (int i = 0; i < c.playerItems.length; i++) {
			if ((c.playerItems[i] - 1) == ItemID) {
				itemCount += c.playerItemsN[i];
			}
		}
		return itemCount;
	}

	public boolean playerHasItem(int itemID, int amt, int slot) {
		itemID++;
		int found = 0;
		if (c.playerItems[slot] == (itemID)) {
			for (int i = 0; i < c.playerItems.length; i++) {
				if (c.playerItems[i] == itemID) {
					if (c.playerItemsN[i] >= amt) {
						return true;
					} else {
						found++;
					}
				}
			}
			if (found >= amt) {
				return true;
			}
			return false;
		}
		return false;
	}

	public boolean playerHasItem(int itemID) {
		itemID++;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] == itemID)
				return true;
		}
		return false;
	}

	public boolean playerHasItem(int itemID, int amt) {
		itemID++;
		int found = 0;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] == itemID) {
				if (c.playerItemsN[i] >= amt) {
					return true;
				} else {
					found++;
				}
			}
		}
		if (found >= amt) {
			return true;
		}
		return false;
	}

	public int getUnnotedItem(int ItemID) {
		int NewID = ItemID - 1;
		String NotedName = "";
		if (ItemID > 0) {
			if (ItemDefinitions.forId(ItemID) != null) {
				if (ItemDefinitions.forId(ItemID).getId() == ItemID) {
					NotedName = ItemDefinitions.forId(ItemID).getName();
				}
			}
		}
		if (ItemID > 0) {
			if (ItemDefinitions.forId(ItemID) != null) {
				if (ItemDefinitions.forId(ItemID).getName() == NotedName) {
					if (ItemDefinitions.forId(ItemID).getExamine().startsWith("Swap this note at any bank for a") == false) {
						NewID = ItemDefinitions.forId(ItemID).getId();
					}
				}
			}
		}
		return NewID;
	}

	/**
	 * Drop Item
	 **/

	public void createGroundItem(int itemID, int itemX, int itemY, int itemAmount) {
		c.getClient().getOutStream().createFrame(85);
		c.getClient().getOutStream().writeByteC((itemY - 8 * c.mapRegionY));
		c.getClient().getOutStream().writeByteC((itemX - 8 * c.mapRegionX));
		c.getClient().getOutStream().createFrame(44);
		c.getClient().getOutStream().writeWordBigEndianA(itemID);
		c.getClient().getOutStream().writeWord(itemAmount);
		c.getClient().getOutStream().writeByte(0);
	}

	/**
	 * Pickup Item
	 **/

	public void removeGroundItem(int itemID, int itemX, int itemY, int Amount) {
		if (c == null) {
			return;
		}
		c.getClient().getOutStream().createFrame(85);
		c.getClient().getOutStream().writeByteC((itemY - 8 * c.mapRegionY));
		c.getClient().getOutStream().writeByteC((itemX - 8 * c.mapRegionX));
		c.getClient().getOutStream().createFrame(156);
		c.getClient().getOutStream().writeByteS(0);
		c.getClient().getOutStream().writeWord(itemID);
	}

	public boolean ownsCape() {
		if (c.getItems().playerHasItem(2412, 1) || c.getItems().playerHasItem(2413, 1) || c.getItems().playerHasItem(2414, 1))
			return true;
		for (int j = 0; j < Config.BANK_SIZE; j++) {
			// if (c.bankItems[j] == 2412 || c.bankItems[j] == 2413 ||
			// c.bankItems[j] == 2414)
			// return true;
		}
		if (c.getEquipment()[PlayerConstants.PLAYER_CAPE] == 2413 || c.getEquipment()[PlayerConstants.PLAYER_CAPE] == 2414 || c.getEquipment()[PlayerConstants.PLAYER_CAPE] == 2415)
			return true;
		return false;
	}

	public boolean hasAllShards() {
		return playerHasItem(11712, 1) && playerHasItem(11712, 1) && playerHasItem(11714, 1);
	}

	public void makeBlade() {
		deleteItem(11710, 1);
		deleteItem(11712, 1);
		deleteItem(11714, 1);
		addItem(11690, 1);
		c.sendMessage("You combine the shards to make a blade.");
	}

	public void makeGodsword(int i) {
		if (playerHasItem(11690) && playerHasItem(i)) {
			deleteItem(11690, 1);
			deleteItem(i, 1);
			addItem(i - 8, 1);
			c.sendMessage("You combine the hilt and the blade to make a godsword.");
		}
	}

	public boolean isHilt(int i) {
		return i >= 11702 && i <= 11708 && i % 2 == 0;
	}

}