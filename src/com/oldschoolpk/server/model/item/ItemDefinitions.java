package com.oldschoolpk.server.model.item;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Logger;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.core.tools.XStreamUtil;

public class ItemDefinitions {

	/**
	 * Java logger
	 */
	private static final Logger logger = Logger.getLogger(ItemDefinitions.class
			.getName());

	/**
	 * Represents this class' instance
	 */
	private static ItemDefinitions[] def = null;

	/**
	 * ItemDefintions class getter
	 * 
	 * @param itemId
	 * @return
	 */
	public static ItemDefinitions forId(int itemId) {
		if (itemId < 0 || itemId >= def.length) {
			return null;
		}
		return def[itemId];
	}

	/**
	 * Reads all the item definitions
	 * 
	 * @throws FileNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static void init() throws FileNotFoundException {
		logger.info("Loading item definitions...");
		List<ItemDefinitions> itemList = (List<ItemDefinitions>) XStreamUtil
				.getxStream().fromXML(
						new FileInputStream("./data/itemdefinitions.xml"));
		def = new ItemDefinitions[Config.ITEM_LIMIT];
		for (ItemDefinitions defs : itemList) {
			def[defs.getId()] = defs;
		}
		logger.info(def[4151].getName());
		logger.info(def.length + " item definitions has been unpacked...");
	}

	/**
	 * Respresents the items id
	 */
	private int id;

	/**
	 * Represents the items name
	 */
	private String name;

	/**
	 * Respresents the items description
	 */
	private String examine;

	/**
	 * States wether this item can be noted or not
	 */
	private boolean noted;

	/**
	 * States wether this item is stackable or not
	 */
	private boolean stackable;

	/**
	 * Represents the items note id
	 */
	private int noteId;

	/**
	 * Respresents the items shop value
	 */
	private int shopValue;

	/**
	 * 
	 * @param id
	 * @param name
	 * @param examine
	 * @param noted
	 * @param stackable
	 * @param noteId
	 * @param shopValue
	 */
	private ItemDefinitions(int id, String name, String examine, boolean noted,
			boolean stackable, int noteId, int shopValue) {
		this.id = id;
		this.name = name;
		this.examine = examine;
		this.noted = noted;
		this.stackable = stackable;
		this.noteId = noteId;
		this.shopValue = shopValue;
	}

	/**
	 * id getter
	 * 
	 * @return
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Name getter
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Examine getter
	 * 
	 * @return
	 */
	public String getExamine() {
		return this.examine;
	}

	/**
	 * Noted getter
	 * 
	 * @return
	 */
	public boolean isNoteable() {
		return this.noted;
	}

	/**
	 * Stackable getter
	 * 
	 * @return
	 */
	public boolean isStackable() {
		return this.stackable;
	}

	/**
	 * Noteid getter
	 * 
	 * @return
	 */
	public int getNoteId() {
		return this.noteId;
	}

	/**
	 * Shopvalue getter
	 * 
	 * @return
	 */
	public int getShopValue() {
		return this.shopValue;
	}

	public static boolean isStackable(int itemId) {
		if (forId(itemId) != null) {
			if (forId(itemId).isStackable()) {
				return true;
			}
		}
		return false;
	}
}
