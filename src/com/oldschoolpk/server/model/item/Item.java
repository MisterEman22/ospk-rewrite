package com.oldschoolpk.server.model.item;

public class Item {

	private int id;
	private int amount;
	
	public Item(int id) {
		this(id, 1);
	}
	
	public Item(int id, int amount) {
		this.id = id;
		this.amount = amount;
	}
	
	public int getId() {
		return id;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
}