package com.oldschoolpk.server.model.item;

import com.oldschoolpk.server.model.content.ItemCombining;
import com.oldschoolpk.server.model.content.ItemSets;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.util.cache.defs.ItemDef;

/**
 * @author Ryan / Lmctruck30
 */

public class UseItem {

    public static void ItemonObject(Player c, int objectID, int objectX, int objectY, int itemId) {
        if (!c.getItems().playerHasItem(itemId, 1))
            return;
        if (objectID == 11744 || objectID == 6943) {
            if (ItemDef.forId(itemId).getName().toLowerCase().contains("armour set")) {
                ItemSets.exchange(c, itemId);
                return;
            }
            ItemSets.exchangeBack(c, itemId);
        }
        switch (objectID) {

        default:
            if (c.getRights().isSuperior(PlayerRights.ADMINISTRATOR))
                Misc.println("Player At Object id: " + objectID + " with Item id: " + itemId);
            break;
        }

    }

    public static void ItemonItem(Player c, int itemUsed, int useWith) {
        ItemCombining.combineItems(c, itemUsed, useWith);
        switch (itemUsed) {

        default:
            if (c.getRights().isSuperior(PlayerRights.ADMINISTRATOR))
                Misc.println("Player used Item id: " + itemUsed + " with Item id: " + useWith);
            break;
        }
    }

    public static void ItemonNpc(Player c, int itemId, int npcId, int slot) {
        switch (itemId) {

        default:
            if (c.getRights().isSuperior(PlayerRights.ADMINISTRATOR))
                Misc.println("Player used Item id: " + itemId + " with Npc id: " + npcId + " With Slot : " + slot);
            break;
        }

    }

}
