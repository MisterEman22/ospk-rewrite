package com.oldschoolpk.server.model.player;

import com.oldschoolpk.server.util.Misc;

/**
 * Handles the donator rights a player can contain
 * 
 * @author Arithium
 *
 */
public enum DonatorRights implements Rights {
	NONE(0, -1, "", 0),
	PREMIUM(4, 10, "<shad=FF7F00>", 1),
	SUPER_PREMIUM(5, 5, "<shad=0><col=787878>", 2),
	EXTREME_PREMIUM(6, 0, "<col=D9D919><shad=0>", 3),
	LEGENDARY_PREMIUM(4, 0, "<shad=697998>", 4),
	ULTIMATE_PREMIUM(5, 0, "", 5);

	/**
	 * The id of the crown to display
	 */
	private int crown;

	/**
	 * The delay in seconds between each yell
	 */
	private int yellDelay;

	/**
	 * The color of the yell title
	 */
	private String yellColor;

	/**
	 * The priority of the rank
	 */
	private int priority;

	private DonatorRights(int crown, int yellDelay, String yellColor, int priority) {
		this.crown = crown;
		this.yellDelay = yellDelay;
		this.yellColor = yellColor;
		this.priority = priority;
	}
	
	public boolean isDonator() {
		return this != DonatorRights.NONE;
	}

	@Override
	public String yellColor() {
		return yellColor;
	}

	@Override
	public int getCrownId() {
		return crown;
	}

	@Override
	public int yellDelay() {
		return yellDelay;
	}

	@Override
	public int priority() {
		return priority;
	}

}
