package com.oldschoolpk.server.model.player;

import com.oldschoolpk.server.util.Misc;

/**
 * Represents a set of rights a player can have
 * 
 * @author Arithium
 *
 */
public interface Rights {

	/**
	 * The priority of the rank
	 * 
	 * @return The priority of the rank
	 */
	public int priority();

	/**
	 * The yell color of the rank
	 * 
	 * @return The yell color of the rank
	 */
	public String yellColor();

	/**
	 * Gets the crown id to display client sided
	 * 
	 * @return The client id to display
	 */
	public int getCrownId();

	/**
	 * Gets the delay between each yell
	 * 
	 * @return The delay between each yell
	 */
	public int yellDelay();

	/**
	 * Checks if these rights are superior to {@link Rights}
	 * 
	 * @param rights
	 *            The {@link Rights} to check if is superior too
	 * @return If these rights are superior to the provided {@link Rights}
	 */
	public default boolean isSuperior(Rights rights) {
		return priority() > rights.priority();
	}

	/**
	 * Checks if these rights are inferior to {@link Rights}
	 * 
	 * @param rights
	 *            The {@link Rights} to compare these rights too
	 * @return If these rights are inferior to the provided {@link Rights}
	 */
	public default boolean isInferior(Rights rights) {
		return priority() < rights.priority();
	}
	
	public default String getTitle() {
		return Misc.formatPlayerName(toString().replaceAll("_", " "));
	}

}