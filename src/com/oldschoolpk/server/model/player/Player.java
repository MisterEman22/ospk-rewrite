package com.oldschoolpk.server.model.player;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.constants.Skills;
import com.oldschoolpk.server.core.cycle.CycleEvent;
import com.oldschoolpk.server.core.cycle.CycleEventContainer;
import com.oldschoolpk.server.core.cycle.CycleEventHandler;
import com.oldschoolpk.server.model.ActionHandler;
import com.oldschoolpk.server.model.Animation;
import com.oldschoolpk.server.model.Client;
import com.oldschoolpk.server.model.Entity;
import com.oldschoolpk.server.model.Graphic;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.content.QuestTab;
import com.oldschoolpk.server.model.content.ShopAssistant;
import com.oldschoolpk.server.model.content.TradeHandler;
import com.oldschoolpk.server.model.content.combat.CombatAssistant;
import com.oldschoolpk.server.model.content.combat.Experience;
import com.oldschoolpk.server.model.content.combat.SpellData;
import com.oldschoolpk.server.model.content.dialogue.DialogueManager;
import com.oldschoolpk.server.model.content.dialogue.impl.PinDialogue;
import com.oldschoolpk.server.model.content.minigame.Duelling;
import com.oldschoolpk.server.model.content.pet.Pets;
import com.oldschoolpk.server.model.item.GameItem;
import com.oldschoolpk.server.model.item.ItemAssistant;
import com.oldschoolpk.server.model.item.Items;
import com.oldschoolpk.server.model.item.bank.Bank;
import com.oldschoolpk.server.model.item.bank.BankHandler;
import com.oldschoolpk.server.model.item.bank.BankPin;
import com.oldschoolpk.server.model.item.equipment.EquipmentBonuses;
import com.oldschoolpk.server.model.npc.NPC;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.net.ConnectionSet;
import com.oldschoolpk.server.util.ISAACRandomGen;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.util.StopWatch;
import com.oldschoolpk.server.util.Stream;
import com.oldschoolpk.server.world.Clan;

import lombok.Getter;
import lombok.Setter;

public class Player extends Entity {

	private ItemAssistant itemAssistant = new ItemAssistant(this);
	private ShopAssistant shopAssistant = new ShopAssistant(this);
	private PlayerAssistant playerAssistant = new PlayerAssistant(this);
	private CombatAssistant combatAssistant = new CombatAssistant(this);
	private ActionHandler actionHandler = new ActionHandler(this);
	private DialogueManager dialogueManager = new DialogueManager(this);
	private TradeHandler tradeHandler = new TradeHandler(this);
	private BankHandler bankHandler = new BankHandler(this);
	private Duelling duelling = new Duelling(this);
	private Bank bank = new Bank(this);
	private BankPin pin = new BankPin(this);

	private @Getter @Setter Stream cachedBlock;

	@Getter
	private Pets pets = new Pets(this);

	@Getter
	private Client client;

	public int pathX, pathY;

	public int teleportToHeight;

	public int followingX, followingY;

	public boolean checkHeight = false;

	public int tileIndex;

	public long lastStuck;

	private long usernameHash;

	private @Getter @Setter int brightness;

	private @Getter @Setter boolean splitChat = true;

	private PlayerRights rights = PlayerRights.PLAYER;

	private DonatorRights donatorRights = DonatorRights.NONE;

	public ArrayList<String> killedPlayers = new ArrayList<String>();
	public ArrayList<Integer> attackedPlayers = new ArrayList<Integer>();

	public ArrayList<GameItem> itemsToBuy = new ArrayList<GameItem>();

	public long lastButton;

	public ArrayList<String> lastKilledPlayers = new ArrayList<String>();

	public long lastCast = 0;

	private int petId = -1;

	private int petIndex = -1;

	public long lastRuleSelected;
	public boolean hideMe;
	private long waitTime;

	public boolean hasPid = false;

	public int[] keepItems = new int[4];
	public int[] keepItemsN = new int[4];
	public int willKeepAmt1, willKeepAmt2, willKeepAmt3, willKeepAmt4, willKeepItem1, willKeepItem2, willKeepItem3,
			willKeepItem4, willKeepItem1Slot, willKeepItem2Slot, willKeepItem3Slot, willKeepItem4Slot, equipStatus;
	public boolean teleporting;

	private final StopWatch sqlTimer = new StopWatch();

	private int tzhaarWaveId;
	private int tzhaarToKill = 0;

	/**
	 * Duelling features.
	 */
	public int duelCount, duelTimer, duelTeleX, duelTeleY, duelSlot, duelSpaceReq, duelOption, duelingWith, duelStatus;

	public long duelDelay;

	public boolean duelRequested, inDuel = false;
	public boolean[] duelRule = new boolean[22];
	public final int[] DUEL_RULE_ID = new int[] { 1, 2, 16, 32, 64, 128, 256, 512, 1024, 4096, 8192, 16384, '\u8000',
			65536, 131072, 262144, 524288, 2097152, 8388608, 16777216, 67108864, 134217728 };

	public String connectedFrom = "";
	public String globalMessage = "";

	public int playerId = -1;
	public String playerName = null;
	public String playerName2 = null;
	public String playerPass = null;
	public int playerItems[] = new int[28];
	public int playerItemsN[] = new int[28];
	public boolean bankNotes = false;

	public int playerStandIndex = 0x328;
	public int playerTurnIndex = 0x337;
	public int playerWalkIndex = 0x333;
	public int playerTurn180Index = 0x334;
	public int playerTurn90CWIndex = 0x335;
	public int playerTurn90CCWIndex = 0x336;
	public int playerRunIndex = 0x338;

	public int[] playerEquipment = new int[14];

	public int[] playerEquipmentN = new int[14];
	private int[] playerLevel = new int[25];

	private int[] playerXP = new int[25];

	public static final int maxPlayerListSize = 255;
	public Player playerList[] = new Player[maxPlayerListSize];
	public int playerListSize = 0;

	public byte playerInListBitmap[] = new byte[(Config.MAX_PLAYERS + 7) >> 3];

	public static final int maxNPCListSize = 255;
	public NPC npcList[] = new NPC[maxNPCListSize];
	public int npcListSize = 0;

	public byte npcInListBitmap[] = new byte[(NPCHandler.MAX_NPCS + 7) >> 3];

	public final List<Player> localPlayers = new LinkedList<Player>();
	public final List<NPC> localNpcs = new LinkedList<NPC>();

	public boolean initialized = false, disconnected = false, ruleAgreeButton = false, RebuildNPCList = false,
			isActive = false, isKicked = false, isSkulled = false, friendUpdate = false, newPlayer = false,
			hasMultiSign = false, saveCharacter = false, mouseButton = false, chatEffects = true, acceptAid = false,
			nextDialogue = false, autocasting = false, usedSpecial = false, mageFollow = false, dbowSpec = false,
			craftingLeather = false, properLogout = false, secDbow = false, maxNextHit = false, ssSpec = false,
			vengOn = false, addStarter = false, accountFlagged = false, msbSpec = false, isBanking = false,
			armadylSpecial = false, itemInsert = false, experienceLock = false;

	public int saveDelay, playerKilled, pkPoints, totalPlayerDamageDealt, killedBy, lastChatId = 1, privateChat,
			friendSlot = 0, randomCoffin, newLocation, specEffect, specBarId, attackLevelReq, defenceLevelReq,
			strengthLevelReq, rangeLevelReq, magicLevelReq, prayerLevelReq, followId, skullTimer, autocastId,
			followDistance, followId2, barrageCount = 0, delayedDamage = 0, delayedDamage2 = 0, pcPoints = 0,
			magePoints = 0, desertTreasure = 0, lastArrowUsed = -1, autoRet = 0, pcDamage = 0, xInterfaceId = 0,
			xRemoveId = 0, xRemoveSlot = 0, frozenBy = 0, poisonDamage = 0, venomDamage, bonusAttack = 0,
			lastNpcAttacked = 0, killCount = 0, osrsPoints = 0, kills = 0, deaths = 0, vengeanceIndex = 0,
			lastFlowerPlanted = 0, flowerX, flowerY, vengTimer;

	public int mapRegionX, mapRegionY;
	public int absX, absY;
	public int currentX, currentY;

	public int heightLevel;
	public int playerSE = 0x328;
	public int playerSEW = 0x333;
	public int playerSER = 0x334;

	public boolean updateRequired = true;

	public final int walkingQueueSize = 50;
	public int walkingQueueX[] = new int[walkingQueueSize], walkingQueueY[] = new int[walkingQueueSize];
	public int wQueueReadPtr = 0;
	public int wQueueWritePtr = 0;
	public boolean isRunning = true;
	public int teleportToX = -1, teleportToY = -1;

	public int killStreak = 0;
	public int votePoints = 0;
	public int donationPoints = 0;
	public double donationAmount = 0;
	public int bounty = 0;

	public int dbowIndex, dbowIndex1, dbowTimer;

	private int warnIndex = -1;

	public long muteTime;

	public int skillToChange = -1;

	private int[] hits = new int[100];
	public int[] damageTaken = new int[Config.MAX_PLAYERS];
	public int[] damage = new int[4];

	public int combatStyle = 0;

	/**
	 * Clan Chat Variables
	 */
	public boolean inOSRSPVPCC;
	public Clan clan;
	public String clanName, properName;
	public String lastClanChat = "";

	public int[] voidStatus = new int[5];
	public int[] itemKeptId = new int[4];
	public int[] pouches = new int[4];
	public final int[] POUCH_SIZE = { 3, 6, 9, 12 };
	public boolean[] invSlot = new boolean[28], equipSlot = new boolean[14];
	public long friends[] = new long[200];
	public long ignores[] = new long[200];
	public double specAmount = 10;
	public double specAccuracy = 1;
	public double specDamage = 1;
	public double prayerPoint = 1.0;
	public int teleGrabItem, teleGrabX, teleGrabY, underAttackBy, underAttackBy2, wildLevel, teleTimer, respawnTimer,
			saveTimer = 0, teleBlockLength, poisonDelay, venomDelay;
	public long lastPlayerMove, lastPoison, lastVenom, venomImmunity, lastPoisonSip, poisonImmune, lastSpear,
			lastProtItem, dfsDelay, lastVeng, lastYell, teleGrabDelay, protMageDelay, protMeleeDelay, protRangeDelay,
			lastAction, lastThieve, lastLockPick, alchDelay, specDelay = System.currentTimeMillis(), teleBlockDelay,
			godSpellDelay, singleCombatDelay, singleCombatDelay2, reduceStat, restoreStatsDelay, logoutDelay,
			actionDelay, foodDelay, potDelay, lastBankDeposit, stofdDelay;
	public boolean canChangeAppearance = false;
	public boolean mageAllowed;
	public byte poisonMask = 0, venomMask = 0;

	public int DirectionCount = 0;
	public boolean appearanceUpdateRequired = true;
	public int hitDiff2;
	public int hitDiff = 0;
	public boolean hitUpdateRequired2;
	public boolean hitUpdateRequired = false;
	public boolean isDead = false;

	public int npcId2 = 0;
	public boolean isNpc;

	protected static Stream playerProps;
	public String playerTitle = "";
	public long attackableTimer;
	public int[][] barrowsNpcs = { { 2030, 0 }, // verac
			{ 2029, 0 }, // toarg
			{ 2028, 0 }, // karil
			{ 2027, 0 }, // guthan
			{ 2026, 0 }, // dharok
			{ 2025, 0 } // ahrim
	};
	public int barrowsKillCount;

	public int reduceSpellId;
	public final int[] REDUCE_SPELL_TIME = { 250000, 250000, 250000, 500000, 500000, 500000 };
	public long[] reduceSpellDelay = new long[6];
	public final int[] REDUCE_SPELLS = { 1153, 1157, 1161, 1542, 1543, 1562 };
	public boolean[] canUseReducingSpell = { true, true, true, true, true, true };

	public int slayerTask, taskAmount;

	public int prayerId = -1;
	public int headIcon = -1;
	public int bountyIcon = 0;
	public long stopPrayerDelay, prayerDelay;
	public boolean usingPrayer;

	public boolean[] prayerActive = { false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

	public int headIconPk = -1, headIconHints;

	public boolean doubleHit, usingSpecial, npcDroppingItems, usingRangeWeapon, usingBow, usingMagic, castingMagic;
	public int specMaxHitIncrease, freezeDelay, freezeTimer = -6, killerId, playerIndex, oldPlayerIndex, lastWeaponUsed,
			projectileStage, crystalBowArrowCount, playerMagicBook, teleGfx, teleEndAnimation, teleHeight, teleX, teleY,
			rangeItemUsed, killingNpcIndex, totalDamageDealt, oldNpcIndex, fightMode, attackTimer, npcIndex,
			npcClickIndex, npcType, castingSpellId, oldSpellId, spellId, hitDelay;
	public boolean magicFailed, oldMagicFailed;
	public int bowSpecShot, clickNpcType, clickObjectType, objectId, objectX, objectY, objectXOffset, objectYOffset,
			objectDistance;
	public int pItemX, pItemY, pItemId;
	public boolean isMoving, walkingToItem;
	public boolean isShopping, updateShop;
	public int myShopId;
	public int tradeStatus, tradeWith;
	public boolean forcedChatUpdateRequired, tradeAccepted, goodTrade, inTrade, tradeRequested, tradeResetNeeded,
			tradeConfirmed, tradeConfirmed2, canOffer, acceptTrade, acceptedTrade;
	public int attackAnim, animationRequest = -1, animationWaitCycles;
	public int[] playerBonus = new int[14];

	private int[] combatBonus = new int[12];

	public boolean isRunning2 = true;
	public boolean takeAsNote;
	public int combatLevel;
	public boolean saveFile = false;
	public int playerAppearance[] = new int[13];
	public int apset;
	public int actionID;
	public int wearItemTimer, wearId, wearSlot, interfaceId;
	public int XremoveSlot, XinterfaceID, XremoveID, Xamount;

	public int tutorial = 15;

	public boolean antiFirePot = false;

	/**
	 * Castle Wars
	 */
	public int castleWarsTeam;
	public boolean inCwGame;
	public boolean inCwWait;

	/**
	 * Fight Pits
	 */
	public boolean inPits = false;
	public int pitsStatus = 0;

	private List<Map.Entry<String, Long>> rewardedKills = new LinkedList<>();

	public List<Map.Entry<String, Long>> getRewardedKills() {
		return rewardedKills;
	}

	public void addRewardedKill(String victimHost, long timestamp) {
		if (getRewardedKills().size() == Config.PVP_REWARD_GAP) {
			getRewardedKills().remove(0);
		}
		getRewardedKills().add(new AbstractMap.SimpleEntry<String, Long>(victimHost, timestamp));
	}

	public boolean shouldRewardKill(String victimHost) {
		for (Map.Entry<String, Long> rewardedKill : getRewardedKills()) {
			if (!rewardedKill.getKey().equals(victimHost)) {
				continue;
			}
			if (rewardedKill.getValue() + Config.PVP_REWARD_COOLDOWN > System.currentTimeMillis()) {
				return false;
			}
		}
		return true;
	}

	public String spellName = "Select Spell";

	public void assignAutocast(int button) {
		for (int j = 0; j < SpellData.autocastIds.length; j++) {
			if (SpellData.autocastIds[j] == button) {
				autocasting = true;
				autocastId = SpellData.autocastIds[j + 1];
				getPA().sendFrame36(108, 1);
				getClient().setSidebarInterface(0, 328);
				spellName = SpellData.getSpellName(autocastId);
				break;
			}
		}
	}

	public int getChatIcon() {

		if (getRights().isStaff()) {
			return getRights().getCrownId();
		} else if (getDonatorRights().isDonator()) {
			return getDonatorRights().getCrownId();
		}
		return 0;
	}

	public boolean hasVoidRobes() {
		return (playerEquipment[PlayerConstants.PLAYER_LEGS] == 8840
				|| playerEquipment[PlayerConstants.PLAYER_LEGS] == 13073)
				&& (playerEquipment[PlayerConstants.PLAYER_BODY] == 8839
						|| playerEquipment[PlayerConstants.PLAYER_BODY] == 13072)
				&& playerEquipment[PlayerConstants.PLAYER_GLOVES] == 8842;
	}

	public boolean fullVoidRange() {
		return playerEquipment[PlayerConstants.PLAYER_HAT] == 11664 && hasVoidRobes();
	}

	public boolean fullVoidMage() {
		return playerEquipment[PlayerConstants.PLAYER_HAT] == 11663 && hasVoidRobes();
	}

	public boolean fullVoidMelee() {
		return playerEquipment[PlayerConstants.PLAYER_HAT] == 11665 && hasVoidRobes();
	}

	public final int[] PRAYER_DRAIN_RATE = { 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
			500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500 };
	public final int[] PRAYER_LEVEL_REQUIRED = { 1, 4, 7, 8, 9, 10, 13, 16, 19, 22, 25, 26, 27, 28, 31, 34, 37, 40, 43,
			44, 45, 46, 49, 52, 60, 70 };
	public final int[] PRAYER = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
			24, 25 };
	public final String[] PRAYER_NAME = { "Thick Skin", "Burst of Strength", "Clarity of Thought", "Sharp Eye",
			"Mystic Will", "Rock Skin", "Superhuman Strength", "Improved Reflexes", "Rapid Restore", "Rapid Heal",
			"Protect Item", "Hawk Eye", "Mystic Lore", "Steel Skin", "Ultimate Strength", "Incredible Reflexes",
			"Protect from Magic", "Protect from Missiles", "Protect from Melee", "Eagle Eye", "Mystic Might",
			"Retribution", "Redemption", "Smite", "Chivalry", "Piety" };
	public final int[] PRAYER_GLOW = { 83, 84, 85, 601, 602, 86, 87, 88, 89, 90, 91, 603, 604, 92, 93, 94, 95, 96, 97,
			605, 606, 98, 99, 100, 607, 608 };
	public final int[] PRAYER_HEAD_ICONS = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2, 1, 0,
			-1, -1, 3, 5, 4, -1, -1 };

	// {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,2,1,4,6,5};

	/**
	 * SouthWest, NorthEast, SouthWest, NorthEast
	 */

	public boolean isInTut() {
		if (absX >= 2625 && absX <= 2687 && absY >= 4670 && absY <= 4735) {
			return true;
		}
		return false;
	}

	public boolean inBarrows() {
		if (absX > 3520 && absX < 3598 && absY > 9653 && absY < 9750) {
			return true;
		}
		return false;
	}

	public boolean inArea(int x, int y, int x1, int y1) {
		if (absX > x && absX < x1 && absY < y && absY > y1) {
			return true;
		}
		return false;
	}

	public boolean inWild() {
		if (absX > 2941 && absX < 3392 && absY > 3518 && absY < 3966
				|| absX > 2941 && absX < 3392 && absY > 9918 && absY < 10366) {
			return true;
		}
		return false;
	}

	public boolean inGambleZone() {
		return this.absX <= 2102 && this.absY <= 4475 && this.absX >= 2074 && this.absY >= 4455;
	}

	public boolean inFunPK() {
		return this.absX >= 3280 && this.absY >= 4760 && this.absX <= 3392 && this.absY <= 4864;
	}

	public boolean inSafeZone() {
		if (this.inDuelArena()) {
			return true;
		}
		if (this.absX == 3227 && this.absY == 3220 || this.absX == 3228 && this.absY == 3220
				|| this.absX == 3229 && this.absY == 3220) {
			return true;
		}
		if (this.absX <= 3098 && this.absY <= 3499 && this.absX >= 3091 && this.absY >= 3488
				|| this.absX <= 3226 && this.absY <= 3228 && this.absX >= 3201 && this.absY >= 3209
				|| this.absX >= 3227 && this.absY <= 3219 && this.absX <= 3229 && this.absY >= 3217
				|| this.absX >= 3201 && this.absY >= 3229 && this.absX <= 3226 && this.absY <= 3236
				|| this.absX >= 3201 && this.absY >= 3201 && this.absX <= 3226 && this.absY <= 3208
				|| this.absX <= 2547 && this.absY >= 4708 && this.absX >= 2529 && this.absY <= 4725
				|| this.absX <= 2949 && this.absY <= 3369 && this.absX >= 2943 && this.absY >= 3368
				|| this.absX <= 2947 && this.absY >= 3369 && this.absX >= 2943 && this.absY <= 3373
				|| this.absX <= 2102 && this.absY <= 4475 && this.absX >= 2074 && this.absY >= 4455
				|| (absX >= 3060 && absX <= 3073 && absY >= 10245 && absY <= 10265) // outside
				|| (absX >= 1225 && absX <= 1255 && absY >= 1230 && absY <= 1265) // of
				// kbd
				|| (absX >= 2250 && absX <= 2320 && absY >= 4675 && absY <= 4725) // kbd
																					// area
				|| (absX >= 2875 && absX <= 2950 && absY >= 3575 && absY <= 3650) // glod
																					// area
				|| (absX >= 2721 && absX <= 2730 && absY >= 3490 && absY <= 3493) // camelot
																					// bank
				// area
				|| (absX >= 2800 && absX <= 2950 && absY >= 5200 && absY <= 5400)
				|| this.absX <= 3356 && this.absY <= 4759 && this.absX >= 3304 && this.absY >= 4741
				|| this.absX >= 3039 && this.absY <= 3524 && this.absX <= 3138 && this.absY >= 3471) {
			return true;
		}
		return false;
	}

	public boolean arenas() {
		if (absX > 3331 && absX < 3391 && absY > 3242 && absY < 3260) {
			return true;
		}
		return false;
	}

	public boolean inDuelArena() {
		if ((absX > 3322 && absX < 3394 && absY > 3195 && absY < 3291)
				|| (absX > 3311 && absX < 3323 && absY > 3223 && absY < 3248)) {
			return true;
		}
		return false;
	}

	public boolean inMulti() {
		if ((absX >= 3136 && absX <= 3327 && absY >= 3519 && absY <= 3607)
				|| (absX >= 3190 && absX <= 3327 && absY >= 3648 && absY <= 3839)
				|| (absX >= 3200 && absX <= 3390 && absY >= 3840 && absY <= 3967)
				|| (absX >= 2992 && absX <= 3007 && absY >= 3912 && absY <= 3967)
				|| (absX >= 2946 && absX <= 2959 && absY >= 3816 && absY <= 3831)
				|| (absX >= 3008 && absX <= 3199 && absY >= 3856 && absY <= 3903)
				|| (absX >= 3008 && absX <= 3071 && absY >= 3600 && absY <= 3711)
				|| (absX >= 3072 && absX <= 3327 && absY >= 3608 && absY <= 3647)
				|| (absX >= 2624 && absX <= 2690 && absY >= 2550 && absY <= 2619)
				|| (absX >= 2371 && absX <= 2422 && absY >= 5062 && absY <= 5117)
				|| (absX >= 2896 && absX <= 2927 && absY >= 3595 && absY <= 3630)
				|| (absX >= 2892 && absX <= 2932 && absY >= 4435 && absY <= 4464)
				|| (absX >= 2256 && absX <= 2287 && absY >= 4680 && absY <= 4711)
				|| (absX >= 3216 && absY <= 10355 && absX <= 3247 && absY >= 10329)
				|| (absX >= 3684 && absY >= 5797 && absX <= 3708 && absY <= 5821)
				|| (absX >= 2800 && absX <= 2950 && absY >= 5200 && absY <= 5400)
				|| (absX >= 2971 && absY <= 4398 && absX <= 3001 && absY >= 4365)) {
			return true;
		}
		return false;
	}

	public boolean inFightCaves() {
		return absX >= 2360 && absX <= 2445 && absY >= 5045 && absY <= 5125;
	}

	public boolean inPirateHouse() {
		return absX >= 3038 && absX <= 3044 && absY >= 3949 && absY <= 3959;
	}

	public void updateshop(int i) {
		getShops().resetShop(i);
	}

	public void println_debug(String str) {
		System.out.println("[player-" + playerId + "]: " + str);
	}

	public void println(String str) {
		System.out.println("[player-" + playerId + "]: " + str);
	}

	public Player(Client client) {
		this.client = client;
		client.setPlayer(this);

		playerLevel[3] = 10;
		playerXP[3] = 1300;

		playerAppearance[0] = 0; // gender
		playerAppearance[1] = 1; // head
		playerAppearance[2] = 18;// Torso
		playerAppearance[3] = 31; // arms
		playerAppearance[4] = 33; // hands
		playerAppearance[5] = 38; // legs
		playerAppearance[6] = 42; // feet
		playerAppearance[7] = 16; // beard
		playerAppearance[8] = 3; // hair colour
		playerAppearance[9] = 1; // torso colour
		playerAppearance[10] = 2; // legs colour
		playerAppearance[11] = 0; // feet colour
		playerAppearance[12] = 0; // skin colour

		apset = 0;
		actionID = 0;

		Arrays.fill(playerEquipment, -1);

		heightLevel = 0;

		teleportToX = Config.START_LOCATION_X;
		teleportToY = Config.START_LOCATION_Y;
		teleportToHeight = 0;
		absX = absY = -1;
		mapRegionX = mapRegionY = -1;
		currentX = currentY = 0;
		resetWalkingQueue();
	}

	public void destruct() {
		playerListSize = 0;
		for (int i = 0; i < maxPlayerListSize; i++) {
			playerList[i] = null;
		}
		if (World.lastIndex - 1 == this.playerId) {
			World.lastIndex--;
			System.out.println("LOGOUT: " + World.lastIndex);
		}
		oldX = absX;
		oldY = absY;
		absX = absY = mapRegionX = mapRegionY = -1;
		currentX = currentY = 0;
		resetWalkingQueue();
	}

	public boolean withinDistance(Player otherPlr) {
		if (heightLevel != otherPlr.heightLevel)
			return false;
		int deltaX = otherPlr.absX - absX, deltaY = otherPlr.absY - absY;
		return deltaX <= 15 && deltaX >= -16 && deltaY <= 15 && deltaY >= -16;
	}

	public boolean withinDistance(NPC npc) {
		if (!npc.isVisible())
			return false;
		if (heightLevel != npc.heightLevel)
			return false;
		if (npc.needRespawn == true)
			return false;
		int deltaX = npc.currentX - absX, deltaY = npc.currentY - absY;
		return deltaX <= 15 && deltaX >= -16 && deltaY <= 15 && deltaY >= -16;
	}

	public int distanceToPoint(int pointX, int pointY) {
		return (int) Math.sqrt(Math.pow(absX - pointX, 2) + Math.pow(absY - pointY, 2));
	}

	public void resetWalkingQueue() {
		if (wQueueReadPtr == wQueueWritePtr) {
			return;
		}
		wQueueReadPtr = wQueueWritePtr = 0;
		for (int i = 0; i < walkingQueueSize; i++) {
			walkingQueueX[i] = currentX;
			walkingQueueY[i] = currentY;
		}
	}

	public void addToWalkingQueue(int x, int y) {
		int next = (wQueueWritePtr + 1) % walkingQueueSize;
		if (next == wQueueWritePtr) {
			return;
		}
		walkingQueueX[wQueueWritePtr] = x;
		walkingQueueY[wQueueWritePtr] = y;
		wQueueWritePtr = next;
	}

	public boolean smallWalkLength() {
		if (Misc.goodDistance(absX, absY, (getMapRegionX() * 8 + walkingQueueX[getLastMovementWrite()]),
				(getMapRegionY() * 8 + walkingQueueY[getLastMovementWrite()]), 2))
			return false;
		return Misc.goodDistance(absX, absY, (getMapRegionX() * 8 + walkingQueueX[getLastMovementWrite()]),
				(getMapRegionY() * 8 + walkingQueueY[getLastMovementWrite()]), 2);
	}

	public boolean goodDistance(int objectX, int objectY, int playerX, int playerY, int distance) {
		return ((objectX - playerX <= distance && objectX - playerX >= -distance)
				&& (objectY - playerY <= distance && objectY - playerY >= -distance));
	}

	public boolean firstWalk = true;

	public int getNextWalkingDirection() {
		if (wQueueReadPtr == wQueueWritePtr) {
			firstWalk = true;
			return -1;
		}
		int dir;
		do {
			dir = Misc.direction(currentX, currentY, walkingQueueX[wQueueReadPtr], walkingQueueY[wQueueReadPtr]);
			if (dir == -1) {
				wQueueReadPtr = (wQueueReadPtr + 1) % walkingQueueSize;
			} else if ((dir & 1) != 0) {
				println_debug("Invalid waypoint in walking queue!");
				resetWalkingQueue();
				return -1;
			}
		} while ((dir == -1) && (wQueueReadPtr != wQueueWritePtr));
		if (dir == -1) {
			return -1;
		}
		dir >>= 1;
		oldX = absX;
		oldY = absY;
		currentX += Misc.directionDeltaX[dir];
		currentY += Misc.directionDeltaY[dir];
		absX += Misc.directionDeltaX[dir];
		absY += Misc.directionDeltaY[dir];

		// Server.region.move(this, oldX, oldY, heightLevel, currentX, currentY,
		// heightLevel);
		return dir;
	}

	public int getLastMovementWrite() {
		return wQueueWritePtr - 1 < 0 ? 0 : wQueueWritePtr - 1;
	}

	public int oldX = absX;
	public int oldY = absY;

	public boolean didTeleport = false;
	public boolean mapRegionDidChange = false;
	public int dir1 = -1, dir2 = -1;
	public boolean createItems = false;
	public int poimiX = 0, poimiY = 0;

	public void getNextPlayerMovement() {
		mapRegionDidChange = false;
		didTeleport = false;
		dir1 = dir2 = -1;

		if (teleportToX != -1 && teleportToY != -1) {
			mapRegionDidChange = true;
			if (mapRegionX != -1 && mapRegionY != -1) {
				int relX = teleportToX - mapRegionX * 8, relY = teleportToY - mapRegionY * 8;
				if (relX >= 2 * 8 && relX < 11 * 8 && relY >= 2 * 8 && relY < 11 * 8) {
					mapRegionDidChange = false;
				}
			}
			if (mapRegionDidChange) {
				mapRegionX = (teleportToX >> 3) - 6;
				mapRegionY = (teleportToY >> 3) - 6;
			}
			if (teleportToX != absX || teleportToY != absY) {
			}
			currentX = teleportToX - 8 * mapRegionX;
			currentY = teleportToY - 8 * mapRegionY;
			absX = teleportToX;
			absY = teleportToY;
			heightLevel = teleportToHeight;
			oldX = absX;
			oldY = absY + 1;
			resetWalkRequest();
			resetWalkingQueue();
			teleportToX = teleportToY = teleportToHeight = -1;
			didTeleport = true;
		} else {
			getPA().handleEntityFollowing();
			dir1 = getNextWalkingDirection();
			if (dir1 == -1) {
				return;
			}
			if (isRunning) {
				getPA().handleEntityFollowing();
				dir2 = getNextWalkingDirection();
			}

			int deltaX = 0, deltaY = 0;
			if (currentX < 2 * 8) {
				deltaX = 4 * 8;
				mapRegionX -= 4;
				mapRegionDidChange = true;
			} else if (currentX >= 11 * 8) {
				deltaX = -4 * 8;
				mapRegionX += 4;
				mapRegionDidChange = true;
			}
			if (currentY < 2 * 8) {
				deltaY = 4 * 8;
				mapRegionY -= 4;
				mapRegionDidChange = true;
			} else if (currentY >= 11 * 8) {
				deltaY = -4 * 8;
				mapRegionY += 4;
				mapRegionDidChange = true;
			}
			if (mapRegionDidChange) {
				currentX += deltaX;
				currentY += deltaY;
				for (int i = 0; i < walkingQueueSize; i++) {
					walkingQueueX[i] += deltaX;
					walkingQueueY[i] += deltaY;
				}
			}
		}

	}

	private void resetWalkRequest() {
		clickNpcType = 0;
		clickObjectType = 0;
	}

	/**
	 * Handles logging in the player
	 */
	public void handleLogin() {
		getClient().initialize();
		setAttribute("verified_login", true);

		if (!getAttribute("custom_pin", "").isEmpty() && getAttribute("custom_pin_settings", 0) != 0) {
			setAttribute("verified_login", false);
			getDialogue().start(new PinDialogue(2));
		}

		boolean differentAddress = !getAttribute("connected_from", "").equalsIgnoreCase(connectedFrom);
		if (differentAddress) {
			sendMessage("@blu@You last logged in from a different IP address.");
			if (!getAttribute("custom_pin", "").isEmpty() && getAttribute("custom_pin_settings", 0) == 0) {
				setAttribute("verified_login", false);
				getDialogue().start(new PinDialogue(2));
			}
		}

		long wealth = getItems().getTotalWealth();

		if (wealth >= 150_000_000 && getRights().isInferior(PlayerRights.DEVELOPER)) {
			String line = "Possible bug abuse! Check acount: " + getUsername() + ", wealth: " + Misc.format(wealth);
			System.out.println(line);
			for (Player player : World.players) {
				if (player != null && player.getRights().isSuperior(PlayerRights.VETERAN)) {
					player.sendMessage(line);
				}
			}
		}

		/*
		 * Check if the uid doesn't match
		 */
		long login_uid = getAttribute("login_uid", 0L);
		long saved_uid = getAttribute("saved_uid", 0L);
		boolean differentUid = login_uid != saved_uid;
		if (differentUid) {
			sendMessage("@blu@You last logged in from a different computer.");
			if (!getAttribute("custom_pin", "").isEmpty() && getAttribute("custom_pin_settings", 0) == 0) {
				setAttribute("verified_login", false);
				getDialogue().start(new PinDialogue(2));
			}
		}

		for (int i = 0; i < 25; i++) {
			getPA().setSkillLevel(i, getLevel()[i], getExperience()[i]);
			getPA().refreshSkill(i);
		}
		for (int p = 0; p < PRAYER.length; p++) { // reset prayer
													// glows
			prayerActive[p] = false;
			getPA().sendFrame36(PRAYER_GLOW[p], 0);
		}
		// brightness
		getPA().sendFrame36(166, 4);
		// split chat
		getItems().resetItems(3214);
		sendMessage("@bla@Welcome to Oldschool PK.");
		sendMessage("@blu@There are currently@bla@ " + World.getPlayerCount() + " Players @blu@online");
		sendMessage("@blu@Type ::commands for useful commands!");
		sendMessage("@blu@TELEPORT tab is in the bottom right corner of your tabs �T�");
		sendMessage("<col=871C17>The spawn items tab is in the bottom left corner of your tabs.");

		NewPlayer.newPlayer(this);

		final Player p = this;
		CycleEventHandler.getSingleton().addEvent(this, new CycleEvent() {

			@Override
			public void execute(CycleEventContainer container) {
				try {
					if (inDuelArena()) {
						getPA().movePlayer(3362 + Misc.random(5), 3263 + Misc.random(5), 0);
					}
					getClient().correctCoordinates();
					getPA().handleWeaponStyle();
					getPA().handleLoginText();
					getPA().setChatOptions(0, 0, 0); // reset private messaging
					// options
					QuestTab.updateInterface(p);
					getPA().clearClanChat();
					getPA().setClanData();
					if (clan == null) {
						getPA().joinClanChat("Ospk");
					}
					getPA().resetFollow();
					getPA().logIntoPM();
					getItems().addSpecialBar(getEquipment()[PlayerConstants.PLAYER_WEAPON]);
					getPA().sendFrame36(287, isSplitChat() ? 1 : 0);
					getPA().sendFrame126("" + (int) specAmount * 10, 155);
					getPA().sendFrame126("Lumbridge", 28070);
				} catch (Exception e) {
					e.printStackTrace();
					container.stop();
				}
				container.stop();
			}

		}, 2);

		setAttribute("full_body", Items.isFullBody(getEquipment()[PlayerConstants.PLAYER_BODY]));
		setAttribute("full_mask", Items.isFullMask(getEquipment()[PlayerConstants.PLAYER_HAT]));
		setAttribute("full_helm", Items.isFullMask(getEquipment()[PlayerConstants.PLAYER_HAT]));
		combatLevel = PlayerConstants.getCombatLevel(p);
		getPA().requestUpdates();
		// getPA().sendFrame36(43, fightMode-1);
		getPA().sendFrame36(108, 0);// resets autocast button
		getPA().sendFrame36(172, 1);
		getPA().sendFrame107(); // reset screen
		getClient().setSidebarInterface(1, 3917);
		getClient().setSidebarInterface(2, 639); // Quest Tab.
		getClient().setSidebarInterface(3, 3213);
		getClient().setSidebarInterface(4, 1644);
		getClient().setSidebarInterface(5, 5608);
		if (playerMagicBook == 0) {
			getClient().setSidebarInterface(6, 1151); // modern
		} else {
			if (playerMagicBook == 2) {
				getClient().setSidebarInterface(6, 29999); // lunar
			} else {
				getClient().setSidebarInterface(6, 12855); // ancient
			}
		}
		getClient().setSidebarInterface(7, 18128);
		getClient().setSidebarInterface(8, 5065);
		getClient().setSidebarInterface(9, 5715);
		getClient().setSidebarInterface(10, 2449);
		// setSidebarInterface(11, 4445); // wrench tab - bugged don't use
		getClient().setSidebarInterface(11, 904); // wrench tab
		getClient().setSidebarInterface(12, 147); // run tab
		getClient().setSidebarInterface(13, 28060);
		getClient().setSidebarInterface(14, 37000);
		getClient().setSidebarInterface(0, 2423);
		getPA().sendOption("Follow", 2);
		getPA().sendOption("Trade With", 3);
		getItems().sendWeapon(getEquipment()[PlayerConstants.PLAYER_WEAPON],
				ItemAssistant.getItemName(getEquipment()[PlayerConstants.PLAYER_WEAPON]));
		EquipmentBonuses.resetPlayerBonuses(this);
		EquipmentBonuses.getItemBonuses(this);
		EquipmentBonuses.writeItemBonuses(this);

		for (int i = 0; i < getEquipment().length; i++) {
			getItems().setEquipment(getEquipment()[i], playerEquipmentN[i], i);
		}

		getCombat().getPlayerAnimIndex();
		saveTimer = Config.SAVE_TIMER;
		saveCharacter = true;
		Misc.println("[REGISTERED]: " + playerName + "");
		ConnectionSet.add(connectedFrom);
		update();
		getPA().sendFrame36(172, autoRet);

		if (getAttribute("custom_pin", "").isEmpty()) {
			sendMessage("@red@You do not have a login pin set, your account is NOT secure, type ::pinsettings.");
		}

		for (Player player : World.players) {
			if (player != null) {
				player.getPA().sendFrame126("Players Online " + "@gre@[" + (World.getPlayerCount()) + "]", 39160);
			}
		}
	}

	public void update() {
		World.updatePlayer(this, getClient().getOutStream());
		World.updateNPC(this, getClient().getOutStream());
		getClient().flushOutStream();
	}

	public void process() {
		if (getAttribute("invincible", false)) {
			getLevel()[Skills.HITPOINTS] = getLevelForXP(getExperience()[Skills.HITPOINTS]);
		}
		if (getAttribute("special", false)) {
			specAmount = 10;
		}
		if (getPetId() != -1 && getPetIndex() == -1) {
			NPCHandler.spawnPet(this, getPetId());
		}
		if (getPetIndex() > -1) {
			getPets().process();
		}

		getPA().sendFrame126("Players In Wilderness " + "@red@[" + World.getPlayersInWildy() + "]", 39167);
		if (System.currentTimeMillis() - waitTime >= 1000) {
			if (vengTimer >= 0) {
				getPA().sendFrame126(Integer.toString(vengTimer), 33000);
				vengTimer--;
			}
			waitTime = System.currentTimeMillis();
		}

		if (freezeTimer > 0) {
			freezeTimer--;
			if (frozenBy > 0) {
				if (World.players[frozenBy] == null) {
					freezeTimer = -1;
					frozenBy = -1;
					getPA().sendFrame126(Integer.toString(freezeTimer), 33001);
				} else if (!goodDistance(absX, absY, World.players[frozenBy].absX, World.players[frozenBy].absY, 20)) {
					freezeTimer = -1;
					frozenBy = -1;
					getPA().sendFrame126(Integer.toString(freezeTimer), 33001);
				}
			}
		}

		if (System.currentTimeMillis() - duelDelay > 800L && duelCount > 0 && World.players[duelingWith] != null) {
			if (duelCount != 1) {
				forcedChat("" + (duelCount -= 1));
				duelDelay = System.currentTimeMillis();
			} else {
				forcedChat("FIGHT!");
				duelCount = 0;
			}
		}

		if (System.currentTimeMillis() - lastVenom > 20000 && venomDamage >= 6) {
			if (!getHitUpdateRequired()) {
				setHitUpdateRequired(true);
				setHitDiff(venomDamage);
				updateRequired = true;
				venomMask = 1;
			} else if (!getHitUpdateRequired2()) {
				setHitUpdateRequired2(true);
				setHitDiff2(venomDamage);
				updateRequired = true;
				venomMask = 2;
			}
			dealDamage(venomDamage);
			venomDamage += 2;
			if (venomDamage > 20) {
				venomDamage = 20;
			}
			getPA().refreshSkill(3);
			lastVenom = System.currentTimeMillis();
		}

		if (System.currentTimeMillis() - lastPoison > 20000 && poisonDamage > 0) {
			int damage = poisonDamage / 2;
			if (damage > 0) {
				if (!getHitUpdateRequired()) {
					setHitUpdateRequired(true);
					setHitDiff(damage);
					updateRequired = true;
					poisonMask = 1;
				} else if (!getHitUpdateRequired2()) {
					setHitUpdateRequired2(true);
					setHitDiff2(damage);
					updateRequired = true;
					poisonMask = 2;
				}
				lastPoison = System.currentTimeMillis();
				poisonDamage--;
				if (damage > getLevel()[3])
					damage = getLevel()[3];
				dealDamage(damage);
				getPA().refreshSkill(3);
			} else {
				poisonDamage = -1;
				sendMessage("You are no longer poisoned.");
			}
		}

		if (followId2 > 0) {
			getPA().followNpc();
		}

		getCombat().handlePrayerDrain();

		if (System.currentTimeMillis() - singleCombatDelay > 4400) {
			underAttackBy = 0;
		}
		if (System.currentTimeMillis() - singleCombatDelay2 > 3300) {
			underAttackBy2 = 0;
		}

		if (System.currentTimeMillis() - teleGrabDelay > 1550 && usingMagic) {
			usingMagic = false;
			if (Server.itemHandler.itemExists(teleGrabItem, teleGrabX, teleGrabY)) {
				Server.itemHandler.removeGroundItem(this, teleGrabItem, teleGrabX, teleGrabY, true);
			}
		}

		if (!hasMultiSign && inMulti()) {
			hasMultiSign = true;
			getPA().multiWay(1);
		}

		if (hasMultiSign && !inMulti()) {
			hasMultiSign = false;
			getPA().multiWay(-1);
		}

		if (skullTimer > 0) {
			skullTimer--;
			if (skullTimer == 1) {
				isSkulled = false;
				attackedPlayers.clear();
				headIconPk = -1;
				skullTimer = -1;
				getPA().requestUpdates();
			}
		}

		if (isDead && respawnTimer == 0) {
			getPA().applyDead();
		}

		if (respawnTimer == 3) {
			respawnTimer = 0;
			getPA().giveLife();
		}

		if (respawnTimer > 0) {
			respawnTimer--;
		}

		if (hitDelay > 0) {
			hitDelay--;
		}

		if (hitDelay == 1) {
			if (oldNpcIndex > 0) {
				getCombat().appendDamageNPC(oldNpcIndex, combatStyle, 1);
				if (doubleHit) {
					Experience.calculateDamageNPC(this, oldNpcIndex, combatStyle);
					getCombat().appendDamageNPC(oldNpcIndex, combatStyle, 2);
				}
			} else if (oldPlayerIndex > 0) {
				getCombat().appendDamage(oldPlayerIndex, combatStyle, 1);
			}
		}

		if (dbowTimer > 0) {
			dbowTimer--;
			if (dbowTimer == 2) {
				if (dbowIndex > 0) {
					getCombat().appendDamage(dbowIndex, 1, 1);
				}
				if (dbowIndex1 > 1) {
					getCombat().appendDamageNPC(dbowIndex1, 1, 1);
				}
			}
			if (dbowTimer == 1) {
				if (dbowIndex > 0) {
					if (dbowSpec) {
						World.players[dbowIndex].playGraphic(Graphic.create(1100, 0, 100));
					}
					Experience.calculateDamage(this, World.players[dbowIndex], 1, false);
					getCombat().appendDamage(dbowIndex, 1, 1);
				}
				if (dbowIndex1 > 1) {
					if (dbowSpec) {
						NPCHandler.npcs[dbowIndex1].gfx100(1100);
					}
					Experience.calculateDamageNPC(this, dbowIndex1, 1);
					getCombat().appendDamageNPC(dbowIndex1, 1, 1);
				}
				dbowSpec = false;
				dbowIndex = -1;
				dbowIndex1 = -1;
				dbowTimer = 0;
			}
		}

		if (attackTimer > 0) {
			attackTimer--;
		}

		if (attackTimer == 1) {
			if (npcIndex > 0 && clickNpcType == 0) {
				getCombat().attackNpc(npcIndex);
				if (npcIndex > 0) {
					getPA().removeAllWindows();
					NPC npc = NPCHandler.npcs[npcIndex];
					if (npc != null)
						getPA().sendFrame126("true:" + npcIndex + ":" + npc.HP + ":" + npc.getMaxHP(), 35000);
				}
			}
			if (playerIndex > 0) {
				getCombat().attackPlayer(playerIndex);
				if (playerIndex > 0) {
					getPA().removeAllWindows();
					Player player = World.players[playerIndex];
					if (player != null)
						getPA().sendFrame126("false:" + playerIndex + ":" + player.getLevel()[3] + ":"
								+ player.getLevelForXP(player.getExperience()[3]), 35000);
				}
			}
		} else if (attackTimer <= 0 && (npcIndex > 0 || playerIndex > 0)) {
			if (npcIndex > 0) {
				attackTimer = 0;
				getCombat().attackNpc(npcIndex);
				if (npcIndex > 0) {
					getPA().removeAllWindows();
					NPC npc = NPCHandler.npcs[npcIndex];
					int hitpoints = npc == null ? 0 : npc.HP;
					int max = npc == null ? 0 : npc.getMaxHP();
					getPA().sendFrame126("true:" + npcIndex + ":" + hitpoints + ":" + max, 35000);
				}
			} else if (playerIndex > 0) {
				attackTimer = 0;
				getCombat().attackPlayer(playerIndex);
				if (playerIndex > 0) {
					getPA().removeAllWindows();
					getPA().sendFrame126("false:" + playerIndex + ":" + World.players[playerIndex].getLevel()[3] + ":"
							+ World.players[playerIndex].getLevelForXP(World.players[playerIndex].getExperience()[3]),
							35000);
				}
			}
		}
	}

	public int updatedX, updatedY;
	public boolean updateContainer = false;

	public void updateThisPlayerMovement(Stream str) {
		if (mapRegionDidChange) {
			str.createFrame(73);
			str.writeWordA(mapRegionX + 6);
			str.writeWord(mapRegionY + 6);
		}
		updatedX = absX;
		updatedY = absY;
		if (didTeleport) {
			if (checkHeight) {
				updateContainer = true;
				checkHeight = false;
			}
			str.createFrameVarSizeWord(81);
			str.initBitAccess();
			str.writeBits(1, 1);
			str.writeBits(2, 3);
			str.writeBits(2, heightLevel);
			str.writeBits(1, 1);
			str.writeBits(1, (updateRequired) ? 1 : 0);
			str.writeBits(7, currentY);
			str.writeBits(7, currentX);
			return;
		}

		if (dir1 == -1) {
			// don't have to update the character position, because we're just
			// standing
			str.createFrameVarSizeWord(81);
			str.initBitAccess();
			isMoving = false;
			if (updateRequired) {
				// tell client there's an update block appended at the end
				str.writeBits(1, 1);
				str.writeBits(2, 0);
			} else {
				str.writeBits(1, 0);
			}
			if (DirectionCount < 50) {
				DirectionCount++;
			}
		} else {
			DirectionCount = 0;
			str.createFrameVarSizeWord(81);
			str.initBitAccess();
			str.writeBits(1, 1);

			if (dir2 == -1) {
				isMoving = true;
				str.writeBits(2, 1);
				str.writeBits(3, Misc.xlateDirectionToClient[dir1]);
				if (updateRequired) {
					str.writeBits(1, 1);
				} else {
					str.writeBits(1, 0);
				}
			} else {
				isMoving = true;
				str.writeBits(2, 2);
				str.writeBits(3, Misc.xlateDirectionToClient[dir1]);
				str.writeBits(3, Misc.xlateDirectionToClient[dir2]);
				if (updateRequired) {
					str.writeBits(1, 1);
				} else {
					str.writeBits(1, 0);
				}
			}
		}
	}

	public void updatePlayerMovement(Stream str) {
		if (dir1 == -1) {
			if (updateRequired || isChatTextUpdateRequired()) {

				str.writeBits(1, 1);
				str.writeBits(2, 0);
			} else {
				str.writeBits(1, 0);
			}
		} else if (dir2 == -1) {

			str.writeBits(1, 1);
			str.writeBits(2, 1);
			str.writeBits(3, Misc.xlateDirectionToClient[dir1]);
			str.writeBits(1, (updateRequired || isChatTextUpdateRequired()) ? 1 : 0);
		} else {

			str.writeBits(1, 1);
			str.writeBits(2, 2);
			str.writeBits(3, Misc.xlateDirectionToClient[dir1]);
			str.writeBits(3, Misc.xlateDirectionToClient[dir2]);
			str.writeBits(1, (updateRequired || isChatTextUpdateRequired()) ? 1 : 0);
		}
	}

	public byte cachedPropertiesBitmap[] = new byte[(Config.MAX_PLAYERS + 7) >> 3];

	public void addNewNPC(NPC npc, Stream str, Stream updateBlock) {
		int id = npc.npcId;
		npcInListBitmap[id >> 3] |= 1 << (id & 7);
		npcList[npcListSize++] = npc;

		str.writeBits(14, id);

		int z = npc.currentY - absY;
		if (z < 0)
			z += 32;
		str.writeBits(5, z);
		z = npc.currentX - absX;
		if (z < 0)
			z += 32;
		str.writeBits(5, z);

		str.writeBits(1, 0);
		str.writeBits(14, npc.getNPCType());

		boolean savedUpdateRequired = npc.updateRequired;
		npc.updateRequired = true;
		npc.appendNPCUpdateBlock(updateBlock);
		npc.updateRequired = savedUpdateRequired;
		str.writeBits(1, 1);
	}

	public void addNewPlayer(Player plr, Stream str, Stream updateBlock, Player observer) {
		int id = plr.playerId;
		playerInListBitmap[id >> 3] |= 1 << (id & 7);
		playerList[playerListSize++] = plr;
		str.writeBits(11, id);
		str.writeBits(1, 1);
		boolean savedFlag = plr.isAppearanceUpdateRequired();
		boolean savedUpdateRequired = plr.updateRequired;
		plr.setAppearanceUpdateRequired(true);
		plr.updateRequired = true;
		plr.appendPlayerUpdateBlock(updateBlock, observer, true);
		plr.setAppearanceUpdateRequired(savedFlag);
		plr.updateRequired = savedUpdateRequired;
		str.writeBits(1, 1);
		int z = plr.absY - absY;
		if (z < 0)
			z += 32;
		str.writeBits(5, z);
		z = plr.absX - absX;
		if (z < 0)
			z += 32;
		str.writeBits(5, z);
	}

	static {
		playerProps = new Stream(new byte[100]);
	}

	public String getPlayerTitle() {
		if (kills < 10) {
			playerTitle = "Starter";
		} else if (kills >= 10 && kills <= 20) {
			playerTitle = "Hunter";
		} else if (kills >= 21 && kills <= 30) {
			playerTitle = "Predator";
		} else if (kills >= 31 && kills <= 40) {
			playerTitle = "Terminator";
		} else if (kills >= 41 && kills <= 50) {
			playerTitle = "Exterminator";
		} else if (kills >= 51 && kills <= 60) {
			playerTitle = "Overpowered";
		} else if (kills >= 61 && kills <= 70) {
			playerTitle = "Assasin";
		} else if (kills >= 71 && kills <= 80) {
			playerTitle = "Supremacy";
		} else if (kills >= 81 && kills <= 90) {
			playerTitle = "Legacy";
		} else if (kills > 90 && kills <= 100) {
			playerTitle = "Jason Bourne";
		} else if (kills > 100 && kills < 125) {
			playerTitle = "Punisher";
		} else if (kills >= 125 && kills <= 149) {
			playerTitle = "The Mighty";
		} else if (kills >= 150 && kills <= 174) {
			playerTitle = "Hellraiser";
		} else if (kills >= 175 && kills <= 199) {
			playerTitle = "The Devastator";
		} else if (kills >= 200 && kills <= 249) {
			playerTitle = "The Executioner";
		} else if (kills >= 250 && kills <= 299) {
			playerTitle = "Legendary";
		} else if (kills >= 300) {
			playerTitle = "Godlike";
		}
		if (getRights() == PlayerRights.VETERAN) {
			playerTitle = "Veteran";
		} else if (getRights() == PlayerRights.YOUTUBER) {
			playerTitle = "Youtuber";
		} else if (getRights() == PlayerRights.DICER) {
			playerTitle = "Dicer";
		} else if (getRights() == PlayerRights.SUPPORT) {
			playerTitle = "Supporter";
		} else if (getRights() == PlayerRights.MODERATOR) {
			playerTitle = "Moderator";
		} else if (getRights() == PlayerRights.ADMINISTRATOR) {
			playerTitle = "Administrator";
		} else if (getRights() == PlayerRights.DEVELOPER) {
			playerTitle = "Community Manager";
		} else if (getRights() == PlayerRights.OWNER) {
			playerTitle = "Owner";
		} else if (getDonatorRights() == DonatorRights.PREMIUM) {
			playerTitle = "Premium";
		} else if (getDonatorRights() == DonatorRights.SUPER_PREMIUM) {
			playerTitle = "Super";
		} else if (getDonatorRights() == DonatorRights.EXTREME_PREMIUM) {
			playerTitle = "Extreme";
		} else if (getDonatorRights() == DonatorRights.LEGENDARY_PREMIUM) {
			playerTitle = "Legendary";
		} else if (getDonatorRights() == DonatorRights.ULTIMATE_PREMIUM) {
			playerTitle = "Ultimate";
		}

		if (this.playerName.equalsIgnoreCase("mod adam")) {
			playerTitle = "Developer";
		}
		if (this.playerName.equalsIgnoreCase("william")) {
			playerTitle = "CEO";
		}
		if (this.playerName.equalsIgnoreCase("klepto")) {
			playerTitle = "Lead Developer";
		} else if (this.playerName.equalsIgnoreCase("kylie")) {
			playerTitle = "Whoreable";
		}
		return playerTitle;
	}

	protected void appendPlayerAppearance(Stream str) {
		playerProps.currentOffset = 0;
		playerProps.writeByte(playerAppearance[0]);
		playerProps.writeByte(headIcon);
		playerProps.writeByte(headIconPk);
		playerProps.writeString(getPlayerTitle());
		// playerProps.writeByte(headIconHints);
		// playerProps.writeByte(bountyIcon);
		if (!isNpc) {
			if (playerEquipment[PlayerConstants.PLAYER_HAT] > 1) {
				playerProps.writeWord(0x200 + playerEquipment[PlayerConstants.PLAYER_HAT]);
			} else {
				playerProps.writeByte(0);
			}

			if (playerEquipment[PlayerConstants.PLAYER_CAPE] > 1) {
				playerProps.writeWord(0x200 + playerEquipment[PlayerConstants.PLAYER_CAPE]);
			} else {
				playerProps.writeByte(0);
			}

			if (playerEquipment[PlayerConstants.PLAYER_AMULET] > 1) {
				playerProps.writeWord(0x200 + playerEquipment[PlayerConstants.PLAYER_AMULET]);
			} else {
				playerProps.writeByte(0);
			}

			if (playerEquipment[PlayerConstants.PLAYER_WEAPON] > 1) {
				playerProps.writeWord(0x200 + playerEquipment[PlayerConstants.PLAYER_WEAPON]);
			} else {
				playerProps.writeByte(0);
			}

			if (playerEquipment[PlayerConstants.PLAYER_BODY] > 1) {
				playerProps.writeWord(0x200 + playerEquipment[PlayerConstants.PLAYER_BODY]);
			} else {
				playerProps.writeWord(0x100 + playerAppearance[2]);
			}

			if (playerEquipment[PlayerConstants.PLAYER_SHIELD] > 1) {
				playerProps.writeWord(0x200 + playerEquipment[PlayerConstants.PLAYER_SHIELD]);
			} else {
				playerProps.writeByte(0);
			}

			if (!getAttribute("full_body", false)) {
				playerProps.writeWord(0x100 + playerAppearance[3]);
			} else {
				playerProps.writeByte(0);
			}

			if (playerEquipment[PlayerConstants.PLAYER_LEGS] > 1) {
				playerProps.writeWord(0x200 + playerEquipment[PlayerConstants.PLAYER_LEGS]);
			} else {
				playerProps.writeWord(0x100 + playerAppearance[5]);
			}

			if (!getAttribute("full_helm", false) && !getAttribute("full_mask", false)) {
				playerProps.writeWord(0x100 + playerAppearance[1]);
			} else {
				playerProps.writeByte(0);
			}

			if (playerEquipment[PlayerConstants.PLAYER_GLOVES] > 1) {
				playerProps.writeWord(0x200 + playerEquipment[PlayerConstants.PLAYER_GLOVES]);
			} else {
				playerProps.writeWord(0x100 + playerAppearance[4]);
			}

			if (playerEquipment[PlayerConstants.PLAYER_FEET] > 1) {
				playerProps.writeWord(0x200 + playerEquipment[PlayerConstants.PLAYER_FEET]);
			} else {
				playerProps.writeWord(0x100 + playerAppearance[6]);
			}

			if (playerAppearance[0] != 1 && !getAttribute("full_mask", false)) {
				playerProps.writeWord(0x100 + playerAppearance[7]);
			} else {
				playerProps.writeByte(0);
			}
		} else {
			playerProps.writeWord(-1);
			playerProps.writeWord(npcId2);
		}

		playerProps.writeByte(playerAppearance[8]);
		playerProps.writeByte(playerAppearance[9]);
		playerProps.writeByte(playerAppearance[10]);
		playerProps.writeByte(playerAppearance[11]);
		playerProps.writeByte(playerAppearance[12]);
		playerProps.writeWord(playerStandIndex); // standAnimIndex
		playerProps.writeWord(playerTurnIndex); // standTurnAnimIndex
		playerProps.writeWord(playerWalkIndex); // walkAnimIndex
		playerProps.writeWord(playerTurn180Index); // turn180AnimIndex
		playerProps.writeWord(playerTurn90CWIndex); // turn90CWAnimIndex
		playerProps.writeWord(playerTurn90CCWIndex); // turn90CCWAnimIndex
		playerProps.writeWord(playerRunIndex); // runAnimIndex
		playerProps.writeQWord(Misc.playerNameToInt64(playerName));
		playerProps.writeByte(combatLevel); // combat level
		playerProps.writeWord(0);
		str.writeByteC(playerProps.currentOffset);
		str.writeBytes(playerProps.buffer, playerProps.currentOffset, 0);
	}

	public int getLevelForXP(int exp) {
		if (exp > 13034430) {
			return 99;
		} else {
			int points = 0;
			for (int lvl = 1; lvl <= 99; ++lvl) {
				points = (int) ((double) points
						+ Math.floor((double) lvl + 300.0D * Math.pow(2.0D, (double) lvl / 7.0D)));
				int var5 = (int) Math.floor((double) (points / 4));
				if (var5 >= exp) {
					return lvl;
				}
			}

			return 99;
		}
	}

	public boolean chatTextUpdateRequired = false;
	public byte chatText[] = new byte[4096];
	public byte chatTextSize = 0;
	public int chatTextColor = 0;
	public int chatTextEffects = 0;

	protected void appendPlayerChatText(Stream str) {
		str.writeWordBigEndian(((getChatTextColor() & 0xFF) << 8) + (getChatTextEffects() & 0xFF));
		str.writeByte(getChatIcon());
		str.writeByteC(getChatTextSize());
		str.writeBytes_reverse(getChatText(), getChatTextSize(), 0);
	}

	public void forcedChat(String text) {
		forcedText = text;
		forcedChatUpdateRequired = true;
		updateRequired = true;
		setAppearanceUpdateRequired(true);
	}

	public String forcedText = "null";

	public void appendForcedChat(Stream str) {
		str.writeString(forcedText);
	}

	/**
	 * Graphics
	 **/

	public int mask100var1 = 0;
	public int mask100var2 = 0;
	protected boolean mask100update = false;

	public void appendMask100Update(Stream str) {
		str.writeWordBigEndian(mask100var1);
		str.writeDWord(mask100var2);
	}

	/**
	 * Animations
	 **/
	public void playAnimation(Animation animation) {
		animationRequest = animation.getId();
		animationWaitCycles = animation.getDelay();
		updateRequired = true;
	}

	public void startAnimation(int animId, int time) {
		animationRequest = animId;
		animationWaitCycles = time;
		updateRequired = true;
	}

	public void playGraphic(Graphic graphic) {
		mask100var1 = graphic.getId();
		mask100var2 = graphic.getDelay() + (65536 * graphic.getHeight());
		mask100update = true;
		updateRequired = true;
	}

	public void appendAnimationRequest(Stream str) {
		str.writeWordBigEndian((animationRequest == -1) ? 65535 : animationRequest);
		str.writeByteC(animationWaitCycles);
	}

	/**
	 * Face Update
	 **/

	protected boolean faceUpdateRequired = false;
	public int face = -1;
	public int FocusPointX = -1, FocusPointY = -1;

	public void faceUpdate(int index) {
		face = index;
		faceUpdateRequired = true;
		updateRequired = true;
	}

	public void appendFaceUpdate(Stream str) {
		str.writeWordBigEndian(face);
	}

	public void turnPlayerTo(int pointX, int pointY) {
		FocusPointX = 2 * pointX + 1;
		FocusPointY = 2 * pointY + 1;
		updateRequired = true;
	}

	private void appendSetFocusDestination(Stream str) {
		str.writeWordBigEndianA(FocusPointX);
		str.writeWordBigEndian(FocusPointY);
	}

	/**
	 * Hit Update
	 **/

	protected void appendHitUpdate(Stream str) {
		str.writeByte(getHitDiff()); // What the perseon got 'hit' for
		if (poisonMask == 1) {
			str.writeByteA(2);
		} else if (venomMask == 1) {
			str.writeByteA(5);
		} else if (getHitDiff() > 0) {
			str.writeByteA(1); // 0: red hitting - 1: blue hitting
		} else {
			str.writeByteA(0); // 0: red hitting - 1: blue hitting
		}
		if (playerLevel[3] <= 0) {
			playerLevel[3] = 0;
			isDead = true;
		}
		str.writeByteC(playerLevel[3]); // Their current hp, for HP bar
		str.writeByte(getLevelForXP(playerXP[3]));
	}

	protected void appendHitUpdate2(Stream str) {
		str.writeByte(hitDiff2); // What the perseon got 'hit' for
		if (poisonMask == 2) {
			str.writeByteS(2);
			poisonMask = -1;
		} else if (venomMask == 2) {
			str.writeByteS(5);
			venomMask = -1;
		} else if (hitDiff2 > 0) {
			str.writeByteS(1); // 0: red hitting - 1: blue hitting
		} else {
			str.writeByteS(0); // 0: red hitting - 1: blue hitting
		}
		if (playerLevel[3] <= 0) {
			playerLevel[3] = 0;
			isDead = true;
		}
		str.writeByte(playerLevel[3]); // Their current hp, for HP bar
		str.writeByteC(getLevelForXP(playerXP[3])); // Their max hp, for HP
	}

	public void appendPlayerUpdateBlock(Stream str, Player observer, boolean needsBlock) {
		if (!updateRequired && !isChatTextUpdateRequired())
			return; // nothing required

		if (getCachedBlock() != null && !this.playerName.equals(observer.playerName) && !needsBlock) {
			str.writeBytes(getCachedBlock().buffer, getCachedBlock().currentOffset, 0);
			return;
		}
		Stream cacheStream = new Stream(new byte[300]);
		int updateMask = 0;
		if (forceMovementUpdateRequired) {
			updateMask |= 0x400;
		}
		if (mask100update) {
			updateMask |= 0x100;
		}
		if (animationRequest != -1) {
			updateMask |= 8;
		}
		if (forcedChatUpdateRequired) {
			updateMask |= 4;
		}
		if (isChatTextUpdateRequired()) {
			updateMask |= 0x80;
		}
		if (isAppearanceUpdateRequired()) {
			updateMask |= 0x10;
		}
		if (faceUpdateRequired) {
			updateMask |= 1;
		}
		if (FocusPointX != -1) {
			updateMask |= 2;
		}
		if (isHitUpdateRequired()) {
			updateMask |= 0x20;
		}

		if (hitUpdateRequired2) {
			updateMask |= 0x200;
		}

		if (updateMask >= 0x100) {
			updateMask |= 0x40;
			cacheStream.writeByte(updateMask & 0xFF);
			cacheStream.writeByte(updateMask >> 8);
		} else {
			cacheStream.writeByte(updateMask);
		}
		if (forceMovementUpdateRequired) {
			appendForceMovementMask(cacheStream, observer);
		}
		// now writing the various update blocks itself - note that their
		// order crucial
		if (mask100update) {
			appendMask100Update(cacheStream);
		}
		if (animationRequest != -1) {
			appendAnimationRequest(cacheStream);
		}
		if (forcedChatUpdateRequired) {
			appendForcedChat(cacheStream);
		}
		if (isChatTextUpdateRequired()) {
			appendPlayerChatText(cacheStream);
		}
		if (faceUpdateRequired) {
			appendFaceUpdate(cacheStream);
		}
		if (isAppearanceUpdateRequired()) {
			appendPlayerAppearance(cacheStream);
		}
		if (FocusPointX != -1) {
			appendSetFocusDestination(cacheStream);
		}
		if (isHitUpdateRequired()) {
			appendHitUpdate(cacheStream);
		}
		if (hitUpdateRequired2) {
			appendHitUpdate2(cacheStream);
		}

		if (!this.playerName.equals(observer.playerName) && !needsBlock) {
			setCachedBlock(cacheStream);
		}

		/** Add the cached block to the update block. */
		str.writeBytes(cacheStream.buffer, cacheStream.currentOffset, 0);
	}

	public boolean forceMovementUpdateRequired = false;
	public int startX = -1;
	public int startY = -1;
	public int endX = -1;
	public int endY = -1;
	public int startSpeed = -1;
	public int endSpeed = -1;
	public int direction = -1;

	public void setForceMovement(int startX, int startY, int endX, int endY, int startSpeed, int endSpeed,
			int directionn) {
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
		this.startSpeed = startSpeed;
		this.endSpeed = endSpeed;
		this.direction = directionn;
		this.forceMovementUpdateRequired = true;
		this.updateRequired = true;
	}

	public void appendForceMovementMask(Stream str, Player observer) {
		int localX = getX() - 8 * observer.getMapRegionX();
		int localY = getY() - 8 * observer.getMapRegionY();
		str.writeByteS(localX + startX);
		str.writeByteS(localY + startY);
		str.writeByteS(localX + endX);
		str.writeByteS(localY + endY);
		str.writeWordBigEndianA(startSpeed);
		str.writeWordA(endSpeed);
		str.writeByteS(direction);
	}

	public void clearUpdateFlags() {
		updateRequired = false;
		setChatTextUpdateRequired(false);
		setAppearanceUpdateRequired(false);
		setHitUpdateRequired(false);
		hitUpdateRequired2 = false;
		forcedChatUpdateRequired = false;
		forceMovementUpdateRequired = false;
		mask100update = false;
		animationRequest = -1;
		FocusPointX = -1;
		FocusPointY = -1;
		poisonMask = -1;
		venomMask = -1;
		faceUpdateRequired = false;
		face = 65535;
	}

	public void stopMovement() {
		resetWalkingQueue();
		stopFollowing();
	}

	public void stopFollowing() {
		followId = 0;
		followId2 = 0;
		followingX = 0;
		followingY = 0;
		faceUpdate(0);
	}

	private int newWalkCmdX[] = new int[walkingQueueSize];
	private int newWalkCmdY[] = new int[walkingQueueSize];
	public int newWalkCmdSteps = 0;
	private boolean newWalkCmdIsRunning = false;
	protected int travelBackX[] = new int[walkingQueueSize];
	protected int travelBackY[] = new int[walkingQueueSize];
	protected int numTravelBackSteps = 0;

	public void preProcessing() {
		newWalkCmdSteps = 0;
	}

	public void postProcessing() {
		if (this.newWalkCmdSteps > 0) {
			int firstX = this.getNewWalkCmdX()[0];
			int firstY = this.getNewWalkCmdY()[0];
			boolean found = false;
			this.numTravelBackSteps = 0;
			int ptr = this.wQueueReadPtr;
			int dir = Misc.direction(this.currentX, this.currentY, firstX, firstY);
			if (dir != -1 && (dir & 1) != 0) {
				do {
					int var13 = dir;
					--ptr;
					if (ptr < 0) {
						ptr = 49;
					}

					this.travelBackX[this.numTravelBackSteps] = this.walkingQueueX[ptr];
					this.travelBackY[this.numTravelBackSteps++] = this.walkingQueueY[ptr];
					dir = Misc.direction(this.walkingQueueX[ptr], this.walkingQueueY[ptr], firstX, firstY);
					if (var13 != dir) {
						found = true;
						break;
					}
				} while (ptr != this.wQueueWritePtr);
			} else {
				found = true;
			}

			if (found) {
				this.wQueueWritePtr = this.wQueueReadPtr;
				this.addToWalkingQueue(this.currentX, this.currentY);
				int i;
				if (dir != -1 && (dir & 1) != 0) {
					for (i = 0; i < this.numTravelBackSteps - 1; ++i) {
						this.addToWalkingQueue(this.travelBackX[i], this.travelBackY[i]);
					}

					i = this.travelBackX[this.numTravelBackSteps - 1];
					int wayPointY2 = this.travelBackY[this.numTravelBackSteps - 1];
					int wayPointX1;
					int wayPointY1;
					if (this.numTravelBackSteps == 1) {
						wayPointX1 = this.currentX;
						wayPointY1 = this.currentY;
					} else {
						wayPointX1 = this.travelBackX[this.numTravelBackSteps - 2];
						wayPointY1 = this.travelBackY[this.numTravelBackSteps - 2];
					}

					dir = Misc.direction(wayPointX1, wayPointY1, i, wayPointY2);
					if (dir != -1 && (dir & 1) == 0) {
						dir >>= 1;
						found = false;
						int x = wayPointX1;
						int y = wayPointY1;

						while (x != i || y != wayPointY2) {
							x += Misc.directionDeltaX[dir];
							y += Misc.directionDeltaY[dir];
							if ((Misc.direction(x, y, firstX, firstY) & 1) == 0) {
								found = true;
								break;
							}
						}

						if (!found) {
							this.println_debug("Fatal: Internal error: unable to determine connection vertex!  wp1=("
									+ wayPointX1 + ", " + wayPointY1 + "), wp2=(" + i + ", " + wayPointY2 + "), "
									+ "first=(" + firstX + ", " + firstY + ")");
						} else {
							this.addToWalkingQueue(wayPointX1, wayPointY1);
						}
					} else {
						this.println_debug("Fatal: The walking queue is corrupt! wp1=(" + wayPointX1 + ", " + wayPointY1
								+ "), " + "wp2=(" + i + ", " + wayPointY2 + ")");
					}
				} else {
					for (i = 0; i < this.numTravelBackSteps; ++i) {
						this.addToWalkingQueue(this.travelBackX[i], this.travelBackY[i]);
					}
				}

				for (i = 0; i < this.newWalkCmdSteps; ++i) {
					this.addToWalkingQueue(this.getNewWalkCmdX()[i], this.getNewWalkCmdY()[i]);
				}
			}

			this.isRunning = this.isNewWalkCmdIsRunning() || this.isRunning2;
		}
	}

	public int getMapRegionX() {
		return mapRegionX;
	}

	public int getMapRegionY() {
		return mapRegionY;
	}

	public int getX() {
		return absX;
	}

	public int getY() {
		return absY;
	}

	public int getLocalX() {
		return getX() - 8 * getMapRegionX();
	}

	public int getLocalY() {
		return getY() - 8 * getMapRegionY();
	}

	public int localX() {
		return this.getX() - this.getMapRegionX() * 8;
	}

	public int localY() {
		return this.getY() - this.getMapRegionY() * 8;
	}

	public int getId() {
		return playerId;
	}

	public int getFace() {
		return this.playerId + '\u8000';
	}

	public boolean inPcBoat() {
		return absX >= 2660 && absX <= 2663 && absY >= 2638 && absY <= 2643;
	}

	public boolean inPcGame() {
		return absX >= 2624 && absX <= 2690 && absY >= 2550 && absY <= 2619;
	}

	public void setHitDiff(int hitDiff) {
		this.hitDiff = hitDiff;
	}

	public void setHitDiff2(int hitDiff2) {
		this.hitDiff2 = hitDiff2;
	}

	public int getHitDiff() {
		return hitDiff;
	}

	public void setHitUpdateRequired(boolean hitUpdateRequired) {
		this.hitUpdateRequired = hitUpdateRequired;
	}

	public void setHitUpdateRequired2(boolean hitUpdateRequired2) {
		this.hitUpdateRequired2 = hitUpdateRequired2;
	}

	public boolean isHitUpdateRequired() {
		return hitUpdateRequired;
	}

	public boolean getHitUpdateRequired() {
		return hitUpdateRequired;
	}

	public boolean getHitUpdateRequired2() {
		return hitUpdateRequired2;
	}

	public void setAppearanceUpdateRequired(boolean appearanceUpdateRequired) {
		this.appearanceUpdateRequired = appearanceUpdateRequired;
	}

	public boolean isAppearanceUpdateRequired() {
		return appearanceUpdateRequired;
	}

	public void setChatTextEffects(int chatTextEffects) {
		this.chatTextEffects = chatTextEffects;
	}

	public int getChatTextEffects() {
		return chatTextEffects;
	}

	public void setChatTextSize(byte chatTextSize) {
		this.chatTextSize = chatTextSize;
	}

	public byte getChatTextSize() {
		return chatTextSize;
	}

	public void setChatTextUpdateRequired(boolean chatTextUpdateRequired) {
		this.chatTextUpdateRequired = chatTextUpdateRequired;
	}

	public boolean isChatTextUpdateRequired() {
		return chatTextUpdateRequired;
	}

	public void setChatText(byte chatText[]) {
		this.chatText = chatText;
	}

	public byte[] getChatText() {
		return chatText;
	}

	public void setChatTextColor(int chatTextColor) {
		this.chatTextColor = chatTextColor;
	}

	public int getChatTextColor() {
		return chatTextColor;
	}

	public void setNewWalkCmdX(int newWalkCmdX[]) {
		this.newWalkCmdX = newWalkCmdX;
	}

	public int[] getNewWalkCmdX() {
		return newWalkCmdX;
	}

	public void setNewWalkCmdY(int newWalkCmdY[]) {
		this.newWalkCmdY = newWalkCmdY;
	}

	public int[] getNewWalkCmdY() {
		return newWalkCmdY;
	}

	public void setNewWalkCmdIsRunning(boolean newWalkCmdIsRunning) {
		this.newWalkCmdIsRunning = newWalkCmdIsRunning;
	}

	public boolean isNewWalkCmdIsRunning() {
		return newWalkCmdIsRunning;
	}

	public void setInStreamDecryption(ISAACRandomGen inStreamDecryption) {
	}

	public void setOutStreamDecryption(ISAACRandomGen outStreamDecryption) {
	}

	public boolean samePlayer() {
		for (int j = 0; j < World.players.length; j++) {
			if (j == playerId)
				continue;
			if (World.players[j] != null) {
				if (World.players[j].playerName.equalsIgnoreCase(playerName)) {
					disconnected = true;
					return true;
				}
			}
		}
		return false;
	}

	public void putInCombat(int attacker) {
		underAttackBy = attacker;
		logoutDelay = System.currentTimeMillis();
		singleCombatDelay = System.currentTimeMillis();
	}

	public void dealDamage(int damage) {
		if (teleTimer <= 0)
			playerLevel[3] -= damage;
		else {
			if (hitUpdateRequired)
				hitUpdateRequired = false;
			if (hitUpdateRequired2)
				hitUpdateRequired2 = false;
		}

	}

	public void handleHitMask(int damage) {
		if (!hitUpdateRequired) {
			hitUpdateRequired = true;
			hitDiff = damage;
		} else if (!hitUpdateRequired2) {
			hitUpdateRequired2 = true;
			hitDiff2 = damage;
		}
		updateRequired = true;
	}

	public int calculatePVPLevel() {
		return (int) (0.1 * this.combatLevel) + 5 + this.wildLevel;
	}

	public boolean withinRange(Player otherPlayer) {
		int highRange = otherPlayer.combatLevel + calculatePVPLevel();
		int lowRange = otherPlayer.combatLevel - calculatePVPLevel();
		if (combatLevel >= lowRange && combatLevel <= highRange) {
			return true;
		}
		return false;
	}

	public String drawLevels() {
		int low = this.combatLevel - calculatePVPLevel();
		int high = this.combatLevel + calculatePVPLevel();
		if (high > 126)
			high = 126;
		if (low < 3)
			low = 3;
		return low + "-" + high;
	}

	/**
	 * @return the petIndex
	 */
	public int getPetIndex() {
		return petIndex;
	}

	/**
	 * @param petIndex
	 *            the petIndex to set
	 */
	public void setPetIndex(int petIndex) {
		this.petIndex = petIndex;
	}

	public long getUsernameHash() {
		return usernameHash;
	}

	public void setUsernameHash(long hash) {
		this.usernameHash = hash;
	}

	public String getUsername() {
		return playerName;
	}

	public PlayerRights getRights() {
		return rights;
	}

	public void setRights(PlayerRights rights) {
		this.rights = rights;
	}

	public DonatorRights getDonatorRights() {
		return donatorRights;
	}

	public void setDonatorRights(DonatorRights rights) {
		this.donatorRights = rights;
	}

	public void sendMessage(String string) {
		getClient().sendMessage(string);
	}

	public ItemAssistant getItems() {
		return itemAssistant;
	}

	public PlayerAssistant getPA() {
		return playerAssistant;
	}

	public DialogueManager getDialogue() {
		return dialogueManager;
	}

	public ShopAssistant getShops() {
		return shopAssistant;
	}

	public CombatAssistant getCombat() {
		return combatAssistant;
	}

	public ActionHandler getActions() {
		return actionHandler;
	}

	public Bank getBank() {
		return bank;
	}

	public BankPin getBankPin() {
		return pin;
	}

	public TradeHandler getTradeHandler() {
		return tradeHandler;
	}

	public BankHandler getBankHandler() {
		return bankHandler;
	}

	public Duelling getDuel() {
		return duelling;
	}

	public int[] getHits() {
		return hits;
	}

	public int[] getDamage() {
		return this.damage;
	}

	public int getTzhaarWaveId() {
		return this.tzhaarWaveId;
	}

	public int getTzhaarToKill() {
		return this.tzhaarToKill;
	}

	public void setTzhaarToKill(int toKill) {
		this.tzhaarToKill = toKill;
	}

	public int getPetId() {
		return this.petId;
	}

	public boolean hasPid() {
		return hasPid;
	}

	public void setTzhaarWaveId(int wave) {
		this.tzhaarWaveId = wave;
	}

	public StopWatch getSqlTimer() {
		return sqlTimer;
	}

	public void setPetId(int id) {
		this.petId = id;
	}

	private boolean isBusy = false;

	public boolean isBusy() {
		return this.isBusy;
	}

	public void setBusyState(boolean state) {
		this.isBusy = state;
	}

	public int getWarnIndex() {
		return this.warnIndex;
	}

	public void setWarnIndex(int index) {
		this.warnIndex = index;
	}

	public int[] getCombatBonus() {
		return this.combatBonus;
	}

	public int[] getEquipment() {
		return this.playerEquipment;
	}

	public int[] getLevel() {
		return this.playerLevel;
	}

	public int[] getExperience() {
		return this.playerXP;
	}

	public Client getClient() { return this.client; }

	public Pets getPets() {
		return this.pets;
	}

	public boolean isSplitChat() { return this.splitChat; }

	public Stream getCachedBlock() { return this.cachedBlock; }

	public void setCachedBlock(Stream cachedBlock) { this.cachedBlock = cachedBlock; }

	public void setSplitChat(boolean b) { this.splitChat = b; }
}