package com.oldschoolpk.server.model.player;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

/**
 * Represents a player's privilege rights.
 * 
 * @author Gabriel Hannason
 */

public enum PlayerRights implements Rights {

	/*
	 * A regular member of the server.
	 */
	PLAYER(0, 30, "<col=000000>", 0),

	/*
	 * A member who has been with the server for a long time.
	 */
	VETERAN(9, 10, "<col=CD661D>", 0),
	
	DICER(8, 0, "", 0),
	
	YOUTUBER(10, 0, "", 0),

	/*
	 * A member who has the ability to help people better.
	 */
	SUPPORT(7, 0, "<col=FF0000><shad=0>", 1),

	/*
	 * A moderator who has more privilege than other regular members and
	 * donators.
	 */
	MODERATOR(1, 0, "<col=42ddd4><shad=0>", 2),

	/*
	 * The second-highest-privileged member of the server.
	 */
	ADMINISTRATOR(2, 0, "<col=FFFF64><shad=0>", 4),

	/*
	 * The Developer of the server, has same rights as the owner.
	 */
	DEVELOPER(3, 0, "<shad=345B7F>", 5),

	/*
	 * The highest-privileged member of the server
	 */
	OWNER(2, 0, "<col=B40404>", 6);

	PlayerRights(int crown, int yellDelaySeconds, String yellHexColorPrefix, int priority) {
		this.yellDelay = yellDelaySeconds;
		this.yellHexColorPrefix = yellHexColorPrefix;
		this.priority = priority;
		this.crown = crown;
	}

	private static final ImmutableSet<PlayerRights> STAFF = Sets.immutableEnumSet(SUPPORT, MODERATOR, ADMINISTRATOR, OWNER, DEVELOPER);

	private int priority;
	private int crown;
	private int yellDelay;
	private String yellHexColorPrefix;

	public int getYellDelay() {
		return yellDelay;
	}

	public String getYellPrefix() {
		return yellHexColorPrefix;
	}

	/**
	 * Checks if the player is a staff member
	 * 
	 * @return
	 */
	public boolean isStaff() {
		return STAFF.contains(this);
	}

	/**
	 * Gets the rank for a certain id.
	 * 
	 * @param id
	 *            The id (ordinal()) of the rank.
	 * @return rights.
	 */
	public static PlayerRights forId(int id) {
		for (PlayerRights rights : PlayerRights.values()) {
			if (rights.ordinal() == id) {
				return rights;
			}
		}
		return null;
	}

	@Override
	public int getCrownId() {
		return crown;
	}

	@Override
	public int yellDelay() {
		return yellDelay;
	}

	@Override
	public String yellColor() {
		return yellHexColorPrefix;
	}

	@Override
	public int priority() {
		return priority;
	}
	
	static PlayerRights lookup(int id) {
		for (PlayerRights rights : values()) {
			if (rights.ordinal() == id) {
				return rights;
			}
		}

		return null;
	}

}