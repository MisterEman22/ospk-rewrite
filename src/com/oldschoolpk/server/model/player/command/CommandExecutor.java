package com.oldschoolpk.server.model.player.command;

import com.oldschoolpk.server.model.player.Player;

/**
 * Command executor implementation.
 *
 * @Author Klepto
 */
public interface CommandExecutor {

	public abstract void execute(Player client, Command command);

	default boolean canExecute(Player client) {
		return true;
	}

}