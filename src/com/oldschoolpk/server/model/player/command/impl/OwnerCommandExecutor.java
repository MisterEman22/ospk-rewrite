package com.oldschoolpk.server.model.player.command.impl;

import java.io.FileNotFoundException;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.item.ItemDefinitions;
import com.oldschoolpk.server.model.item.equipment.EquipmentBonuses;
import com.oldschoolpk.server.model.npc.NPC;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.model.npc.drop.Drops;
import com.oldschoolpk.server.model.player.DonatorRights;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.model.player.command.Command;
import com.oldschoolpk.server.model.player.command.CommandExecutor;
import com.oldschoolpk.server.model.player.command.CommandExecutors;
import com.oldschoolpk.server.util.cache.defs.NPCDef;
import com.oldschoolpk.server.world.ShopHandler;

public class OwnerCommandExecutor implements CommandExecutor {

	@Override
	public void execute(Player client, Command command) {
		if (command.getKey().equalsIgnoreCase("invincible")) {
			client.sendMessage("You are now invincible.");
			client.setAttribute("invincible", true);
		}
		if (command.getKey().equalsIgnoreCase("special")) {
			client.sendMessage("You now have unlimited special.");
			client.setAttribute("special", true);
		}
		if (command.getKey().equalsIgnoreCase("test")) {
			for (NPC npc : NPCHandler.npcs) {
				if (npc == null) {
					continue;
				}
				if (npc.currentX != client.absX || npc.currentY != client.absY) {
					continue;
				}
				System.out.println(npc.getNPCType());
			}
		}

		if (command.getKey().startsWith("kick")) {
			String username = command.getArguments();
			
			Player target = World.getByUsername(username);
			
			if (target == null) {
				client.sendMessage("Cannot find the player " + username + ".");
				return;
			}
			
			target.disconnected = true;
		}
		if (command.getKey().startsWith("update")) {
			World.updateSeconds = command.getInt(0);
			World.updateAnnounced = false;
			World.updateRunning = true;
			World.updateStartTime = System.currentTimeMillis();
		} else if (command.getKey().startsWith("item")) {
			try {
				if (command.getLength() == 2) {
					int newItemID = command.getInt(0);
					int newItemAmount = command.getInt(1);
					if ((newItemID <= 25000) && (newItemID >= 0)) {
						client.getItems().addItem(newItemID, newItemAmount);
						System.out.println("Spawned: " + newItemID + " by: " + client.playerName);
					} else {
						client.sendMessage("No such item.");
					}
				} else {
					client.sendMessage("Use as ::item 995 200");
				}
			} catch (Exception e) {
			}
		} else if (command.getKey().startsWith("pnpc")) {
			int npc = Integer.parseInt(command.getArguments());
			if (npc < 9999) {
				client.npcId2 = npc;
				client.isNpc = true;
				client.updateRequired = true;
				client.appearanceUpdateRequired = true;
			}
		} else if (command.getKey().startsWith("unpc")) {
			client.isNpc = false;
			client.updateRequired = true;
			client.appearanceUpdateRequired = true;
		} else if (command.getKey().startsWith("getid")) {
			String name = command.getArguments();
			if (name.length() > 0) {
				for (int i = 0; i < NPCDef.totalNpcs; i++) {
					if (NPCDef.forId(i) == null)
						continue;
					if (NPCDef.forId(i).name == null)
						continue;
					if (name.contains(NPCDef.forId(i).name)) {
						client.sendMessage(NPCDef.forId(i).name);
					}
				}
			}
		} else if (command.getKey().startsWith("getanim")) {
			int npcId = Integer.valueOf(command.getArguments());
			client.sendMessage("[NPC_EMOTES] Stand: " + NPCDef.forId(npcId).standAnim + "	Walk: " + NPCDef.forId(npcId).walkAnim + "");
		} else if (command.getKey().startsWith("getitem")) {
			String name = command.getArguments();

			for (int i = 0; i < 13576; i++) {
				ItemDefinitions def = ItemDefinitions.forId(i);

				if (def != null && def.getName().toLowerCase().contains(name.toLowerCase())) {
					client.sendMessage("Id = " + i + ", name = " + def.getName());
				}
			}
		} else if (command.getKey().startsWith("reloadnpcs")) {
			for (int i = 0; i < NPCHandler.MAX_LISTED_NPCS; i++) {
				if (NPCHandler.npcs[i] != null) {
					NPCHandler.npcs[i].currentX = 0;
					NPCHandler.npcs[i].currentY = 0;
					NPCHandler.npcs[i] = null;
				}
			}
			Server.npcHandler = new NPCHandler();
			client.sendMessage("Successfully reloaded npcs.");
		} else if (command.getKey().startsWith("reloadshops")) {
			Server.shopHandler = new ShopHandler();
			Server.shopHandler.loadShops("shops.cfg");
		} else if (command.getKey().startsWith("reloaddrops")) {
			Drops.parse();
			client.sendMessage("Successfully reloaded drops.");
		} else if (command.getKey().equalsIgnoreCase("reloaditems")) {
			try {
				ItemDefinitions.init();
				EquipmentBonuses.init();
				client.sendMessage("Successfully reloaded item definitions.");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else if (command.getKey().equalsIgnoreCase("reloadcommands")) {
			CommandExecutors.reload();
		} else if (command.getKey().startsWith("giverights")) {
			if (command.getLength() < 2) {
				client.sendMessage("Invalid command, [::givedonator rank name]");
				return;
			}
			String rankName = command.get(0).toUpperCase();

			PlayerRights right = null;

			try {
				right = PlayerRights.valueOf(rankName);
			} catch (Exception e) {
				client.sendMessage("The rank " + rankName + " doesn't exist.");
				return;
			}
			String name = command.getAfter(1);

			Player target = World.getByUsername(name);

			if (target == null) {
				client.sendMessage("Unable to find the player " + name + ".");
				return;
			}

			target.setRights(right);
			target.sendMessage("Your player rank has been set to " + right.getTitle() + ". Please relog to show the crown.");
			client.sendMessage("You have set the player " + target.playerName + "'s player rank to " + right.getTitle() + ".");
		} else if (command.getKey().startsWith("givedonator")) {

			if (command.getLength() < 2) {
				client.sendMessage("Invalid command, [::givedonator rank name]");
				return;
			}
			String rankName = command.get(0).toUpperCase();

			DonatorRights right = null;

			try {
				right = DonatorRights.valueOf(rankName);
			} catch (Exception e) {
				client.sendMessage("The rank " + rankName + " doesn't exist.");
				return;
			}
			String name = command.getAfter(1);

			Player target = World.getByUsername(name);

			if (target == null) {
				client.sendMessage("Unable to find the player " + name + ".");
				return;
			}

			target.setDonatorRights(right);
			target.sendMessage("Your donator rank has been set to " + right.getTitle() + ". Please relog to show the crown.");
			client.sendMessage("You have set the player " + target.playerName + "'s donator rank to " + right.getTitle() + ".");
		} else if (command.getKey().equalsIgnoreCase("maxhit")) {
			client.sendMessage("Max Hit: " + client.getCombat().calculateMeleeMaxHit());
		} else if (command.getKey().equalsIgnoreCase("selfhurt")) {
			int damage = command.getInt(0);
			client.dealDamage(damage);
			client.handleHitMask(damage);
		} else if (command.getKey().startsWith("givetokens")) {
			int tokens = command.getInt(0);
			Player player = World.getByUsername(command.getAfter(1));
			
			if (player == null) {
				client.sendMessage("The player is offline.");
				return;
			}
			player.donationPoints += tokens;
			client.sendMessage("Gave a total donation token amount of " + tokens + " to " + player.getUsername() + ".");
			player.sendMessage("The player " + client.getUsername() + " has given you " + tokens + " donator tokens.");
		} else if (command.getKey().startsWith("giveamount")) {
			int amount = command.getInt(0);
			Player player = World.getByUsername(command.getAfter(1));
			
			if (player == null) {
				client.sendMessage("The player is offline.");
				return;
			}
			client.sendMessage("Gave a total donated amount of " + amount + " to " + player.getUsername() + ".");
			client.donationAmount += amount;
			player.sendMessage("The player " + client.getUsername() + " has given you " + amount + " donation total.");
		}
	}

	@Override
	public boolean canExecute(Player client) {
		return client.getRights().isSuperior(PlayerRights.ADMINISTRATOR);
	}

}
