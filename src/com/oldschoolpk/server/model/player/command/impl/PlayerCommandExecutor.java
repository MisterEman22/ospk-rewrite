package com.oldschoolpk.server.model.player.command.impl;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.model.Position;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.content.dialogue.impl.PinDialogue;
import com.oldschoolpk.server.model.content.teleport.TeleportExecutor;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.command.Command;
import com.oldschoolpk.server.model.player.command.CommandExecutor;
import com.oldschoolpk.server.sql.DonationSQL;
import com.oldschoolpk.server.sql.VoteSQL;
import com.oldschoolpk.server.util.Misc;

public class PlayerCommandExecutor implements CommandExecutor {

	@Override
	public void execute(Player client, Command command) {

		if (command.getKey().equalsIgnoreCase("pinsettings")) {
			client.getDialogue().start(new PinDialogue(1));
		}
		if (command.getKey().equalsIgnoreCase("stuck")) {
			if (System.currentTimeMillis() - client.lastStuck < 30000) {
				client.sendMessage("You can only use this command every once 30 seconds.");
				return;
			}
			if (client.duelStatus > 0 && client.duelStatus <= 5 || client.underAttackBy > 0) {
				client.sendMessage("You can't do this right now.");
			}
			client.getPA().globalYell("@cr6@[@blu@STUCK@bla@]@red@" + client.playerName + " is stuck and needs help.");
			client.lastStuck = System.currentTimeMillis();
		}
		if (command.getKey().equalsIgnoreCase("resetkdr")) {
			if (client.getDonatorRights().isDonator()) {
				client.kills = 0;
				client.deaths = 0;
			} else {
				client.sendMessage("You need to be a donator or higher rank to use this command.");
			}
		}

		if (command.getKey().startsWith("setlevel") || command.getKey().startsWith("setlvl")) {
			client.sendMessage("@blu@To set your levels click the Skill tab, click the skill and set the values");
		}
		if (command.getKey().equalsIgnoreCase("commands")) {
			client.sendMessage("::duel, ::players, ::barrage, ::veng, ::changepassword <pass>, ::kdr");
			client.sendMessage("::vote, ::donate, ::funpk (you don't lose items here), ::stuck");
			client.sendMessage("::vengtimer, ::hpoverlay");
		}
		if (command.getKey().equalsIgnoreCase("vote")) {
			client.getPA().sendFrame126("http://oldschoolpk.com/", 12000);
		}
		if (command.getKey().equalsIgnoreCase("donate")) {
			client.getPA().sendFrame126("http://oldschoolpk.com/", 12000);
		}
		if (command.getKey().equalsIgnoreCase("barrage")) {
			if (client.inDuelArena()) {
				client.sendMessage("You can't do this here.");
				return;
			}
			client.getItems().addItem(560, 5000);
			client.getItems().addItem(565, 5000);
			client.getItems().addItem(555, 5000);
		}
		if (command.getKey().equalsIgnoreCase("veng")) {
			if (client.inDuelArena()) {
				client.sendMessage("You can't do this here.");
				return;
			}
			client.getItems().addItem(9075, 5000);
			client.getItems().addItem(557, 5000);
			client.getItems().addItem(560, 5000);
		}

		if (command.getKey().startsWith("changepassword")) {
			client.playerPass = command.getArguments();
			client.sendMessage("Your password is now: " + client.playerPass);
		}

		if (command.getKey().equalsIgnoreCase("funpk")) {
			if (!client.inSafeZone()) {
				client.sendMessage("Please run to safety first.");
				return;
			}
			TeleportExecutor.teleport(client, new Position(3328, 4751, 0));
			client.sendMessage("Risk free PKing zone.");
		}
		if (command.getKey().equalsIgnoreCase("gamble")) {
			if (!client.inSafeZone()) {
				client.sendMessage("Please run to safety first.");
				return;
			}
			TeleportExecutor.teleport(client, new Position(2086, 4466, 0));
			client.sendMessage("Gambling is at your own risk.");
		}
		if (command.getKey().equalsIgnoreCase("duel")) {
			if (!client.inSafeZone()) {
				client.sendMessage("Please run to safety first.");
				return;
			}
			TeleportExecutor.teleport(client, new Position(3367, 3269, 0));
		}
		if (command.getKey().equalsIgnoreCase("players")) {
			if (World.getPlayerCount() > 1)
				client.sendMessage("There are currently @blu@" + World.getPlayerCount() + "@bla@ players online!");
			else
				client.sendMessage("There are currently @blu@" + World.getPlayerCount() + "@bla@ player online!");
		}
		if (command.getKey().equalsIgnoreCase("claimvote")) {
			VoteSQL.claimVoteRewards(client);
		}
		if (command.getKey().equalsIgnoreCase("claimpayment")) {
			DonationSQL.claimDonationRewards(client);
		}
		if (command.getKey().equalsIgnoreCase("skull")) {
			client.isSkulled = true;
			client.skullTimer = Config.SKULL_TIMER;
			client.headIconPk = 0;
			client.getPA().requestUpdates();
		}
		if (command.getKey().equalsIgnoreCase("xplock")) {
			client.experienceLock = !client.experienceLock;
			if (client.experienceLock) {
				client.sendMessage("Your experience is now @red@LOCKED@bla@.");
			} else {
				client.sendMessage("Your experience is now @gre@UNLOCKED@bla@.");
			}
		}

		if (command.getKey().startsWith("empty")) {
			client.getItems().removeAllItems();
		}
	}

}
