package com.oldschoolpk.server.model.player.command.impl;

import com.oldschoolpk.server.model.Client;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.model.player.command.Command;
import com.oldschoolpk.server.model.player.command.CommandExecutor;
import com.oldschoolpk.server.sanction.SanctionHandler;

public class SanctionCommandExecutor implements CommandExecutor {

	@Override
	public void execute(Player client, Command command) {
		
		if (command.getKey().startsWith("unmute")) {

			if (client.getRights().isInferior(PlayerRights.MODERATOR)) {
				client.sendMessage("You do not have the rights to unmute players.");
				return;
			}
			String playerName = command.getArguments();
			Player target = World.getByUsername(playerName);

			if (target == null) {
				client.sendMessage("Could not find the player " + target + ".");
				return;
			}

			target.sendMessage("Your account has been unmuted by @blu@" + client.playerName + "@bla@.");
			SanctionHandler.deleteFromSactionList(SanctionHandler.LOCATION + "mutes.txt", playerName);
			client.sendMessage("You have unmuted: @blu@" + playerName + "@bla@.");
		} else if (command.getKey().startsWith("mute")) {
			String playerName = command.getArguments();
			Player target = World.getByUsername(playerName);

			if (target == null) {
				client.sendMessage("Could not find the player " + target + ".");
				return;
			}
			target.sendMessage("Your account has temporarily been muted by @blu@" + client.playerName + "@bla@.");
			SanctionHandler.mutePlayer(playerName);
			client.sendMessage("You have muted: @blu@" + playerName + "@bla@.");
		} else if (command.getKey().startsWith("unban")) {
			if (client.getRights().isInferior(PlayerRights.ADMINISTRATOR)) {
				client.sendMessage("You do not have the rights to use this command.");
				return;
			}
			String playerName = command.getArguments();
			if (playerName.length() > 0) {
				SanctionHandler.deleteFromSactionList(SanctionHandler.LOCATION + "bans.txt", playerName);
				client.sendMessage("You have unbanned: @blu@" + playerName + "@bla@.");
			}
		} else if (command.getKey().startsWith("ban")) {
			if (client.getRights().isInferior(PlayerRights.MODERATOR)) {
				client.sendMessage("You do not have the rights to use this command.");
				return;
			}

			String playerName = command.getArguments();
			Player target = World.getByUsername(playerName);

			if (target == null) {
				client.sendMessage("Unable to find the player " + playerName + ".");
				return;
			}
			target.disconnected = true;
			SanctionHandler.banPlayer(playerName);
			client.sendMessage("You have banned: @blu@" + playerName + "@bla@.");
		} else if (command.getKey().startsWith("ipban")) {
			if (client.getRights().isInferior(PlayerRights.ADMINISTRATOR)) {
				client.sendMessage("You do not have the rights to use this command.");
				return;
			}

			String playerName = command.getArguments();
			Player target = World.getByUsername(playerName);

			if (target == null) {
				client.sendMessage("Unable to find the player " + playerName + ".");
				return;
			}
			client.sendMessage("You have IP banned name = " + target.playerName + ", ip = " + target.connectedFrom);
			SanctionHandler.IPBanPlayer(target.connectedFrom);
			for (Player p : World.players) {
				if (p != null && p.connectedFrom.equalsIgnoreCase(target.connectedFrom)) {
					p.disconnected = true;
					client.sendMessage("Affected: " + p.playerName);
				}
			}
		} else if (command.getKey().startsWith("ipmute")) {

			if (client.getRights().isInferior(PlayerRights.MODERATOR)) {
				client.sendMessage("You do not have the rights to use this command.");
				return;
			}

			String playerName = command.getArguments();
			Player target = World.getByUsername(playerName);

			if (target == null) {
				client.sendMessage("Unable to find the player " + playerName + ".");
				return;
			}
			client.sendMessage("You have IP muted name = " + target.playerName + ", ip = " + target.connectedFrom);
			target.sendMessage("Your account has been IP muted by @blu@" + client.playerName + "@bla@.");
			SanctionHandler.IPMutePlayer(target.connectedFrom);
		}
	}

	@Override
	public boolean canExecute(Player client) {
		return client.getRights().isStaff();
	}

}
