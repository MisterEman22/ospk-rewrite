package com.oldschoolpk.server.model.player.command.impl;

import com.oldschoolpk.server.model.Client;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.model.player.command.Command;
import com.oldschoolpk.server.model.player.command.CommandExecutor;

public class StaffCommandExecutor implements CommandExecutor {

	@Override
	public void execute(Player client, Command command) {
		if (command.getKey().startsWith("teletome")) {

			if (client.getRights().isInferior(PlayerRights.MODERATOR)) {
				client.sendMessage("You do not have the rights to unmute players.");
				return;
			}

			String playerName = command.getArguments();
			Player target = World.getByUsername(playerName);

			if (target == null) {
				client.sendMessage("Could not find the player " + playerName + ".");
				return;
			}
			client.sendMessage("You have teleported to @blu@" + target.playerName + "@bla@ to you.");
			target.sendMessage("You have been teleported to @blu@" + client.playerName + "@bla@.");
			target.getPA().movePlayer(client.getX(), client.getY(), client.heightLevel);
		} else if (command.getKey().startsWith("teleto")) {
			String playerName = command.getArguments();
			Player target = World.getByUsername(playerName);

			if (target == null) {
				client.sendMessage("Could not find the player " + playerName + ".");
				return;
			}
			client.sendMessage("You have teleported to @blu@" + target.playerName + "@bla@.");
			client.getPA().movePlayer(target.getX(), target.getY(), target.heightLevel);
		} else if (command.getKey().startsWith("tele")) {
			if (command.getLength() > 2)
				client.getPA().movePlayer(command.getInt(0), command.getInt(1), command.getInt(2));
			else if (command.getLength() == 2)
				client.getPA().movePlayer(command.getInt(0), command.getInt(1), client.heightLevel);
		} else if (command.getKey().startsWith("checkinv")) {
			String playerName = command.getArguments();
			Player target = World.getByUsername(playerName);

			if (target == null) {
				client.sendMessage("Could not find the player " + playerName + ".");
				return;
			}
			int[] items = target.playerItems;
			int[] amounts = target.playerItemsN;

			client.getClient().getOutStream().createFrameVarSizeWord(53);
			client.getClient().getOutStream().writeWord(3214);
			client.getClient().getOutStream().writeWord(items.length);
			for (int i = 0; i < items.length; i++) {
				if (amounts[i] > 254) {
					client.getClient().getOutStream().writeByte(255);
					client.getClient().getOutStream().writeDWord_v2(amounts[i]);
				} else {
					client.getClient().getOutStream().writeByte(amounts[i]);
				}
				client.getClient().getOutStream().writeWordBigEndianA(items[i]);
			}
			client.getClient().getOutStream().endFrameVarSizeWord();
			client.getClient().flushOutStream();
			client.sendMessage("Viewing the inventory of : " + target.getUsername() + ", type ::myinv to return.");
		} else if (command.getKey().equalsIgnoreCase("myinv")) {
			client.getItems().resetItems(3214);
		} else if (command.getKey().startsWith("checkbank")) {
			String playerName = command.getArguments();
			Player target = World.getByUsername(playerName);

			if (target == null) {
				client.sendMessage("Could not find the player " + playerName + ".");
				return;
			}
			
			client.setAttribute("viewing_other_bank", target);
			if (client.getClient().getOutStream() != null) {
				client.isBanking = true;
				client.getItems().resetBank();
				client.getItems().resetTempItems();
				client.getClient().getOutStream().createFrame(248);
				client.getClient().getOutStream().writeWordA(5292);
				client.getClient().getOutStream().writeWord(5063);
			}
		}
	}

	@Override
	public boolean canExecute(Player client) {
		return client.getRights().isStaff();
	}

}
