package com.oldschoolpk.server.model.player.command;

/**
 * Player command wrapper.
 *
 * @Author Klepto
 */
public class Command {

	private String key, arguments;
	private String[] argumentArray;

	public Command(String command) {
		key = arguments = "";
		argumentArray = new String[0];

		String[] parts = command.toLowerCase().split(" ", 2);
		if (parts.length > 0) {
			key = parts[0];
		}
		if (parts.length > 1) {
			arguments = parts[1];
			argumentArray = arguments.split(" ");
		}
	}

	public String getKey() {
		return key;
	}

	public String getArguments() {
		return arguments;
	}

	public String get(int index) {
		try {
			return argumentArray[index];
		} catch (ArrayIndexOutOfBoundsException excention) {
			return "";
		}
	}

	public String getAfter(int index) {
		return arguments.split(" ", index + 1)[index];
	}

	public int getInt(int index) {
		try {
			return Integer.parseInt(get(index));
		} catch (NumberFormatException exception) {
			return -1;
		}
	}
	
	public boolean getBoolean(int index) {
		try {
			return Boolean.parseBoolean(get(index));
		} catch (Throwable exception) {
			return false;
		}
	}

	public int getLength() {
		return argumentArray.length;
	}

}