package com.oldschoolpk.server.model.player.command;

import java.util.HashSet;
import java.util.Set;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.command.impl.DonatorCommandExecutor;
import com.oldschoolpk.server.model.player.command.impl.OwnerCommandExecutor;
import com.oldschoolpk.server.model.player.command.impl.PlayerCommandExecutor;
import com.oldschoolpk.server.model.player.command.impl.SanctionCommandExecutor;
import com.oldschoolpk.server.model.player.command.impl.StaffCommandExecutor;

/**
 * Command executors manager.
 *
 * @Author Klepto
 */
public class CommandExecutors {

	private static final Set<CommandExecutor> executors = new HashSet<>();
	
	static {
		init();
	}

	public static void init() {
		executors.add(new StaffCommandExecutor());
		executors.add(new OwnerCommandExecutor());
		executors.add(new DonatorCommandExecutor());
		executors.add(new PlayerCommandExecutor());
		executors.add(new SanctionCommandExecutor());
	}

	public static void reload() {
		executors.clear();
		init();
	}

	public static void submit(Player player, String commandString) {
		Command command = new Command(commandString);
		for (CommandExecutor executor : executors) {
			if (executor.canExecute(player)) {
				executor.execute(player, command);
			}
		}
	}

}