package com.oldschoolpk.server.model.player;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.Animation;
import com.oldschoolpk.server.model.Graphic;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.content.QuestTab;
import com.oldschoolpk.server.model.content.RewardHandler;
import com.oldschoolpk.server.model.item.Item;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.util.cache.region.Region;
import com.oldschoolpk.server.world.Clan;

public class PlayerAssistant {

	private Player c;

	public PlayerAssistant(Player Client) {
		this.c = Client;
	}

	public int CraftInt, Dcolor, FletchInt;

	/**
	 * MulitCombat icon
	 *
	 * @param i1
	 *            0 = off 1 = on
	 */
	public void multiWay(int i1) {
		// synchronized(c) {
		c.getClient().getOutStream().createFrame(61);
		c.getClient().getOutStream().writeByte(i1);
		c.updateRequired = true;
		c.setAppearanceUpdateRequired(true);
	}

	public int getTotalWealth() {
		int wealth = 0;
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] > 0) {
				wealth += c.getShops().getItemShopValue(c.playerItems[i] - 1);
			}
		}
		for (int i = 0; i < c.getEquipment().length; i++) {
			if (c.getEquipment()[i] > 0) {
				wealth += c.getShops().getItemShopValue(c.getEquipment()[i]);
			}
		}
		return wealth;
	}

	/*
	 * Vengeance
	 */
	public void castVeng() {
		if (c.getLevel()[6] < 94) {
			c.sendMessage("You need a magic level of 94 to cast this spell.");
			return;
		}
		if (c.getLevel()[1] < 40) {
			c.sendMessage("You need a defence level of 40 to cast this spell.");
			return;
		}
		if (!c.getItems().playerHasItem(9075, 4) || !c.getItems().playerHasItem(557, 10) || !c.getItems().playerHasItem(560, 2)) {
			c.sendMessage("You don't have the required runes to cast this spell.");
			return;
		}
		if (c.duelRule[4]) {
			c.sendMessage("Magic has been disabled in this duel.");
			return;
		}
		if (System.currentTimeMillis() - c.lastCast < 30000) {
			c.sendMessage("You can only cast vengeance every 30 seconds.");
			return;
		}
		if (c.vengOn) {
			c.sendMessage("You already have vengeance casted.");
			return;
		}

		c.vengTimer = 30;
		c.playAnimation(Animation.create(4410));
		c.playGraphic(Graphic.create(726, 0, 100));// Just use c.gfx100
		c.getItems().deleteItem2(9075, 4);
		c.getItems().deleteItem2(557, 10);// For these you need to change to
		// deleteItem(item, itemslot,
		// amount);.
		c.getItems().deleteItem2(560, 2);
		addSkillXP(112, 6);
		refreshSkill(6);
		c.vengOn = true;
		c.lastCast = System.currentTimeMillis();
	}

	public void resetAutocast() {
		c.autocastId = 0;
		c.autocasting = false;
		c.getPA().sendFrame36(108, 0);
	}

	public void sendFrame126(String s, int id) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrameVarSizeWord(126);
			c.getClient().getOutStream().writeString(s);
			c.getClient().getOutStream().writeWordA(id);
			c.getClient().getOutStream().endFrameVarSizeWord();
		}
	}

	public void sendLink(String s) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrameVarSizeWord(187);
			c.getClient().getOutStream().writeString(s);
		}
	}

	public void setSkillLevel(int skillNum, int currentLevel, int XP) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(134);
			c.getClient().getOutStream().writeByte(skillNum);
			c.getClient().getOutStream().writeDWord_v1(XP);
			c.getClient().getOutStream().writeByte(currentLevel);
		}
	}

	public void sendFrame106(int sideIcon) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(106);
			c.getClient().getOutStream().writeByteC(sideIcon);
			requestUpdates();
		}
	}

	public void sendFrame107() {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(107);
		}
	}

	public void sendFrame36(int id, int state) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(36);
			c.getClient().getOutStream().writeWordBigEndian(id);
			c.getClient().getOutStream().writeByte(state);
		}
	}

	public void sendFrame185(int Frame) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(185);
			c.getClient().getOutStream().writeWordBigEndianA(Frame);
		}
	}

	public void showInterface(int interfaceid) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(97);
			c.getClient().getOutStream().writeWord(interfaceid);
			c.getClient().flushOutStream();
		}
	}

	public void sendFrame248(int MainFrame, int SubFrame) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(248);
			c.getClient().getOutStream().writeWordA(MainFrame);
			c.getClient().getOutStream().writeWord(SubFrame);
		}
	}

	public void sendFrame246(int MainFrame, int SubFrame, int SubFrame2) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(246);
			c.getClient().getOutStream().writeWordBigEndian(MainFrame);
			c.getClient().getOutStream().writeWord(SubFrame);
			c.getClient().getOutStream().writeWord(SubFrame2);
		}
	}

	public void sendFrame171(int MainFrame, int SubFrame) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(171);
			c.getClient().getOutStream().writeByte(MainFrame);
			c.getClient().getOutStream().writeWord(SubFrame);
		}
	}

	public void sendFrame200(int MainFrame, int SubFrame) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(200);
			c.getClient().getOutStream().writeWord(MainFrame);
			c.getClient().getOutStream().writeWord(SubFrame);
		}
	}

	public void sendFrame70(int i, int o, int id) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(70);
			c.getClient().getOutStream().writeWord(i);
			c.getClient().getOutStream().writeWordBigEndian(o);
			c.getClient().getOutStream().writeWordBigEndian(id);
		}
	}

	public void sendFrame75(int MainFrame, int SubFrame) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(75);
			c.getClient().getOutStream().writeWordBigEndianA(MainFrame);
			c.getClient().getOutStream().writeWordBigEndianA(SubFrame);
		}
	}

	public void sendFrame164(int Frame) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(164);
			c.getClient().getOutStream().writeWordBigEndian_dup(Frame);
		}
	}

	public void setPrivateMessaging(int i) { // friends and ignore list status
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(221);
			c.getClient().getOutStream().writeByte(i);
		}
	}

	public void setChatOptions(int publicChat, int privateChat, int tradeBlock) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(206);
			c.getClient().getOutStream().writeByte(publicChat);
			c.getClient().getOutStream().writeByte(privateChat);
			c.getClient().getOutStream().writeByte(tradeBlock);
		}
	}

	public void sendFrame87(int id, int state) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(87);
			c.getClient().getOutStream().writeWordBigEndian_dup(id);
			c.getClient().getOutStream().writeDWord_v1(state);
		}
	}

	public void sendPM(long name, int rights, byte[] chatmessage, int messagesize) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrameVarSize(196);
			c.getClient().getOutStream().writeQWord(name);
			c.getClient().getOutStream().writeDWord(c.lastChatId++);
			c.getClient().getOutStream().writeByte(rights);
			c.getClient().getOutStream().writeBytes(chatmessage, messagesize, 0);
			c.getClient().getOutStream().endFrameVarSize();
			Misc.textUnpack(chatmessage, messagesize);
			Misc.longToPlayerName(name);
		}
	}

	public void createPlayerHints(int type, int id) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(254);
			c.getClient().getOutStream().writeByte(type);
			c.getClient().getOutStream().writeWord(id);
			c.getClient().getOutStream().write3Byte(0);
		}
	}

	public void createObjectHints(int x, int y, int height, int pos) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(254);
			c.getClient().getOutStream().writeByte(pos);
			c.getClient().getOutStream().writeWord(x);
			c.getClient().getOutStream().writeWord(y);
			c.getClient().getOutStream().writeByte(height);
		}
	}

	public void loadPM(long playerName, int world) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			if (world != 0) {
				world += 9;
			} else if (!Config.WORLD_LIST_FIX) {
				world += 1;
			}
			c.getClient().getOutStream().createFrame(50);
			c.getClient().getOutStream().writeQWord(playerName);
			c.getClient().getOutStream().writeByte(world);
		}
	}

	public void removeAllWindows() {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getPA().resetVariables();
			c.getDialogue().interrupt();
			c.removeAttribute("viewing_other_bank");
			c.getClient().getOutStream().createFrame(219);
		}
	}

	public void closeAllWindows() {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(219);
			c.removeAttribute("viewing_other_bank");
			c.getDialogue().interrupt();
		}
	}

	public void sendFrame34(int id, int slot, int column, int amount) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrameVarSizeWord(34); // init
																		// item
																		// to
																		// smith
			// screen
			c.getClient().getOutStream().writeWord(column); // Column Across
															// Smith Screen
			c.getClient().getOutStream().writeByte(4); // Total Rows?
			c.getClient().getOutStream().writeDWord(slot); // Row Down The Smith
															// Screen
			c.getClient().getOutStream().writeWord(id + 1); // item
			c.getClient().getOutStream().writeByte(amount); // how many there
															// are?
			c.getClient().getOutStream().endFrameVarSizeWord();
		}
	}

	public void walkableInterface(int id) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(208);
			c.getClient().getOutStream().writeWordBigEndian_dup(id);
		}
	}

	public int mapStatus = 0;

	public void sendFrame99(int state) { // used for disabling map
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			if (mapStatus != state) {
				mapStatus = state;
				c.getClient().getOutStream().createFrame(99);
				c.getClient().getOutStream().writeByte(state);
			}
		}
	}

	public void sendCrashFrame() { // used for crashing cheat clients
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(123);
		}
	}

	/**
	 * Reseting animations for everyone
	 **/

	public void frame1() {
		for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			if (World.players[i] != null) {
				Player person = World.players[i];
				if (person != null) {
					if (person.getClient().getOutStream() != null && !person.disconnected) {
						if (c.distanceToPoint(person.getX(), person.getY()) <= 25) {
							person.getClient().getOutStream().createFrame(1);
							person.getPA().requestUpdates();
						}
					}
				}
			}
		}
	}

	/**
	 * Creating projectile
	 **/
	public void createProjectile(int x, int y, int offX, int offY, int angle, int speed, int gfxMoving, int startHeight, int endHeight, int lockon, int time) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(85);
			c.getClient().getOutStream().writeByteC((y - (c.getMapRegionY() * 8)) - 2);
			c.getClient().getOutStream().writeByteC((x - (c.getMapRegionX() * 8)) - 3);
			c.getClient().getOutStream().createFrame(117);
			c.getClient().getOutStream().writeByte(angle);
			c.getClient().getOutStream().writeByte(offY);
			c.getClient().getOutStream().writeByte(offX);
			c.getClient().getOutStream().writeWord(lockon);
			c.getClient().getOutStream().writeWord(gfxMoving);
			c.getClient().getOutStream().writeByte(startHeight);
			c.getClient().getOutStream().writeByte(endHeight);
			c.getClient().getOutStream().writeWord(time);
			c.getClient().getOutStream().writeWord(speed);
			c.getClient().getOutStream().writeByte(16);
			c.getClient().getOutStream().writeByte(64);
		}
	}

	public void createProjectile2(int x, int y, int offX, int offY, int angle, int speed, int gfxMoving, int startHeight, int endHeight, int lockon, int time, int slope) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(85);
			c.getClient().getOutStream().writeByteC((y - (c.getMapRegionY() * 8)) - 2);
			c.getClient().getOutStream().writeByteC((x - (c.getMapRegionX() * 8)) - 3);
			c.getClient().getOutStream().createFrame(117);
			c.getClient().getOutStream().writeByte(angle);
			c.getClient().getOutStream().writeByte(offY);
			c.getClient().getOutStream().writeByte(offX);
			c.getClient().getOutStream().writeWord(lockon);
			c.getClient().getOutStream().writeWord(gfxMoving);
			c.getClient().getOutStream().writeByte(startHeight);
			c.getClient().getOutStream().writeByte(endHeight);
			c.getClient().getOutStream().writeWord(time);
			c.getClient().getOutStream().writeWord(speed);
			c.getClient().getOutStream().writeByte(slope);
			c.getClient().getOutStream().writeByte(64);
		}
	}

	public void createProjectile(int x, int y, int offX, int offY, int angle, int speed, int gfxMoving, int startHeight, int endHeight, int lockon, int time, int slope) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(85);
			c.getClient().getOutStream().writeByteC((y - (c.getMapRegionY() * 8)) - 2);
			c.getClient().getOutStream().writeByteC((x - (c.getMapRegionX() * 8)) - 3);
			c.getClient().getOutStream().createFrame(117);
			c.getClient().getOutStream().writeByte(angle);
			c.getClient().getOutStream().writeByte(offY);
			c.getClient().getOutStream().writeByte(offX);
			c.getClient().getOutStream().writeWord(lockon);
			c.getClient().getOutStream().writeWord(gfxMoving);
			c.getClient().getOutStream().writeByte(startHeight);
			c.getClient().getOutStream().writeByte(endHeight);
			c.getClient().getOutStream().writeWord(time);
			c.getClient().getOutStream().writeWord(speed);
			c.getClient().getOutStream().writeByte(slope);
			c.getClient().getOutStream().writeByte(64);
		}
	}

	public void createPlayersProjectile(int x, int y, int offX, int offY, int angle, int speed, int gfxMoving, int startHeight, int endHeight, int lockon, int time, int slope) {
		// synchronized(c) {
		for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			Player p = World.players[i];
			if (p != null) {
				if (p.getClient().getOutStream() != null) {
					if (p.distanceToPoint(x, y) <= 25) {
						if (p.heightLevel == c.heightLevel)
							p.getPA().createProjectile(x, y, offX, offY, angle, speed, gfxMoving, startHeight, endHeight, lockon, time, slope);
					}
				}
			}
		}
	}

	// projectiles for everyone within 25 squares
	public void createPlayersProjectile(int x, int y, int offX, int offY, int angle, int speed, int gfxMoving, int startHeight, int endHeight, int lockon, int time) {
		// synchronized(c) {
		for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			Player p = World.players[i];
			if (p != null) {
				if (p.getClient().getOutStream() != null) {
					if (p.distanceToPoint(x, y) <= 25) {
						if (p.heightLevel == c.heightLevel)
							p.getPA().createProjectile(x, y, offX, offY, angle, speed, gfxMoving, startHeight, endHeight, lockon, time);
					}
				}
			}
		}
	}

	public void createPlayersProjectile2(int x, int y, int offX, int offY, int angle, int speed, int gfxMoving, int startHeight, int endHeight, int lockon, int time, int slope) {
		// synchronized(c) {
		for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			Player p = World.players[i];
			if (p != null) {
				if (p.getClient().getOutStream() != null) {
					if (p.distanceToPoint(x, y) <= 25) {
						p.getPA().createProjectile2(x, y, offX, offY, angle, speed, gfxMoving, startHeight, endHeight, lockon, time, slope);
					}
				}
			}
		}
	}

	/**
	 * * GFX
	 **/
	public void stillGfx(int id, int x, int y, int height, int time) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(85);
			c.getClient().getOutStream().writeByteC(y - (c.getMapRegionY() * 8));
			c.getClient().getOutStream().writeByteC(x - (c.getMapRegionX() * 8));
			c.getClient().getOutStream().createFrame(4);
			c.getClient().getOutStream().writeByte(0);
			c.getClient().getOutStream().writeWord(id);
			c.getClient().getOutStream().writeByte(height);
			c.getClient().getOutStream().writeWord(time);
		}
	}

	// creates gfx for everyone
	public void createPlayersStillGfx(int id, int x, int y, int height, int time) {
		// synchronized(c) {
		for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			Player p = World.players[i];
			if (p != null) {
				if (p.getClient().getOutStream() != null) {
					if (p.distanceToPoint(x, y) <= 25) {
						p.getPA().stillGfx(id, x, y, height, time);
					}
				}
			}
		}
	}

	/**
	 * Objects, add and remove
	 **/
	public void object(int objectId, int objectX, int objectY, int face, int objectType) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(85);
			c.getClient().getOutStream().writeByteC(objectY - (c.getMapRegionY() * 8));
			c.getClient().getOutStream().writeByteC(objectX - (c.getMapRegionX() * 8));
			c.getClient().getOutStream().createFrame(101);
			c.getClient().getOutStream().writeByteC((objectType << 2) + (face & 3));
			c.getClient().getOutStream().writeByte(0);

			if (objectId != -1) { // removing
				c.getClient().getOutStream().createFrame(151);
				c.getClient().getOutStream().writeByteS(0);
				c.getClient().getOutStream().writeWordBigEndian(objectId);
				c.getClient().getOutStream().writeByteS((objectType << 2) + (face & 3));
			}
		}
	}

	public void checkObjectSpawn(int objectId, int objectX, int objectY, int height, int face, int objectType) {
		if (c.distanceToPoint(objectX, objectY) > 60 || c.heightLevel != height)
			return;
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(85);
			c.getClient().getOutStream().writeByteC(objectY - (c.getMapRegionY() * 8));
			c.getClient().getOutStream().writeByteC(objectX - (c.getMapRegionX() * 8));
			c.getClient().getOutStream().createFrame(101);
			c.getClient().getOutStream().writeByteC((objectType << 2) + (face & 3));
			c.getClient().getOutStream().writeByte(0);

			if (objectId != -1) { // removing
				c.getClient().getOutStream().createFrame(151);
				c.getClient().getOutStream().writeByteS(0);
				c.getClient().getOutStream().writeWordBigEndian(objectId);
				c.getClient().getOutStream().writeByteS((objectType << 2) + (face & 3));
			}
		}
	}

	/**
	 * Show option, attack, trade, follow etc
	 **/
	public String optionType = "null";

	public void sendOption(String s, int pos) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrameVarSize(104);
			c.getClient().getOutStream().writeByteC(pos);
			c.getClient().getOutStream().writeByteA(0);
			c.getClient().getOutStream().writeString(s);
			c.getClient().getOutStream().endFrameVarSize();
		}
	}

	public void showOption(int i, int l, String s, int a) {
		// synchronized(c) {
		if (c.getClient().getOutStream() != null && c != null) {
			if (!optionType.equalsIgnoreCase(s)) {
				optionType = s;
				c.getClient().getOutStream().createFrameVarSize(104);
				c.getClient().getOutStream().writeByteC(i);
				c.getClient().getOutStream().writeByteA(l);
				c.getClient().getOutStream().writeString(s);
				c.getClient().getOutStream().endFrameVarSize();
			}
		}
	}

	/**
	 * Open bank
	 **/
	public void sendFrame34a(int frame, int item, int slot, int amount) {
		c.getClient().getOutStream().createFrameVarSizeWord(34);
		c.getClient().getOutStream().writeWord(frame);
		c.getClient().getOutStream().writeByte(slot);
		c.getClient().getOutStream().writeWord(item + 1);
		c.getClient().getOutStream().writeByte(255);
		c.getClient().getOutStream().writeDWord(amount);
		c.getClient().getOutStream().endFrameVarSizeWord();
	}

	public void openUpBank() {
		if (c.getTradeHandler().getCurrentTrade() != null) {
			if (c.getTradeHandler().getCurrentTrade().isOpen()) {
				c.getTradeHandler().decline();
			}
		}
		if (c.getClient().getOutStream() != null && c != null) {
			c.isBanking = true;
			c.getItems().resetItems(5064);
			c.getItems().resetBank();
			c.getItems().resetTempItems();
			c.getClient().getOutStream().createFrame(248);
			c.getClient().getOutStream().writeWordA(5292);
			c.getClient().getOutStream().writeWord(5063);
		}
	}

	public void setScrollPos(int interfaceId, int scrollPos) {
		if (c.getClient().getOutStream() != null && c != null) {
			c.getClient().getOutStream().createFrame(79);
			c.getClient().getOutStream().writeWordBigEndian(interfaceId);
			c.getClient().getOutStream().writeWordA(scrollPos);
		}
	}
	
	private void sendIgnoreList() {
		c.getClient().getOutStream().createFrameVarSizeWord(214);
		for (long hash : c.ignores) {
			if (hash != 0)
				c.getClient().getOutStream().writeQWord(hash);
		}
		c.getClient().getOutStream().endFrameVarSizeWord();
	}
	
	public void logIntoPM() {
		setPrivateMessaging(2);
		sendIgnoreList();

		for (Long l : c.friends) {
			if (l != 0) {
				Player target = World.getByUsernameHash(l);
				if (target == null) {
					loadPM(l, 0);
				}
			}
		}

		/**
		 * Loop all the players
		 */
		for (Player p : World.players) {

			/**
			 * If the player is null, continue
			 */
			if (p == null)
				continue;

			/**
			 * The player is ignored
			 */
			boolean ignored = false;

			/**
			 * The person is on your friends list
			 */
			boolean friend = friendsContains(c, p.getUsernameHash());

			/**
			 * Check if the player is a friend, if so update them for us
			 */
			if (friend) {
				/**
				 * If your not a staff member and the person is ignoring you,
				 * dont send them as online
				 */
				if (!c.getRights().isStaff() && ignoresContains(p, c.getUsernameHash())) {
					ignored = true;
				}

				/**
				 * If your private chat is online or friends only, send you as
				 * online
				 */
				if (p.privateChat == 0 || p.privateChat == 1 && p.getPA().isInPM(c.getUsernameHash())) {
					loadPM(p.getUsernameHash(), ignored ? 2 : 1);
				}
			}

			/**
			 * Check if the person has us added
			 */
			friend = friendsContains(p, c.getUsernameHash());

			/**
			 * Reset being ignored
			 */
			ignored = false;

			/**
			 * Check if the other player has us as a friend, if so update them
			 * for us
			 */
			if (friend) {
				/**
				 * If your not a staff member and the person is ignoring you,
				 * dont send them as online
				 */
				if (!p.getRights().isStaff() && ignoresContains(c, p.getUsernameHash())) {
					ignored = true;
				}

				/**
				 * If your private chat is online or friends only, send you as
				 * online
				 */
				if (c.privateChat == 0 || c.privateChat == 1 && isInPM(p.getUsernameHash())) {
					p.getPA().loadPM(c.getUsernameHash(), ignored ? 2 : 1);
				}
			}
		}
	}

	private static boolean friendsContains(Player player, long username) {
		for (long l : player.friends) {
			if (username == l) {
				return true;
			}
		}
		return false;
	}

	public static boolean ignoresContains(Player player, long username) {
		for (long l : player.ignores) {
			if (username == l) {
				return true;
			}
		}
		return false;
	}

	public void updatePM(int pID, int world) { // used for private chat updates
		Player p = World.players[pID];
		if (p == null || p.playerName == null || p.playerName.equals("null")) {
			return;
		}
		long l = Misc.playerNameToInt64(p.playerName);

		if (p.privateChat == 0) {
			for (int i = 0; i < c.friends.length; i++) {
				if (c.friends[i] != 0) {
					if (l == c.friends[i]) {
						loadPM(l, world);
						return;
					}
				}
			}
		} else if (p.privateChat == 1) {
			for (int i = 0; i < c.friends.length; i++) {
				if (c.friends[i] != 0) {
					if (l == c.friends[i]) {
						if (p.getPA().isInPM(Misc.playerNameToInt64(c.playerName))) {
							loadPM(l, world);
							return;
						} else {
							loadPM(l, 0);
							return;
						}
					}
				}
			}
		} else if (p.privateChat == 2) {
			//
			for (int i = 0; i < c.friends.length; i++) {
				if (c.friends[i] != 0) {
					if (l == c.friends[i]) {
						loadPM(l, 0);
						return;
					}
				}
			}
		}
	}

	public boolean isInPM(long l) {
		for (int i = 0; i < c.friends.length; i++) {
			if (c.friends[i] != 0) {
				if (l == c.friends[i]) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Magic on items
	 **/

	public void magicOnItems(int slot, int itemId, int spellId) {
		if (!c.getItems().playerHasItem(itemId, 1, slot)) {
			return;
		}
		switch (spellId) {
		case 1162: // low alch
			break;

		case 1178: // high alch
			break;
		}
	}

	/**
	 * Dieing
	 **/

	private String deathMessage(int roll, String name) {
		switch (roll) {
		case 0:
			return "You have defeated " + name + "!";

		case 1:
			return "You have defeated " + name + " in battle.";

		case 2:
			return name + " was clearly no match for you.";

		case 3:
			return "Well done, you've pwned " + name + ".";

		case 4:
			return "You have wiped the floor with " + name + ".";

		case 5:
			return "You rock, " + name + " clearly does not.";

		case 6:
			return "You have proven your superiority over " + name + ".";

		case 7:
			return "You made " + name + " lose the game.";

		case 8:
			return "It's official, you are far more awesome than " + name + ".";

		case 9:
			return "Let all warriors learn from the fate of " + name + " and fear you.";

		case 10:
			return name + " was no match for your unchecked power.";

		case 11:
			return "Your god smiles at you, as " + name + " meets his demise.";
		}
		return "";
	}

	public void setLevel(int levelId, int targetLevel) {
		c.getLevel()[levelId] = targetLevel;
		c.getExperience()[levelId] = c.getPA().getXPForLevel(targetLevel + 1);
		c.getPA().refreshSkill(levelId);
		c.combatLevel = PlayerConstants.getCombatLevel(c);
		c.getPA().requestUpdates();
	}

	public void applyDead() {
		c.isDead = false;
		c.respawnTimer = 6;
		c.poisonDamage = -1;
		c.venomDamage = -1;
		c.playAnimation(Animation.create(836));
		// c.victimId = c.getCombat().getKillerId(c.playerId);
		c.killerId = findKiller();
		Player o = World.players[c.killerId];
		if (o != null) {
			if (c.killerId != c.playerId) {
				o.poisonDamage = -1;
				o.sendMessage(deathMessage(Misc.random(11), c.playerName));
				o.getPA().resetFollow();
				if (!c.inDuelArena() && !c.inFunPK()) {
					o.kills++;
					c.deaths++;
					o.getPA().handleLoginText();
					c.getPA().handleLoginText();
					QuestTab.updateInterface(o);
					QuestTab.updateInterface(c);
				} else {
					o.specAmount = 10;
					o.getItems().addSpecialBar(o.getEquipment()[3]);
				}
				this.c.getDuel().stakedItems.clear();
			}
		}

		c.faceUpdate(0);
		c.npcIndex = 0;
		c.playerIndex = 0;
		c.stopMovement();
		resetDamageDone();
		c.specAmount = 10;
		c.getItems().addSpecialBar(c.getEquipment()[PlayerConstants.PLAYER_WEAPON]);
		c.lastVeng = 0;
		c.vengOn = false;
		resetFollowers();
		c.attackTimer = 10;
		removeAllWindows();
		c.tradeResetNeeded = true;
	}

	public void resetDamageDone() {
		for (int i = 0; i < World.players.length; i++) {
			if (World.players[i] != null) {
				World.players[i].damageTaken[c.playerId] = 0;
			}
		}
	}

	public void vengMe() {
		if (System.currentTimeMillis() - c.lastVeng > 30000) {
			if (c.getItems().playerHasItem(557, 10) && c.getItems().playerHasItem(9075, 4) && c.getItems().playerHasItem(560, 2)) {
				c.vengOn = true;
				c.lastVeng = System.currentTimeMillis();
				c.playAnimation(Animation.create(4410));
				c.playGraphic(Graphic.create(726, 0, 100));
				c.getItems().deleteItem(557, c.getItems().getItemSlot(557), 10);
				c.getItems().deleteItem(560, c.getItems().getItemSlot(560), 2);
				c.getItems().deleteItem(9075, c.getItems().getItemSlot(9075), 4);
			} else {
				c.sendMessage("You do not have the required runes to cast this spell. (9075 for astrals)");
			}
		} else {
			c.sendMessage("You must wait 30 seconds before casting this again.");
		}
	}

	public void resetTb() {
		c.teleBlockLength = 0;
		c.teleBlockDelay = 0;
	}

	public void resetFollowers() {
		for (int j = 0; j < World.players.length; j++) {
			if (World.players[j] != null) {
				if (World.players[j].followId == c.playerId) {
					Player c = World.players[j];
					c.getPA().resetFollow();
				}
			}
		}
	}

	public List<Item> getAllItems() {
		List<Item> items = new ArrayList<>();
		for (int i = 0; i < c.playerItems.length; i++) {
			if (c.playerItems[i] > 0) {
				items.add(new Item(c.playerItems[i] - 1, c.playerItemsN[i]));
			}
		}
		for (int i = 0; i < c.playerEquipment.length; i++) {
			if (c.playerEquipment[i] > 0) {
				items.add(new Item(c.playerEquipment[i], c.playerEquipmentN[i]));
			}
		}
		return items;
	}

	public void resetKeptOnDeathInterface() {
		for (int i = 17109; i < 17131; i++) {
			c.getPA().sendFrame126("", i);
		}
		for (int k = 0; k < 4; k++) {
			c.getPA().sendFrame34a(10494, -1, k, 1);
		}
		for (int k = 0; k < 39; k++) {
			c.getPA().sendFrame34a(10600, -1, k, 1);
		}
		c.getPA().sendFrame126("Untradeables are", 17111);
		c.getPA().sendFrame126("@red@ALWAYS @lre@kept on death.", 17112);
		c.getPA().sendFrame126("Max items kept on death:", 17107);
		c.getPA().sendFrame126("~ " + getKeptOnDeathAmount() + " ~", 17108);
	}

	public void openKeptOnDeathInterface() {
		// Get items
		List<Item> items = getAllItems();
		List<Item> keptItems = getKeptOnDeathItems(items);
		List<Item> droppedItems = getDroppedOnDeathItems(items, keptItems);
		// Reset the interface
		resetKeptOnDeathInterface();
		// Send kept items
		for (int i = 0; i < getKeptOnDeathAmount(); i++) {
			if (keptItems.size() == i) {
				break;
			}
			c.getPA().sendFrame34a(10494, keptItems.get(i).getId(), i, keptItems.get(i).getAmount());
		}
		// Send dropped items
		for (int i = 0; i < 39; i++) {
			if (droppedItems.size() == i) {
				break;
			}
			c.getPA().sendFrame34a(10600, droppedItems.get(i).getId(), i, droppedItems.get(i).getAmount());
		}
		// Open interface
		c.getPA().showInterface(17100);
	}

	public int getKeptOnDeathAmount() {
		int itemAmount = c.isSkulled ? 0 : Config.ITEMS_KEPT_ON_DEATH;
		if (c.prayerActive[10]) {
			itemAmount++;
		}
		return itemAmount;
	}

	public List<Item> getDroppedOnDeathItems(List<Item> items, List<Item> keptItems) {
		items = new ArrayList<Item>(items);
		keptItems = new ArrayList<Item>(keptItems);
		KeptItemsLoop: for (Item keptItem : keptItems) {
			Iterator<Item> itemIterator = items.iterator();
			while (itemIterator.hasNext()) {
				Item item = itemIterator.next();
				if (!c.getItems().isTradeable(item.getId())) {
					itemIterator.remove();
					continue;
				}
				if (keptItem.getId() != item.getId()) {
					continue;
				}
				if (item.getAmount() > 1) {
					item.setAmount(item.getAmount() - 1);
				} else {
					itemIterator.remove();
				}
				continue KeptItemsLoop;
			}
		}
		return items;
	}

	public List<Item> getKeptOnDeathItems(List<Item> items) {
		items = new ArrayList<Item>(items);
		items.sort(new Comparator<Item>() {
			@Override
			public int compare(Item itemA, Item itemB) {
				int priceA = c.getShops().getItemShopValue(itemA.getId());
				int priceB = c.getShops().getItemShopValue(itemB.getId());
				if (priceA < priceB) {
					return 1;
				} else if (priceA > priceB) {
					return -1;
				}
				return 0;
			}
		});
		List<Item> keptItems = new ArrayList<>();
		List<Item> untradeables = new ArrayList<>();
		int itemAmount = getKeptOnDeathAmount();
		for (Item item : items) {
			if (c.getItems().isTradeable(item.getId())) {
				if (itemAmount == 0) {
					continue;
				}
				if (item.getAmount() > 1) {
					int amount = item.getAmount() > itemAmount ? itemAmount : item.getAmount();
					for (int i = 0; i < amount; i++) {
						keptItems.add(new Item(item.getId()));
					}
					itemAmount -= amount;
				} else {
					keptItems.add(new Item(item.getId()));
					itemAmount--;
				}
			} else {
				untradeables.add(item);
			}
		}
		keptItems.addAll(untradeables);
		return keptItems;
	}

	public void giveLife() {
		c.isDead = false;
		c.faceUpdate(-1);
		c.freezeTimer = 0;
		if (c.getRights().isInferior(PlayerRights.DEVELOPER) && !c.inDuelArena() && !c.inPits && !c.inFightCaves() && !c.inFunPK()) {
			List<Item> items = getAllItems();
			List<Item> keptItems = getKeptOnDeathItems(items);
			List<Item> droppedItems = getDroppedOnDeathItems(items, keptItems);
			Player killer = World.players[c.killerId];
			if (killer == null) {
				killer = c;
			} else {
				RewardHandler.giveWealth(killer, 0, c);
			}
			for (Item item : droppedItems) {
				Server.itemHandler.createGroundItem(killer, item.getId(), c.getX(), c.getY(), item.getAmount(), c.killerId);
			}
			c.getItems().deleteAllItems();
			for (Item item : keptItems) {
				c.getItems().addItem(item.getId(), item.getAmount());
			}

		}
		c.getCombat().resetPrayers();
		for (int i = 0; i < 20; i++) {
			c.getLevel()[i] = getLevelForXP(c.getExperience()[i]);
			c.getPA().refreshSkill(i);
		}
		if (c.inDuelArena() && c.duelStatus == 5) {
			c.getPA().movePlayer(Config.DUELING_RESPAWN_X, Config.DUELING_RESPAWN_Y, 0);
			Player otherClient = World.players[c.duelingWith];
			if (otherClient == null) {
				return;
			}
			otherClient.getDuel().duelVictory();
			if (c.duelStatus != 6) {
				c.getDuel().resetDuel();
			}
		} else if (c.inFunPK()) {
			movePlayer(3328, 4751, 0);
		} else {
			movePlayer(Config.RESPAWN_X, Config.RESPAWN_Y, 0);
		}
		c.isSkulled = false;
		c.skullTimer = 0;
		c.attackedPlayers.clear();
		// PlayerSaving.getSingleton().requestSave(c.playerId);
		PlayerSave.saveGame(c);
		c.getCombat().resetPlayerAttack();
		frame1();
		resetTb();
		c.isSkulled = false;
		c.attackedPlayers.clear();
		c.headIconPk = -1;
		c.skullTimer = -1;
		c.followId = -1;
		c.damageTaken = new int[Config.MAX_PLAYERS];
		c.getPA().resetAnimation();
		removeAllWindows();
		c.getPA().requestUpdates();
	}

	/**
	 * Location change for digging, levers etc
	 **/

	public void changeLocation() {
		switch (c.newLocation) {
		case 1:
			sendFrame99(2);
			movePlayer(3578, 9706, -1);
			break;
		case 2:
			sendFrame99(2);
			movePlayer(3568, 9683, -1);
			break;
		case 3:
			sendFrame99(2);
			movePlayer(3557, 9703, -1);
			break;
		case 4:
			sendFrame99(2);
			movePlayer(3556, 9718, -1);
			break;
		case 5:
			sendFrame99(2);
			movePlayer(3534, 9704, -1);
			break;
		case 6:
			sendFrame99(2);
			movePlayer(3546, 9684, -1);
			break;
		}
		c.newLocation = 0;
	}

	public void switchSpellbook(int spellbookId) {
		c.playerMagicBook = spellbookId;
		switch (spellbookId) {
		case 0:
			c.getClient().setSidebarInterface(6, 1151);
			c.sendMessage("You feel a drain on your memory.");
			break;
		case 1:
			c.getClient().setSidebarInterface(6, 12855);
			c.sendMessage("An ancient wisdomin fills your mind.");
			break;
		case 2:
			c.getClient().setSidebarInterface(6, 29999);
			c.sendMessage("Lunar wisdom fills your mind.");
			break;
		}
		c.getPA().resetAutocast();
	}

	public void processTeleport() {
		c.teleportToX = c.teleX;
		c.teleportToY = c.teleY;
		c.teleportToHeight = c.teleHeight;
		if (c.teleEndAnimation > 0) {
			c.playAnimation(Animation.create(c.teleEndAnimation));
		}
	}

	public void movePlayer(int x, int y, int h) {
		if (c.heightLevel != h) {
			c.checkHeight = true;
		}
		c.resetWalkingQueue();
		c.teleportToX = x;
		c.teleportToY = y;
		c.heightLevel = h;
		c.teleportToHeight = h;
		if (c.teleportToHeight != c.heightLevel) {
			c.RebuildNPCList = true;
		}
		requestUpdates();
	}

	/**
	 * Following
	 **/

	public boolean normalFollow(int i) {
		if (c.followId == 0 || World.players[c.followId] == null || World.players[c.followId].isDead || (c.duelRule[1] && c.duelStatus == 5)) {
			return false;
		}

		Player o = World.players[c.followId];
		int otherX = World.players[c.followId].getX();
		int otherY = World.players[c.followId].getY();

		if (c.isDead || c.getLevel()[3] <= 0 || !c.goodDistance(otherX, otherY, c.getX(), c.getY(), 17) || c.freezeTimer > 0) {
			return false;
		}

		c.followId2 = 0;
		c.faceUpdate(c.followId + 32768);
		if (c.followingX == otherX && c.followingY == otherY) {
			return true;
		}
		c.followingX = otherX;
		c.followingY = otherY;
		boolean checkClip = Region.checkClip(o, o.oldX, o.oldY);
		if (!checkClip) {
			if (Region.checkClip(o, o.absX - 1, o.absY)) {
				o.oldX = o.absX - 1;
				o.oldY = o.absY;
			} else if (Region.checkClip(o, o.absX + 1, o.absY)) {
				o.oldX = o.absX + 1;
				o.oldY = o.absY;
			} else if (Region.checkClip(o, o.absX, o.absY - 1)) {
				o.oldX = o.absX;
				o.oldY = o.absY - 1;
			}
		}
		c.sendMessage("Here?");
		c.resetWalkingQueue();
		setOpponentCoordinates(otherX, otherY);
		playerWalk(o.oldX, o.oldY, true);
		// c.getNextPlayerMovement();
		return true;
	}

	public void handleEntityFollowing() {
		if (c.followId > 0) {
			if (c.playerIndex == 0 && !c.mageFollow) {
			} else {
				combatFollowPlayer(c.playerIndex);
			}
		} else if (c.followId2 > 0) {
			followNpc();
		}
	}

	public void combatFollowPlayer(int i) {
		if (c.followId == 0 || World.players[c.followId] == null || World.players[c.followId].isDead || (c.duelRule[1] && c.duelStatus == 5)) {
			c.followId = 0;
			return;
		}
		Player otherPlayer = World.players[c.followId];
		int otherX = otherPlayer.getX();
		int otherY = otherPlayer.getY();

		if (c.inTrade || c.isDead || c.getLevel()[3] <= 0 || inPitsWait() || !Misc.goodDistance(otherX, otherY, c.getX(), c.getY(), 25)) {
			c.followId = 0;
			return;
		}

		if (c.freezeTimer > 0) {
			return;
		}

		if (checkStopMovement(otherPlayer)) {
			return;
		}

		c.followId2 = 0;
		c.faceUpdate(c.followId + 32768);
		// System.out.println("x: "+c.getX()+" - y: "+c.getY()
		// +" - clipped: "+checkClip);
		/**
		 * Walking to the assigned player.
		 */

		if (c.followingX == otherX && c.followingY == otherY) {
			return;
		}
		c.followingX = otherX;
		c.followingY = otherY;
		walkTowards(otherX, otherY);
	}

	public boolean checkStopMovement(Player otherPlayer) {
		if (otherPlayer == null) {
			return false;
		}

		int otherX = otherPlayer.getX();
		int otherY = otherPlayer.getY();

		boolean withinDistance = Misc.goodDistance(otherX, otherY, c.getX(), c.getY(), 2);
		boolean goodDistance = Misc.goodDistance(otherX, otherY, c.getX(), c.getY(), 1);
		boolean hallyDistance = Misc.goodDistance(otherX, otherY, c.getX(), c.getY(), 2);
		boolean bowDistance = Misc.goodDistance(otherX, otherY, c.getX(), c.getY(), 8);
		boolean rangeWeaponDistance = Misc.goodDistance(otherX, otherY, c.getX(), c.getY(), 4);
		boolean sameSpot = c.absX == otherX && c.absY == otherY;
		boolean checkClip = Region.checkClip(c, otherX, otherY);

		if ((c.usingBow || c.autocasting || c.usingMagic || c.mageFollow || (c.playerIndex > 0 && c.autocastId > 0)) && bowDistance && !sameSpot) {
			if (checkClip) {
				if (c.usingMagic) {
					c.followId = 0;
				}
				c.stopMovement();
				return true;
			}
		}

		if (c.getCombat().usingHally() && hallyDistance && !sameSpot) {
			return true;
		}

		if (c.usingRangeWeapon && rangeWeaponDistance && !sameSpot) {
			if (checkClip) {
				c.stopMovement();
				return true;
			}
		}

		if (Misc.goodDistance(otherX, otherY, c.getX(), c.getY(), 1)) {
			if (checkClip) {
				if (otherX != c.getX() && otherY != c.getY()) {
					stopDiagonal(otherX, otherY);
				}
				return true;
			}
		}

		return false;
	}

	public boolean walkTowards(int otherX, int otherY) {
		setOpponentCoordinates(otherX, otherY);
		boolean foundPath = false;
		if (otherY == c.getY() && otherX == c.getX()) {
			foundPath = combatWalk(otherX, otherY - 1);
		} else {
			if (otherY > c.getY() && otherX == c.getX()) {
				foundPath = combatWalk(otherX, otherY - 1);
			} else if (otherY < c.getY() && otherX == c.getX()) {
				foundPath = combatWalk(otherX, otherY + 1);
			} else if (otherX > c.getX() && otherY == c.getY()) {
				foundPath = combatWalk(otherX - 1, otherY);
			} else if (otherX < c.getX() && otherY == c.getY()) {
				foundPath = combatWalk(otherX + 1, otherY);
			} else if (otherX < c.getX() && otherY < c.getY()) {
				foundPath = combatWalk(otherX + 1, otherY);
			} else if (otherX > c.getX() && otherY > c.getY()) {
				foundPath = combatWalk(otherX - 1, otherY);
			} else if (otherX < c.getX() && otherY > c.getY()) {
				foundPath = combatWalk(otherX + 1, otherY);
			} else if (otherX > c.getX() && otherY < c.getY()) {
				foundPath = combatWalk(otherX - 1, otherY);
			}
		}
		return foundPath;
	}

	public void setOpponentCoordinates(int absX, int absY) {
		opponentX = absX;
		opponentY = absY;
	}

	private int opponentX;
	private int opponentY;

	public boolean combatWalk(int x, int y) {
		if (c.duelRule[1] && c.duelStatus == 5) {
			return false;
		}
		boolean east = opponentX - c.getX() > 0;
		boolean west = c.getX() - opponentX > 0;
		boolean north = opponentY - c.getY() > 0;
		boolean south = c.getY() - opponentY > 0;
		boolean foundPath = false;
		// System.out.println("south: "+south+" - east: "+east+" - north:
		// "+north
		// + " - west: "+west);
		if (south) {
			if (east) {
				if (Region.checkNormalClip(x, y, opponentX, opponentY, c.heightLevel) && Region.getClipping(x, y, c.heightLevel, false) != 2228480) {
					foundPath = playerWalk(x, y, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY + 1, c.heightLevel)) {
					foundPath = playerWalk(opponentX, opponentY + 1, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX - 1, opponentY, c.heightLevel)) {
					foundPath = playerWalk(opponentX + 1, opponentY, true);
				}
				return foundPath;
			}
			if (west) {
				if (Region.checkNormalClip(x, y, opponentX, opponentY, c.heightLevel) && Region.getClipping(x, y, c.heightLevel, false) != 2228480) {
					foundPath = playerWalk(x, y, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY + 1, c.heightLevel)) {
					foundPath = playerWalk(opponentX, opponentY + 1, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX - 1, opponentY, c.heightLevel)) {
					foundPath = playerWalk(opponentX - 1, opponentY, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY - 1, c.heightLevel)) {
					foundPath = playerWalk(opponentX, opponentY - 1, true);
				}
				return foundPath;
			}
			if (Region.checkNormalClip(x, y, opponentX, opponentY, c.heightLevel) && Region.getClipping(x, y, c.heightLevel, false) != 2228480) {
				foundPath = playerWalk(x, y, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX - 1, opponentY, c.heightLevel)) {
				foundPath = playerWalk(opponentX - 1, opponentY, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX + 1, opponentY, c.heightLevel)) {
				foundPath = playerWalk(opponentX + 1, opponentY, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY - 1, c.heightLevel)) {
				foundPath = playerWalk(opponentX, opponentY - 1, true);
			}
			return foundPath;
		}
		if (north) {
			if (east) {
				if (Region.checkNormalClip(x, y, opponentX, opponentY, c.heightLevel) && Region.getClipping(x, y, c.heightLevel, false) != 2228480) {
					foundPath = playerWalk(x, y, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY - 1, c.heightLevel)) {
					foundPath = playerWalk(opponentX, opponentY - 1, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX + 1, opponentY, c.heightLevel)) {
					foundPath = playerWalk(opponentX + 1, opponentY, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY + 1, c.heightLevel)) {
					foundPath = playerWalk(opponentX, opponentY + 1, true);
				}
				return foundPath;
			}
			if (west) {
				if (Region.checkNormalClip(opponentX, opponentY, x, y, c.heightLevel) && Region.getClipping(x, y, c.heightLevel, false) != 2228480) {
					foundPath = playerWalk(x, y, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY - 1, c.heightLevel)) {
					foundPath = playerWalk(opponentX, opponentY - 1, true);
				} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY + 1, c.heightLevel)) {
					foundPath = playerWalk(opponentX, opponentY + 1, true);
				}
				return foundPath;
			}
			if (Region.checkNormalClip(opponentX, opponentY, x, y, c.heightLevel)) {
				foundPath = playerWalk(x, y, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY - 1, c.heightLevel)) {
				foundPath = playerWalk(opponentX, opponentY - 1, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX - 1, opponentY, c.heightLevel)) {
				foundPath = playerWalk(opponentX - 1, opponentY, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX + 1, opponentY, c.heightLevel)) {
				foundPath = playerWalk(opponentX + 1, opponentY, true);
			}
			return foundPath;
		}
		if (east) {
			if (Region.checkNormalClip(opponentX, opponentY, x, y, c.heightLevel) && Region.getClipping(x, y, c.heightLevel, false) != 2228480) {
				foundPath = playerWalk(x, y, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY - 1, c.heightLevel)) {
				foundPath = playerWalk(opponentX, opponentY - 1, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY + 1, c.heightLevel)) {
				foundPath = playerWalk(opponentX, opponentY + 1, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX + 1, opponentY, c.heightLevel)) {
				foundPath = playerWalk(opponentX + 1, opponentY, true);
			}
			return foundPath;
		}
		if (west) {
			if (Region.checkNormalClip(opponentX, opponentY, x, y, c.heightLevel) && Region.getClipping(x, y, c.heightLevel, false) != 2228480) {
				foundPath = playerWalk(x, y, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY - 1, c.heightLevel)) {
				foundPath = playerWalk(opponentX, opponentY - 1, true);
			} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY + 1, c.heightLevel)) {
				foundPath = playerWalk(opponentX, opponentY - 1, true);
			}
			return foundPath;
		}
		if (Region.checkNormalClip(opponentX, opponentY, x, y, c.heightLevel)) {
			foundPath = playerWalk(x, y, true);
		} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY - 1, c.heightLevel)) {
			foundPath = playerWalk(opponentX, opponentY - 1, true);
		} else if (Region.checkNormalClip(opponentX, opponentY, opponentX, opponentY + 1, c.heightLevel)) {
			foundPath = playerWalk(opponentX, opponentY - 1, true);
		}
		return foundPath;
	}

	public boolean playerWalk(int x, int y, boolean combatWalk) {
		// System.out.println("playerwalk - X: "+x+" - Y: "+y);
		if ((c.duelRule[1] && c.duelStatus == 5) || c.freezeTimer > 0) {
			return false;
		}
		c.pathX = x;
		c.pathY = y;
		try {
			return PathFinder.findRoute(c, x, y, true, 1, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public void followNpc() {
		if (NPCHandler.npcs[c.followId2] == null || NPCHandler.npcs[c.followId2].isDead) {
			c.followId2 = 0;
			return;
		}
		if (c.freezeTimer > 0) {
			return;
		}
		if (c.isDead || c.getLevel()[3] <= 0)
			return;

		int otherX = NPCHandler.npcs[c.followId2].getX();
		int otherY = NPCHandler.npcs[c.followId2].getY();
		boolean withinDistance = c.goodDistance(otherX, otherY, c.getX(), c.getY(), 2);
		c.goodDistance(otherX, otherY, c.getX(), c.getY(), 1);
		boolean hallyDistance = c.goodDistance(otherX, otherY, c.getX(), c.getY(), 2);
		boolean bowDistance = c.goodDistance(otherX, otherY, c.getX(), c.getY(), 8);
		boolean rangeWeaponDistance = c.goodDistance(otherX, otherY, c.getX(), c.getY(), 4);
		boolean sameSpot = c.absX == otherX && c.absY == otherY;
		if (!c.goodDistance(otherX, otherY, c.getX(), c.getY(), 25)) {
			c.followId2 = 0;
			return;
		}
		if (c.goodDistance(otherX, otherY, c.getX(), c.getY(), 1)) {
			if (otherX != c.getX() && otherY != c.getY()) {
				stopDiagonal(otherX, otherY);
				return;
			}
		}

		if ((c.usingBow || c.mageFollow || (c.npcIndex > 0 && c.autocastId > 0)) && bowDistance && !sameSpot) {
			return;
		}

		if (c.getCombat().usingHally() && hallyDistance && !sameSpot) {
			return;
		}

		if (c.usingRangeWeapon && rangeWeaponDistance && !sameSpot) {
			return;
		}
		
		c.faceUpdate(c.followId2);
		if (otherX == c.absX && otherY == c.absY) {
			clippedWalkTo();
		} else if (c.isRunning2 && !withinDistance) {
			if (otherY > c.getY() && otherX == c.getX()) {
				walkTo(0, getMove(c.getY(), otherY - 1) + getMove(c.getY(), otherY - 1));
			} else if (otherY < c.getY() && otherX == c.getX()) {
				walkTo(0, getMove(c.getY(), otherY + 1) + getMove(c.getY(), otherY + 1));
			} else if (otherX > c.getX() && otherY == c.getY()) {
				walkTo(getMove(c.getX(), otherX - 1) + getMove(c.getX(), otherX - 1), 0);
			} else if (otherX < c.getX() && otherY == c.getY()) {
				walkTo(getMove(c.getX(), otherX + 1) + getMove(c.getX(), otherX + 1), 0);
			} else if (otherX < c.getX() && otherY < c.getY()) {
				walkTo(getMove(c.getX(), otherX + 1) + getMove(c.getX(), otherX + 1), getMove(c.getY(), otherY + 1) + getMove(c.getY(), otherY + 1));
			} else if (otherX > c.getX() && otherY > c.getY()) {
				walkTo(getMove(c.getX(), otherX - 1) + getMove(c.getX(), otherX - 1), getMove(c.getY(), otherY - 1) + getMove(c.getY(), otherY - 1));
			} else if (otherX < c.getX() && otherY > c.getY()) {
				walkTo(getMove(c.getX(), otherX + 1) + getMove(c.getX(), otherX + 1), getMove(c.getY(), otherY - 1) + getMove(c.getY(), otherY - 1));
			} else if (otherX > c.getX() && otherY < c.getY()) {
				walkTo(getMove(c.getX(), otherX + 1) + getMove(c.getX(), otherX + 1), getMove(c.getY(), otherY - 1) + getMove(c.getY(), otherY - 1));
			}
		} else {
			if (otherY > c.getY() && otherX == c.getX()) {
				walkTo(0, getMove(c.getY(), otherY - 1));
			} else if (otherY < c.getY() && otherX == c.getX()) {
				walkTo(0, getMove(c.getY(), otherY + 1));
			} else if (otherX > c.getX() && otherY == c.getY()) {
				walkTo(getMove(c.getX(), otherX - 1), 0);
			} else if (otherX < c.getX() && otherY == c.getY()) {
				walkTo(getMove(c.getX(), otherX + 1), 0);
			} else if (otherX < c.getX() && otherY < c.getY()) {
				walkTo(getMove(c.getX(), otherX + 1), getMove(c.getY(), otherY + 1));
			} else if (otherX > c.getX() && otherY > c.getY()) {
				walkTo(getMove(c.getX(), otherX - 1), getMove(c.getY(), otherY - 1));
			} else if (otherX < c.getX() && otherY > c.getY()) {
				walkTo(getMove(c.getX(), otherX + 1), getMove(c.getY(), otherY - 1));
			} else if (otherX > c.getX() && otherY < c.getY()) {
				walkTo(getMove(c.getX(), otherX - 1), getMove(c.getY(), otherY + 1));
			}
		}
		c.faceUpdate(c.followId2);
	}

	public void clippedWalkTo() {
		if (Region.getClipping(c.getX() - 1, c.getY(), c.heightLevel, -1, 0)) {
			c.getPA().walkTo(-1, 0);
		} else if (Region.getClipping(c.getX() + 1, c.getY(), c.heightLevel, 1, 0)) {
			c.getPA().walkTo(1, 0);
		} else if (Region.getClipping(c.getX(), c.getY() - 1, c.heightLevel, 0, -1)) {
			c.getPA().walkTo(0, -1);
		} else if (Region.getClipping(c.getX(), c.getY() + 1, c.heightLevel, 0, 1)) {
			c.getPA().walkTo(0, 1);
		}
		c.postProcessing();
	}

	public void handleRealFollowing() {
		if (c.followId > 0 && c.playerIndex == 0 && !c.mageFollow) {
			if (!normalFollow(c.followId)) {
				c.followId = 0;
				c.stopMovement();
			}
		}
	}

	public int getRunningMove(int i, int j) {
		if (j - i > 2)
			return 2;
		else if (j - i < -2)
			return -2;
		else
			return j - i;
	}

	public void resetFollow() {
		if (c.followId != 0 || c.followId2 != 0) {
			c.followId = 0;
			c.followId2 = 0;
			setOpponentCoordinates(0, 0);
		}
	}

	public void walkTo(int x, int y) {
//		c.newWalkCmdSteps = 0;
//		if (++c.newWalkCmdSteps > 50)
//			c.newWalkCmdSteps = 0;
//		int k = c.getX() + i;
//		k -= c.mapRegionX * 8;
//		c.getNewWalkCmdX()[0] = c.getNewWalkCmdY()[0] = 0;
//		int l = c.getY() + j;
//		l -= c.mapRegionY * 8;
//
//		for (int n = 0; n < c.newWalkCmdSteps; n++) {
//			c.getNewWalkCmdX()[n] += k;
//			c.getNewWalkCmdY()[n] += l;
//		}
		Region.findRoute(c, x, y, false, 1, 1);
	}

	public void walkTo2(int x, int y) {
		if (c.freezeDelay > 0)
			return;
//		c.newWalkCmdSteps = 0;
//		if (++c.newWalkCmdSteps > 50)
//			c.newWalkCmdSteps = 0;
//		int k = c.getX() + i;
//		k -= c.mapRegionX * 8;
//		c.getNewWalkCmdX()[0] = c.getNewWalkCmdY()[0] = 0;
//		int l = c.getY() + j;
//		l -= c.mapRegionY * 8;
//
//		for (int n = 0; n < c.newWalkCmdSteps; n++) {
//			c.getNewWalkCmdX()[n] += k;
//			c.getNewWalkCmdY()[n] += l;
//		}
		Region.findRoute(c, x, y, false, 1, 1);
	}

	public void stopDiagonal(int otherX, int otherY) {
		if (c.freezeDelay > 0)
			return;
		c.newWalkCmdSteps = 1;
		int xMove = otherX - c.getX();
		int yMove = 0;
		if (xMove == 0)
			yMove = otherY - c.getY();
		/*
		 * if (!clipHor) { yMove = 0; } else if (!clipVer) { xMove = 0; }
		 */

//		int k = c.getX() + xMove;
//		k -= c.mapRegionX * 8;
//		c.getNewWalkCmdX()[0] = c.getNewWalkCmdY()[0] = 0;
//		int l = c.getY() + yMove;
//		l -= c.mapRegionY * 8;
//
//		for (int n = 0; n < c.newWalkCmdSteps; n++) {
//			c.getNewWalkCmdX()[n] += k;
//			c.getNewWalkCmdY()[n] += l;
//		}
		Region.findRoute(c, c.getX() + xMove, c.getY() + yMove, false, 1, 1);
	}

	public void walkToCheck(int x, int y) {
		if (c.freezeDelay > 0)
			return;
//		c.newWalkCmdSteps = 0;
//		if (++c.newWalkCmdSteps > 50)
//			c.newWalkCmdSteps = 0;
//		int k = c.getX() + i;
//		k -= c.mapRegionX * 8;
//		c.getNewWalkCmdX()[0] = c.getNewWalkCmdY()[0] = 0;
//		int l = c.getY() + j;
//		l -= c.mapRegionY * 8;
//
//		for (int n = 0; n < c.newWalkCmdSteps; n++) {
//			c.getNewWalkCmdX()[n] += k;
//			c.getNewWalkCmdY()[n] += l;
//		}
		Region.findRoute(c, x, y, false, 1, 1);
	}

	public int getMove(int place1, int place2) {
		if (System.currentTimeMillis() - c.lastSpear < 4000)
			return 0;
		if ((place1 - place2) == 0) {
			return 0;
		} else if ((place1 - place2) < 0) {
			return 1;
		} else if ((place1 - place2) > 0) {
			return -1;
		}
		return 0;
	}

	public boolean fullVeracs() {
		return c.getEquipment()[PlayerConstants.PLAYER_HAT] == 4753 && c.getEquipment()[PlayerConstants.PLAYER_BODY] == 4757 && c.getEquipment()[PlayerConstants.PLAYER_LEGS] == 4759
				&& c.getEquipment()[PlayerConstants.PLAYER_WEAPON] == 4755;
	}

	public boolean fullGuthans() {
		return c.getEquipment()[PlayerConstants.PLAYER_HAT] == 4724 && c.getEquipment()[PlayerConstants.PLAYER_BODY] == 4728 && c.getEquipment()[PlayerConstants.PLAYER_LEGS] == 4730
				&& c.getEquipment()[PlayerConstants.PLAYER_WEAPON] == 4726;
	}

	/**
	 * reseting animation
	 **/
	public void resetAnimation() {
		c.getCombat().getPlayerAnimIndex();
		c.playAnimation(Animation.create(65535));
		requestUpdates();
	}

	public void requestUpdates() {
		c.updateRequired = true;
		c.setAppearanceUpdateRequired(true);
	}

	public void handleAlt(int id) {
		if (!c.getItems().playerHasItem(id)) {
			c.getItems().addItem(id, 1);
		}
	}

	public void levelUp(int skill) {
		int totalLevel = (getLevelForXP(c.getExperience()[0]) + getLevelForXP(c.getExperience()[1]) + getLevelForXP(c.getExperience()[2]) + getLevelForXP(c.getExperience()[3])
				+ getLevelForXP(c.getExperience()[4]) + getLevelForXP(c.getExperience()[5]) + getLevelForXP(c.getExperience()[6]) + getLevelForXP(c.getExperience()[7])
				+ getLevelForXP(c.getExperience()[8]) + getLevelForXP(c.getExperience()[9]) + getLevelForXP(c.getExperience()[10]) + getLevelForXP(c.getExperience()[11])
				+ getLevelForXP(c.getExperience()[12]) + getLevelForXP(c.getExperience()[13]) + getLevelForXP(c.getExperience()[14]) + getLevelForXP(c.getExperience()[15])
				+ getLevelForXP(c.getExperience()[16]) + getLevelForXP(c.getExperience()[17]) + getLevelForXP(c.getExperience()[18]) + getLevelForXP(c.getExperience()[19])
				+ getLevelForXP(c.getExperience()[20]));
		sendFrame126("Total Lvl: " + totalLevel, 3984);
		switch (skill) {
		case 0:
			sendFrame126("Congratulations, you just advanced an attack level!", 6248);
			sendFrame126("Your attack level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6249);
			c.sendMessage("Congratulations, you just advanced an attack level.");
			sendFrame164(6247);
			break;

		case 1:
			sendFrame126("Congratulations, you just advanced a defence level!", 6254);
			sendFrame126("Your defence level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6255);
			c.sendMessage("Congratulations, you just advanced a defence level.");
			sendFrame164(6253);
			break;

		case 2:
			sendFrame126("Congratulations, you just advanced a strength level!", 6207);
			sendFrame126("Your strength level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6208);
			c.sendMessage("Congratulations, you just advanced a strength level.");
			sendFrame164(6206);
			break;

		case 3:
			sendFrame126("Congratulations, you just advanced a hitpoints level!", 6217);
			sendFrame126("Your hitpoints level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6218);
			c.sendMessage("Congratulations, you just advanced a hitpoints level.");
			sendFrame164(6216);
			break;

		case 4:
			sendFrame126("Congratulations, you just advanced a ranged level!", 5453);
			sendFrame126("Your ranged level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6114);
			c.sendMessage("Congratulations, you just advanced a ranging level.");
			sendFrame164(4443);
			break;

		case 5:
			sendFrame126("Congratulations, you just advanced a prayer level!", 6243);
			sendFrame126("Your prayer level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6244);
			c.sendMessage("Congratulations, you just advanced a prayer level.");
			sendFrame164(6242);
			break;

		case 6:
			sendFrame126("Congratulations, you just advanced a magic level!", 6212);
			sendFrame126("Your magic level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6213);
			c.sendMessage("Congratulations, you just advanced a magic level.");
			sendFrame164(6211);
			break;

		case 7:
			sendFrame126("Congratulations, you just advanced a cooking level!", 6227);
			sendFrame126("Your cooking level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6228);
			c.sendMessage("Congratulations, you just advanced a cooking level.");
			sendFrame164(6226);
			break;

		case 8:
			sendFrame126("Congratulations, you just advanced a woodcutting level!", 4273);
			sendFrame126("Your woodcutting level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 4274);
			c.sendMessage("Congratulations, you just advanced a woodcutting level.");
			sendFrame164(4272);
			break;

		case 9:
			sendFrame126("Congratulations, you just advanced a fletching level!", 6232);
			sendFrame126("Your fletching level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6233);
			c.sendMessage("Congratulations, you just advanced a fletching level.");
			sendFrame164(6231);
			break;

		case 10:
			sendFrame126("Congratulations, you just advanced a fishing level!", 6259);
			sendFrame126("Your fishing level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6260);
			c.sendMessage("Congratulations, you just advanced a fishing level.");
			sendFrame164(6258);
			break;

		case 11:
			sendFrame126("Congratulations, you just advanced a fire making level!", 4283);
			sendFrame126("Your firemaking level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 4284);
			c.sendMessage("Congratulations, you just advanced a fire making level.");
			sendFrame164(4282);
			break;

		case 12:
			sendFrame126("Congratulations, you just advanced a crafting level!", 6264);
			sendFrame126("Your crafting level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6265);
			c.sendMessage("Congratulations, you just advanced a crafting level.");
			sendFrame164(6263);
			break;

		case 13:
			sendFrame126("Congratulations, you just advanced a smithing level!", 6222);
			sendFrame126("Your smithing level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6223);
			c.sendMessage("Congratulations, you just advanced a smithing level.");
			sendFrame164(6221);
			break;

		case 14:
			sendFrame126("Congratulations, you just advanced a mining level!", 4417);
			sendFrame126("Your mining level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 4438);
			c.sendMessage("Congratulations, you just advanced a mining level.");
			sendFrame164(4416);
			break;

		case 15:
			sendFrame126("Congratulations, you just advanced a herblore level!", 6238);
			sendFrame126("Your herblore level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 6239);
			c.sendMessage("Congratulations, you just advanced a herblore level.");
			sendFrame164(6237);
			break;

		case 16:
			sendFrame126("Congratulations, you just advanced a agility level!", 4278);
			sendFrame126("Your agility level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 4279);
			c.sendMessage("Congratulations, you just advanced an agility level.");
			sendFrame164(4277);
			break;

		case 17:
			sendFrame126("Congratulations, you just advanced a thieving level!", 4263);
			sendFrame126("Your theiving level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 4264);
			c.sendMessage("Congratulations, you just advanced a thieving level.");
			sendFrame164(4261);
			break;

		case 18:
			sendFrame126("Congratulations, you just advanced a slayer level!", 12123);
			sendFrame126("Your slayer level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 12124);
			c.sendMessage("Congratulations, you just advanced a slayer level.");
			sendFrame164(12122);
			break;

		case 20:
			sendFrame126("Congratulations, you just advanced a runecrafting level!", 4268);
			sendFrame126("Your runecrafting level is now " + getLevelForXP(c.getExperience()[skill]) + ".", 4269);
			c.sendMessage("Congratulations, you just advanced a runecrafting level.");
			sendFrame164(4267);
			break;
		}
	}

	public void refreshSkill(int i) {
		switch (i) {
		case 0:
			sendFrame126("" + c.getLevel()[0] + "", 4004);
			sendFrame126("" + getLevelForXP(c.getExperience()[0]) + "", 4005);
			sendFrame126("" + c.getExperience()[0] + "", 4044);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[0]) + 1) + "", 4045);
			break;

		case 1:
			sendFrame126("" + c.getLevel()[1] + "", 4008);
			sendFrame126("" + getLevelForXP(c.getExperience()[1]) + "", 4009);
			sendFrame126("" + c.getExperience()[1] + "", 4056);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[1]) + 1) + "", 4057);
			break;

		case 2:
			sendFrame126("" + c.getLevel()[2] + "", 4006);
			sendFrame126("" + getLevelForXP(c.getExperience()[2]) + "", 4007);
			sendFrame126("" + c.getExperience()[2] + "", 4050);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[2]) + 1) + "", 4051);
			break;

		case 3:
			sendFrame126("" + c.getLevel()[3] + "", 4016);
			sendFrame126("" + getLevelForXP(c.getExperience()[3]) + "", 4017);
			sendFrame126("" + c.getExperience()[3] + "", 4080);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[3]) + 1) + "", 4081);
			break;

		case 4:
			sendFrame126("" + c.getLevel()[4] + "", 4010);
			sendFrame126("" + getLevelForXP(c.getExperience()[4]) + "", 4011);
			sendFrame126("" + c.getExperience()[4] + "", 4062);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[4]) + 1) + "", 4063);
			break;

		case 5:
			sendFrame126("" + c.getLevel()[5] + "", 4012);
			sendFrame126("" + getLevelForXP(c.getExperience()[5]) + "", 4013);
			sendFrame126("" + c.getExperience()[5] + "", 4068);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[5]) + 1) + "", 4069);
			sendFrame126("" + c.getLevel()[5] + "/" + getLevelForXP(c.getExperience()[5]) + "", 687);// Prayer
			// frame
			break;

		case 6:
			sendFrame126("" + c.getLevel()[6] + "", 4014);
			sendFrame126("" + getLevelForXP(c.getExperience()[6]) + "", 4015);
			sendFrame126("" + c.getExperience()[6] + "", 4074);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[6]) + 1) + "", 4075);
			break;

		case 7:
			sendFrame126("" + c.getLevel()[7] + "", 4034);
			sendFrame126("" + getLevelForXP(c.getExperience()[7]) + "", 4035);
			sendFrame126("" + c.getExperience()[7] + "", 4134);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[7]) + 1) + "", 4135);
			break;

		case 8:
			sendFrame126("" + c.getLevel()[8] + "", 4038);
			sendFrame126("" + getLevelForXP(c.getExperience()[8]) + "", 4039);
			sendFrame126("" + c.getExperience()[8] + "", 4146);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[8]) + 1) + "", 4147);
			break;

		case 9:
			sendFrame126("" + c.getLevel()[9] + "", 4026);
			sendFrame126("" + getLevelForXP(c.getExperience()[9]) + "", 4027);
			sendFrame126("" + c.getExperience()[9] + "", 4110);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[9]) + 1) + "", 4111);
			break;

		case 10:
			sendFrame126("" + c.getLevel()[10] + "", 4032);
			sendFrame126("" + getLevelForXP(c.getExperience()[10]) + "", 4033);
			sendFrame126("" + c.getExperience()[10] + "", 4128);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[10]) + 1) + "", 4129);
			break;

		case 11:
			sendFrame126("" + c.getLevel()[11] + "", 4036);
			sendFrame126("" + getLevelForXP(c.getExperience()[11]) + "", 4037);
			sendFrame126("" + c.getExperience()[11] + "", 4140);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[11]) + 1) + "", 4141);
			break;

		case 12:
			sendFrame126("" + c.getLevel()[12] + "", 4024);
			sendFrame126("" + getLevelForXP(c.getExperience()[12]) + "", 4025);
			sendFrame126("" + c.getExperience()[12] + "", 4104);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[12]) + 1) + "", 4105);
			break;

		case 13:
			sendFrame126("" + c.getLevel()[13] + "", 4030);
			sendFrame126("" + getLevelForXP(c.getExperience()[13]) + "", 4031);
			sendFrame126("" + c.getExperience()[13] + "", 4122);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[13]) + 1) + "", 4123);
			break;

		case 14:
			sendFrame126("" + c.getLevel()[14] + "", 4028);
			sendFrame126("" + getLevelForXP(c.getExperience()[14]) + "", 4029);
			sendFrame126("" + c.getExperience()[14] + "", 4116);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[14]) + 1) + "", 4117);
			break;

		case 15:
			sendFrame126("" + c.getLevel()[15] + "", 4020);
			sendFrame126("" + getLevelForXP(c.getExperience()[15]) + "", 4021);
			sendFrame126("" + c.getExperience()[15] + "", 4092);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[15]) + 1) + "", 4093);
			break;

		case 16:
			sendFrame126("" + c.getLevel()[16] + "", 4018);
			sendFrame126("" + getLevelForXP(c.getExperience()[16]) + "", 4019);
			sendFrame126("" + c.getExperience()[16] + "", 4086);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[16]) + 1) + "", 4087);
			break;

		case 17:
			sendFrame126("" + c.getLevel()[17] + "", 4022);
			sendFrame126("" + getLevelForXP(c.getExperience()[17]) + "", 4023);
			sendFrame126("" + c.getExperience()[17] + "", 4098);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[17]) + 1) + "", 4099);
			break;

		case 18:
			sendFrame126("" + c.getLevel()[18] + "", 12166);
			sendFrame126("" + getLevelForXP(c.getExperience()[18]) + "", 12167);
			sendFrame126("" + c.getExperience()[18] + "", 12171);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[18]) + 1) + "", 12172);
			break;

		case 19:
			sendFrame126("" + c.getLevel()[19] + "", 13926);
			sendFrame126("" + getLevelForXP(c.getExperience()[19]) + "", 13927);
			sendFrame126("" + c.getExperience()[19] + "", 13921);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[19]) + 1) + "", 13922);
			break;

		case 20:
			sendFrame126("" + c.getLevel()[20] + "", 4152);
			sendFrame126("" + getLevelForXP(c.getExperience()[20]) + "", 4153);
			sendFrame126("" + c.getExperience()[20] + "", 4157);
			sendFrame126("" + getXPForLevel(getLevelForXP(c.getExperience()[20]) + 1) + "", 4158);
			break;
		}
	}

	public int getXPForLevel(int level) {
		if (level >= Config.LEVEL_TO_EXPERIENCE.length) {
			return Config.LEVEL_TO_EXPERIENCE[99];
		}

		return Config.LEVEL_TO_EXPERIENCE[level];
	}

	public int getLevelForXP(int exp) {
		int points = 0;
		int output = 0;
		if (exp > 13034430)
			return 99;
		for (int lvl = 1; lvl <= 99; lvl++) {
			points += Math.floor((double) lvl + 300.0 * Math.pow(2.0, (double) lvl / 7.0));
			output = (int) Math.floor(points / 4);
			if (output >= exp) {
				return lvl;
			}
		}
		return 0;
	}

	public boolean addSkillXP(int amount, int skill) {
		if (c.experienceLock) {
			return true;
		}
		if (amount + c.getExperience()[skill] < 0 || c.getExperience()[skill] > 200000000) {
			if (c.getExperience()[skill] > 200000000) {
				c.getExperience()[skill] = 200000000;
			}
			return false;
		}
		int oldLevel = getLevelForXP(c.getExperience()[skill]);
		c.getExperience()[skill] += amount;
		if (oldLevel < getLevelForXP(c.getExperience()[skill])) {
			if (c.getLevel()[skill] < c.getLevelForXP(c.getExperience()[skill]) && skill != 3 && skill != 5)
				c.getLevel()[skill] = c.getLevelForXP(c.getExperience()[skill]);
			levelUp(skill);
			c.playGraphic(Graphic.create(199, 0, 100));
			requestUpdates();
		}
		setSkillLevel(skill, c.getLevel()[skill], c.getExperience()[skill]);
		refreshSkill(skill);
		return true;
	}

	public void resetBarrows() {
		c.barrowsNpcs[0][1] = 0;
		c.barrowsNpcs[1][1] = 0;
		c.barrowsNpcs[2][1] = 0;
		c.barrowsNpcs[3][1] = 0;
		c.barrowsNpcs[4][1] = 0;
		c.barrowsNpcs[5][1] = 0;
		c.barrowsKillCount = 0;
		c.randomCoffin = Misc.random(3) + 1;
	}

	public static int Barrows[] = { 4708, 4710, 4712, 4714, 4716, 4718, 4720, 4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745, 4747, 4749, 4751, 4753, 4755, 4757, 4759 };
	public static int Runes[] = { 4740, 558, 560, 565 };
	public static int Pots[] = {};

	public int randomBarrows() {
		return Barrows[(int) (Math.random() * Barrows.length)];
	}

	public int randomRunes() {
		return Runes[(int) (Math.random() * Runes.length)];
	}

	public int randomPots() {
		return Pots[(int) (Math.random() * Pots.length)];
	}

	/**
	 * Show an arrow icon on the selected player.
	 *
	 * @Param i - Either 0 or 1; 1 is arrow, 0 is none.
	 * @Param j - The player/Npc that the arrow will be displayed above.
	 * @Param k - Keep this set as 0
	 * @Param l - Keep this set as 0
	 */
	public void drawHeadicon(int i, int j, int k, int l) {
		// synchronized(c) {
		c.getClient().getOutStream().createFrame(254);
		c.getClient().getOutStream().writeByte(i);

		if (i == 1 || i == 10) {
			c.getClient().getOutStream().writeWord(j);
			c.getClient().getOutStream().writeWord(k);
			c.getClient().getOutStream().writeByte(l);
		} else {
			c.getClient().getOutStream().writeWord(k);
			c.getClient().getOutStream().writeWord(l);
			c.getClient().getOutStream().writeByte(j);
		}
	}

	public int getNpcId(int id) {
		for (int i = 0; i < NPCHandler.MAX_NPCS; i++) {
			if (NPCHandler.npcs[i] != null) {
				if (NPCHandler.npcs[i].npcId == id) {
					return i;
				}
			}
		}
		return -1;
	}

	public void removeObject(int x, int y) {
		object(-1, x, x, 10, 10);
	}

	private void objectToRemove(int X, int Y) {
		object(-1, X, Y, 10, 10);
	}

	private void objectToRemove2(int X, int Y) {
		object(-1, X, Y, -1, 0);
	}

	public void removeObjects() {
		objectToRemove2(3213, 3221);
		objectToRemove2(3213, 3222);
	}

	public void Deletewalls(Player c) {
	}

	public Clan getClan() {
		if (Server.clanManager.clanExists(c.playerName)) {
			return Server.clanManager.getClan(c.playerName);
		}
		return null;
	}

	public void sendClan(String name, String message, String clan, int rights) {
		c.getClient().getOutStream().createFrameVarSizeWord(217);
		c.getClient().getOutStream().writeString(name);
		c.getClient().getOutStream().writeString(Misc.formatPlayerName(message));
		c.getClient().getOutStream().writeString(clan);
		c.getClient().getOutStream().writeWord(rights);
		c.getClient().getOutStream().endFrameVarSize();
	}

	public void clearClanChat() {
		c.getPA().sendFrame126("Talking in: ", 18139);
		c.getPA().sendFrame126("Owner: ", 18140);
		c.getPA().sendFrame126("resetclan:1", 0);
	}

	public void setClanData() {
		boolean exists = Server.clanManager.clanExists(c.playerName);
		if (!exists || c.clan == null) {
			sendFrame126("Join chat", 18135);
			sendFrame126("Talking in: Not in chat", 18139);
			sendFrame126("Owner: None", 18140);
		}
		if (!exists) {
			sendFrame126("Chat Disabled", 18306);
			String title = "";
			for (int id = 18307; id < 18317; id += 3) {
				if (id == 18307) {
					title = "Anyone";
				} else if (id == 18310) {
					title = "Anyone";
				} else if (id == 18313) {
					title = "General+";
				} else if (id == 18316) {
					title = "Only Me";
				}
				sendFrame126(title, id + 2);
			}
			sendFrame126("resetclan:2", 0);
			return;
		}
		Clan clan = Server.clanManager.getClan(c.playerName);
		sendFrame126(clan.getTitle(), 18306);
		String title = "";
		for (int id = 18307; id < 18317; id += 3) {
			if (id == 18307) {
				title = clan.getRankTitle(clan.whoCanJoin) + (clan.whoCanJoin > Clan.Rank.ANYONE && clan.whoCanJoin < Clan.Rank.OWNER ? "+" : "");
			} else if (id == 18310) {
				title = clan.getRankTitle(clan.whoCanTalk) + (clan.whoCanTalk > Clan.Rank.ANYONE && clan.whoCanTalk < Clan.Rank.OWNER ? "+" : "");
			} else if (id == 18313) {
				title = clan.getRankTitle(clan.whoCanKick) + (clan.whoCanKick > Clan.Rank.ANYONE && clan.whoCanKick < Clan.Rank.OWNER ? "+" : "");
			} else if (id == 18316) {
				title = clan.getRankTitle(clan.whoCanBan) + (clan.whoCanBan > Clan.Rank.ANYONE && clan.whoCanBan < Clan.Rank.OWNER ? "+" : "");
			}
			sendFrame126(title, id + 2);
		}
		if (clan.rankedMembers != null) {
			for (int index = 0; index < 100; index++) {
				if (index < clan.rankedMembers.size()) {
					sendFrame126("<clan=" + clan.ranks.get(index) + ">" + clan.rankedMembers.get(index), 18323 + index);
				} else {
					sendFrame126("", 18323 + index);
				}
			}
		}
		if (clan.bannedMembers != null) {
			for (int index = 0; index < 100; index++) {
				if (index < clan.bannedMembers.size()) {
					sendFrame126(clan.bannedMembers.get(index), 18424 + index);
				} else {
					sendFrame126("", 18424 + index);
				}
			}
		}
	}

	public void joinClanChat(String chatName) {
		if (c.clan == null) {
			Clan clan = Server.clanManager.getClan(chatName);
			if (clan != null) {
				clan.addMember(c);
			} else if (chatName.equalsIgnoreCase(c.playerName)) {
				Server.clanManager.create(c);
			} else {
				c.sendMessage(Misc.formatPlayerName(chatName) + " has not created a clan yet.");
			}
			refreshSkill(21);
			refreshSkill(22);
			refreshSkill(23);
		}
	}

	public void resetVariables() {
		c.skillToChange = -1;
		c.canChangeAppearance = false;
		c.isShopping = false;
		c.isBanking = false;
	}

	public boolean inPitsWait() {
		return c.getX() <= 2404 && c.getX() >= 2394 && c.getY() <= 5175 && c.getY() >= 5169;
	}

	public void castleWarsObjects() {
		object(-1, 2373, 3119, -3, 10);
		object(-1, 2372, 3119, -3, 10);
	}

	public int antiFire() {
		int toReturn = 0;
		if (c.antiFirePot)
			toReturn++;
		if (c.getEquipment()[PlayerConstants.PLAYER_SHIELD] == 1540 || c.prayerActive[12] || c.getEquipment()[PlayerConstants.PLAYER_SHIELD] == 11284)
			toReturn++;
		return toReturn;
	}

	public boolean checkForFlags() {
		int[][] itemsToCheck = { { 995, 100000000 }, { 35, 5 }, { 667, 5 }, { 2402, 5 }, { 746, 5 }, { 4151, 150 }, { 565, 100000 }, { 560, 100000 }, { 555, 300000 }, { 11235, 10 } };
		for (int j = 0; j < itemsToCheck.length; j++) {
			if (itemsToCheck[j][1] < c.getItems().getTotalCount(itemsToCheck[j][0]))
				return true;
		}
		return false;
	}

	public int getWearingAmount() {
		int count = 0;
		for (int j = 0; j < c.getEquipment().length; j++) {
			if (c.getEquipment()[j] > 0)
				count++;
		}
		return count;
	}

	public int getInventoryAmount() {
		int count = 0;
		for (int j = 0; j < c.playerItems.length; j++) {
			if (c.playerItems[j] > 0)
				count++;
		}
		return count;
	}

	public void useOperate(int itemId) {
		switch (itemId) {
		case 1712:
		case 1710:
		case 1708:
		case 1706:
//			handleGlory(itemId);
			break;
		case 11283:
		case 11284:
			if (c.playerIndex > 0) {
				c.getCombat().handleDfs();
			} else if (c.npcIndex > 0) {
				c.getCombat().handleDfsNPC();
			}
			break;
		}
	}

	public void getSpeared(int otherX, int otherY) {
		int x = c.absX - otherX;
		int y = c.absY - otherY;
		if (x > 0)
			x = 1;
		else if (x < 0)
			x = -1;
		if (y > 0)
			y = 1;
		else if (y < 0)
			y = -1;
		moveCheck(x, y);
		c.playGraphic(Graphic.create(254, 0, 100));
		c.lastSpear = System.currentTimeMillis();
	}

	public void moveCheck(int xMove, int yMove) {
		if (Region.getClipping(c.absX + xMove, c.absY + yMove, c.heightLevel) == 0) {
			movePlayer(c.absX + xMove, c.absY + yMove, c.heightLevel);
		}
	}

	public int findKiller() {
		int killer = c.playerId;
		int damage = 0;
		for (int j = 0; j < Config.MAX_PLAYERS; j++) {
			if (World.players[j] == null)
				continue;
			if (j == c.playerId)
				continue;
			if (c.goodDistance(c.absX, c.absY, World.players[j].absX, World.players[j].absY, 40)
					|| c.goodDistance(c.absX, c.absY + 9400, World.players[j].absX, World.players[j].absY, 40)
					|| c.goodDistance(c.absX, c.absY, World.players[j].absX, World.players[j].absY + 9400, 40))
				if (c.damageTaken[j] > damage) {
					damage = c.damageTaken[j];
					killer = j;
				}
		}
		return killer;
	}

	public void appendPoison(int damage) {
		if (System.currentTimeMillis() - c.lastPoisonSip > c.poisonImmune) {
			c.sendMessage("You have been poisoned!");
			c.poisonDamage = damage;
		}
	}

	public void appendVenom() {
		if (System.currentTimeMillis() - c.venomImmunity < 600000) {
			return;
		}
		c.venomDamage = 6;
	}

	public boolean checkForPlayer(int x, int y) {
		for (Player p : World.players) {
			if (p != null) {
				if (p.getX() == x && p.getY() == y)
					return true;
			}
		}
		return false;
	}

	public void checkPouch(int i) {
		if (i < 0)
			return;
		c.sendMessage("This pouch has " + c.pouches[i] + " rune ess in it.");
	}

	public void fillPouch(int i) {
		if (i < 0)
			return;
		int toAdd = c.POUCH_SIZE[i] - c.pouches[i];
		if (toAdd > c.getItems().getItemAmount(1436)) {
			toAdd = c.getItems().getItemAmount(1436);
		}
		if (toAdd > c.POUCH_SIZE[i] - c.pouches[i])
			toAdd = c.POUCH_SIZE[i] - c.pouches[i];
		if (toAdd > 0) {
			c.getItems().deleteItem(1436, toAdd);
			c.pouches[i] += toAdd;
		}
	}

	public void emptyPouch(int i) {
		if (i < 0)
			return;
		int toAdd = c.pouches[i];
		if (toAdd > c.getItems().freeSlots()) {
			toAdd = c.getItems().freeSlots();
		}
		if (toAdd > 0) {
			c.getItems().addItem(1436, toAdd);
			c.pouches[i] -= toAdd;
		}
	}

	public void fixAllBarrows() {
		int totalCost = 0;
		int cashAmount = c.getItems().getItemAmount(995);
		for (int j = 0; j < c.playerItems.length; j++) {
			boolean breakOut = false;
			for (int i = 0; i < c.getItems().brokenBarrows.length; i++) {
				if (c.playerItems[j] - 1 == c.getItems().brokenBarrows[i][1]) {
					if (totalCost + 80000 > cashAmount) {
						breakOut = true;
						c.sendMessage("You have run out of money.");
						break;
					} else {
						totalCost += 80000;
					}
					c.playerItems[j] = c.getItems().brokenBarrows[i][0] + 1;
				}
			}
			if (breakOut)
				break;
		}
		if (totalCost > 0)
			c.getItems().deleteItem(995, c.getItems().getItemSlot(995), totalCost);
	}

	/*
	 * public double getKdr() { return (double) c.kills / (c.deaths == 0 ? 1 :
	 * c.deaths); }
	 */

	public String getKdr() {
		DecimalFormat df = new DecimalFormat("#.##");
		double ratio = ((double) c.kills) / ((double) c.deaths);
		return df.format(ratio);
	}

	public void handleLoginText() {
		c.getPA().sendFrame126("resetquest:", 0);
		c.getPA().sendFrame126("@gre@Player Rank: @whi@"
				+ (c.getRights() != PlayerRights.PLAYER ? c.getRights().getTitle() : c.getDonatorRights().isDonator() ? c.getDonatorRights().getTitle() : c.getRights().getTitle()), 16026);
		c.getPA().sendFrame126("@gre@OldSchool-PVP Points: @whi@" + c.osrsPoints, 16027);
		c.getPA().sendFrame126("@gre@PVP Points: @whi@" + c.pkPoints, 16028);
		c.getPA().sendFrame126("@gre@Player Kills: @whi@" + c.kills, 16030);
		c.getPA().sendFrame126("@gre@Player Deaths: @whi@" + c.deaths, 16031);
		c.getPA().sendFrame126("@gre@K/D/R: @whi@" + getKdr(), 16032);

		c.getPA().sendFrame126("@red@Quick Links", 16036);
		c.getPA().sendFrame126("@dre@Community", 16037);
		c.getPA().sendFrame126("@dre@Store", 16038);
		c.getPA().sendFrame126("@dre@Vote", 16039);
		c.getPA().sendFrame126("@dre@Item List", 16040);
		
		c.getPA().sendFrame126("Boss Teleport", 1300);
		c.getPA().sendFrame126("Teleport to various bosses.", 1301);
		c.getPA().sendFrame126("Boss Teleport", 13037);
		c.getPA().sendFrame126("Teleport to various bosses.", 13038);
		c.getPA().sendFrame126("Boss Teleport", 1300);
		c.getPA().sendFrame126("Teleport to various bosses.", 1301);
		c.getPA().sendFrame126("Boss Teleport", 30067);
		c.getPA().sendFrame126("Teleport to various bosses.", 30068);
		
		c.getPA().sendFrame126("Minigame Teleport", 13047);
		c.getPA().sendFrame126("Teleport to various minigames.", 13048);
		c.getPA().sendFrame126("Minigame Teleport", 1325);
		c.getPA().sendFrame126("Teleport to various minigames.", 1326);
		c.getPA().sendFrame126("Minigame Teleport", 30078);
		c.getPA().sendFrame126("Teleport to various minigames.", 30079);
		
		c.getPA().sendFrame126("Pking Teleport", 1350);
		c.getPA().sendFrame126("Teleport to various pking \\n hotspots.", 1351);
		c.getPA().sendFrame126("Pking Teleport", 13055);
		c.getPA().sendFrame126("Teleport to various pking \\n hotspots.", 13056);
		c.getPA().sendFrame126("Pking Teleport", 30086);
		c.getPA().sendFrame126("Teleport to various pking \\n hotspots.", 30087);
		
		c.getPA().sendFrame126("City Teleport", 1382);
		c.getPA().sendFrame126("Teleport to various cities.", 1383);
		c.getPA().sendFrame126("City Teleport", 13063);
		c.getPA().sendFrame126("Teleport to various cities.", 13064);
		c.getPA().sendFrame126("City Teleport", 30109);
		c.getPA().sendFrame126("Teleport to various cities.", 30110);
		
		c.getPA().sendFrame126("Skill Teleport", 1415);
		c.getPA().sendFrame126("Teleport to various skilling \\n locations.", 1416);
		c.getPA().sendFrame126("Skill Teleport", 13071);
		c.getPA().sendFrame126("Teleport to various skilling \\n locations.", 13072);
		c.getPA().sendFrame126("Skill Teleport", 30117);
		c.getPA().sendFrame126("Teleport to various skilling \\n locations.", 30117);
		
	}

	public void handleWeaponStyle() {
		if (c.fightMode == 0) {
			c.getPA().sendFrame36(43, c.fightMode);
		} else if (c.fightMode == 1) {
			c.getPA().sendFrame36(43, 3);
		} else if (c.fightMode == 2) {
			c.getPA().sendFrame36(43, 1);
		} else if (c.fightMode == 3) {
			c.getPA().sendFrame36(43, 2);
		}
	}

	public void globalYell(String txt) {
		for (int i = 0; i < World.players.length; i++) {
			if (World.players[i] == null)
				continue;
			World.players[i].sendMessage(txt);
		}
	}

	private static int[] optionIds = { 2460, 2470, 2481, 2493, 2493 };

	public void showOptions(Player c, String... lines) {
		if (lines == null || lines.length < 2 || lines.length > 5) {
			return;
		}
		int id = optionIds[lines.length - 2];
		c.getPA().sendFrame126("Select an Option", id++);
		for (int i = 0; i < 5; i++) {
			c.getPA().sendFrame126(i >= lines.length ? "" : lines[i], id++);
		}
		c.getPA().sendFrame164(optionIds[lines.length - 2] - 1);
	}
}
