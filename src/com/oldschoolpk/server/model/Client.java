package com.oldschoolpk.server.model;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.core.cycle.CycleEventHandler;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.net.ConnectionSet;
import com.oldschoolpk.server.net.NettyPacket;
import com.oldschoolpk.server.net.packet.PacketHandler;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.util.Stream;
import com.oldschoolpk.server.world.Clan;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;

public class Client {

	public byte buffer[] = null;
	private Stream inStream = null, outStream = null;
	private Channel channel;
	private Queue<NettyPacket> queuedPackets = new ArrayDeque<>();
	protected AtomicInteger packetsReceived = new AtomicInteger();
	
	private Stream updateBlock = new Stream(new byte[5000]);
	
	private @Getter @Setter Player player;

	public int lowMemoryVersion = 0;
	public int timeOutCounter = 0;
	public int returnCode = 2;
	private Future<?> currentTask;

	public Client(Channel s) {
		channel = s;
		outStream = new Stream(new byte[Config.BUFFER_SIZE]);
		outStream.currentOffset = 0;
		inStream = new Stream(new byte[Config.BUFFER_SIZE]);
		inStream.currentOffset = 0;
		buffer = new byte[Config.BUFFER_SIZE];
	}

	public void flushOutStream() {
		if (getPlayer().disconnected || outStream.currentOffset == 0)
			return;
		synchronized (outStream) {
			byte[] temp = new byte[outStream.currentOffset];
			ByteBuf payload = Unpooled.buffer(temp.length);
			System.arraycopy(outStream.buffer, 0, temp, 0, temp.length);
			payload.writeBytes(temp);
			channel.writeAndFlush(new NettyPacket(payload));
			outStream.currentOffset = 0;
		}
	}

	public void sendClan(String name, String message, String clan, int rights) {
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		message = message.substring(0, 1).toUpperCase() + message.substring(1);
		clan = clan.substring(0, 1).toUpperCase() + clan.substring(1);
		getOutStream().createFrameVarSizeWord(217);
		getOutStream().writeString(name);
		getOutStream().writeString(message);
		getOutStream().writeString(clan);
		getOutStream().writeWord(rights);
		getOutStream().endFrameVarSize();
	}

	public void joinOSRSPVPCC() {
		if (getPlayer().clan == null) {
			Clan localClan = Server.clanManager.getClan("help");
			if (localClan != null)
				localClan.addMember(getPlayer());
			else if ("help".equalsIgnoreCase(getPlayer().playerName))
				Server.clanManager.create(getPlayer());
			else {
				sendMessage(Misc.formatPlayerName("Help") + " has disabled this clan for now.");
			}
			getPlayer().getPA().refreshSkill(21);
			getPlayer().getPA().refreshSkill(22);
			getPlayer().getPA().refreshSkill(23);
		}
	}

	public static final int PACKET_SIZES[] = { 0, 0, 0, 1, -1, 0, 0, 0, 0, 0, // 0
			0, 0, 0, 0, 8, 0, 6, 2, 2, 0, // 10
			0, 2, 0, 6, 0, 12, 0, 0, 0, 0, // 20
			0, 0, 0, 0, 0, 8, 4, 0, 0, 2, // 30
			2, 6, 0, 6, 0, -1, 0, 0, 0, 0, // 40
			0, 0, 0, 12, 0, 0, 0, 8, 8, 0, // 50
			8, 8, 0, 0, 0, 0, 0, 0, 0, 0, // 60
			6, 0, 2, 2, 8, 6, 0, -1, 0, 6, // 70
			0, 0, 0, 0, 0, 1, 4, 6, 0, 0, // 80
			0, 0, 0, 0, 0, 3, 0, 0, -1, 0, // 90
			0, 13, 0, -1, 0, 0, 0, 0, 0, 0, // 100
			0, 0, 0, 0, 0, 0, 0, 6, 0, 0, // 110
			1, 0, 6, 0, 0, 0, -1, /* 0 */-1, 2, 6, // 120
			0, 4, 6, 8, 0, 6, 0, 0, 0, 2, // 130
			6, 10, -1, 0, 0, 6, 0, 0, 0, 0, // 140
			0, 0, 1, 2, 0, 2, 6, 0, 0, 0, // 150
			0, 0, 0, 0, -1, -1, 0, 0, 0, 0, // 160
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 170
			0, 8, 0, 3, 0, 2, 6, 0, 8, 1, // 180
			0, 0, 12, 0, 0, 0, 0, 0, 0, 0, // 190
			2, 0, 0, 0, 0, 0, 0, 0, 4, 0, // 200
			4, 0, 0, /* 0 */4, 7, 8, 0, 0, 10, 0, // 210
			0, 0, 0, 0, 0, 0, -1, 0, 6, 0, // 220
			1, 0, 0, 0, 6, 0, 6, 8, 1, 0, // 230
			0, 4, 0, 0, 0, 0, -1, 0, -1, 4, // 240
			0, 0, 6, 6, 0, 0, 0 // 250
	};

	public void destruct() {
		if (channel == null)
			return;
		if (getPlayer().clan != null) {
			getPlayer().clan.removeMember(getPlayer());
		}
		CycleEventHandler.getSingleton().stopEvents(getPlayer());
		if (getPlayer().getTradeHandler().getCurrentTrade() != null) {
			if (getPlayer().getTradeHandler().getCurrentTrade().isOpen()) {
				getPlayer().getTradeHandler().decline();
			}
		}
		if (getPlayer().duelStatus > 0 && getPlayer().duelStatus < 4) {
			Player o = World.players[getPlayer().duelingWith];
			if (o == null) {
				getPlayer().getDuel().declineDuel();
				return;
			}
			getPlayer().getDuel().declineDuel();
			o.getClient().getPlayer().getDuel().declineDuel();
		}
		for (Player p : World.players) {
			if (p != null) {
				p.getClient().getPlayer().getPA().sendFrame126("Players Online " + "@gre@[" + (World.getPlayerCount()) + "]", 39160);
			}
		}
		Misc.println("[DEREGISTERED]: " + getPlayer().playerName + "");
		ConnectionSet.remove(getPlayer().connectedFrom);
		getPlayer().disconnected = true;
		channel.close();
		channel = null;
		inStream = null;
		outStream = null;
		getPlayer().isActive = false;
		buffer = null;
		getPlayer().destruct();
	}

	public void sendMessage(String s) {
		if (getOutStream() != null) {
			getOutStream().createFrameVarSize(253);
			getOutStream().writeString(s);
			getOutStream().endFrameVarSize();
		}
	}

	/**
	 * Sets a sidebar interface id
	 * 
	 * @param menuId
	 *            The interface id to display on the sidebar
	 * @param form
	 *            The id if the sidebar to display the interface
	 */
	public void setSidebarInterface(int menuId, int form) {
		if (getOutStream() != null) {
			getOutStream().createFrame(71);
			getOutStream().writeWord(form);
			getOutStream().writeByteA(menuId);
		}
	}

	/**
	 * Send our players index and membership status
	 */
	public void initialize() {
		getOutStream().createFrame(249);
		getOutStream().writeByteA(1); // 1 for members, zero for free
		getOutStream().writeWordBigEndianA(getPlayer().playerId);
		flushOutStream();
	}

	public void logout() {
		if (getPlayer().duelStatus > 4) {
			sendMessage("You can not log out in a duel!");
			return;
		}

		if (getPlayer().privateChat != 2) {
			Arrays.stream(World.players).filter(Objects::nonNull).filter(p -> p.isActive).forEach(p -> p.getClient().getPlayer().getPA().updatePM(getPlayer().playerIndex, 1));
		} // Try that

		if (System.currentTimeMillis() - getPlayer().logoutDelay > 10000) {
			if (getPlayer().clan != null) {
				getPlayer().clan.removeMember(getPlayer());
			}
			if (getPlayer().getPetIndex() != -1) {
				if (NPCHandler.npcs[getPlayer().getPetIndex()] != null)
					NPCHandler.npcs[getPlayer().getPetIndex()].remove();
			}
			CycleEventHandler.getSingleton().stopEvents(getPlayer());
			getOutStream().createFrame(109);
			flushOutStream();
			getPlayer().properLogout = true;
		} else {
			sendMessage("You must wait a few seconds from being out of combat to logout.");
		}
	}

	public int packetSize = 0, packetType = -1;

	public void setCurrentTask(Future<?> task) {
		currentTask = task;
	}

	public Future<?> getCurrentTask() {
		return currentTask;
	}

	public synchronized Stream getInStream() {
		return inStream;
	}

	public synchronized int getPacketType() {
		return packetType;
	}

	public synchronized int getPacketSize() {
		return packetSize;
	}

	public synchronized Stream getOutStream() {
		return outStream;
	}
	
	public synchronized Stream getUpdateBlock() {
		return updateBlock;
	}

	/**
	 * End of Skill Constructors
	 */

	public void queueMessage(NettyPacket p) {

		getPacketsReceived().incrementAndGet();
		synchronized (queuedPackets) {
			queuedPackets.add(p);
		}
	}

	public void processQueuedPackets() {
		getPacketsReceived().set(0);
		int processed = 0;
		for (;;) {
			NettyPacket next = queuedPackets.poll();
			if (next == null || processed >= Config.MAX_INCOMONG_PACKETS_PER_CYCLE) {
				break;
			}
			inStream.currentOffset = 0;
			packetType = next.getOpcode();
			packetSize = next.getLength();
			inStream.buffer = next.array();
			if (packetType > 0) {
				PacketHandler.processPacket(getPlayer(), packetType, packetSize);
				processed++;
			}
			timeOutCounter = 0;
		}
	}

	public void correctCoordinates() {
		if (getPlayer().inPcGame()) {
			getPlayer().getPA().movePlayer(2657, 2639, 0);
		}
		if (getPlayer().inFightCaves()) {
			getPlayer().getPA().movePlayer(getPlayer().absX, getPlayer().absY, getPlayer().playerId * 4);
			sendMessage("Your wave will start in 10 seconds.");

		}

	}

	/**
	 * Gets the channel for netty networking
	 */
	public Channel getChannel() {
		return channel;
	}

	public int setPacketsReceived(int packetsReceived) {
		return packetsReceived;
	}

	public AtomicInteger getPacketsReceived() {
		return packetsReceived;
	}

	public void setPlayer(Player p) { this.player = p; }

	public Player getPlayer() { return this.player; }
}
