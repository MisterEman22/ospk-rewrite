package com.oldschoolpk.server.model.npc;

import com.oldschoolpk.server.model.Animation;
import com.oldschoolpk.server.model.Entity;
import com.oldschoolpk.server.model.Graphic;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.util.Stream;
import com.oldschoolpk.server.util.WeaponAnimations;

import java.util.ArrayList;
import java.util.List;

public abstract class NPC extends Entity {

	protected static final int ATTACK_TYPE_MELEE = 0;
	protected static final int ATTACK_TYPE_RANGE = 1;
	protected static final int ATTACK_TYPE_MAGIC = 2;
	private int currentAttackType;

	// The distance OUTSIDE the normal random walk bounds that the NPC can follow someone to
	protected static final int OUTSIDE_FOLLOW_DISTANCE = 7;


	public int npcId;
	public int spawnX, spawnY, currentX, currentY, maxMoveX, maxMoveY;
	public int moveX, moveY;
	public int heightLevel;
	public int HP;
	public int viewX, viewY, direction;
	public int tileIndex;
	public int spawnedBy, hitDiff, animNumber;
	public boolean applyDead, isDead, needRespawn;
	public boolean walkingHome;
	public int freezeTimer, attackTimer, hitDelayTimer, actionTimer;
	public int victimId, delayedVictimId, killedBy, attackerId;
	public String forcedText;

	// Update flags
	public boolean dirUpdateRequired;
	public boolean animUpdateRequired;
	public boolean hitUpdateRequired;
	public boolean updateRequired;


	public int followIndex;
	public long lastDamageTaken;

	public boolean visible = true;
	
	public NPC(int npcId, int spawnX, int spawnY, int maxMoveX, int maxMoveY, int heightLevel) {
		this.npcId = npcId;
		direction = -1;
		isDead = false;
		applyDead = false;
		actionTimer = 0;
		followIndex = -1;

		this.spawnX = spawnX;
		this.spawnY = spawnY;
		this.currentX = spawnX;
		this.currentY = spawnY;
		this.maxMoveX = maxMoveX;
		this.maxMoveY = maxMoveY;
		this.heightLevel = heightLevel;

		this.HP = getMaxHP();
		this.spawnedBy = 0;
	}


	public void startAnimation(int animId) {
		animNumber = animId;
		animUpdateRequired = true;
		updateRequired = true;
	}

	public void updateNPCMovement(Stream str) {
		if (direction == -1) {
			if (updateRequired) {
				str.writeBits(1, 1);
				str.writeBits(2, 0);
			} else {
				str.writeBits(1, 0);
			}
		} else {
			str.writeBits(1, 1);
			str.writeBits(2, 1);
			str.writeBits(3, Misc.xlateDirectionToClient[direction]);
			if (updateRequired) {
				str.writeBits(1, 1);
			} else {
				str.writeBits(1, 0);
			}
		}
	}

	/**
	 * Text update
	 **/

	public void forceChat(String text) {
		forcedText = text;
		updateRequired = true;
	}

	/**
	 * Graphics
	 **/

	public int mask80var1 = 0;
	public int mask80var2 = 0;
	protected boolean mask80update = false;

	public void appendMask80Update(Stream str) {
		str.writeWord(mask80var1);
		str.writeDWord(mask80var2);
	}

	public void gfx100(int gfx) {
		mask80var1 = gfx;
		mask80var2 = 6553600;
		mask80update = true;
		updateRequired = true;
	}

	public void gfx0(int gfx) {
		mask80var1 = gfx;
		mask80var2 = 65536;
		mask80update = true;
		updateRequired = true;
	}

	public void appendAnimUpdate(Stream str) {
		str.writeWordBigEndian(animNumber);
		str.writeByte(1);
	}

	/**
	 * Face
	 **/

	public int FocusPointX = -1, FocusPointY = -1;
	public int face = 0;

	private void appendSetFocusDestination(Stream str) {
		str.writeWordBigEndian(FocusPointX);
		str.writeWordBigEndian(FocusPointY);
	}

	public void turnNpc(int i, int j) {
		FocusPointX = 2 * i + 1;
		FocusPointY = 2 * j + 1;
		updateRequired = true;

	}

	public void appendFaceEntity(Stream str) {
		str.writeWord(face);
	}

	public void facePlayer(int player) {
		face = player + 32768;
		dirUpdateRequired = true;
		updateRequired = true;
	}

	public void appendNPCUpdateBlock(Stream str) {
		if (!updateRequired)
			return;
		int updateMask = 0;
		if (animUpdateRequired)
			updateMask |= 0x10;
		if (hitUpdateRequired2)
			updateMask |= 8;
		if (mask80update)
			updateMask |= 0x80;
		if (dirUpdateRequired)
			updateMask |= 0x20;
		if (forcedText != null)
			updateMask |= 1;
		if (hitUpdateRequired)
			updateMask |= 0x40;
		if (FocusPointX != -1)
			updateMask |= 4;

		str.writeByte(updateMask);

		if (animUpdateRequired)
			appendAnimUpdate(str);
		if (hitUpdateRequired2)
			appendHitUpdate2(str);
		if (mask80update)
			appendMask80Update(str);
		if (dirUpdateRequired)
			appendFaceEntity(str);
		if (forcedText != null)
			str.writeString(forcedText);
		if (hitUpdateRequired)
			appendHitUpdate(str);
		if (FocusPointX != -1)
			appendSetFocusDestination(str);

	}

	public void clearUpdateFlags() {
		updateRequired = false;
		hitUpdateRequired = false;
		hitUpdateRequired2 = false;
		animUpdateRequired = false;
		dirUpdateRequired = false;
		mask80update = false;
		forcedText = null;
		moveX = 0;
		moveY = 0;
		direction = -1;
		FocusPointX = -1;
		FocusPointY = -1;
	}

	public int getNextWalkingDirection() {
		int dir;
		dir = Misc.direction(currentX, currentY, (currentX + moveX), (currentY + moveY));
		if (dir == -1)
			return -1;
		dir >>= 1;
		currentX += moveX;
		currentY += moveY;
		return dir;
	}

	public void setNextDirection() {
		direction = -1;
		if (this.freezeTimer == 0) {
			direction = getNextWalkingDirection();
		}
	}

	public void appendHitUpdate(Stream str) {
		if (this.HP <= 0) {
			this.isDead = true;
		}
		str.writeByteC(this.hitDiff);
		if (this.hitDiff > 0) {
			str.writeByteS(1);
		} else {
			str.writeByteS(0);
		}
		str.writeDWord(this.HP);
		str.writeDWord(getMaxHP());
	}

	public int hitDiff2 = 0;
	public boolean hitUpdateRequired2 = false;

	public void appendHitUpdate2(Stream str) {
		if (HP <= 0) {
			isDead = true;
		}
		str.writeByteA(hitDiff2);
		if (hitDiff2 > 0) {
			str.writeByteC(1);
		} else {
			str.writeByteC(0);
		}
		str.writeDWord(HP);
		str.writeDWord(getMaxHP());
	}

	public void handleHitMask(int damage) {
		if (!hitUpdateRequired) {
			hitUpdateRequired = true;
			hitDiff = damage;
		} else if (!hitUpdateRequired2) {
			hitUpdateRequired2 = true;
			hitDiff2 = damage;
		}
		updateRequired = true;
	}

	public int getX() {
		return currentX;
	}

	public int getY() {
		return currentY;
	}

	public boolean inMulti() {
		if ((currentX >= 3136 && currentX <= 3327 && currentY >= 3519 && currentY <= 3607) || (currentX >= 3190 && currentX <= 3327 && currentY >= 3648 && currentY <= 3839)
				|| (currentX >= 3200 && currentX <= 3390 && currentY >= 3840 && currentY <= 3967) || (currentX >= 2992 && currentX <= 3007 && currentY >= 3912 && currentY <= 3967)
				|| (currentX >= 2946 && currentX <= 2959 && currentY >= 3816 && currentY <= 3831) || (currentX >= 3008 && currentX <= 3199 && currentY >= 3856 && currentY <= 3903)
				|| (currentX >= 3008 && currentX <= 3071 && currentY >= 3600 && currentY <= 3711) || (currentX >= 3072 && currentX <= 3327 && currentY >= 3608 && currentY <= 3647)
				|| (currentX >= 2624 && currentX <= 2690 && currentY >= 2550 && currentY <= 2619) || (currentX >= 2371 && currentX <= 2422 && currentY >= 5062 && currentY <= 5117)
				|| (currentX >= 2896 && currentX <= 2927 && currentY >= 3595 && currentY <= 3630) || (currentX >= 2892 && currentX <= 2932 && currentY >= 4435 && currentY <= 4464)
				|| (currentX >= 2256 && currentX <= 2287 && currentY >= 4680 && currentY <= 4711) || (currentX >= 3216 && currentY <= 10355 && currentX <= 3247 && currentY >= 10329)
				|| (currentX >= 3684 && currentY >= 5797 && currentX <= 3708 && currentY <= 5821) || (currentX >= 2800 && currentX <= 2950 && currentY >= 5200 && currentY <= 5400)
				|| (currentX >= 2971 && currentY <= 4398 && currentX <= 3001 && currentY >= 4365) || (currentX >= 2800 && currentX <= 2950 && currentY >= 5200 && currentY <= 5400)
				|| (currentX >= 1225 && currentX <= 1255 && currentY >= 1230 && currentY <= 1265)|| (currentX >= 1400 && currentX <= 3640 && currentY >= 1700 && currentY <= 3700)
				) {
			return true;
		}
		return false;
	}

	public boolean inWild() {
		return (currentX > 2941 && currentX < 3392 && currentY > 3518 && currentY < 3966 || currentX > 2941 && currentX < 3392 && currentY > 9918 && currentY < 10366);
	}

public void process() {
	if (NPCHandler.npcs[npcId] == null)
		return;

	processTimer();
	if (!isDead) {
		if (victimId > -1) {
			if (attackTimer == 0) processStartAttack();
		} else {
			processWalk();
		}
	} else {
		processDead();
	}
	if (hitDelayTimer == 0 && delayedVictimId > -1) {
		processAttackHit();
	}

	if (spawnedBy > 0) { // delete summoned NPCs
		if (World.players[spawnedBy] == null
				|| World.players[spawnedBy].heightLevel != heightLevel
				|| World.players[spawnedBy].respawnTimer > 0
				|| !World.players[spawnedBy].goodDistance(getX(), getY(), World.players[spawnedBy].getX(), World.players[spawnedBy].getY(), 20)) {

			NPCHandler.npcs[npcId] = null;
		}
	}
	if (NPCHandler.npcs[npcId] == null)
		return;
}

	/**
	 * Timer part of the process
	 * Decrements timers
	 */
	private void processTimer() {
		if (actionTimer > 0) {
			actionTimer--;
		}

		if (freezeTimer > 0) {
			freezeTimer--;
		}

		if (hitDelayTimer > 0) {
			hitDelayTimer--;
		}

		if (attackTimer > 0) {
			attackTimer--;
		}
	}


	private void processFindVictim() {
		if (victimId != 0 && !doesSwitchVictims()) {
			return;
		}
		List<Player> possibleVictims = new ArrayList<>();
		if (isAggressive() && victimId == 0) {
			possibleVictims = NPCHandler.getClosePlayers(this);
		}
	}


	private void processStartAttack() {
		Player player = World.players[victimId];
		if (inMulti() && (player.underAttackBy > 0 || (player.underAttackBy2 > 0 && player.underAttackBy2 != npcId))) {
			victimId = 0;
			return;
		}
		if (heightLevel != player.heightLevel) {
			victimId = 0;
			return;
		}

		facePlayer(victimId);
		followPlayer(victimId);
		if (NPCHandler.goodDistance(getX(), getY(), player.getX(), player.getY(), getMaxAttackDistance())) {
			if (player.respawnTimer <= 0) {
				// Set the attack type, timers, and victim to be hit
				this.currentAttackType = getAttackType();
				attackTimer = getAttackDelay();
				hitDelayTimer = getHitDelay();
				this.delayedVictimId = victimId;

				// Fire the projectile
				if (getProjectileId() > 0) {
					int nX = getX();
					int nY = getY();
					int pX = player.getX();
					int pY = player.getY();
					int offX = (nY - pY) * -1;
					int offY = (nX - pX) * -1;
					player.getPA().createPlayersProjectile(nX, nY, offX, offY, 50, getProjectileSpeed(),
							getProjectileId(), 43, 31, -player.getId() - 1, 65);
				}
				player.underAttackBy2 = npcId;
				player.singleCombatDelay2 = System.currentTimeMillis();
				startAnimation(getAttackAnimation());
				player.getPA().removeAllWindows();
			}
		}
	}


	/**
	 * Calculates and applies the damage from the delayed hit
     */
	private void processAttackHit() {
		Player player = World.players[delayedVictimId];
		if (player == null) return;
		// Player auto retaliation
		if (player.playerIndex <= 0 && player.npcIndex <= 0 && player.autoRet == 1) {
			player.npcIndex = this.npcId;
		}
		if (player.attackTimer <= 3 && player.npcIndex == 0 && player.oldNpcIndex == 0) {
			player.playAnimation(Animation.create(WeaponAnimations.blockAnimation(player)));
		}

		if (player.respawnTimer <= 0) {
			int damage = 0;
			if (this.currentAttackType == ATTACK_TYPE_MELEE) {
				int playerDef = player.getCombat().calculateMeleeDefence();
				if (!(player.prayerActive[18] || (Misc.random(playerDef) > Misc.random(getAttack())))) {
					damage = Misc.random(getMaxHit());
				}
			} else if (this.currentAttackType == ATTACK_TYPE_RANGE) {
				int playerDef = player.getCombat().calculateRangeDefence();
				if (!(player.prayerActive[17] || (Misc.random(playerDef) > Misc.random(getAttack())))) {
					damage = Misc.random(getMaxHit());
				}
			} else if (this.currentAttackType == ATTACK_TYPE_MAGIC) {
				int playerDef = player.getCombat().calculateMagicDefence();
				boolean magicSuccess = false;
				if (!(player.prayerActive[16] || (Misc.random(playerDef) > Misc.random(getAttack())))) {
					damage = Misc.random(getMaxHit());
					magicSuccess = true;
				}
				if (magicSuccess) {
					player.playGraphic(Graphic.create(getMagicHitAnimation(), 0, 100));
				} else {
					player.playGraphic(Graphic.create(85, 0, 100));
				}
			}
			if (damage > player.getLevel()[3]) {
				damage = player.getLevel()[3];
			}
			if (!player.disconnected)
				player.logoutDelay = System.currentTimeMillis(); // logout delay
			player.handleHitMask(damage);
			player.getLevel()[3] -= damage;
			player.getPA().refreshSkill(3);
			player.updateRequired = true;
			this.delayedVictimId = -1;
		}
	}

	/**
	 * Walking part of the process
	 * Deals with random movement and walking home
	 */
	private void processWalk() {
		facePlayer(0);
		victimId = 0;
		if (spawnedBy == 0 && isOutsideFollowBound()) {
			walkingHome = true;
		}
		if (walkingHome) {
			if (isHome()) {
				walkingHome = false;
			} else {
				moveX = NPCHandler.GetMove(currentX, spawnX);
				moveY = NPCHandler.GetMove(currentY, spawnY);
				setNextDirection();
				updateRequired = true;
			}
		} else {
			if (Misc.random(4) == 0) {    // 20% chance to move
				this.moveX = getRandomMoveX();
				this.moveY = getRandomMoveY();
				setNextDirection();
				updateRequired = true;
			}
		}
	}

	/**
	 * Process to run if NPC is dead
	 */
	private void processDead() {
		if (actionTimer == 0 && applyDead == false && needRespawn == false) {
			updateRequired = true;
			facePlayer(0);
			killedBy = NPCHandler.getNpcKillerId(npcId);
			animNumber = getDeathAnimation();
			animUpdateRequired = true;
			freezeTimer = 0;
			applyDead = true;
			actionTimer = 4; // delete time
			NPCHandler.resetPlayersInCombat(npcId);
		} else if (actionTimer == 0 && applyDead == true && needRespawn == false) {
			needRespawn = true;
			actionTimer = getRespawnDelay(); // respawn time
			NPCHandler.dropItems(npcId); // npc drops items!
			NPCHandler.appendSlayerExperience(npcId);
			NPCHandler.appendKillCount(npcId);
			currentX = spawnX;
			currentY = spawnY;
			HP = getMaxHP();
			animNumber = 0x328;
			updateRequired = true;
			animUpdateRequired = true;
		} else if (actionTimer == 0 && needRespawn == true) {
			if (spawnedBy > 0) {
				NPCHandler.npcs[npcId] = null;
			} else {
				NPCHandler.npcs[npcId] = null;
				NPCHandler.createNPC(getNPCType(), spawnX, spawnY, maxMoveX, maxMoveY, heightLevel);
			}
		}
	}

	public void followPlayer(int playerId) {
		if (World.players[playerId] == null) {
			return;
		}
		if (World.players[playerId].respawnTimer > 0) {
			facePlayer(0);
			return;
		}

		int playerX = World.players[playerId].absX;
		int playerY = World.players[playerId].absY;
		if (NPCHandler.goodDistance(getX(), getY(), playerX, playerY, getMaxAttackDistance()))
			return;
		if ((spawnedBy > 0) || !isOutsideFollowBound()) {
			if (heightLevel == World.players[playerId].heightLevel) {
				if (World.players[playerId] != null && NPCHandler.npcs[npcId] != null) {
					if (playerX == currentX || playerY == currentY) {
						int o = Misc.random(3);
						switch (o) {
							case 0:
								moveX = NPCHandler.GetMove(currentX, playerX);
								moveY = NPCHandler.GetMove(currentY, playerY + 1);
								break;

							case 1:
								moveX = NPCHandler.GetMove(currentX, playerX);
								moveY = NPCHandler.GetMove(currentY, playerY - 1);
								break;

							case 2:
								moveX = NPCHandler.GetMove(currentX, playerX + 1);
								moveY = NPCHandler.GetMove(currentY, playerY);
								break;

							case 3:
								moveX = NPCHandler.GetMove(currentX, playerX - 1);
								moveY = NPCHandler.GetMove(currentY, playerY);
								break;
						}
					} else {
						moveX = NPCHandler.GetMove(currentX, playerX);
						moveY = NPCHandler.GetMove(currentY, playerY);
					}
					facePlayer(playerId);
					setNextDirection();
					facePlayer(playerId);
					updateRequired = true;
				}
			}
		} else {
			facePlayer(0);
		}
	}
	
	public void teleport(int x, int y, int heightLevel) {
		this.currentX = x;
		this.currentY = y;
		this.heightLevel = heightLevel;
		this.updateRequired = true;
	}
	
	public void remove() {
		NPCHandler.npcs[npcId] = null;
		this.currentX = 0;
		this.currentY = 0;
		this.updateRequired = true;
	}

	public boolean isVisible() { return this.visible; }

	public void setVisible(boolean b) { this.visible = b; }

	private int getRandomMoveX() {
		int moveX = Misc.random(2) - 1;
		if (Math.abs(this.spawnX - (this.currentX + moveX)) > this.maxMoveX) {
			moveX = 0;
		}
		return moveX;
	}

	private int getRandomMoveY() {
		int moveY = Misc.random(2) - 1;
		if (Math.abs(this.spawnY - (this.currentY + moveY)) > this.maxMoveY) {
			moveY = 0;
		}
		return moveY;
	}

	private boolean isOutsideFollowBound() {
		return Math.abs(this.spawnX - this.currentX) > this.maxMoveX + OUTSIDE_FOLLOW_DISTANCE ||
				Math.abs(this.spawnY - this.currentY) > this.maxMoveY + OUTSIDE_FOLLOW_DISTANCE;
	}

	private boolean isOutsideRandomWalkBound() {
		return Math.abs(this.spawnX - this.currentX) > this.maxMoveX + OUTSIDE_FOLLOW_DISTANCE ||
				Math.abs(this.spawnY - this.currentY) > this.maxMoveY + OUTSIDE_FOLLOW_DISTANCE;
	}

	public boolean isHome() {
		return (this.currentX == this.spawnX) && (this.currentY == this.spawnY);
	}

	// Implementation/Override methods for subclasses

	public abstract int getAttackAnimation();

	public abstract int getDeathAnimation();

	public abstract int getProjectileId();

	public abstract int getProjectileSpeed();

	public abstract int getMagicHitAnimation();

	public abstract int getMaxAttackDistance();

	public abstract boolean doesSwitchVictims();

	public abstract int getAttackType();

	public abstract int getMaxHP();

	public abstract int getMaxHit();

	public abstract int getAttack();

	public abstract int getDefence();

	public abstract int getNPCType();

	public boolean doesRetaliate() {
		return true;
	}

	public boolean isAggressive() {
		return false;
	}

	public int getAttackDelay() {
		return 5;
	}

	public int getHitDelay() {
		return 1;
	}

	public int getRespawnDelay() {
		return 25;
	}

	public int boneDropId() {
		return 526;
	}

	// TODO: May need an offset function so projectiles originate from center of NPC
	// TODO: May need to add multiAttacks() function. I think it made attack hit multiple people at once
	// TODO: npcSize() function? not sure what it did or if it's needed

}
