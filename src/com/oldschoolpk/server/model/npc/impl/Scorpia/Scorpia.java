package com.oldschoolpk.server.model.npc.impl.Scorpia;

import com.oldschoolpk.server.model.npc.NPC;

/**
 * Scorpia boss
 */
public class Scorpia extends NPC {



    public Scorpia(int npcId, int spawnX, int spawnY, int maxMoveX, int maxMoveY, int heightLevel) {
        super(npcId, spawnX, spawnY, maxMoveX, maxMoveY, heightLevel);
    }

    @Override
    public int getAttackAnimation() {
        return 6261;
    }

    @Override
    public int getDeathAnimation() {
        return 6260;
    }

    @Override
    public int getProjectileId() {
        return 0;
    }

    @Override
    public int getProjectileSpeed() {
        return 0;
    }

    @Override
    public int getMagicHitAnimation() {
        return 0;
    }

    @Override
    public int getMaxAttackDistance() {
        return 1;
    }

    @Override
    public boolean isAggressive() {
        return true;
    }

    @Override
    public boolean doesSwitchVictims() {
        return false;
    }

    @Override
    public int getAttackType() {
        return ATTACK_TYPE_MELEE;
    }

    @Override
    public int getMaxHP() {
        return 30;
    }

    @Override
    public int getMaxHit() {
        return 16;
    }

    @Override
    public int getAttack() {
        return 150;
    }

    @Override
    public int getDefence() {
        return 150;
    }

    @Override
    public int getNPCType() {
        return 6615;
    }
}
