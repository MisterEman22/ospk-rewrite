package com.oldschoolpk.server.model.npc.impl;

import com.oldschoolpk.server.model.npc.NPC;

/**
 * Created by Nick on 5/4/2016.
 */
public class HillGiant extends NPC {
    public HillGiant(int npcId, int spawnX, int spawnY, int maxMoveX, int maxMoveY, int heightLevel) {
        super(npcId, spawnX, spawnY, maxMoveX, maxMoveY, heightLevel);
    }

    @Override
    public int getAttackAnimation() {
        return 4652;
    }

    @Override
    public int getDeathAnimation() {
        return 4653;
    }

    @Override
    public int getProjectileId() {
        return 0;
    }

    @Override
    public int getProjectileSpeed() {
        return 0;
    }

    @Override
    public int getMagicHitAnimation() {
        return 0;
    }

    @Override
    public int getMaxAttackDistance() {
        return 1;
    }

    @Override
    public boolean doesRetaliate() {
        return true;
    }

    @Override
    public boolean isAggressive() {
        return true;
    }

    @Override
    public boolean doesSwitchVictims() {
        return false;
    }

    @Override
    public int getAttackType() {
        return ATTACK_TYPE_RANGE;
    }

    @Override
    public int getMaxHP() {
        return 50;
    }

    @Override
    public int getMaxHit() {
        return 10;
    }

    @Override
    public int getAttack() {
        return 50;
    }

    @Override
    public int getDefence() {
        return 50;
    }

    @Override
    public int getNPCType() {
        return 2098;
    }

    @Override
    public int getHitDelay() {
        return 1;
    }
}
