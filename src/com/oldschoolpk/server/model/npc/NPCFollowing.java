package com.oldschoolpk.server.model.npc;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.util.cache.defs.NPCDef;

/*
 * TODO: Fucking terrible 2010 PI following, needs complete rework.
 */
public class NPCFollowing {

	public static void followPlayer(int i, int followId) {
		if (NPCHandler.npcs[i] == null) {
			return;
		}
		if (World.players[followId] == null) {
			NPCHandler.npcs[i].currentX = 8888;
			NPCHandler.npcs[i].currentY = 8888;
			NPCHandler.npcs[i].followIndex = -1;
			NPCHandler.npcs[i].updateRequired = true;
			return;
		}
		int playerX = World.players[followId].absX;
		int playerY = World.players[followId].absY;
		NPCHandler.npcs[i].facePlayer(followId);
		NPCDef def = NPCDef.forId(NPCHandler.npcs[i].getNPCType());
		if (def == null) {
			System.out.println(NPCHandler.npcs[i].getNPCType());
			return;
		}
		int size = def.boundDim;
		if (!Server.npcHandler.goodDistance(NPCHandler.npcs[i].getX(), NPCHandler.npcs[i].getY(), playerX, playerY, 15)) {
			NPCHandler.npcs[i].currentX = playerX;
			NPCHandler.npcs[i].currentY = playerY;
		}
		if (Server.npcHandler
				.goodDistance(NPCHandler.npcs[i].getX(), NPCHandler.npcs[i].getY(), playerX, playerY, size)) {
			return;
		}
		if (NPCHandler.npcs[i].heightLevel == World.players[followId].heightLevel) {
			if (playerY < NPCHandler.npcs[i].currentY) {
				NPCHandler.npcs[i].moveX = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentX, playerX);
				NPCHandler.npcs[i].moveY = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentY, playerY);
			} else if (playerY > NPCHandler.npcs[i].currentY) {
				NPCHandler.npcs[i].moveX = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentX, playerX);
				NPCHandler.npcs[i].moveY = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentY, playerY);
			} else if (playerX < NPCHandler.npcs[i].currentX) {
				NPCHandler.npcs[i].moveX = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentX, playerX);
				NPCHandler.npcs[i].moveY = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentY, playerY);
			} else if (playerX > NPCHandler.npcs[i].currentX) {
				NPCHandler.npcs[i].moveX = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentX, playerX);
				NPCHandler.npcs[i].moveY = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentY, playerY);
			} else if (playerX == NPCHandler.npcs[i].currentX || playerY == NPCHandler.npcs[i].currentY) {
				int o = Misc.random(3);
				switch (o) {
				case 0:
					NPCHandler.npcs[i].moveX = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentX, playerX);
					NPCHandler.npcs[i].moveY = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentY, playerY + 1);
					break;

				case 1:
					NPCHandler.npcs[i].moveX = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentX, playerX);
					NPCHandler.npcs[i].moveY = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentY, playerY - 1);
					break;

				case 2:
					NPCHandler.npcs[i].moveX = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentX, playerX + 1);
					NPCHandler.npcs[i].moveY = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentY, playerY);
					break;

				case 3:
					NPCHandler.npcs[i].moveX = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentX, playerX - 1);
					NPCHandler.npcs[i].moveY = Server.npcHandler.GetMove(NPCHandler.npcs[i].currentY, playerY);
					break;
				}
			}
			NPCHandler.npcs[i].facePlayer(followId);
			NPCHandler.npcs[i].setNextDirection();
			NPCHandler.npcs[i].facePlayer(followId);
			NPCHandler.npcs[i].updateRequired = true;
		}
	}

}
