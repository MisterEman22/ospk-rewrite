package com.oldschoolpk.server.model.npc.drop;

import com.oldschoolpk.server.model.item.Item;

import lombok.Getter;

public class Drop {

	private final Item item;
	private final int rarity;
	private @Getter final int random;

	public Drop(int itemId, int itemAmount, int rarity, int random) {
		this.item = new Item(itemId, itemAmount);
		this.rarity = rarity;
		this.random = random;
	}

	public Item getItem() {
		return item;
	}

	public int getRarity() {
		return rarity;
	}

	public int getRandom() { return this.random; }

}
