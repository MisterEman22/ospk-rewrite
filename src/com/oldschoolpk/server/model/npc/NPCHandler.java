package com.oldschoolpk.server.model.npc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.Graphic;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.content.minigame.Barrows;
import com.oldschoolpk.server.model.content.minigame.FightCaves;
import com.oldschoolpk.server.model.item.Item;
import com.oldschoolpk.server.model.npc.drop.Drops;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.util.Misc;

// Imports for implemented NPC classes
import com.oldschoolpk.server.model.npc.impl.*;
import com.oldschoolpk.server.model.npc.impl.Scorpia.*;

public class NPCHandler {
	private static final String AUTO_SPAWN_FILE_NAME = "./data/cfg/autospawn.csv";

	public static final int MAX_NPCS = 10000;
	public static final int MAX_LISTED_NPCS = 10000;
	public static final int maxNPCDrops = 10000;
	public static NPC npcs[] = new NPC[MAX_NPCS];

	public NPCHandler() {
		Arrays.fill(npcs, null);
		loadAutoSpawn(AUTO_SPAWN_FILE_NAME);
		return;
	}

	public static int getFreeSlot() {
		int index = -1;

		for (int i = 1; i < npcs.length; i++) {
			if (npcs[i] == null) {
				index = i;
				break;
			}
		}

		return index;
	}


	public static int getCloseRandomPlayer(int i) {
		List<Player> players = getClosePlayers(npcs[i]);
		if (players.size() > 0)
			return players.get(Misc.random(players.size() - 1)).playerId;
		else
			return 0;
	}

	public static List<Player> getClosePlayers(NPC npc) {
		ArrayList<Player> players = new ArrayList<Player>();
		for (int j = 0; j < World.players.length; j++) {
			if (World.players[j] != null) {
				if (goodDistance(World.players[j].absX, World.players[j].absY, npc.currentX, npc.currentY, 2
						+ npc.getMaxAttackDistance() + npc.maxMoveX)) {
					if (World.players[j].heightLevel == npc.heightLevel)
						players.add(World.players[j]);
				}
			}
		}
		return players;
	}


	/**
	 * Summon npc, barrows, etc
	 **/
	public static NPC spawnPet(Player c, int npcType) {
		NPC pet = createNPC(npcType, c.getX(), c.getY(), c.heightLevel, -1, -1);
		pet.followIndex = c.getId();
		c.setPetIndex(pet.npcId);
		c.setPetId(npcType);
		return pet;
	}

	/**
	 * Spawns a new NPC to the world by creating it then adding it to npcs[]
	 *
	 * @param npcType - Type of the NPC
	 * @param x - Spawn X location
	 * @param y - Spawn Y location
	 * @param maxMoveX - Max movement distance on X axis
	 * @param maxMoveY - Max movement distance on Y axis
	 * @param heightLevel - Spawn (and permanent) height level
     * @return - new NPC that is spawned
     */
	public static NPC createNPC(int npcType, int x, int y, int maxMoveX, int maxMoveY, int heightLevel) {
		int slot = getFreeSlot();
		if (slot == -1) {
			return null;
		}
		NPC npc = getNewNPC(npcType, slot, x, y, maxMoveX, maxMoveY, heightLevel);

		npcs[slot] = npc;
		return npc;
	}

	/**
	 * Gets the new specific NPC object based on parameters
	 * Only method in NPCHandler that needs to be modified when a new NPC is implemented
	 *
	 * @param npcType - Type of the NPC, determines what type of NPC will be returned
	 * @param slot - Index of the NPC in npcs[]. Stored in the NPC object as npcId
	 * @param x - Spawn X location, passed on to constructor of NPC
	 * @param y - Spawn Y location, passed on to constructor of NPC
	 * @param maxMoveX - Max movement distance on X axis, passed on to constructor of NPC
	 * @param maxMoveY - Max movement distance on Y axis, passed on to constructor of NPC
	 * @param heightLevel - Spawn (and permanent) height level, passed on to constructor of NPC
     * @return - New NPC subclass object that is created
     */
	private static NPC getNewNPC(int npcType, int slot, int x, int y, int maxMoveX, int maxMoveY, int heightLevel) {
		switch (npcType) {
			case 154:
				return new HugeSpider(slot, x, y, maxMoveX, maxMoveY, heightLevel);
			case 2098:
				return new HillGiant(slot, x, y, maxMoveX, maxMoveY, heightLevel);
			case 6615:
				return new Scorpia(slot, x, y, maxMoveX, maxMoveX, heightLevel);
			default:
				return null;
		}
	}

	public static void process() {
		for (int i = 0; i < MAX_NPCS; i++) {
			if (npcs[i] == null)
				continue;
			npcs[i].clearUpdateFlags();

		}

		for (int i = 0; i < MAX_NPCS; i++) {
			if (npcs[i] != null) {
				if (npcs[i].getNPCType() > 6900) {
					npcs[i].setVisible(false);
					npcs[i] = null;
					continue;
				}
				npcs[i].process();
			}
		}
	}


	/**
	 * Gets the ID of the player that killed the NPC. Should be the one getting the drop?
	 * @param npcId - ID of the NPC that was killed
	 * @return - int ID of the player that dealt the most damage to NPC
     */
	public static int getNpcKillerId(int npcId) {
		int oldDamage = 0;
		int killerId = 0;
		for (int p = 1; p < Config.MAX_PLAYERS; p++) {
			if (World.players[p] != null) {
				if (World.players[p].lastNpcAttacked == npcId) {
					if (World.players[p].totalDamageDealt > oldDamage) {
						oldDamage = World.players[p].totalDamageDealt;
						killerId = p;
					}
					World.players[p].totalDamageDealt = 0;
				}
			}
		}
		return killerId;
	}


	/**
	 * Dropping Items!
	 **/

	public static void dropItems(int npcIndex) {
		/*
		 * Find killer and npc objects.
		 */
		NPC npc = npcs[npcIndex];
		Player client = World.players[npc.killedBy];
		if (client == null || npc == null) {
			return;
		}
		if (FightCaves.fightCaveMonsters(npcIndex)) {
			if (client.getTzhaarWaveId() >= 5) {
				FightCaves.defeatGame(client);
				return;
			}
			client.setTzhaarToKill(client.getTzhaarToKill() - 1);
			if (client.getTzhaarToKill() <= 0) {
				client.setTzhaarWaveId(client.getTzhaarWaveId() + 1);
				FightCaves.startWave(client);
			}
		}
		for (Item item : Drops.getRandomDrop(client, npc.getNPCType())) {
			if (!client.getItems().isStackable(item.getId()) && item.getAmount() > 1) {
				for (int i = 0; i < item.getAmount(); i++) {
					Server.itemHandler.createGroundItem(client, item.getId(), npc.currentX, npc.currentY, 1, client.playerId);
				}
			} else {
				Server.itemHandler.createGroundItem(client, item.getId(), npc.currentX, npc.currentY, item.getAmount(),
						client.playerId);
			}
		}
	}

	public static void appendKillCount(int i) {
		Player c = World.players[npcs[i].killedBy];
		if (c != null) {
			int[] kcMonsters = { 122, 49, 2558, 2559, 2560, 2561, 2550, 2551, 2552, 2553, 2562, 2563, 2564, 2565 };
			for (int j : kcMonsters) {
				if (npcs[i].getNPCType() == j) {
					if (c.killCount < 20) {
						c.killCount++;
						c.sendMessage("Killcount: " + c.killCount);
					} else {
						c.sendMessage("You already have 20 kill count");
					}
					break;
				}
			}
		}
	}

	public static int getStackedDropAmount(int itemId, int npcId) {
		switch (itemId) {
		case 995:
			switch (npcId) {
			case 1:
				return 50 + Misc.random(50);
			case 9:
				return 133 + Misc.random(100);
			case 1624:
				return 1000 + Misc.random(300);
			case 1618:
				return 1000 + Misc.random(300);
			case 1643:
				return 1000 + Misc.random(300);
			case 1610:
				return 1000 + Misc.random(1000);
			case 1613:
				return 1500 + Misc.random(1250);
			case 1615:
				return 3000;
			case 18:
				return 500;
			case 101:
				return 60;
			case 913:
			case 912:
			case 914:
				return 750 + Misc.random(500);
			case 1612:
				return 250 + Misc.random(500);
			case 1648:
				return 250 + Misc.random(250);
			case 90:
				return 200;
			case 82:
				return 1000 + Misc.random(455);
			case 52:
				return 400 + Misc.random(200);
			case 49:
				return 1500 + Misc.random(2000);
			case 1341:
				return 1500 + Misc.random(500);
			case 26:
				return 500 + Misc.random(100);
			case 20:
				return 750 + Misc.random(100);
			case 21:
				return 890 + Misc.random(125);
			case 117:
				return 500 + Misc.random(250);
			case 2607:
				return 500 + Misc.random(350);
			}
			break;
		case 11212:
			return 10 + Misc.random(4);
		case 565:
		case 561:
			return 10;
		case 560:
		case 563:
		case 562:
			return 15;
		case 555:
		case 554:
		case 556:
		case 557:
			return 20;
		case 892:
			return 40;
		case 886:
			return 100;
		case 6522:
			return 6 + Misc.random(5);

		}

		return 1;
	}

	/**
	 * Slayer Experience
	 **/
	public static void appendSlayerExperience(int i) {
		Player c = World.players[npcs[i].killedBy];
		if (c != null) {
			if (c.slayerTask == npcs[i].getNPCType()) {
				c.taskAmount--;
				c.getPA().addSkillXP(npcs[i].getMaxHP() * Config.SLAYER_EXPERIENCE, 18);
				if (c.taskAmount <= 0) {
					c.getPA().addSkillXP((npcs[i].getMaxHP() * 8) * Config.SLAYER_EXPERIENCE, 18);
					c.slayerTask = -1;
					c.sendMessage("You completed your slayer task. Please see a slayer master to get a new one.");
				}
			}
		}
	}

	/**
	 * Resets players in combat
	 */
	// ^^^Thats an A+ comment right there
	public static void resetPlayersInCombat(int i) {
		for (int j = 0; j < World.players.length; j++) {
			if (World.players[j] != null)
				if (World.players[j].underAttackBy2 == i)
					World.players[j].underAttackBy2 = 0;
		}
	}

	/**
	 * Npc Follow Player
	 **/

	public static int GetMove(int Place1, int Place2) {
		if ((Place1 - Place2) == 0) {
			return 0;
		} else if ((Place1 - Place2) < 0) {
			return 1;
		} else if ((Place1 - Place2) > 0) {
			return -1;
		}
		return 0;
	}





	public static boolean goodDistance(int objectX, int objectY, int playerX, int playerY, int distance) {
		return ((objectX - playerX <= distance && objectX - playerX >= -distance) && (objectY - playerY <= distance && objectY
				- playerY >= -distance));
	}


	public static boolean loadAutoSpawn(String fileName) {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
			String line;
			String[] lineValues;
			while ((line = bufferedReader.readLine()) != null) {
				if (line.charAt(0) != '#') {
					lineValues = line.split(",");
					int npcType = Integer.parseInt(lineValues[0]);
					int spawnX = Integer.parseInt(lineValues[1]);
					int spawnY = Integer.parseInt(lineValues[2]);
					int maxMoveX = Integer.parseInt(lineValues[3]);
					int maxMoveY = Integer.parseInt(lineValues[4]);
					int heightLevel = Integer.parseInt(lineValues[5]);
					createNPC(npcType, spawnX, spawnY, maxMoveX, maxMoveY, heightLevel);
				}
			}
		} catch (FileNotFoundException fileex) {
			Misc.println(fileName + ": file not found.");
			return false;
		} catch (Exception e) {
			Misc.println(fileName + ": error loading file.");
		}

		return false;
	}
}
