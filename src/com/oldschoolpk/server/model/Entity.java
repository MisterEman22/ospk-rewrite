package com.oldschoolpk.server.model;

import java.util.HashMap;
import java.util.Map;

public abstract class Entity {
	
	/**
	 * Attributes a creature can hold
	 */
	private final HashMap<String, Object> attributes = new HashMap<>();
	
	public <T> T removeAttribute(String key) {
		@SuppressWarnings("unchecked")
		T t = (T) attributes.remove(key);
		return t;
	}

	public void setAttribute(final String key, final Object value) {
		attributes.put(key, value);
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public <T> T getAttribute(String key, T fail) {

		Object object = attributes.get(key);

		if (object != null) {
			@SuppressWarnings("unchecked")
			T t = (T) object;
			return t;
		}

		return fail;
	}

}
