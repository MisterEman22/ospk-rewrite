package com.oldschoolpk.server.model;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.npc.NPC;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerSave;
import com.oldschoolpk.server.util.Stream;

/**
 * Represents the entire game world of the server
 * 
 * @author Arithium
 *
 */
public class World {

	/**
	 * An array of players registered to the server
	 */
	public static Player players[] = new Player[Config.MAX_PLAYERS];

	/**
	 * A map to store players by their username for faster referencing
	 */
	private static Map<String, Player> playerByUsername = new HashMap<>();

	/**
	 * A map to store players by their username hash for faster referencing
	 */
	private static Map<Long, Player> playerByHash = new HashMap<>();

	/**
	 * The amount of players online
	 */
	private static int playersOnline;

	/**
	 * The last index of all players online to prevent looping empty slots
	 */
	public static int lastIndex = 2;

	public static boolean updateAnnounced;
	public static boolean updateRunning;
	public static int updateSeconds;
	public static long updateStartTime;
	private static boolean kickAllPlayers = false;

	private static ExecutorService threadPool;

	static {
		int cpus = Runtime.getRuntime().availableProcessors();
		threadPool = Executors.newFixedThreadPool(cpus);
	}

	/**
	 * Registers a new player to the server
	 * 
	 * @param player
	 *            The {@link Player} to register to the server
	 * @return Whether or not the player was registered
	 */
	public static boolean register(Player player) {
		int slot = -1;
		for (int i = 1; i < World.lastIndex + 1; i++) {
			if (slot == -1 && players[i] == null) {
				slot = i;
			}
		}
		if (slot == -1)
			return false;
		if (slot == World.lastIndex - 1 || slot == World.lastIndex) {
			World.lastIndex++;
			System.out.println("LOGIN: NEW COUNT: " + World.lastIndex);
		}
		player.playerId = slot;
		players[slot] = player;
		players[slot].isActive = true;
		players[slot].connectedFrom = ((InetSocketAddress) player.getClient().getChannel().remoteAddress()).getAddress()
				.getHostAddress();
		playerByUsername.put(player.getUsername().toLowerCase(), player);
		playerByHash.put(player.getUsernameHash(), player);
		playersOnline++;
		return true;
	}

	/**
	 * Deregisters a player from the server
	 * 
	 * @param player
	 *            The {@link Player} to deregister from the server
	 */
	private static void deregister(Player player) {
		if (player.getTradeHandler().stage > 0) {
		} else {
			PlayerSave.saveGame(player);
		}
		for (Player p : players) {
			if (p != null && p.getPA().isInPM(player.getUsernameHash())) {
				p.getPA().updatePM(player.playerId, 0);
			}
		}
		playerByUsername.remove(player.getUsername().toLowerCase());
		playerByHash.remove(player.getUsernameHash());
		player.getClient().destruct();
	}

	public void destruct() {
		for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			if (players[i] == null) {
				continue;
			}
			players[i].destruct();
			players[i] = null;
		}
	}

	/**
	 * Handles updating all of the online players
	 */
	public static void process() {

		long start = System.currentTimeMillis();

		if (kickAllPlayers) {
			for (int i = 1; i < lastIndex; i++) {
				if (players[i] != null) {
					players[i].disconnected = true;
				}
			}
		}
		for (int i = 0; i < lastIndex; i++) {
			Player player = players[i];
			if (player == null || !player.isActive)
				continue;
			try {

				if (player.disconnected && (System.currentTimeMillis() - player.logoutDelay > 10000
						|| player.properLogout || kickAllPlayers)) {
					if (player.getTradeHandler().getStage() > 0) {
						player.getTradeHandler().cancelCurrentTrade();
					}
					if (player.duelStatus > 0) {
						Player o1 = World.players[player.duelingWith];

						if (player.duelStatus == 5 && o1 != null) {
							o1.getDuel().duelVictory();
							continue;
						}
						player.getDuel().declineDuel();
						if (o1 != null)
							o1.getDuel().declineDuel();
						continue;
					}
					if (PlayerSave.saveGame(player)) {
						System.out.println("Game saved for player " + player.playerName);
					} else {
						System.out.println("Could not save for " + player.playerName);
					}
					deregister(player);
					playersOnline--;
					players[i] = null;
					continue;
				}

				player.preProcessing();
				player.getClient().processQueuedPackets();
				player.getPA().handleRealFollowing();
				player.process();
				player.postProcessing();
				player.getNextPlayerMovement();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		boolean multithreaded = false;

		if (!multithreaded) {
			for (int i = 0; i < lastIndex; i++) {
				Player player = players[i];
				if (player == null || !player.isActive)
					continue;
				try {
					if (!player.initialized) {
						player.handleLogin();
						player.initialized = true;
					} else {
						updatePlayer(player, player.getClient().getOutStream());
						updateNPC(player, player.getClient().getOutStream());
						player.getClient().flushOutStream();
					}
				} catch (Exception e) {
					e.printStackTrace();
					player.disconnected = true;
				}
			}
		} else {
			final CountDownLatch latch = new CountDownLatch(getPlayerCount());
			for (int i = 0; i < lastIndex; i++) {
				Player player = players[i];
				if (player == null || !player.isActive)
					continue;
				threadPool.execute(new Runnable() {
					@Override
					public void run() {
						synchronized (player.getClient().getOutStream()) {
							try {
								if (!player.initialized) {
									player.handleLogin();
									player.initialized = true;
								} else {
									updatePlayer(player, player.getClient().getOutStream());
									updateNPC(player, player.getClient().getOutStream());
									player.getClient().flushOutStream();
								}
							} catch (Exception e) {
								e.printStackTrace();
								player.disconnected = true;
							} finally {
								latch.countDown();
							}
						}
					}
				});
			}
			try {
				latch.await();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}

		if (updateRunning && !updateAnnounced)

		{
			updateAnnounced = true;
			Server.UpdateServer = true;
		}
		if (updateRunning && (System.currentTimeMillis() - updateStartTime > (updateSeconds * 1000)))

		{
			kickAllPlayers = true;
		}

		for (

		int i = 0; i < lastIndex; i++)

		{
			Player player = players[i];
			if (player == null || !player.isActive)
				continue;
			try {
				player.clearUpdateFlags();
				player.setCachedBlock(null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		long end = System.currentTimeMillis() - start;

		// System.out.println("Time Taken: " + end);
	}

	/**
	 * Handles updating all of the npcs for the player
	 * 
	 * @param player
	 *            The {@link Player} to update all npcs for
	 * @param buffer
	 *            The {@link Stream} to write the data too
	 */
	public static void updateNPC(Player player, Stream buffer) {
		player.getClient().getUpdateBlock().currentOffset = 0;

		buffer.createFrameVarSizeWord(65);
		buffer.initBitAccess();

		buffer.writeBits(8, player.npcListSize);
		int size = player.npcListSize;
		player.npcListSize = 0;
		for (int i = 0; i < size; i++) {
			NPC npc = player.npcList[i];
			if (!player.RebuildNPCList && player.withinDistance(npc)) {
				npc.updateNPCMovement(buffer);
				npc.appendNPCUpdateBlock(player.getClient().getUpdateBlock());
				player.npcList[player.npcListSize++] = npc;
			} else {
				int id = npc.npcId;
				player.npcInListBitmap[id >> 3] &= ~(1 << (id & 7));
				buffer.writeBits(1, 1);
				buffer.writeBits(2, 3);
			}
		}

		for (int i = 0; i < NPCHandler.MAX_NPCS; i++) {
			if (NPCHandler.npcs[i] != null) {
				NPC npc = NPCHandler.npcs[i];
				int id = NPCHandler.npcs[i].npcId;
				if (!player.RebuildNPCList && (player.npcInListBitmap[id >> 3] & (1 << (id & 7))) != 0) {
				} else if (player.withinDistance(NPCHandler.npcs[i])) {
					player.addNewNPC(NPCHandler.npcs[i], buffer, player.getClient().getUpdateBlock());
				}
			}
		}

		player.RebuildNPCList = false;

		if (player.getClient().getUpdateBlock().currentOffset > 0) {
			buffer.writeBits(14, 16383);
			buffer.finishBitAccess();
			buffer.writeBytes(player.getClient().getUpdateBlock().buffer,
					player.getClient().getUpdateBlock().currentOffset, 0);
		} else {
			buffer.finishBitAccess();
		}
		buffer.endFrameVarSizeWord();
	}

	/**
	 * Handles updating all players for a single player
	 * 
	 * @param player
	 *            The {@link Player} to update other players for
	 * @param buffer
	 *            The {@link Stream} to write the update block too
	 */
	public static void updatePlayer(Player player, Stream buffer) {
		player.getClient().getUpdateBlock().currentOffset = 0;
		if (updateRunning && !updateAnnounced) {
			buffer.createFrame(114);
			buffer.writeWordBigEndian(updateSeconds * 50 / 30);
		}
		player.updateThisPlayerMovement(buffer);
		boolean saveChatTextUpdate = player.isChatTextUpdateRequired();
		player.setChatTextUpdateRequired(false);
		player.appendPlayerUpdateBlock(player.getClient().getUpdateBlock(), player, true);
		player.setChatTextUpdateRequired(saveChatTextUpdate);
		buffer.writeBits(8, player.playerListSize);
		int size = player.playerListSize;
		player.playerListSize = 0;
		for (int i = 0; i < size; i++) {
			if (!player.didTeleport && !player.playerList[i].didTeleport
					&& player.withinDistance(player.playerList[i])) {
				player.playerList[i].updatePlayerMovement(buffer);
				player.playerList[i].appendPlayerUpdateBlock(player.getClient().getUpdateBlock(), player, false);
				player.playerList[player.playerListSize++] = player.playerList[i];
			} else {
				int id = player.playerList[i].playerId;
				player.playerInListBitmap[id >> 3] &= ~(1 << (id & 7));
				buffer.writeBits(1, 1);
				buffer.writeBits(2, 3);
			}
		}
		int j = 0;
		for (int i = 0; i < Config.MAX_PLAYERS; i++) {
			if (player.playerListSize >= 254)
				break;
			if (player.getClient().getUpdateBlock().currentOffset + buffer.currentOffset >= 4900)
				break;
			if (players[i] == null || !players[i].isActive || players[i] == player)
				continue;
			int id = players[i].playerId;
			if ((player.playerInListBitmap[id >> 3] & (1 << (id & 7))) != 0)
				continue;
			if (j >= 10)
				break;
			if (!player.withinDistance(players[i]))
				continue;
			if (players[i].combatLevel < 1) {
				continue;
			}
			player.addNewPlayer(players[i], buffer, player.getClient().getUpdateBlock(), player);
			j++;
		}

		if (player.getClient().getUpdateBlock().currentOffset > 0) {
			buffer.writeBits(11, 2047);
			buffer.finishBitAccess();

			buffer.writeBytes(player.getClient().getUpdateBlock().buffer,
					player.getClient().getUpdateBlock().currentOffset, 0);
		} else
			buffer.finishBitAccess();

		buffer.endFrameVarSizeWord();
	}

	/**
	 * Gets a player by their username
	 * 
	 * @param name
	 *            The username of the player
	 * @return The {@link Player} reference
	 */
	public static Player getByUsername(String name) {
		return playerByUsername.get(name.toLowerCase());
	}

	/**
	 * Gets a player by their username hash
	 * 
	 * @param hash
	 *            The username hash of the player
	 * @return The {@link Player} reference
	 */
	public static Player getByUsernameHash(long hash) {
		return playerByHash.get(hash);
	}

	/**
	 * Gets the amount of players online
	 * 
	 * @return The amount of players online
	 */
	public static int getPlayerCount() {
		return playersOnline;
	}

	/**
	 * Gets the amount of players in the wilderness
	 * 
	 * @return The amount of players in the wilderness
	 */
	public static int getPlayersInWildy() {
		return (int) Arrays.stream(players).filter(Objects::nonNull).filter(p -> !p.disconnected)
				.filter(p -> p.inWild()).count();
	}

	/**
	 * Checks if a player is online by looking up their username
	 * 
	 * @param playerName
	 *            The username of the player
	 * @return If the player is online
	 */
	public static boolean isPlayerOn(String playerName) {
		return getByUsername(playerName) != null;
	}

}
