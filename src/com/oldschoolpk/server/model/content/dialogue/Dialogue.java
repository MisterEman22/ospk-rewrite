package com.oldschoolpk.server.model.content.dialogue;

import java.util.Objects;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.content.dialogue.DialogueAssistant.Expression;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;

public abstract class Dialogue {

	/**
	 * The make single item title interface id.
	 */
	private static final int MAKE_ITEM_TITLE_INTERFACE_ID = 2799;

	/**
	 * The make single item model interface id.
	 */
	private static final int MAKE_ITEM_MODEL_INTERFACE_ID = 1746;

	/**
	 * The make single item interface id.
	 */
	private static final int MAKE_ITEM_INTERFACE_ID = 4429;

	/**
	 * The statement interface ids.
	 */
	private static final int[] STATEMENT_DIALOGUE_IDS = { 356, 359, 363, 368, 374 };

	/**
	 * The option interface ids.
	 */
	private static final int[] OPTION_DIALOGUE_IDS = { 13760, 2461, 2471, 2482, 2494 };

	/**
	 * Formula for the player dialogue lines
	 */
	private static final int getPlayerDialogueFormula(int i) {
		return 968 + i - 1 + i * i + i / 2 - 1 - i / 4 - i / 4;
	}

	/**
	 * Formula for the npc dialogue lines
	 */
	private static final int getNpcDialogueFormula(int i) {
		return 4882 + i - 1 + i * i + i / 2 - 1 - i / 4 - i / 4;
	}

	protected final String DEFAULT_OPTION_TITLE = "Select an Option";

	private int phase;

	protected Player player;

	public Dialogue() {
		this(0);
	}

	protected Dialogue(int phase) {
		this.phase = phase;
	}

	/**
	 * Handles player chatting
	 *
	 * @param expression
	 *            The {@link Expression} to display
	 * @param text
	 *            The text to write
	 */
	protected final void chat(Expression expression, String... text) {
		Objects.requireNonNull(text);
		int length = text.length;

		if (length < 0 || length > 5) {
			return;
		}
		player.getPA().sendFrame126(player.getUsername(), getPlayerDialogueFormula(text.length) + 2);
		player.getPA().sendFrame200(getPlayerDialogueFormula(text.length) + 1, expression.getEmoteId());
		player.getPA().sendFrame185(getPlayerDialogueFormula(text.length) + 1);
		player.getPA().sendFrame164(getPlayerDialogueFormula(text.length));
		for (int i = 0; i < text.length; i++) {
			player.getPA().sendFrame126(text[i], getPlayerDialogueFormula(text.length) + 3 + i);
		}
	}

	/**
	 * Sends a list of choices
	 *
	 * @param title
	 *            The title of the choice interface
	 * @param options
	 *            The options to send on the choice interface
	 */
	protected final void choice(String title, String... options) {
		if (title == null) {
			title = "Select an Option";
		}

		int length = options.length;
		int dialogueId = OPTION_DIALOGUE_IDS[length - 1];

		// If the title is too long, we need to switch the layers
		// if (title.length() > "Select an Option".length()) {
		// int id = dialogueId + length + 4;
		// player.send(new ToggleWidgetLayer(false, id));
		// player.send(new ToggleWidgetLayer(true, id + 3));
		// }

		player.getPA().sendFrame126(title, dialogueId - 1);

		for (int index = 0; index < length; index++) {
			player.getPA().sendFrame126(options[index], OPTION_DIALOGUE_IDS[length - 1] + index);
		}

		player.getPA().sendFrame164(dialogueId - 2);
	}

	/**
	 * Stops the dialogue
	 */
	protected void finish() {
		stop();
	}

	/**
	 * Accepts the input from the integer input class
	 *
	 * @param value
	 *            The value read
	 */
	protected void input(int value) {

		if (player.getRights().isSuperior(PlayerRights.ADMINISTRATOR)) {
			player.sendMessage("Unhandled dialogue value input: " + value);
			player.sendMessage("in script: " + getClass().getName());
		}

		stop();
	}

	/**
	 * Accepts the input from the integer input class
	 *
	 * @param value
	 *            The value read
	 */
	protected void input(String value) {
		if (player.getRights().isSuperior(PlayerRights.ADMINISTRATOR)) {
			player.sendMessage("Unhandled dialogue name input: " + value);
			player.sendMessage("in script: " + getClass().getName());
		}

		stop();
	}

	/**
	 * Sends a mob chatting with the player
	 *
	 * @param id
	 *            The id of the mob
	 * @param expression
	 *            The {@link Expression} the mob is making
	 * @param text
	 *            The lines of text the mob is saying
	 */
	protected final void mobChat(int id, Expression expression, String... text) {
		player.getPA().sendFrame164(getNpcDialogueFormula(text.length));
		player.getPA().sendFrame126("FooBar", getNpcDialogueFormula(text.length) + 2);
		player.getPA().sendFrame200(getNpcDialogueFormula(text.length) + 1, expression.getEmoteId());
		player.getPA().sendFrame75(id, getNpcDialogueFormula(text.length) + 1);
		for (int i = 0; i < text.length; i++) {
			player.getPA().sendFrame126(text[i], getNpcDialogueFormula(text.length) + 3 + i);
		}

	}

	/**
	 * Continues the dialogue to the next phase
	 */
	protected void next() {
		stop();
	}

	/**
	 * Selects an option from a choice menu
	 *
	 * @param index
	 *            The index of the option
	 */
	protected void select(int index) {
	}

	/**
	 * Starts the dialogue
	 */
	protected abstract void start();

	/**
	 * Sends a statement.
	 *
	 * @param lines
	 *            The lines of dialogue for the statement.
	 */
	public void sendStatement(String... lines) {
		int id = STATEMENT_DIALOGUE_IDS[lines.length - 1];
		for (int i = 0; i < lines.length; i++) {
			player.getPA().sendFrame126(lines[i], id + 1 + i);
		}
		player.getPA().sendFrame164(id);
	}

	/**
	 * Sends a chat with an item on it
	 *
	 * @param header
	 *            The Title of the dialogue
	 * @param one
	 *            The first line of text
	 * @param two
	 *            The second line of text
	 * @param three
	 *            The third line of text
	 * @param four
	 *            The fourth line of text
	 * @param item
	 *            The item to display on the interface
	 * @param zoom
	 *            The zoom of the item
	 */
	public void sendItemChat(String header, String one, String two, String three, String four, int item, int zoom) {
		player.getPA().sendFrame246(4901, zoom, item);
		player.getPA().sendFrame126(header, 4902);
		player.getPA().sendFrame126(one, 4903);
		player.getPA().sendFrame126(two, 4904);
		player.getPA().sendFrame126(three, 4905);
		player.getPA().sendFrame126(four, 4906);
		player.getPA().sendFrame164(4900);
	}

	public void sendMakeItemChat(int id, int zoom, String title) {
		player.getPA().sendFrame246(MAKE_ITEM_MODEL_INTERFACE_ID, zoom, id);
		player.getPA().sendFrame126(title, MAKE_ITEM_TITLE_INTERFACE_ID);
		player.getPA().sendFrame164(MAKE_ITEM_INTERFACE_ID);
	}

	/**
	 * Stops the dialogue from continuing and closes the interface
	 */
	protected final void stop() {
		player.getPA().removeAllWindows();
	}

	/**
	 * Sends the string integer packet
	 */
	protected final void takeNameInput() {
		player.getClient().getOutStream().createFrame(219);
		player.getClient().getOutStream().createFrame(187);
	}

	/**
	 * Sends the integer input packet
	 */
	protected final void takeValueInput() {
		player.getClient().getOutStream().createFrame(219);
		player.getClient().getOutStream().createFrame(27);
	}

	/**
	 * Checks if the phase matches the specific phase
	 *
	 * @param phase
	 *            The phase to check for
	 * @return If the current phase matches the provided phase
	 */
	public boolean isPhase(int phase) {
		return this.phase == phase;
	}

	/**
	 * Sets the current phase value
	 *
	 * @param phase
	 *            The phase value to set
	 */
	public void setPhase(int phase) {
		this.phase = phase;
	}

	/**
	 * Gets the current phase
	 *
	 * @return The current phase
	 */
	public int getPhase() {
		return phase;
	}

}
