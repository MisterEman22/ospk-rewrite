package com.oldschoolpk.server.model.content.dialogue.impl.teleport;

import com.oldschoolpk.server.model.Position;
import com.oldschoolpk.server.model.content.dialogue.Dialogue;
import com.oldschoolpk.server.model.content.teleport.TeleportExecutor;
import com.oldschoolpk.server.util.Misc;

public class PkingTeleportDialogue extends Dialogue {

	@Override
	protected void start() {
		choice(null, "Edgeville", "Greater Demons (Wild)", "27 Portal (Wild)", "Mage Bank", "Next...");
		setPhase(1);
	}
	
	@Override
	public void select(int index) {
		if (isPhase(1)) {
			switch (index) {
			case 1: // edgeville
				TeleportExecutor.teleport(player, new Position(3087, 3517));
				break;
			case 2: // greater demons
				TeleportExecutor.teleport(player, new Position(3285 + Misc.random(2), 3882 + Misc.random(2)));
				break;
			case 3: // 27 portal
				TeleportExecutor.teleport(player, new Position(3030 + Misc.random(3), 3724 + Misc.random(3)));
				break;
			case 4: // mage bank
				TeleportExecutor.teleport(player, new Position(2538 + Misc.random(3), 4714 + Misc.random(3)));
				break;
			case 5:
				choice(null, "East Dragons", "Previous...");
				setPhase(2);
				break;
			}
		} else if (isPhase(2)) {
			switch (index) {
			case 1:
				
				break;
			case 2:
				choice(null, "Edgeville", "Greater Demons (Wild)", "27 Portal (Wild)", "Mage Bank", "Next...");
				setPhase(1);
				break;
			}
		}
	}

}
