package com.oldschoolpk.server.model.content.dialogue.impl.teleport;

import com.oldschoolpk.server.model.Position;
import com.oldschoolpk.server.model.content.dialogue.Dialogue;
import com.oldschoolpk.server.model.content.teleport.TeleportExecutor;
import com.oldschoolpk.server.util.Misc;

public class MinigameTeleportDialogue extends Dialogue {

	@Override
	protected void start() {
		choice(null, "Duel Arena", "Barrows", "Fight Caves");
		setPhase(1);
	}
	
	@Override
	public void select(int index) {
		if (isPhase(1)) {
			switch (index) {
			case 1: // duel arena
				TeleportExecutor.teleport(player, new Position(3362 + Misc.random(4), 3264 + Misc.random(2)));
				break;
			case 2: // barrows
				TeleportExecutor.teleport(player, new Position(3564 + Misc.random(4), 3304 + Misc.random(2)));
				break;
			case 3: // fight caves
				TeleportExecutor.teleport(player, new Position(2442 + Misc.random(4), 5168 + Misc.random(2)));
				break;
			}
		} 
	}

}
