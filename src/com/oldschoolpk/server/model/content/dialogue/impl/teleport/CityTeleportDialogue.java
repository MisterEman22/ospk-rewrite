package com.oldschoolpk.server.model.content.dialogue.impl.teleport;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.model.Position;
import com.oldschoolpk.server.model.content.dialogue.Dialogue;
import com.oldschoolpk.server.model.content.teleport.TeleportExecutor;

public class CityTeleportDialogue extends Dialogue {

	@Override
	protected void start() {
		choice(null, "Varrock", "Lumbridge", "Falador", "Camelot", "More...");
		setPhase(1);
	}
	
	@Override
	public void select(int index) {
		if (isPhase(1)) {
			switch (index) {
			case 1:
				TeleportExecutor.teleport(player, new Position(Config.VARROCK_X, Config.VARROCK_Y));
				break;
			case 2:
				TeleportExecutor.teleport(player, new Position(Config.LUMBY_X, Config.LUMBY_Y));
				break;
			case 3:
				TeleportExecutor.teleport(player, new Position(Config.FALADOR_X, Config.FALADOR_Y));
				break;
			case 4:
				TeleportExecutor.teleport(player, new Position(Config.CAMELOT_X, Config.CAMELOT_Y));
				break;
			}
		}
	}

}
