package com.oldschoolpk.server.model.content.dialogue.impl;

import com.oldschoolpk.server.model.content.dialogue.Dialogue;

public class PinDialogue extends Dialogue {

	private String pin = "";

	public PinDialogue(int phase) {
		super(phase);
	}

	@Override
	protected void start() {
		switch (getPhase()) {
		case 1:
			choice(null, "Create Pin", "Delete Pin", "Change login requirements", "Nevermind");
			break;

		case 2:
			sendStatement("Please enter you OSPK account security pin before continuing.");
			break;
		}
	}

	@Override
	public void next() {
		if (isPhase(2)) {
			takeNameInput();
		} else if (isPhase(4)) {
			takeNameInput();
		} else if (isPhase(5)) {
			choice(null, "Yes", "No");
		} else if (isPhase(7)) {
			choice(null, "Create Pin", "Delete Pin", "Change login requirements", "Nevermind");
			setPhase(1);
		} else if (isPhase(3)) {
			choice(null, "Yes", "No");
			setPhase(8);
		} else if (isPhase(10)) {
			stop();
		}
	}

	@Override
	public void select(int index) {
		switch (getPhase()) {
		case 1:
		case 7:
			switch (index) {
			case 1:
				sendStatement("Please enter a valid pin of characters and numbers between ",
						"4-8 characters long, excluding special characters such as [@!%].");
				setPhase(4);
				break;
			case 2:
				if (player.getAttribute("custom_pin", "").isEmpty()) {
					sendStatement("You do not have a custom pin set.");
					setPhase(7);
					return;
				}
				sendStatement("Are you sure you wish to delete your pin?");
				setPhase(3);
				break;
			case 3:
				choice(null, "Enter pin everytime on login.", "Enter pin only upon computer/IP change.", "Nevermind");
				setPhase(9);
				break;
			case 4:
				stop();
				break;
			}
			break;
		case 5:
			switch (index) {
			case 1:
				player.setAttribute("custom_pin", pin);
				sendStatement("Your pin has been set to " + pin + ". Please keep this safe.");
				setPhase(7);
				break;
			case 2:
				choice(null, "Create Pin", "Delete Pin", "Change login requirements", "Nevermind");
				setPhase(1);
				break;
			}
			break;
		case 8:
			sendStatement("Your pin has been deleted. Please note that your", "account is no longer secure.");
			player.setAttribute("custom_pin", null);
			setPhase(7);
			break;
		case 9:
			switch (index) {
			case 1:
				player.setAttribute("custom_pin_settings", 1);
				sendStatement("You will now be required to enter your pin upon", "every login.");
				break;
			case 2:
				player.setAttribute("custom_pin_settings", 0);
				sendStatement("You will now be required to enter your pin upon", "computer/IP change.");
				break;
			case 3:
				setPhase(7);
				next();
				break;
			}
			setPhase(7);
			break;
		}
	}

	@Override
	public void input(String value) {
		value = value.toLowerCase();
		if (isPhase(2)) {
			if (!value.equals(player.getAttribute("custom_pin", ""))) {
				sendStatement("Your pin did not match, please try again.");
				player.setAttribute("failed_login_attempts", player.getAttribute("failed_login_attempts", 0) + 1);
				
				if (player.getAttribute("failed_login_attempts", 0) == 3) {
					player.disconnected = true;
					player.properLogout = true;
				}
			} else {
				sendStatement("Your account has been verified.");
				setPhase(10);
				player.removeAttribute("failed_login_attempts");
				player.setAttribute("verified_login", true);
			}
			return;
		}
		if (value.length() < 4 || value.length() > 8 || !value.matches("^[a-zA-Z0-9]*$")) {
			sendStatement("Please enter a valid pin between 4-8 characters.");
			setPhase(4);
			return;
		}
		sendStatement("Your pin will be changed too " + value + ". Is this okay?");
		pin = value;
		setPhase(5);
	}

}