package com.oldschoolpk.server.model.content.dialogue.impl.teleport;

import com.oldschoolpk.server.model.Position;
import com.oldschoolpk.server.model.content.dialogue.Dialogue;
import com.oldschoolpk.server.model.content.teleport.TeleportExecutor;
import com.oldschoolpk.server.util.Misc;

public class BossTeleportDialogue extends Dialogue {

	private int choice;

	@Override
	protected void start() {
		this.choice(null, "Corporeal Beast lair (PVP Zone)", "Callisto (Wildy)", "Vet'ion (Wildy)", "Chaos Fanatic (Wildy)", "Next Page");
		setPhase(1);
	}

	@Override
	public void next() {
	}

	@Override
	public void select(int index) {
		if (isPhase(1)) {
			switch (index) {
			case 1: // corp
				TeleportExecutor.teleport(player, new Position(2975, 4384, 2));
				break;
			case 2: // callisto
				TeleportExecutor.teleport(player, new Position(3309, 3847, 0));
				break;
			case 3: // Vet'ion
				TeleportExecutor.teleport(player, new Position(3222, 3788, 0));
				break;
			case 4: // chaos fanatic
				TeleportExecutor.teleport(player, new Position(2984, 3856, 0));
				break;
			case 5:
				choice(null, "Godwars Chambers (Safe)", "King Black Dragon (Safe)", "Glod (Safe)", "Cerberus (Safe)", "Next...");
				setPhase(2);
				break;
			}
		} else if (isPhase(2)) {
			switch (index) {
			case 1:
				choice(null, "Armadyl", "Bandos", "Saradomin", "Zamorak", "Back...");
				setPhase(3);
				break;
			case 2: // kbd
				TeleportExecutor.teleport(player, new Position(3067, 10256, 0));
				break;
			case 3: // Glod
				TeleportExecutor.teleport(player, new Position(2899, 3617, 0));
				break;
			case 4:
				TeleportExecutor.teleport(player, new Position(1240, 1237, 0));
				break;
			case 5:
				this.choice(null, "Lizardman Shaman (Incomplete)", "Beginning...");
				setPhase(4);
				break;
			}
		} else if (isPhase(3)) {
			switch (index) {
			case 1: // armadyl
				TeleportExecutor.teleport(player, new Position(2925, 5331, 2));
				break;
			case 2: // bandos
				TeleportExecutor.teleport(player, new Position(2864, 5354, 2));
				break;
			case 3:
				sendStatement("Unavailable.");
				break;
			case 4:
				sendStatement("Unavailable.");
				break;
			case 5:
				choice(null, "Godwars Chambers (Safe)", "King Black Dragon (Safe)", "Glod (Safe)", "Previous...");
				setPhase(2);
				break;
			}
		} else if (isPhase(4)) {
			switch (index) {
			case 1:
				TeleportExecutor.teleport(player, new Position(1498 + Misc.random(5), 3701 + Misc.random(3), 0));
				break;
			case 2:
				this.choice(null, "Corporeal Beast lair (PVP Zone)", "Callisto (Wildy)", "Vet'ion (Wildy)", "Chaos Fanatic (Wildy)", "Next Page");
				setPhase(1);
				break;
			}
		}
	}

}
