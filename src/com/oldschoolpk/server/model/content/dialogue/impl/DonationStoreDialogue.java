package com.oldschoolpk.server.model.content.dialogue.impl;

import com.oldschoolpk.server.model.content.dialogue.Dialogue;

public class DonationStoreDialogue extends Dialogue {

	@Override
	protected void start() {
		choice(null, "Store 1", "Store 2", "Nevermind");
		setPhase(1);
	}
	
	@Override
	public void select(int index) {
		stop();
		if (index != 3)
			player.getShops().openShop(index == 1 ? 15 : 16);
	}

}
