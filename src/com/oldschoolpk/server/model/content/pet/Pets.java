package com.oldschoolpk.server.model.content.pet;

import com.oldschoolpk.server.model.npc.NPC;
import lombok.val;

import com.oldschoolpk.server.model.content.PlayerContent;
import com.oldschoolpk.server.model.npc.NPCHandler;
import com.oldschoolpk.server.model.player.Player;

public class Pets extends PlayerContent {

	public Pets(Player player) {
		this.player = player;
	}
	
	public boolean pickupPet(int npcId) {
		PetDefinitions def = PetDefinitions.getByNpcId(npcId);
		if (def == null) {
			return false;
		}
		NPC npc = NPCHandler.npcs[getPlayer().npcClickIndex];
		if (npc == null) {
			return false;
		}
		if (npc.npcId != getPlayer().getPetIndex()) {
			return false;
		}
		npc.remove();
		getPlayer().getItems().addItem(def.getItemId(), 1);
		getPlayer().setPetIndex(-1);
		getPlayer().setPetId(-1);
		return true;
	}

	public boolean dropPet(int itemId) {
		PetDefinitions def = PetDefinitions.getByItemId(itemId);
		if (def == null) {
			return false;
		}
		if (getPlayer().getPetIndex() != -1) {
			getPlayer().sendMessage("A pet is already following you.");
			return true;
		}
		NPCHandler.spawnPet(getPlayer(), def.getNpcId());
		getPlayer().getItems().deleteItem(def.getItemId(), 1);
		return true;
	}

	public void process() {
		NPC npc = NPCHandler.npcs[getPlayer().getPetIndex()];
		if (npc == null) {
			getPlayer().setPetIndex(-1);
			return;
		}
		if (!getPlayer().withinDistance(npc)) {
			npc.teleport(getPlayer().getX(), getPlayer().getY(), getPlayer().heightLevel);
		}
	}

	public Player getPlayer() {
		return this.player;
	}
}