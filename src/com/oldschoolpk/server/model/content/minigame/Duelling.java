package com.oldschoolpk.server.model.content.minigame;

import java.util.concurrent.CopyOnWriteArrayList;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.core.tools.LoggingSystem;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.item.GameItem;
import com.oldschoolpk.server.model.item.ItemDefinitions;
import com.oldschoolpk.server.model.item.Items;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerConstants;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.util.Misc;

public class Duelling {

	Player c;

	public Duelling(Player client) {
		this.c = client;
	}

	public CopyOnWriteArrayList<GameItem> otherStakedItems = new CopyOnWriteArrayList<GameItem>();
	public CopyOnWriteArrayList<GameItem> stakedItems = new CopyOnWriteArrayList<GameItem>();

	public void requestDuel(int id) {

		if (!c.getAttribute("verified_login", false)) {
			return;
		}

		if (Server.UpdateServer) {
			c.sendMessage("You can't duel when the server is about to update.");
			return;
		}
		Player o = World.players[id];
		if (o == null) {
			return;
		}
		if (c.getId() == id) {
			return;
		}
		if (c.connectedFrom.equalsIgnoreCase(o.connectedFrom) && c.getRights().isInferior(PlayerRights.DEVELOPER)) {
			c.sendMessage("You can't duel someone on your own network.");
			return;
		}
		if (!o.inDuelArena() || !c.inDuelArena()) {
			return;
		}
		if (c.distanceToPoint(o.getX(), o.getY()) > 1) {
			c.sendMessage("Please move closer to " + o.playerName + " to challenge him.");
			return;
		}
		if (c.duelStatus >= 1 && c.duelStatus <= 4) {
			c.sendMessage("You are already in a duel.");
			return;
		}
		if (c.getTradeHandler().getCurrentTrade() != null) {
			if (c.getTradeHandler().getCurrentTrade().isOpen()) {
				c.sendMessage("You are busy trading.");
				return;
			}
		}
		if (o.getTradeHandler().getCurrentTrade() != null) {
			if (o.getTradeHandler().getCurrentTrade().isOpen()) {
				c.sendMessage(o.playerName + " are busy trading.");
				return;
			}
		}
		if (o.isBanking || o.isShopping || o.duelStatus >= 1 && o.duelStatus <= 4) {
			c.sendMessage(o.playerName + " is busy at the moment.");
			return;
		}
		resetDuel();
		resetDuelItems();
		c.duelingWith = id;
		c.duelRequested = true;
		if (c.duelStatus == 0 && o.duelStatus == 0 && c.duelRequested && o.duelRequested && c.duelingWith == o.getId() && o.duelingWith == c.getId()) {
			c.getDuel().openDuel();
			o.getDuel().openDuel();
		} else {
			c.sendMessage("Sending duel request...");
			o.sendMessage(c.playerName + ":duelreq:");
		}
	}

	public void openDuel() {
		Player o = World.players[c.duelingWith];
		if (o == null) {
			return;
		}
		c.duelStatus = 1;
		refreshduelRules();
		refreshDuelScreen();
		c.canOffer = true;
		for (int i = 0; i < c.getEquipment().length; i++) {
			sendDuelEquipment(c.getEquipment()[i], c.playerEquipmentN[i], i);
		}
		c.getPA().sendFrame126("Dueling with: " + o.playerName + " (level-" + o.combatLevel + ")", 6671);
		c.getPA().sendFrame126("", 6684);
		c.getPA().sendFrame248(6575, 3321);
		c.getItems().resetItems(3322);
	}

	public void sendDuelEquipment(int itemId, int amount, int slot) {
		if (itemId != 0) {
			c.getClient().getOutStream().createFrameVarSizeWord(34);
			c.getClient().getOutStream().writeWord(13824);
			c.getClient().getOutStream().writeByte(slot);
			c.getClient().getOutStream().writeWord(itemId + 1);

			if (amount > 254) {
				c.getClient().getOutStream().writeByte(255);
				c.getClient().getOutStream().writeDWord(amount);
			} else {
				c.getClient().getOutStream().writeByte(amount);
			}
			c.getClient().getOutStream().endFrameVarSizeWord();
		}
	}

	public void refreshduelRules() {
		for (int i = 0; i < c.duelRule.length; i++) {
			c.duelRule[i] = false;
		}
		c.getPA().sendFrame87(286, 0);
		c.duelOption = 0;
	}

	public void refreshDuelScreen() {
		Player o = World.players[c.duelingWith];
		if (o == null) {
			return;
		}
		c.getClient().getOutStream().createFrameVarSizeWord(53);
		c.getClient().getOutStream().writeWord(6669);
		c.getClient().getOutStream().writeWord(stakedItems.toArray().length);
		int current = 0;
		for (GameItem item : stakedItems) {
			if (item.amount > 254) {
				c.getClient().getOutStream().writeByte(255);
				c.getClient().getOutStream().writeDWord_v2(item.amount);
			} else {
				c.getClient().getOutStream().writeByte(item.amount);
			}
			if (item.id > Config.ITEM_LIMIT || item.id < 0) {
				item.id = Config.ITEM_LIMIT;
			}
			c.getClient().getOutStream().writeWordBigEndianA(item.id + 1);

			current++;
		}

		if (current < 27) {
			for (int i = current; i < 28; i++) {
				c.getClient().getOutStream().writeByte(1);
				c.getClient().getOutStream().writeWordBigEndianA(-1);
			}
		}
		c.getClient().getOutStream().endFrameVarSizeWord();

		c.getClient().getOutStream().createFrameVarSizeWord(53);
		c.getClient().getOutStream().writeWord(6670);
		c.getClient().getOutStream().writeWord(o.getDuel().stakedItems.toArray().length);
		current = 0;
		for (GameItem item : o.getDuel().stakedItems) {
			if (item.amount > 254) {
				c.getClient().getOutStream().writeByte(255);
				c.getClient().getOutStream().writeDWord_v2(item.amount);
			} else {
				c.getClient().getOutStream().writeByte(item.amount);
			}
			if (item.id > Config.ITEM_LIMIT || item.id < 0) {
				item.id = Config.ITEM_LIMIT;
			}
			c.getClient().getOutStream().writeWordBigEndianA(item.id + 1);
			current++;
		}

		if (current < 27) {
			for (int i = current; i < 28; i++) {
				c.getClient().getOutStream().writeByte(1);
				c.getClient().getOutStream().writeWordBigEndianA(-1);
			}
		}
		c.getClient().getOutStream().endFrameVarSizeWord();
	}

	public boolean stakeItem(int itemID, int fromSlot, int amount) {

		Player o = World.players[c.duelingWith];
		if (o == null) {
			declineDuel();
			return false;
		}
		if (!c.inDuelArena() || !o.inDuelArena()) {
			declineDuel();
			o.getDuel().declineDuel();
			return false;
		}

		if (c.duelStatus < 1 || c.duelStatus > 2) {
			return false;
		}

		for (int i : Config.ITEM_TRADEABLE) {
			if (i == itemID) {
				c.sendMessage("You can't stake this item.");
				return false;
			}
		}

		if (fromSlot < 0 || fromSlot > 27) {
			return false;
		}

		if (amount <= 0 || amount >= Integer.MAX_VALUE) {
			return false;
		}

		if (c.playerItems[fromSlot] - 1 != itemID || c.playerItemsN[fromSlot] <= 0) {
			return false;
		}

		if (!c.canOffer) {
			return false;
		}

		boolean stackable = ItemDefinitions.forId(itemID).isStackable();

		boolean noted = ItemDefinitions.forId(itemID).isNoteable();

		int current = c.getItems().getItemAmount(itemID);

		if ((stackable || noted || amount == 1) && c.playerItemsN[fromSlot] < amount) {
			amount = c.playerItemsN[fromSlot];
		} else if (!stackable && !noted && amount > current) {
			amount = current;
		}

		changeDuelStuff();

		if (!stackable && !noted) {

			if (amount > 1) {
				for (int a = 0; a < amount; a++) {
					if (c.getItems().playerHasItem(itemID, 1)) {
						stakedItems.add(new GameItem(itemID, 1));
						c.getItems().deleteItem(itemID, c.getItems().getItemSlot(itemID), 1);
					}
				}
			} else {
				stakedItems.add(new GameItem(itemID, 1));
				c.getItems().deleteItem(itemID, fromSlot, 1);
			}
		} else if (stackable || noted || amount == 1) {
			boolean found = false;
			for (GameItem item : stakedItems) {
				if (item.id == itemID) {
					found = true;
					item.amount += amount;
					c.getItems().deleteItem(itemID, fromSlot, amount);
					break;
				}
			}
			if (!found) {
				c.getItems().deleteItem(itemID, fromSlot, amount);
				stakedItems.add(new GameItem(itemID, amount));
			}
		}

		c.getItems().resetItems(3214);
		c.getItems().resetItems(3322);
		o.getItems().resetItems(3214);
		o.getItems().resetItems(3322);
		refreshDuelScreen();
		o.getDuel().refreshDuelScreen();
		c.getPA().sendFrame126("", 6684);
		o.getPA().sendFrame126("", 6684);
		return true;
	}

	public boolean fromDuel(int itemID, int fromSlot, int amount) {
		Player o = World.players[c.duelingWith];
		if (o == null) {
			declineDuel();
			return false;
		}
		if (o.duelStatus <= 0 || c.duelStatus <= 0 || !o.inDuelArena() || !c.inDuelArena()) {
			declineDuel();
			o.getDuel().declineDuel();
			return false;
		}

		if (Items.itemStackable[itemID]) {
			if (c.getItems().freeSlots() - 1 < (c.duelSpaceReq)) {
				c.sendMessage("You have too many rules set to remove that item.");
				return false;
			}
		}

		if (!c.canOffer) {
			return false;
		}

		if (c.duelStatus < 1 || c.duelStatus > 2) {
			return false;
		}

		if (fromSlot < 0 || fromSlot > 27) {
			return false;
		}

		if (amount <= 0 || amount >= Integer.MAX_VALUE) {
			return false;
		}

		if (fromSlot >= c.getDuel().stakedItems.size()) {
			return false;
		}

		GameItem existing = c.getDuel().stakedItems.get(fromSlot);

		if (existing == null) {
			return false;
		}

		if (existing.id != itemID) {
			return false;
		}

		boolean stackable = ItemDefinitions.forId(itemID).isStackable();

		boolean noted = ItemDefinitions.forId(itemID).isNoteable();

		int current = 0;

		for (GameItem i : c.getDuel().stakedItems) {
			if (i != null && i.id == itemID) {
				current += i.amount;
			}
		}

		if ((stackable || noted || amount == 1) && existing.amount < amount) {
			amount = existing.amount;
		} else if (!stackable && !noted && amount > current) {
			amount = current;
		}

		changeDuelStuff();
		boolean goodSpace = true;
		if (!stackable && !noted) {
			if (amount > 1) {
				int toDelete = amount;
				for (int a = c.getDuel().stakedItems.size() - 1; a > -1; a--) {
					GameItem item = c.getDuel().stakedItems.get(a);

					if (item == null)
						continue;
					if (item.id == itemID) {
						c.getDuel().stakedItems.remove(item);
						c.getItems().addItem(item.id, 1);
						toDelete--;

						if (toDelete == 0)
							break;
					}
				}
			} else {
				stakedItems.remove(existing);
				c.getItems().addItem(itemID, 1);
			}
		} else {

			GameItem item = c.getDuel().stakedItems.get(fromSlot);

			int curr = item.amount;
			if (curr - amount <= 0) {
				c.getDuel().stakedItems.remove(item);
			} else {
				item.amount -= amount;
			}
			c.getItems().addItem(itemID, amount);
		}

		o.duelStatus = 1;
		c.duelStatus = 1;
		c.getItems().resetItems(3214);
		c.getItems().resetItems(3322);
		o.getItems().resetItems(3214);
		o.getItems().resetItems(3322);
		c.getDuel().refreshDuelScreen();
		o.getDuel().refreshDuelScreen();
		o.getPA().sendFrame126("", 6684);
		if (!goodSpace) {
			c.sendMessage("You have too many rules set to remove that item.");
			return true;
		}
		return true;
	}

	public void confirmDuel() {
		Player o = World.players[c.duelingWith];
		if (o == null) {
			declineDuel();
			return;
		}
		String itemId = "";
		for (GameItem item : stakedItems) {
			if (Items.itemStackable[item.id] || Items.itemIsNote[item.id]) {
				itemId += c.getItems().getItemName(item.id) + " x " + Misc.format(item.amount) + "\\n";
			} else {
				itemId += c.getItems().getItemName(item.id) + "\\n";
			}
		}
		c.getPA().sendFrame126(itemId, 6516);
		itemId = "";
		for (GameItem item : o.getDuel().stakedItems) {
			if (Items.itemStackable[item.id] || Items.itemIsNote[item.id]) {
				itemId += c.getItems().getItemName(item.id) + " x " + Misc.format(item.amount) + "\\n";
			} else {
				itemId += c.getItems().getItemName(item.id) + "\\n";
			}
		}
		c.getPA().sendFrame126(itemId, 6517);
		c.getPA().sendFrame126("", 8242);
		for (int i = 8238; i <= 8253; i++) {
			c.getPA().sendFrame126("", i);
		}
		c.getPA().sendFrame126("Hitpoints will be restored.", 8250);
		c.getPA().sendFrame126("Boosted stats will be restored.", 8238);
		if (c.duelRule[8]) {
			c.getPA().sendFrame126("There will be obstacles in the arena.", 8239);
		}
		c.getPA().sendFrame126("", 8240);
		c.getPA().sendFrame126("", 8241);

		String[] rulesOption = { "Players cannot forfeit!", "Players cannot move.", "Players cannot use range.", "Players cannot use melee.", "Players cannot use magic.", "Players cannot drink pots.", "Players cannot eat food.",
				"Players cannot use prayer." };

		int lineNumber = 8242;
		for (int i = 0; i < 8; i++) {
			if (c.duelRule[i]) {
				c.getPA().sendFrame126("" + rulesOption[i], lineNumber);
				lineNumber++;
			}
		}
		c.getPA().sendFrame126("", 6571);
		c.getPA().sendFrame248(6412, 197);
		// c.getPA().showInterface(6412);
	}

	public void startDuel() {
		Player o = World.players[c.duelingWith];
		if (o == null) {
			duelVictory();
		}
		c.headIconHints = 2;

		if (c.duelRule[7]) {
			for (int p = 0; p < c.PRAYER.length; p++) { // reset prayer glows
				c.prayerActive[p] = false;
				c.getPA().sendFrame36(c.PRAYER_GLOW[p], 0);
			}
			c.headIcon = -1;
			c.getPA().requestUpdates();
		}
		if (c.duelRule[11]) {
			c.getItems().removeItem(0);
		}
		if (c.duelRule[12]) {
			c.getItems().removeItem(1);
		}
		if (c.duelRule[13]) {
			c.getItems().removeItem(2);
		}
		if (c.duelRule[14]) {
			c.getItems().removeItem(3);
		}
		if (c.duelRule[15]) {
			c.getItems().removeItem(4);
		}
		if (c.duelRule[16]) {
			if (c.getEquipment()[3] > 0) {
				if (c.getItems().is2handed(c.getItems().getItemName(c.getEquipment()[3]), c.getEquipment()[3])) {
					c.getItems().removeItem(3);
				}
			}
			c.getItems().removeItem(5);
		}
		if (c.duelRule[17]) {
			c.getItems().removeItem(7);
		}
		if (c.duelRule[18]) {
			c.getItems().removeItem(9);
		}
		if (c.duelRule[19]) {
			c.getItems().removeItem(10);
		}
		if (c.duelRule[20]) {
			c.getItems().removeItem(12);
		}
		if (c.duelRule[21]) {
			c.getItems().removeItem(13);
		}
		c.duelStatus = 5;
		c.getPA().removeAllWindows();
		c.specAmount = 10;
		c.getItems().addSpecialBar(c.getEquipment()[PlayerConstants.PLAYER_WEAPON]);

		if (c.duelRule[8]) {
			if (c.duelRule[1]) {
				c.getPA().movePlayer(c.duelTeleX, c.duelTeleY, 0);
			} else {
				c.getPA().movePlayer(3366 + Misc.random(12), 3246 + Misc.random(6), 0);
			}
		} else {
			if (c.duelRule[1]) {
				c.getPA().movePlayer(c.duelTeleX, c.duelTeleY, 0);
			} else {
				c.getPA().movePlayer(3335 + Misc.random(12), 3246 + Misc.random(6), 0);
			}
		}

		c.getPA().createPlayerHints(10, o.playerId);
		c.getPA().sendOption("Attack", 1);
		for (int i = 0; i < 20; i++) {
			c.getLevel()[i] = c.getPA().getLevelForXP(c.getExperience()[i]);
			c.getPA().refreshSkill(i);
		}
		c.vengOn = false;
		c.poisonDamage = 0;
		c.venomDamage = 0;
		for (GameItem item : o.getDuel().stakedItems) {
			otherStakedItems.add(new GameItem(item.id, item.amount));
		}
		c.getPA().requestUpdates();
	}

	public void duelVictory() {
		Player o = World.players[c.duelingWith];
		if (o != null) {
			c.getPA().sendFrame126("" + o.combatLevel, 6839);
			c.getPA().sendFrame126(o.playerName, 6840);
			o.duelStatus = 0;
		} else {
			c.getPA().sendFrame126("", 6839);
			c.getPA().sendFrame126("", 6840);
		}
		c.duelStatus = 6;
		c.followId = -1;
		c.getCombat().resetPrayers();
		for (int i = 0; i < 20; i++) {
			c.getLevel()[i] = c.getPA().getLevelForXP(c.getExperience()[i]);
			c.getPA().refreshSkill(i);
		}
		c.getPA().refreshSkill(3);
		duelRewardInterface();
		c.getPA().showInterface(6733);
		c.getPA().movePlayer(Config.DUELING_RESPAWN_X + (Misc.random(Config.RANDOM_DUELING_RESPAWN)), Config.DUELING_RESPAWN_Y + (Misc.random(Config.RANDOM_DUELING_RESPAWN)), 0);
		c.getPA().requestUpdates();
		c.getPA().sendOption("Challenge", 1);
		c.getPA().createPlayerHints(10, -1);
		c.canOffer = true;
		c.duelSpaceReq = 0;
		c.duelingWith = 0;
		c.getCombat().resetPlayerAttack();
		c.duelRequested = false;
	}

	public void duelRewardInterface() {
		c.getClient().getOutStream().createFrameVarSizeWord(53);
		c.getClient().getOutStream().writeWord(6822);
		c.getClient().getOutStream().writeWord(otherStakedItems.toArray().length);
		for (GameItem item : otherStakedItems) {
			if (item.amount > 254) {
				c.getClient().getOutStream().writeByte(255);
				c.getClient().getOutStream().writeDWord_v2(item.amount);
			} else {
				c.getClient().getOutStream().writeByte(item.amount);
			}
			if (item.id > Config.ITEM_LIMIT || item.id < 0) {
				item.id = Config.ITEM_LIMIT;
			}
			c.getClient().getOutStream().writeWordBigEndianA(item.id + 1);
		}
		c.getClient().getOutStream().endFrameVarSizeWord();
	}

	public void claimStakedItems() {
		for (GameItem item : otherStakedItems) {
			if (item.id > 0 && item.amount > 0) {
				if (Items.itemStackable[item.id]) {
					if (!c.getItems().addItem(item.id, item.amount)) {
						Server.itemHandler.createGroundItem(c, item.id, c.getX(), c.getY(), item.amount, c.getId());
					}
					ItemDefinitions def = ItemDefinitions.forId(item.id);
					LoggingSystem.writeOutput(c.getUsername(), "duel/won_duel", "Won Item [id = " + item.id + ", name = " + (def != null ? def.getName() : "") + ", amount = " + item.amount + "]");
				} else {
					int amount = item.amount;
					for (int a = 1; a <= amount; a++) {
						if (!c.getItems().addItem(item.id, 1)) {
							Server.itemHandler.createGroundItem(c, item.id, c.getX(), c.getY(), 1, c.getId());
						}
					}
					ItemDefinitions def = ItemDefinitions.forId(item.id);
					LoggingSystem.writeOutput(c.getUsername(), "duel/won_duel", "Won Item [id = " + item.id + ", name = " + (def != null ? def.getName() : "") + ", amount = " + item.amount + "]");
				}
			}
		}
		for (GameItem item : stakedItems) {
			if (item.id > 0 && item.amount > 0) {
				if (Items.itemStackable[item.id]) {
					if (!c.getItems().addItem(item.id, item.amount)) {
						Server.itemHandler.createGroundItem(c, item.id, c.getX(), c.getY(), item.amount, c.getId());
					}
				} else {
					int amount = item.amount;
					for (int a = 1; a <= amount; a++) {
						if (!c.getItems().addItem(item.id, 1)) {
							Server.itemHandler.createGroundItem(c, item.id, c.getX(), c.getY(), 1, c.getId());
						}
					}
				}
			}
		}
		resetDuel();
		resetDuelItems();
		c.duelStatus = 0;
	}

	public void declineDuel() {
		c.canOffer = true;
		c.duelStatus = 0;
		c.duelingWith = 0;
		c.duelSpaceReq = 0;
		c.duelRequested = false;
		for (GameItem item : stakedItems) {
			if (item.amount < 1)
				continue;
			if (Items.itemStackable[item.id] || Items.itemIsNote[item.id]) {
				c.getItems().addItem(item.id, item.amount);
			} else {
				c.getItems().addItem(item.id, 1);
			}
		}
		stakedItems.clear();
		for (int i = 0; i < c.duelRule.length; i++) {
			c.duelRule[i] = false;
		}
		c.getPA().removeAllWindows();
	}

	public void resetDuel() {
		c.getPA().sendOption("Challenge", 1);
		c.headIconHints = 0;
		for (int i = 0; i < c.duelRule.length; i++) {
			c.duelRule[i] = false;
		}
		c.getPA().createPlayerHints(10, -1);
		c.duelStatus = 0;
		c.canOffer = true;
		c.duelSpaceReq = 0;
		c.duelingWith = 0;
		c.getPA().requestUpdates();
		c.getCombat().resetPlayerAttack();
		c.duelRequested = false;
	}

	public void resetDuelItems() {
		stakedItems.clear();
		otherStakedItems.clear();
	}

	public void changeDuelStuff() {
		Player o = World.players[c.duelingWith];
		if (o == null) {
			return;
		}
		o.duelStatus = 1;
		c.duelStatus = 1;
		o.getPA().sendFrame126("", 6684);
		c.getPA().sendFrame126("", 6684);
	}

	public boolean increaseByTwo(int rule) {
		if (rule == 16) {
			if (c.getEquipment()[3] > 0 && c.getItems().is2handed(c.getItems().getItemName(c.getEquipment()[3]), c.getEquipment()[3]) && c.getEquipment()[PlayerConstants.PLAYER_SHIELD] > 0) {
				return true;
			}
		}
		return false;
	}

	public void selectRule(int i) { // rules
		Player o = World.players[c.duelingWith];
		if (o == null) {
			return;
		}
		if (c.duelStatus < 1 || c.duelStatus > 2) {
			return;
		}
		if (!c.canOffer)
			return;
		changeDuelStuff();
		o.duelSlot = c.duelSlot;
		if (i >= 11 && c.duelSlot > -1) {
			if (c.getEquipment()[c.duelSlot] > 0) {
				if (!c.duelRule[i]) {
					c.duelSpaceReq++;
				} else {
					c.duelSpaceReq--;
				}
			}
			if (o.getEquipment()[o.duelSlot] > 0) {
				if (!o.duelRule[i]) {

					o.duelSpaceReq++;
				} else {
					o.duelSpaceReq--;
				}
			}
		}

		if (i >= 11) {
			if (c.getItems().freeSlots() < (c.duelSpaceReq) || o.getItems().freeSlots() < (o.duelSpaceReq)) {
				c.sendMessage("You or your opponent don't have the required space to set this rule.");
				if (c.getEquipment()[c.duelSlot] > 0) {
					c.duelSpaceReq--;
				}
				if (o.getEquipment()[o.duelSlot] > 0) {
					o.duelSpaceReq--;
				}
				return;
			}
		}

		if (!c.duelRule[i]) {
			c.duelRule[i] = true;
			c.duelOption += c.DUEL_RULE_ID[i];
		} else {
			c.duelRule[i] = false;
			c.duelOption -= c.DUEL_RULE_ID[i];
		}
		c.lastRuleSelected = System.currentTimeMillis();
		c.getPA().sendFrame87(286, c.duelOption);
		o.duelOption = c.duelOption;
		o.duelRule[i] = c.duelRule[i];
		o.getPA().sendFrame87(286, o.duelOption);

		if (c.duelRule[8]) {
			if (c.duelRule[1]) {
				c.duelTeleX = 3366 + Misc.random(12);
				o.duelTeleX = c.duelTeleX - 1;
				c.duelTeleY = 3246 + Misc.random(6);
				o.duelTeleY = c.duelTeleY;
			}
		} else {
			if (c.duelRule[1]) {
				c.duelTeleX = 3335 + Misc.random(12);
				o.duelTeleX = c.duelTeleX - 1;
				c.duelTeleY = 3246 + Misc.random(6);
				o.duelTeleY = c.duelTeleY;
			}
		}

	}

}
