package com.oldschoolpk.server.model.content;

import com.oldschoolpk.server.model.player.Player;

public class ItemsKeptOnDeathInterface {
	
	public void ResetKeepItems(Player player) {
		player.willKeepAmt1 = -1;
		player.willKeepItem1 = -1;
		player.willKeepAmt2 = -1;
		player.willKeepItem2 = -1;
		player.willKeepAmt3 = -1;
		player.willKeepItem3 = -1;
		player.willKeepAmt4 = -1;
		player.willKeepItem4 = -1;
	}

	public void StartBestItemScan(Player player) {
		if (player.isSkulled && !player.prayerActive[10]) {
			ItemKeptInfo(player, 0);
			return;
		}
		FindItemKeptInfo(player);
		ResetKeepItems(player);
		BestItem1(player);
	}

	public void FindItemKeptInfo(Player player) {
		if (player.isSkulled && player.prayerActive[10])
			ItemKeptInfo(player, 1);
		else if (!player.isSkulled && !player.prayerActive[10])
			ItemKeptInfo(player, 3);
		else if (!player.isSkulled && player.prayerActive[10])
			ItemKeptInfo(player, 4);
	}

	public void ItemKeptInfo(Player player, int Lose) {
		for (int i = 17109; i < 17131; i++) {
			player.getPA().sendFrame126("", i);
		}
		player.getPA().sendFrame126("Items you will keep on death:", 17104);
		player.getPA().sendFrame126("Items you will lose on death:", 17105);
		player.getPA().sendFrame126("Player Information", 17106);
		player.getPA().sendFrame126("Max items kept on death:", 17107);
		player.getPA().sendFrame126("~ " + Lose + " ~", 17108);
		player.getPA().sendFrame126("The normal amount of", 17111);
		player.getPA().sendFrame126("items kept is three.", 17112);
		switch (Lose) {
		case 0:
		default:
			player.getPA().sendFrame126("Items you will keep on death:", 17104);
			player.getPA().sendFrame126("Items you will lose on death:", 17105);
			player.getPA().sendFrame126("You're marked with a", 17111);
			player.getPA().sendFrame126("@red@skull. @lre@This reduces the", 17112);
			player.getPA().sendFrame126("items you keep from", 17113);
			player.getPA().sendFrame126("three to zero!", 17114);
			break;
		case 1:
			player.getPA().sendFrame126("Items you will keep on death:", 17104);
			player.getPA().sendFrame126("Items you will lose on death:", 17105);
			player.getPA().sendFrame126("You're marked with a", 17111);
			player.getPA().sendFrame126("@red@skull. @lre@This reduces the", 17112);
			player.getPA().sendFrame126("items you keep from", 17113);
			player.getPA().sendFrame126("three to zero!", 17114);
			player.getPA().sendFrame126("However, you also have", 17115);
			player.getPA().sendFrame126("the @red@Protect @lre@Items prayer", 17116);
			player.getPA().sendFrame126("active, which saves you", 17117);
			player.getPA().sendFrame126("one extra item!", 17118);
			break;
		case 3:
			player.getPA().sendFrame126("Items you will keep on death(if not skulled):", 17104);
			player.getPA().sendFrame126("Items you will lose on death(if not skulled):", 17105);
			player.getPA().sendFrame126("You have no factors", 17111);
			player.getPA().sendFrame126("affecting the items you", 17112);
			player.getPA().sendFrame126("keep.", 17113);
			break;
		case 4:
			player.getPA().sendFrame126("Items you will keep on death(if not skulled):", 17104);
			player.getPA().sendFrame126("Items you will lose on death(if not skulled):", 17105);
			player.getPA().sendFrame126("You have the @red@Protect", 17111);
			player.getPA().sendFrame126("@red@Item @lre@prayer active,", 17112);
			player.getPA().sendFrame126("which saves you one", 17113);
			player.getPA().sendFrame126("extra item!", 17114);
			break;
		}
	}

	public void BestItem1(Player player) {
		int BestValue = 0;
		int NextValue = 0;
		int ItemsContained = 0;
		player.willKeepItem1 = 0;
		player.willKeepItem1Slot = 0;
		for (int ITEM = 0; ITEM < 28; ITEM++) {
			if (player.playerItems[ITEM] > 0) {
				ItemsContained += 1;
				NextValue = (int) Math.floor(player.getShops().getItemShopValue(player.playerItems[ITEM] - 1));
				if (NextValue > BestValue) {
					BestValue = NextValue;
					player.willKeepItem1 = player.playerItems[ITEM] - 1;
					player.willKeepItem1Slot = ITEM;
					if (player.playerItemsN[ITEM] > 2 && !player.prayerActive[10]) {
						player.willKeepAmt1 = 3;
					} else if (player.playerItemsN[ITEM] > 3 && player.prayerActive[10]) {
						player.willKeepAmt1 = 4;
					} else {
						player.willKeepAmt1 = player.playerItemsN[ITEM];
					}
				}
			}
		}
		for (int EQUIP = 0; EQUIP < 14; EQUIP++) {
			if (player.playerEquipment[EQUIP] > 0) {
				ItemsContained += 1;
				NextValue = (int) Math.floor(player.getShops().getItemShopValue(player.playerEquipment[EQUIP]));
				if (NextValue > BestValue) {
					BestValue = NextValue;
					player.willKeepItem1 = player.playerEquipment[EQUIP];
					player.willKeepItem1Slot = EQUIP + 28;
					if (player.playerEquipmentN[EQUIP] > 2 && !player.prayerActive[10]) {
						player.willKeepAmt1 = 3;
					} else if (player.playerEquipmentN[EQUIP] > 3 && player.prayerActive[10]) {
						player.willKeepAmt1 = 4;
					} else {
						player.willKeepAmt1 = player.playerEquipmentN[EQUIP];
					}
				}
			}
		}
		if (!player.isSkulled && ItemsContained > 1 && (player.willKeepAmt1 < 3 || (player.prayerActive[10] && player.willKeepAmt1 < 4))) {
			BestItem2(player, ItemsContained);
		}
	}

	public void BestItem2(Player player, int ItemsContained) {
		int BestValue = 0;
		int NextValue = 0;
		player.willKeepItem2 = 0;
		player.willKeepItem2Slot = 0;
		for (int ITEM = 0; ITEM < 28; ITEM++) {
			if (player.playerItems[ITEM] > 0) {
				NextValue = (int) Math.floor(player.getShops().getItemShopValue(player.playerItems[ITEM] - 1));
				if (NextValue > BestValue && !(ITEM == player.willKeepItem1Slot && player.playerItems[ITEM] - 1 == player.willKeepItem1)) {
					BestValue = NextValue;
					player.willKeepItem2 = player.playerItems[ITEM] - 1;
					player.willKeepItem2Slot = ITEM;
					if (player.playerItemsN[ITEM] > 2 - player.willKeepAmt1 && !player.prayerActive[10]) {
						player.willKeepAmt2 = 3 - player.willKeepAmt1;
					} else if (player.playerItemsN[ITEM] > 3 - player.willKeepAmt1 && player.prayerActive[10]) {
						player.willKeepAmt2 = 4 - player.willKeepAmt1;
					} else {
						player.willKeepAmt2 = player.playerItemsN[ITEM];
					}
				}
			}
		}
		for (int EQUIP = 0; EQUIP < 14; EQUIP++) {
			if (player.playerEquipment[EQUIP] > 0) {
				NextValue = (int) Math.floor(player.getShops().getItemShopValue(player.playerEquipment[EQUIP]));
				if (NextValue > BestValue && !(EQUIP + 28 == player.willKeepItem1Slot && player.playerEquipment[EQUIP] == player.willKeepItem1)) {
					BestValue = NextValue;
					player.willKeepItem2 = player.playerEquipment[EQUIP];
					player.willKeepItem2Slot = EQUIP + 28;
					if (player.playerEquipmentN[EQUIP] > 2 - player.willKeepAmt1 && !player.prayerActive[10]) {
						player.willKeepAmt2 = 3 - player.willKeepAmt1;
					} else if (player.playerEquipmentN[EQUIP] > 3 - player.willKeepAmt1 && player.prayerActive[10]) {
						player.willKeepAmt2 = 4 - player.willKeepAmt1;
					} else {
						player.willKeepAmt2 = player.playerEquipmentN[EQUIP];
					}
				}
			}
		}
		if (!player.isSkulled && ItemsContained > 2 && (player.willKeepAmt1 + player.willKeepAmt2 < 3 || (player.prayerActive[10] && player.willKeepAmt1 + player.willKeepAmt2 < 4))) {
			BestItem3(player, ItemsContained);
		}
	}

	public void BestItem3(Player player, int ItemsContained) {
		int BestValue = 0;
		int NextValue = 0;
		player.willKeepItem3 = 0;
		player.willKeepItem3Slot = 0;
		for (int ITEM = 0; ITEM < 28; ITEM++) {
			if (player.playerItems[ITEM] > 0) {
				NextValue = (int) Math.floor(player.getShops().getItemShopValue(player.playerItems[ITEM] - 1));
				if (NextValue > BestValue && !(ITEM == player.willKeepItem1Slot && player.playerItems[ITEM] - 1 == player.willKeepItem1) && !(ITEM == player.willKeepItem2Slot && player.playerItems[ITEM] - 1 == player.willKeepItem2)) {
					BestValue = NextValue;
					player.willKeepItem3 = player.playerItems[ITEM] - 1;
					player.willKeepItem3Slot = ITEM;
					if (player.playerItemsN[ITEM] > 2 - (player.willKeepAmt1 + player.willKeepAmt2) && !player.prayerActive[10]) {
						player.willKeepAmt3 = 3 - (player.willKeepAmt1 + player.willKeepAmt2);
					} else if (player.playerItemsN[ITEM] > 3 - (player.willKeepAmt1 + player.willKeepAmt2) && player.prayerActive[10]) {
						player.willKeepAmt3 = 4 - (player.willKeepAmt1 + player.willKeepAmt2);
					} else {
						player.willKeepAmt3 = player.playerItemsN[ITEM];
					}
				}
			}
		}
		for (int EQUIP = 0; EQUIP < 14; EQUIP++) {
			if (player.playerEquipment[EQUIP] > 0) {
				NextValue = (int) Math.floor(player.getShops().getItemShopValue(player.playerEquipment[EQUIP]));
				if (NextValue > BestValue && !(EQUIP + 28 == player.willKeepItem1Slot && player.playerEquipment[EQUIP] == player.willKeepItem1)
						&& !(EQUIP + 28 == player.willKeepItem2Slot && player.playerEquipment[EQUIP] == player.willKeepItem2)) {
					BestValue = NextValue;
					player.willKeepItem3 = player.playerEquipment[EQUIP];
					player.willKeepItem3Slot = EQUIP + 28;
					if (player.playerEquipmentN[EQUIP] > 2 - (player.willKeepAmt1 + player.willKeepAmt2) && !player.prayerActive[10]) {
						player.willKeepAmt3 = 3 - (player.willKeepAmt1 + player.willKeepAmt2);
					} else if (player.playerEquipmentN[EQUIP] > 3 - player.willKeepAmt1 && player.prayerActive[10]) {
						player.willKeepAmt3 = 4 - (player.willKeepAmt1 + player.willKeepAmt2);
					} else {
						player.willKeepAmt3 = player.playerEquipmentN[EQUIP];
					}
				}
			}
		}
		if (!player.isSkulled && ItemsContained > 3 && player.prayerActive[10] && ((player.willKeepAmt1 + player.willKeepAmt2 + player.willKeepAmt3) < 4)) {
			BestItem4(player);
		}
	}

	public void BestItem4(Player player) {
		int BestValue = 0;
		int NextValue = 0;
		player.willKeepItem4 = 0;
		player.willKeepItem4Slot = 0;
		for (int ITEM = 0; ITEM < 28; ITEM++) {
			if (player.playerItems[ITEM] > 0) {
				NextValue = (int) Math.floor(player.getShops().getItemShopValue(player.playerItems[ITEM] - 1));
				if (NextValue > BestValue && !(ITEM == player.willKeepItem1Slot && player.playerItems[ITEM] - 1 == player.willKeepItem1) && !(ITEM == player.willKeepItem2Slot && player.playerItems[ITEM] - 1 == player.willKeepItem2)
						&& !(ITEM == player.willKeepItem3Slot && player.playerItems[ITEM] - 1 == player.willKeepItem3)) {
					BestValue = NextValue;
					player.willKeepItem4 = player.playerItems[ITEM] - 1;
					player.willKeepItem4Slot = ITEM;
				}
			}
		}
		for (int EQUIP = 0; EQUIP < 14; EQUIP++) {
			if (player.playerEquipment[EQUIP] > 0) {
				NextValue = (int) Math.floor(player.getShops().getItemShopValue(player.playerEquipment[EQUIP]));
				if (NextValue > BestValue && !(EQUIP + 28 == player.willKeepItem1Slot && player.playerEquipment[EQUIP] == player.willKeepItem1)
						&& !(EQUIP + 28 == player.willKeepItem2Slot && player.playerEquipment[EQUIP] == player.willKeepItem2) && !(EQUIP + 28 == player.willKeepItem3Slot && player.playerEquipment[EQUIP] == player.willKeepItem3)) {
					BestValue = NextValue;
					player.willKeepItem4 = player.playerEquipment[EQUIP];
					player.willKeepItem4Slot = EQUIP + 28;
				}
			}
		}
	}

}
