package com.oldschoolpk.server.model.content;

import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.util.Misc;

public class QuestTab {

	public static void updateInterface(Player client) {
		client.getPA().sendFrame126("Player Panel", 39159);
		client.getPA().sendFrame126("Players Online " + "@gre@[" + (World.getPlayerCount()) + "]", 39160);
		client.getPA().sendFrame126("Name " + "@gre@" + Misc.capitalize(client.playerName), 39161);
		client.getPA().sendFrame126("Rank " + "@gre@" + (client.getRights() != PlayerRights.PLAYER ? client.getRights().getTitle()
				: client.getDonatorRights().isDonator() ? client.getDonatorRights().getTitle() : client.getRights().getTitle()), 39162);
		client.getPA().sendFrame126("Kills " + "@red@[" + client.kills + "]", 39163);
		client.getPA().sendFrame126("Deaths " + "@red@[" + client.deaths + "]", 39164);
		client.getPA().sendFrame126("KDR " + "@red@[" + client.getPA().getKdr() + "]", 39165);
		client.getPA().sendFrame126("Killstreak " + "@red@[" + client.killStreak + "]", 39166);
		client.getPA().sendFrame126("Players In Wilderness " + "@red@[" + World.getPlayersInWildy() + "]", 39167);
		client.getPA().sendFrame126("Tokens " + "@or2@[" + client.donationPoints + "]", 39168);
		client.getPA().sendFrame126("Vote Points " + "@or2@[" + client.votePoints + "]", 39169);

	}

}