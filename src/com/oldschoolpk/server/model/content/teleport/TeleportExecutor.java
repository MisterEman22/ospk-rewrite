package com.oldschoolpk.server.model.content.teleport;

import java.util.Objects;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.core.cycle.CycleEvent;
import com.oldschoolpk.server.core.cycle.CycleEventContainer;
import com.oldschoolpk.server.core.cycle.CycleEventHandler;
import com.oldschoolpk.server.model.Animation;
import com.oldschoolpk.server.model.Graphic;
import com.oldschoolpk.server.model.Position;
import com.oldschoolpk.server.model.content.teleport.Teleport.TeleportType;
import com.oldschoolpk.server.model.player.Player;

/**
 * Executes a new teleport.
 * 
 * @date Aug 24, 2015 10:03:43 PM
 *
 */
public class TeleportExecutor {

	/**
	 * Starts a teleport process for the player with just a location using the
	 * players current magic book to determine the type
	 * 
	 * @param player
	 *            The {@link Player} trying to teleport
	 * @param location
	 *            The {@link Position} the player is teleporting too
	 */
	public static void teleport(Player player, Position location) {
		TeleportType type = player.playerMagicBook == 2 ? TeleportType.NORMAL : player.playerMagicBook == 1 ? TeleportType.ANCIENT : TeleportType.NORMAL;
		teleport(player, new Teleport(location, type), true);
	}

	/**
	 * Starts a teleport process for the player
	 * 
	 * @param player
	 *            The {@link Player} attempting to teleport
	 * @param teleport
	 *            The {@link Teleport} type to the location
	 */
	public static void teleport(Player player, Teleport teleport) {
		teleport(player, teleport, true);
	}

	/**
	 * Starts a teleport process for the player
	 * 
	 * @param player
	 *            The {@link Player} attempting to teleport
	 * @param teleport
	 *            The {@link Teleport} to the location
	 * @param requirements
	 *            Check requirements before attempting to teleport
	 */
	public static void teleport(Player player, Teleport teleport, boolean requirements) {
		if (player.teleTimer > 0) {
			return;
		}

		/**
		 * Check if we need to perform requirements on our teleport, if so
		 * validate our teleport, this allows us to perform an unchecked
		 * teleport for teleports such as the lever in the wilderness
		 */
		if (requirements && !canTeleport(player) && teleport.getType() != TeleportType.LEVER) {
			return;
		}

		/**
		 * The start animation of our teleport
		 */
		final int startAnim = teleport.getType().getStartAnimation();

		/**
		 * The end animation of our teleport
		 */
		final int endAnim = teleport.getType().getEndAnimation();

		/**
		 * The start graphic of our teleport
		 */
		int startGraphic = teleport.getType().getStartGraphic();

		/**
		 * The end graphic of our teleport
		 */
		final int endGraphic = teleport.getType().getEndGraphic();

		/**
		 * The initial delay of our teleport, the time it takes to start the
		 * animation till your able to walk again
		 */
		final int initialDelay = teleport.getType().getStartDelay() + teleport.getType().getEndDelay();

		/**
		 * Check if we need to play our start animation
		 */
		if (startAnim != -1) {
			player.playAnimation(Animation.create(startAnim));
		}

		/**
		 * Check if we need to play our start graphic
		 */
		if (startGraphic != -1) {
			if (startGraphic > 65535) {
				startGraphic = (startGraphic - 65535);
				player.playGraphic(Graphic.create(startGraphic, 0, 100));
			} else {
				player.playGraphic(Graphic.create(startGraphic));
			}
		}

		player.getCombat().resetPlayerAttack();
		player.stopMovement();
		player.getPA().removeAllWindows();
		player.npcIndex = player.playerIndex = 0;
		player.faceUpdate(-1);
		player.teleTimer = initialDelay - 1;
		CycleEventHandler.getSingleton().addEvent(player, 1, new CycleEvent() {

			/**
			 * A modifiable end gfx due to the nature of having to finalize the
			 * included variables so we can differentiate between the height
			 * levels of the graphic
			 */
			int endGfx = endGraphic;

			@Override
			public void execute(CycleEventContainer c) {
				player.teleTimer--;
				if (player.teleTimer == initialDelay - teleport.getType().getStartDelay()) {

					/**
					 * Finalize our location by setting our coordinates
					 */
					player.getPA().movePlayer(teleport.getLocation().getX(), teleport.getLocation().getY(), teleport.getLocation().getZ());

					/**
					 * Check if we need to play our end animation
					 */
					if (endAnim != -1) {
						player.playAnimation(Animation.create(endAnim));
					}

					/**
					 * Check if we need to play our end graphic
					 */
					if (endGfx != -1) {
						if (endGfx > 65535) {
							// differentiate between height levels
							endGfx = (endGfx - 65535);
							player.playGraphic(Graphic.create(endGfx, 0, 100));
						} else {
							player.playGraphic(Graphic.create(endGfx));
						}
					}

				}

				if (player.teleTimer == 0) {
					player.teleTimer = -1;
					player.teleporting = false;
					c.stop();
				}
			}

		});
	}

	/**
	 * Determines if the player can teleport under the current circumstances
	 * 
	 * @param player
	 *            The {@link Player} attempting to teleport
	 * @param teleport
	 *            The {@link Teleport} the player is currently doing
	 * @return If the player can teleport
	 */
	public static boolean canTeleport(Player player) {
		if (player.duelStatus == 5) {
			player.sendMessage("You can't teleport during this minigame.");
			return false;
		}
		if (!player.inSafeZone() && player.inWild() && player.wildLevel > Config.NO_TELEPORT_WILD_LEVEL) {
			player.sendMessage("You can't teleport above level " + Config.NO_TELEPORT_WILD_LEVEL + " in the wilderness.");
			return false;
		}
		if (System.currentTimeMillis() - player.teleBlockDelay < player.teleBlockLength) {
			player.sendMessage("You are teleblocked and can't teleport.");
			return false;
		}
		if (player.inFunPK() && !player.inSafeZone()) {
			player.sendMessage("You can't teleport in this area.");
			return false;
		}
		if (player.underAttackBy > 0 && !player.inSafeZone()) {
			player.sendMessage("You can't teleport during combat.");
			return false;
		}
		player.teleporting = true;
		player.getPA().removeAllWindows();
		player.npcIndex = 0;
		player.playerIndex = 0;
		player.faceUpdate(0);
		return true;
	}

	/**
	 * Gets the message sent when attempting to teleport while teleblocked
	 * 
	 * @param player
	 *            The {@link Player} attempting to teleport
	 * @return A converted message of the {@link Player}s teleblock delay
	 */
	private static String getTeleblockMessage(Player player) {
		long remaining = player.teleBlockLength - (System.currentTimeMillis() - player.teleBlockDelay);
		long left = (long) Math.ceil(remaining / 60000.0);
		return "You are teleblocked for the next " + (left == 0 ? 1 : left) + " minutes";
	}

}
