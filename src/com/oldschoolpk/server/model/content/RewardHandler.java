package com.oldschoolpk.server.model.content;

import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.model.player.DonatorRights;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.util.Misc;

public class RewardHandler {

	public static void giveWealth(Player client, long totalCoins, Player otherClient) {
		if (client.connectedFrom.equalsIgnoreCase(otherClient.connectedFrom) && client.getRights().isInferior(PlayerRights.DEVELOPER)) {
			client.sendMessage("You don't get rewards for killing someone on your own network.");
			return;
		}

		if (!client.shouldRewardKill(otherClient.connectedFrom) && client.getRights().isInferior(PlayerRights.DEVELOPER)) {
			client.sendMessage("You have recently killed @blu@" + otherClient.playerName + "@bla@...");
			client.sendMessage("...You have to wait before you can get a reward from the same player.");
			return;
		}

		totalCoins = 100_000;
		int killStreakMultiplier = 10_000;

		if (client.getDonatorRights() == DonatorRights.PREMIUM) {
			totalCoins += 15_000;
			killStreakMultiplier = 12_000;
		} else if (client.getDonatorRights() == DonatorRights.SUPER_PREMIUM) {
			totalCoins += 30_000;
			killStreakMultiplier = 15_000;
		} else if (client.getDonatorRights() == DonatorRights.EXTREME_PREMIUM) {
			totalCoins += 50_000;
			killStreakMultiplier = 17_000;
		}

		totalCoins += killStreakMultiplier * (otherClient.killStreak + client.killStreak);

		client.killStreak++;
		otherClient.killStreak = 0;

		if (client.killStreak > 2) {
			client.getPA().globalYell(
					"@cr6@[@blu@STREAK@bla@] @red@" + Misc.formatPlayerName(client.playerName)
							+ "@bla@ is on a kill streak, and have killed @blu@" + client.killStreak + "@bla@ people!");
		}

		totalCoins += otherClient.bounty;

		if (client.inWild()) {
			totalCoins += client.wildLevel * 500;
		}

		if (totalCoins > Integer.MAX_VALUE) {
			totalCoins = Integer.MAX_VALUE;
		}

		if (client.getItems().freeSlots() > 1) {
			client.getItems().addItem(995, (int) totalCoins);
		} else {
			client.sendMessage("You didn't have enough space in your inventory...");
			client.sendMessage("So @blu@" + Misc.formatNumbers((int) totalCoins)
					+ "@bla@ coins were dropped on the floor.");
			Server.itemHandler.createGroundItem(client, 995, otherClient.getX(), otherClient.getY(), (int) totalCoins,
					otherClient.killerId);
		}
		
		QuestTab.updateInterface(client);

		client.addRewardedKill(otherClient.connectedFrom, System.currentTimeMillis());
	}
}
