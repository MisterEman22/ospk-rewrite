package com.oldschoolpk.server.model.content;

import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.util.cache.defs.ItemDef;

public class ItemCombining {

	private static final int[][] COMBINE_DATA = { { 11818, 11820, 11794 }, { 11818, 11822, 11796 }, { 11820, 11822, 11800 }, { 11818, 11800, 11798 }, { 11822, 11794, 11798 }, { 11820, 11796, 11798 },
			{ 11810, 11798, 11802 }, { 11812, 11798, 11804 }, { 11814, 11798, 11806 }, { 11816, 11798, 11808 }, { 12004, 4151, 12006 }, { 1540, 11286, 11283 }, { 12833, 12829, 12831 },
			{ 12831, 12819, 12817 },

			{ 12831, 12823, 12821 }, { 12831, 12827, 12825 }, { 13231, 11840, 13239 }, { 13227, 6920, 13235 }, { 13229, 2577, 13237 }, { 12804, 11838, 12809 }, { 13200, 12931, 13197 },
			{ 13201, 12931, 13199 }, { 12353, 12432, 12434 } };

	private static final int[][] MAXCAPE = { { 13280, 13281, 6570, 13329, 13330 }, { 13280, 13281, 2412, 13331, 13332 }, { 13280, 13281, 2414, 13333, 13334 }, { 13280, 13281, 2413, 13335, 13336 },
			{ 13280, 13281, 10499, 13337, 13338 } };

	public static void combineItems(Player client, int itemUsed, int useWith) {
		for (int i = 0; i < COMBINE_DATA.length; i++) {
			if (itemUsed == COMBINE_DATA[i][0] && useWith == COMBINE_DATA[i][1] || useWith == COMBINE_DATA[i][0] && itemUsed == COMBINE_DATA[i][1]) {

				if (!client.getItems().playerHasItem(COMBINE_DATA[i][0], 1) || !client.getItems().playerHasItem(COMBINE_DATA[i][1], 1)) {
					return;

				}
				int item0 = COMBINE_DATA[i][0];
				int item1 = COMBINE_DATA[i][1];
				client.getItems().deleteItem(item0, client.getItems().getItemSlot(item0), 1);
				client.getItems().deleteItem(item1, client.getItems().getItemSlot(item1), 1);
				int item2 = COMBINE_DATA[i][2];
				client.getItems().addItem(item2, 1);
//				client.getDH().sendItemChat2(ItemDef.forId(item2).getName(), "You combine a @blu@" + ItemDef.forId(item0).getName() + " @bla@with a @blu@" + ItemDef.forId(item1).getName() + "@bla@,",
//						"and form a @blu@" + ItemDef.forId(item2).getName() + "@bla@.", item2, 250);
				return;
			}
		}
		for (int m = 0; m < MAXCAPE.length; m++) {
			if (itemUsed == MAXCAPE[m][0] && useWith == MAXCAPE[m][2]// 0 - 2
					|| itemUsed == MAXCAPE[m][1] && useWith == MAXCAPE[m][2] // 1
																				// -
																				// 2
					|| itemUsed == MAXCAPE[m][2] && useWith == MAXCAPE[m][0] // 2
																				// -
																				// 0
					|| itemUsed == MAXCAPE[m][2] && useWith == MAXCAPE[m][1]) {

				if (!client.getItems().playerHasItem(MAXCAPE[m][0], 1) || !client.getItems().playerHasItem(MAXCAPE[m][1], 1) || !client.getItems().playerHasItem(MAXCAPE[m][2], 1)) {
					client.sendMessage("Make sure you have both 'Max cape' and 'Max hood' in your inventory.");
					return;
				}
				int item3 = MAXCAPE[m][0];
				int item4 = MAXCAPE[m][1];
				int item5 = MAXCAPE[m][2];
				client.getItems().deleteItem(item3, client.getItems().getItemSlot(item3), 1);
				client.getItems().deleteItem(item4, client.getItems().getItemSlot(item4), 1);
				client.getItems().deleteItem(item5, client.getItems().getItemSlot(item5), 1);
				int item6 = MAXCAPE[m][3];
				int item7 = MAXCAPE[m][4];
				client.getItems().addItem(item6, 1);
				client.getItems().addItem(item7, 1);
				return;
			}
		}
	}

}
