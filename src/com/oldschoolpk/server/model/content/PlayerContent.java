package com.oldschoolpk.server.model.content;

import lombok.Data;

import com.oldschoolpk.server.model.player.Player;

@Data
public abstract class PlayerContent {

	protected Player player;
	
}