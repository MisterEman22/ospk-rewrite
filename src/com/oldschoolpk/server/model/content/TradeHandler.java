package com.oldschoolpk.server.model.content;

import com.oldschoolpk.server.Config;
import com.oldschoolpk.server.Server;
import com.oldschoolpk.server.core.tools.LoggingSystem;
import com.oldschoolpk.server.model.Trade;
import com.oldschoolpk.server.model.item.ItemDefinitions;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerRights;
import com.oldschoolpk.server.model.player.PlayerSave;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.util.cache.defs.ItemDef;

public class TradeHandler {

	private Player client;
	private Trade currentTrade = null;

	public void tradeItem(int itemID, int fromSlot, int amount) {

		Player other = getOther();

		if (other == null) {
			return;
		}

		if (fromSlot < 0 || fromSlot > 27) {
			return;
		}

		if (amount < 0 || amount > Integer.MAX_VALUE || client.playerItems[fromSlot] - 1 != itemID || client.playerItemsN[fromSlot] <= 0) {
			return;
		}

		if (stage != 1)
			return;
		for (int id : Config.ITEM_TRADEABLE) {
			if (itemID == id) {
				client.sendMessage("You can't trade this item.");
				return;
			}
		}

		boolean stackable = ItemDefinitions.forId(itemID).isStackable();
		boolean noted = ItemDefinitions.forId(itemID).isNoteable();
		int current = client.getItems().getItemAmount(itemID);

		if ((stackable || noted) && amount > client.playerItemsN[fromSlot]) {
			amount = client.playerItemsN[fromSlot];
		} else if (amount > 1 && !stackable && !noted && amount > current) {
			amount = current;
		}

		if (stackable || noted || amount == 1) {
			client.playerItemsN[fromSlot] -= amount;
			if (client.playerItemsN[fromSlot] == 0) {
				client.playerItems[fromSlot] = 0;
			}

			boolean found = false;

			if (stackable || noted)
				for (int i = 0; i < offer.length; i++) {
					if (offer[i] == itemID + 1) {
						offerN[i] += amount;
						found = true;
						break;
					}
				}
			if (!found) {
				for (int i = 0; i < offer.length; i++) {
					if (offer[i] == 0) {
						offer[i] = itemID + 1;
						offerN[i] = amount;
						break;
					}
				}
			}
		} else {

			int toDelete = amount;
			for (int i = 0; i < 28; i++) {
				if (client.playerItems[i] - 1 == itemID) {
					client.playerItems[i] = 0;
					client.playerItemsN[i] = 0;
					toDelete--;

					boolean found = false;
					if (stackable)
						for (int x = 0; x < 28; x++) {
							if (offer[x] == itemID + 1) {
								offerN[x]++;
								found = true;
								break;
							}
						}

					if (!found) {
						for (int x = 0; x < offer.length; x++) {
							if (offer[x] == 0) {
								offer[x] = itemID + 1;
								offerN[x] = 1;
								break;
							}
						}
					}
					if (toDelete == 0)
						break;
				}
			}

		}

		resetMyItems(3415);
		other.getTradeHandler().resetOtherItems(3416);
		if (accepted || other.getTradeHandler().accepted()) {
			accepted = false;
			other.getTradeHandler().setAccepted(false);
			client.getPA().sendFrame126("", 3431);
			other.getPA().sendFrame126("", 3431);
		}
		client.getItems().resetItems(3322);
	}

	public void fromTrade(int itemID, int fromSlot, int amount) {
		if (stage == 2)
			return;

		if (itemID < 0 || itemID > Config.ITEM_LIMIT || fromSlot < 0 || fromSlot > 27 || amount == 0) {
			resetMyItems(3415);
			return;
		}

		if (offer[fromSlot] - 1 != itemID || offerN[fromSlot] <= 0) {
			resetMyItems(3415);
			return;
		}

		boolean stackable = ItemDefinitions.forId(itemID).isStackable();
		boolean noted = ItemDefinitions.forId(itemID).isNoteable();
		int current = 0;

		for (int i = 0; i < offer.length; i++) {
			if (offer[i] - 1 == itemID) {
				current += offerN[i];
			}
		}

		if ((stackable || noted) && amount > offerN[fromSlot]) {
			amount = offerN[fromSlot];
		} else if (amount > 1 && !stackable && !noted && amount > current) {
			amount = current;
		}

		if (stackable || noted || amount == 1) {
			offerN[fromSlot] -= amount;
			if (offerN[fromSlot] == 0) {
				offer[fromSlot] = 0;
			}
		} else {

			int toDelete = amount;
			for (int i = 27; i >= 0; i--) {
				if (offer[i] - 1 == itemID) {
					offer[i] = 0;
					offerN[i] = 0;
					toDelete--;
					if (toDelete == 0)
						break;
				}
			}
		}

		client.getItems().addItem(itemID, amount);
		resetMyItems(3415);
		Player other = getOther();
		if (other != null) {
			other.getTradeHandler().resetOtherItems(3416);
		}
		if (accepted || other.getTradeHandler().accepted()) {
			accepted = false;
			other.getTradeHandler().setAccepted(false);
			client.getPA().sendFrame126("", 3431);
			if (other != null) {
				other.getPA().sendFrame126("", 3431);
			}
		}
		client.getItems().resetItems(3322);
	}

	public TradeHandler(Player client) {
		this.client = client;
	}

	public void requestTrade(Player otherClient) {
		if (Server.UpdateServer) {
			client.sendMessage("You can't trade right now, the server is about to update.");
			return;
		}
		if (client.distanceToPoint(otherClient.getX(), otherClient.getY()) < 1) {
			client.sendMessage("Please move closer to the person you are trying to trade.");
			return;
		}
		if (otherClient.getTradeHandler().getCurrentTrade() != null) {
			if (otherClient.getTradeHandler().getCurrentTrade().isOpen()) {
				client.sendMessage(otherClient.playerName + " is busy right now.");
				return;
			}
		}

		if (client.connectedFrom.equals(otherClient.connectedFrom) && client.getRights().isInferior(PlayerRights.DEVELOPER)) {
			client.sendMessage("You can't trade a person on your own network.");
			return;
		}

		if (currentTrade != null) {
			if (currentTrade.getEstablisher() == otherClient) {
				answerTrade(otherClient);
				return;
			} else {
				cancelCurrentTrade();
			}
		}
		currentTrade = new Trade(client, otherClient);
		otherClient.getTradeHandler().setCurrentTrade(currentTrade);
		otherClient.sendMessage(client.playerName + ":tradereq:");
		client.sendMessage("Sending trade request...");
	}

	public int stage = 0;
	public boolean accepted = false;

	public void answerTrade(Player otherClient) {
		if (Server.UpdateServer) {
			client.sendMessage("You can't trade when the server is about to update.");
			return;
		}
		if (client.distanceToPoint(otherClient.getX(), otherClient.getY()) < 1) {
			client.sendMessage("Please move closer to the person you are trying to trade.");
			return;
		}
		if (!client.withinDistance(otherClient)) {
			return;
		}
		if (currentTrade == null) {
			requestTrade(otherClient);
		} else {
			if (currentTrade.isOpen()) {
				cancelCurrentTrade();
				requestTrade(otherClient);
			} else {
				currentTrade.setOpen(true);
				client.getPA().sendFrame248(3323, 3321);
				client.getItems().resetItems(3322);
				resetMyItems(3415);
				resetOtherItems(3416);
				client.getPA().sendFrame126("TradeHandler With: " + otherClient.playerName, 3417);
				client.getPA().sendFrame126("", 3431);
				otherClient.getPA().sendFrame248(3323, 3321);
				otherClient.getItems().resetItems(3322);
				otherClient.getTradeHandler().resetMyItems(3415);
				otherClient.getTradeHandler().resetOtherItems(3416);
				otherClient.getPA().sendFrame126("TradeHandler With: " + client.playerName, 3417);
				otherClient.getPA().sendFrame126("", 3431);
				accepted = false;
				PlayerSave.saveGame(client);
				PlayerSave.saveGame(otherClient);
				client.getTradeHandler().stage = 1;
				otherClient.getTradeHandler().stage = 1;

			}
		}
	}

	private int[] offer = new int[28];
	private int[] offerN = new int[28];

	public void resetMyItems(int frame) {
		client.getClient().getOutStream().createFrameVarSizeWord(53);
		client.getClient().getOutStream().writeWord(frame);
		client.getClient().getOutStream().writeWord(offer.length);
		for (int i = 0; i < offer.length; i++) {
			if (offerN[i] > 254) {
				client.getClient().getOutStream().writeByte(255);
				client.getClient().getOutStream().writeDWord_v2(offerN[i]);
			} else {
				client.getClient().getOutStream().writeByte(offerN[i]);
			}
			client.getClient().getOutStream().writeWordBigEndianA(offer[i]);
		}
		client.getClient().getOutStream().endFrameVarSizeWord();
	}

	public Player getOther() {
		if (currentTrade == null) {
			return null;
		}
		Player other = null;
		;
		if (currentTrade.getReceiver() != client) {
			other = currentTrade.getReceiver();
		}
		if (currentTrade.getEstablisher() != client) {
			other = currentTrade.getEstablisher();
		}
		return other;
	}

	public void resetOtherItems(int frame) {
		Player other = getOther();
		if (other == null)
			return;
		int[] offer = other.getTradeHandler().getOffer();
		int[] offerN = other.getTradeHandler().getOfferN();
		client.getClient().getOutStream().createFrameVarSizeWord(53);
		client.getClient().getOutStream().writeWord(frame);
		client.getClient().getOutStream().writeWord(offer.length);
		for (int i = 0; i < offer.length; i++) {
			if (offerN[i] > 254) {
				client.getClient().getOutStream().writeByte(255);
				client.getClient().getOutStream().writeDWord_v2(offerN[i]);
			} else {
				client.getClient().getOutStream().writeByte(offerN[i]);
			}
			client.getClient().getOutStream().writeWordBigEndianA(offer[i]);
		}
		client.getClient().getOutStream().endFrameVarSizeWord();
	}

	public int[] getOffer() {
		return offer;
	}

	public int[] getOfferN() {
		return offerN;
	}

	public void decline() {
		Player other = getOther();
		if (other != null) {
			other.sendMessage(client.playerName + " has declined the trade.");
			other.getTradeHandler().setStage(0);
		}
		client.sendMessage("You decline the trade.");
		client.getTradeHandler().setStage(0);
		cancelCurrentTrade();
	}

	public void cancelCurrentTrade() {
		if (currentTrade.isOpen()) {
			currentTrade.getReceiver().getPA().removeAllWindows();
			currentTrade.getEstablisher().getPA().removeAllWindows();
			currentTrade.getReceiver().getTradeHandler().transferOfferToInventory();
			currentTrade.getEstablisher().getTradeHandler().transferOfferToInventory();
		}
		if (currentTrade.getReceiver() != client) {
			currentTrade.getReceiver().getTradeHandler().setCurrentTrade(null);
			currentTrade.getReceiver().getTradeHandler().setAccepted(false);
		}
		if (currentTrade.getEstablisher() != client) {
			currentTrade.getEstablisher().getTradeHandler().setCurrentTrade(null);
			currentTrade.getEstablisher().getTradeHandler().setAccepted(false);
		}
		if (currentTrade.getReceiver().getTradeHandler().client != null) {
			currentTrade.getReceiver().getTradeHandler().setStage(0);
		}
		if (currentTrade.getEstablisher().getTradeHandler().client != null) {
			currentTrade.getEstablisher().getTradeHandler().setStage(0);
		}

		currentTrade = null;
		accepted = false;
	}

	public void transferOfferToInventory() {
		Player other = getOther();
		if (other == null) {
			return;
		}
		for (int i = 0; i < offer.length; i++) {
			if (offer[i] == 0)
				continue;

			client.getItems().addItem(offer[i] - 1, offerN[i]);
			offer[i] = 0;
			offerN[i] = 0;
		}
	}

	public Trade getCurrentTrade() {
		return currentTrade;
	}

	public void setCurrentTrade(Trade currentTrade) {
		this.currentTrade = currentTrade;
	}

	public int itemsTraded() {
		int itemsTraded = 0;
		for (int i = 0; i < offer.length; i++) {
			if (offer[i] != 0)
				itemsTraded++;
		}
		return itemsTraded;
	}

	public void acceptStage1() {
		if (stage != 1)
			return;
		if (currentTrade != null && currentTrade.isOpen()) {
			Player other = getOther();
			if (other == null)
				return;
			if (itemsTraded() > other.getItems().freeSlots()) {
				client.sendMessage("The other player doesn't have enough space left in their inventory.");
				return;
			}
			if (other.getTradeHandler().itemsTraded() > client.getItems().freeSlots()) {
				client.sendMessage("There is not enough space in your inventory.");
				return;
			}
			accepted = true;

			if (!other.getTradeHandler().accepted()) {
				client.getPA().sendFrame126("Waiting for other player...", 3431);
				other.getPA().sendFrame126("Other player accepted.", 3431);
			} else {
				client.getPA().sendFrame248(3443, 3213);
				client.getItems().resetItems(3214);
				other.getPA().sendFrame248(3443, 3213);
				other.getItems().resetItems(3214);
				accepted = false;
				client.getTradeHandler().setStage(2);
				other.getTradeHandler().setStage(2);
				other.getTradeHandler().setAccepted(false);
				client.getPA().sendFrame126("Are you sure you want to accept this trade?", 3535);
				other.getPA().sendFrame126("Are you sure you want to accept this trade?", 3535);
				sendOffer2();
				sendOtherOffer2();
				other.getTradeHandler().sendOffer2();
				other.getTradeHandler().sendOtherOffer2();
			}
		}
	}

	public void sendOffer2() {
		StringBuilder trade = new StringBuilder();
		boolean empty = true;
		for (int i = 0; i < offer.length; i++) {
			String prefix = "";
			if (offer[i] > 0) {
				empty = false;
				if (offerN[i] >= 100 && offerN[i] < 1000000) {
					prefix = "@cya@" + (offerN[i] / 1000) + "K @whi@(" + offerN[i] + ")";
				} else if (offerN[i] >= 1000000) {
					prefix = "@gre@" + (offerN[i] / 1000000) + " million @whi@(" + offerN[i] + ")";
				} else {
					prefix = "" + offerN[i];
				}
				if (ItemDef.forId((offer[i] - 1)) != null)
					trade.append(ItemDef.forId((offer[i] - 1)).getName());
				else
					trade.append("");
				trade.append(" x ");
				trade.append(prefix);
				trade.append("\\n");
			}
		}
		if (empty) {
			trade.append("Absolutely nothing!");
		}
		client.getPA().sendFrame126(trade.toString(), 3557);
	}

	public void sendOtherOffer2() {
		Player other = getOther();
		if (other == null)
			return;
		int[] offer = other.getTradeHandler().getOffer();
		int[] offerN = other.getTradeHandler().getOfferN();
		StringBuilder trade = new StringBuilder();
		boolean empty = true;
		for (int i = 0; i < offer.length; i++) {
			String prefix = "";
			if (offer[i] > 0) {
				empty = false;
				if (offerN[i] >= 100 && offerN[i] < 1000000) {
					prefix = "@cya@" + (offerN[i] / 1000) + "K @whi@(" + offerN[i] + ")";
				} else if (offerN[i] >= 1000000) {
					prefix = "@gre@" + (offerN[i] / 1000000) + " million @whi@(" + offerN[i] + ")";
				} else {
					prefix = "" + offerN[i];
				}
				if (ItemDef.forId((offer[i] - 1)) != null)
					trade.append(ItemDef.forId((offer[i] - 1)).getName());
				else
					trade.append("");
				trade.append(" x ");
				trade.append(prefix);
				trade.append("\\n");
			}
		}
		if (empty) {
			trade.append("Absolutely nothing!");
		}
		client.getPA().sendFrame126(trade.toString(), 3558);
	}

	public void acceptStage2() {
		if (stage != 2)
			return;
		if (currentTrade != null && currentTrade.isOpen()) {
			Player other = getOther();
			if (other == null)
				return;

			if (!other.getTradeHandler().accepted()) {
				client.getPA().sendFrame126("Waiting for other player...", 3535);
				other.getPA().sendFrame126("Other player accepted.", 3535);
				accepted = true;
			} else {
				for (int i = 0; i < offer.length; i++) {
					if (offer[i] == 0)
						continue;
					other.getItems().addItem(offer[i] - 1, offerN[i]);
					ItemDefinitions def = ItemDefinitions.forId(offer[i] - 1);
					LoggingSystem.writeOutput(other.getUsername(), "trade/gave", "Gave item [id = " + (offer[i] - 1) + ", name = " + (def != null ? def.getName() : offer[i] - 1) + ", amount = " + Misc.format(offerN[i]) + "], person = " + other.getUsername());
					offer[i] = 0;
					offerN[i] = 0;
				}
				int[] offer = other.getTradeHandler().getOffer();
				int[] offerN = other.getTradeHandler().getOfferN();
				for (int i = 0; i < offer.length; i++) {
					if (offer[i] == 0)
						continue;
					client.getItems().addItem(offer[i] - 1, offerN[i]);
					ItemDefinitions def = ItemDefinitions.forId(offer[i] - 1);
					LoggingSystem.writeOutput(client.getUsername(), "trade/received", "Received item [id = " + (offer[i] - 1) + ", name = " + (def != null ? def.getName() : offer[i] - 1) + ", amount = " + Misc.format(offerN[i]) + "], person = " + other.getUsername());

					offer[i] = 0;
					offerN[i] = 0;
				}
				cancelCurrentTrade();
			}
		}
	}

	public boolean accepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public int getStage() {
		return this.stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}

}
