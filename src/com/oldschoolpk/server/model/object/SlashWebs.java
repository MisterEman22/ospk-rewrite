package com.oldschoolpk.server.model.object;

import com.oldschoolpk.server.model.Animation;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.util.WeaponAnimations;
import com.oldschoolpk.server.util.cache.region.Region;

public class SlashWebs {
	
	// x, y, face
	private static final int[][] WEBS = {
		{3092, 3957, 2, 0}, {3095, 3957, 0, 0}, {3158, 3951, 1, 10}
	};
	
	private static final boolean isWeb(int objectId) {
		switch (objectId) {
		case 733:
			return true;

		default:
			return false;
		}
	}

	private static boolean canSlashWeb(Player client) {
		String name = client.getItems().getItemName(client.getEquipment()[3]).toLowerCase();
		if (name.contains("scimitar") || name.contains("sword") || name.contains("abyssal") || name.contains("dagger")) {
			return true;
		}
		return false;
	}

	public static void slashWeb(Player client, int objectId, int obX, int obY) {
		if (System.currentTimeMillis() - client.actionDelay <= 1000) {
			return;
		}
		if (!isWeb(objectId)) {
			return;
		}

		int face = -1;
		int type = -1;
		
		for (int[] web : WEBS) {
			if (web[0] == obX && web[1] == obY) {
				face = web[2];
				type = web[3];
				break;
			}
		}
		
		if (face == -1 || type == -1) {
			return;
		}
		
		int remX = face == 0 ? obX - 1 : face == 2 ? obX + 1 : obX;
		int remY = face == 1 ? obY + 1 : face == 3 ? obY - 1 : obY;
		
		if (client.getX() == obX && client.getY() == obY) {
			client.turnPlayerTo(remX, remY);
		} else {
			client.turnPlayerTo(obX, obY);
		}
		
		client.playAnimation(Animation.create(WeaponAnimations.attackAnimation(client)));
		if (!canSlashWeb(client)) {
			client.sendMessage("You fail to slash the web...");
			client.sendMessage("...Perhaps you should try and use a melee weapon...");
			client.actionDelay = System.currentTimeMillis();
			return;
		}
		if (Misc.random(2) != 0) {
			client.sendMessage("You fail to slash the web...");
			client.actionDelay = System.currentTimeMillis();
			return;
		}
		
		client.sendMessage("You manage to slash the web.");
		
		new GameObject(734, obX, obY, 0, face, type, objectId, 30);
		Region.removeClipping(obX, obY, client.heightLevel);
		Region.removeClipping(remX, remY, client.heightLevel);

		client.actionDelay = System.currentTimeMillis();
		return;

	}

}
