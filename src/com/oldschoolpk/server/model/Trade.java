package com.oldschoolpk.server.model;

import com.oldschoolpk.server.model.player.Player;

public class Trade {

    private Player establisher;
    private Player receiver;
    private boolean open;

    public Trade(Player establisher, Player receiver) {
        this.establisher = establisher;
        this.receiver = receiver;
        this.open = false;
    }

    public Player getEstablisher() {
        return this.establisher;
    }

    public Player getReceiver() {
        return this.receiver;
    }

    public boolean isOpen() {
        return this.open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

}
