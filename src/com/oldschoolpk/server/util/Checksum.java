package com.oldschoolpk.server.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class Checksum {

	private static String clientChecksum = null;
	
	public static String getClientChecksum() {
		if (clientChecksum == null) {
			clientChecksum = getChecksum("gamepack");
			System.out.println("Client checksum set to: " + clientChecksum);
		}
		return clientChecksum;
	}
	
	private static String getChecksum(String fileName) {
		try {
			HttpURLConnection connection = open(CHECKSUM_SCRIPT + fileName);
			Scanner in = new Scanner(connection.getInputStream());
			String checksum = in.nextLine().trim();
			in.close();
			return checksum;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static HttpURLConnection open(String address) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) new URL(address).openConnection();
		connection.setUseCaches(false);
		connection.setDefaultUseCaches(false);
		connection.setConnectTimeout(20_000);
		connection.setReadTimeout(10_000);
		connection.setRequestMethod("GET");
		connection.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.0.13) Gecko/2009073021 Firefox/3.0.13");
		connection.setRequestProperty("Cache-Control", "no-store,max-age=0,no-cache");
		connection.setRequestProperty("Expires", "0");
		connection.setRequestProperty("Pragma", "no-cache");
		int responseCode = connection.getResponseCode();
		if (responseCode == HttpURLConnection.HTTP_OK) {
			return connection;
		} else {
			throw new IOException("HTTP server refused, response code: " + responseCode);
		}
	}
	
	private static final String CHECKSUM_SCRIPT = "http://www.oldschoolpk.com/assets/md5.php?file=";
	
}