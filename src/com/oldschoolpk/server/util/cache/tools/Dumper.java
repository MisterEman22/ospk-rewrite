package com.oldschoolpk.server.util.cache.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import com.oldschoolpk.server.util.cache.defs.ItemDef;

public class Dumper {

	public static void main(String[] args) {
		ItemDef.unpackConfig();
	}

	public static void dumpExamine(int totalItems) {
		BufferedWriter writer = null;
		ItemDef item = null;
		URL url = null;
		for (int i = 0; i < totalItems; i++) {
			item = ItemDef.forId(i);
			try {
				url = new URL("http://2007.runescape.wikia.com/wiki/"
						+ item.name.replaceAll(" ", "_"));
				System.out.println("Connecting to: " + url);
				URLConnection con;
				con = url.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String line;
				writer = new BufferedWriter(new FileWriter("defs.txt", true));
				while ((line = in.readLine()) != null) {
					writer.write("<item>");
					writer.newLine();
					writer.write("<id>");
					writer.write("" + i);
					writer.write("</id>");
					writer.newLine();
					writer.write("<name>");
					writer.write(item.getName());
					writer.write("</name>");
					writer.newLine();
					writer.write("<examine>");
					if (line.contains("<td colspan=\"2\" style=\"padding:3px 7px 3px 7px; line-height:140%; text-align:center;\"> ")) {
						line = line
								.replace(
										"<td colspan=\"2\" style=\"padding:3px 7px 3px 7px; line-height:140%; text-align:center;\"> ",
										"");
						writer.write(line);
						
					} else {
						writer.write(item.description);
					}
					writer.write("<examine>");
					writer.newLine();
					writer.write("</item>");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
