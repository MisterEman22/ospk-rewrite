package com.oldschoolpk.server.core.cycle.impl;

import com.oldschoolpk.server.core.cycle.CycleEvent;
import com.oldschoolpk.server.core.cycle.CycleEventContainer;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;
import com.oldschoolpk.server.model.player.PlayerConstants;

public class SpecialRestoreEvent extends CycleEvent {

    @Override
    public void execute(CycleEventContainer container) {
        for (int i = 0; i < World.players.length; i++) {
            if (World.players[i] == null) {
                continue;
            }
            Player client = World.players[i];
            client.getPA().sendFrame126("" + (int) client.specAmount * 10, 155);
            if (client.specAmount < 10) {
                client.specAmount += .5;
                if (client.specAmount > 10)
                    client.specAmount = 10;
                client.getItems().addSpecialBar(client.getEquipment()[PlayerConstants.PLAYER_WEAPON]);
            }
        }
    }

}
