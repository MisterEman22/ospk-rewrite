package com.oldschoolpk.server.core.cycle.impl;

import java.io.File;

import com.oldschoolpk.server.core.cycle.CycleEvent;
import com.oldschoolpk.server.core.cycle.CycleEventContainer;
import com.oldschoolpk.server.model.Client;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.util.Misc;

public class GlobalHelpEvent extends CycleEvent {

	private static File directory = new File("./data/characters/");

	@Override
	public void execute(CycleEventContainer container) {
		  String msg[] = {
				  		"@cr6@@blu@Did you know you can zoom in and out? <col=871C17>ctrl + mouse wheel",
				  		"@cr6@@blu@You can claim your vote reward by typing <col=871C17>::claim",
						"@cr6@@blu@You can type <col=871C17>::commands @blu@to find useful commands",
						"@cr6@@blu@Did you know that you can visit our forums at <col=871C17>http://oldschoolpk.com",
						"@cr6@@blu@You can help the server by voting <col=871C17>::vote",
						"@cr6@@blu@Did you know all bosses drop coins and <col=871C17>rare items?",
						"@cr6@@blu@Did you know to achieve the <col=871C17>max cape @blu@you need 100 kills and 5m?",
						"@cr6@@blu@You can help the server by donating for tokens by typing <col=871C17>::store" };
		  for (int i = 0; i < World.players.length; i++) {
			if (World.players[i] == null)
				continue;
			World.players[i].sendMessage(msg[(int) (Math
					.random() * msg.length)]);
		}
	}

}
