package com.oldschoolpk.server.core.cycle.impl;

import com.oldschoolpk.server.core.cycle.CycleEvent;
import com.oldschoolpk.server.core.cycle.CycleEventContainer;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;

public class RestoreStatsEvent extends CycleEvent {

    @Override
    public void execute(CycleEventContainer container) {
        for (int i = 0; i < World.players.length; i++) {
            if (World.players[i] == null) {
                continue;
            }
            Player client = World.players[i];
            for (int level = 0; level < client.getLevel().length; level++) {
                if (client.getLevel()[level] < client.getLevelForXP(client.getExperience()[level])) {
                    if (level != 5) { // prayer doesn't restore
                        client.getLevel()[level] += 1;
                        client.getPA().setSkillLevel(level, client.getLevel()[level], client.getExperience()[level]);
                        client.getPA().refreshSkill(level);
                    }
                } else if (client.getLevel()[level] > client.getLevelForXP(client.getExperience()[level])) {
                    client.getLevel()[level] -= 1;
                    client.getPA().setSkillLevel(level, client.getLevel()[level], client.getExperience()[level]);
                    client.getPA().refreshSkill(level);
                }
            }
        }
    }

}
