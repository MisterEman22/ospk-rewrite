package com.oldschoolpk.server.core.cycle.impl;

import com.oldschoolpk.server.core.cycle.CycleEvent;
import com.oldschoolpk.server.core.cycle.CycleEventContainer;
import com.oldschoolpk.server.model.World;
import com.oldschoolpk.server.model.player.Player;

public class InterfaceTextEvent extends CycleEvent {

	@Override
	public void execute(CycleEventContainer container) {
		for (int i = 0; i < World.players.length; i++) {
			if (World.players[i] == null) {
				continue;
			}
			Player client = World.players[i];
			if (!World.players[i].inSafeZone()) {
				int modY = World.players[i].absY > 6400 ? World.players[i].absY - 6400 : World.players[i].absY;
				World.players[i].wildLevel = (((modY - 3520) / 8) + 1);
				if (World.players[i].wildLevel < 1)
					World.players[i].wildLevel = 1;
				client.getPA().sendFrame36(600, 0);
				client.getPA().walkableInterface(28034);
				client.getPA().sendFrame126(client.drawLevels(), 28036);
				if (client.inWild()) {
					client.getPA().sendFrame126("Level: " + client.wildLevel, 28038);
				} else {
					client.getPA().sendFrame126("", 28038);
				}
				client.getPA().sendOption("Attack", 1);
			} else if (client.inSafeZone()) {
				if (client.inDuelArena() && client.duelStatus != 5) {
					client.getPA().sendOption("Challenge", 1);
				} else if (client.inDuelArena() && client.duelStatus > 4) {
					client.getPA().sendOption("Attack", 1);
				} else {
					client.getPA().sendOption("null", 1);
				}
				client.getPA().sendFrame126(client.drawLevels(), 28036);
				client.getPA().sendFrame126("", 28038);
				client.getPA().sendFrame36(600, 1);
			} else {
				client.getPA().sendFrame99(0);
				client.getPA().walkableInterface(-1);
				client.getPA().sendOption("Null", 1);
			}
		}
	}

}
