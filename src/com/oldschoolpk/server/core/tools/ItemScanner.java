package com.oldschoolpk.server.core.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.oldschoolpk.server.model.item.ItemDefinitions;
import com.oldschoolpk.server.util.Misc;
import com.oldschoolpk.server.world.ShopHandler;

public final class ItemScanner {

	/**
	 * An array of items to scan for
	 */
	private static final int[][] ITEMS = {
			/* Format [ { id, amount } ] */
			{ 12817, 1 }, { 12819, 1 }, { 12821, 1 },
			{ 12823, 1 }, { 12825, 1 }, { 12827, 1 },
			{ 12829, 1 }, { 12831, 1 }, 
			
	};
	
	private static enum ScanType {
		ITEM_SCAN("items"), 
		MONEYPOUCH_SCAN("money pouch totals"), 
		VOTE_POINT_SCAN("vote point totals"), 
		PK_POINT_SCAN("pvp point totals"), 
		ACHIEVEMENT_SCAN("achievement point totals"), 
		SECONDARY_ITEM_SCAN("secondary item scan");

		private final String description;

		ScanType(String description) {
			this.description = description;
		}
	}

	/**
	 * The total amount of hours to check for, 1 is the minimum.
	 */
	private static final int HOURS = 200000;

	/**
	 * The maximum amount of hours to check, anything less then this will be
	 * scanned
	 */
	public static final int TIME_PLAYED_REQUIRED = HOURS * 6000;

	/**
	 * The total amount of accounts scanned
	 */
	private static int totalAccounts;

	/**
	 * The type of scan to perform CHANGE THIS TO CHANGE THE TYPE OF SCANNING
	 * YOU WANT TO DO
	 */
	private static ScanType scanType = ScanType.ITEM_SCAN;

	/**
	 * The required minimum amount to check for money pouch totals
	 */
	private static final int MONEY_POUCH_TOTAL = 1_000_000_000;

	private static final int VOTE_POINT_TOTAL = 4;

	private static final int PK_POINT_TOTAL = 300;
	private static final int ACHIEVEMENT_POINT_TOTAL = 300;

	/**
	 * The total amount of each item found
	 */
	private static final Map<Integer, Long> totals = new HashMap<>();

	/**
	 * The total amount each player has of the items
	 */
	private static final Map<String, Integer> playerTotals = new HashMap<>();

	/**
	 * Starts up the scanner
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Loading up the item scanner...");
		try {
			System.out.println("Parsing Item Definitions...");
			ItemDefinitions.init();
			System.out.println("Scanning accounts for " + scanType.description + ".");
			scan();
		} catch (Exception e) {
			System.err.println("Something went wrong while loading definitions or scanning for items!");
			throw new RuntimeException(e);
		}
	}

	/**
	 * Performs the scan on the account
	 * 
	 * @throws IOException
	 */
	private static void scan() throws IOException {
		Files.walkFileTree(Paths.get("data/characters"), new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
				String character = path.toString();

				if (!character.endsWith(".txt")) {
					return FileVisitResult.CONTINUE;
				}

				totalAccounts++;

				int timePlayed = 0;

				try (BufferedReader reader = Files.newBufferedReader(path)) {
					for (String line; (line = reader.readLine()) != null; line = line.trim()) {
						int spot = line.indexOf("=");
						if (spot > -1) {
							String key = line.substring(0, spot).trim();
							String value = line.substring(spot + 1).trim();
							String[] values = value.split("\t");

							// Skip administrators
							if (key.startsWith("character-rights") && value.equals("3")) {
								continue;
							}

							if (key.startsWith("total-playtime")) {
								timePlayed = Integer.parseInt(value);
							}

							if (timePlayed >= TIME_PLAYED_REQUIRED) {
								return FileVisitResult.CONTINUE;
							}

							if (scanType == ScanType.ITEM_SCAN) {
								if (key.startsWith("character-item") || key.startsWith("bank-tab") || key.startsWith("character-equip")) {
									for (int[] expected : ITEMS) {
										int id = Integer.valueOf(values[1]);
										int amount = Integer.valueOf(values[2]);

										if (key.startsWith("character-item") || key.startsWith("bank-tab")) {
											id = id - 1;
										}
										ItemDefinitions def = ItemDefinitions.forId(id);
										if ((id == expected[0] || def != null && def.getNoteId() == expected[0]) && amount >= expected[1]) {
											totals.put(id, (long) (totals.get(id) == null ? 0 : totals.get(id)) + amount);
											playerTotals.put(character.replace(".txt", ""), amount);

											Path writePath = Files.createDirectories(Paths.get("data/items"));
											Path logPath = writePath.resolve(character.substring(character.lastIndexOf("\\") + 1));

											Files.deleteIfExists(logPath);
											Files.createFile(logPath);

											try (BufferedWriter writer = Files.newBufferedWriter(logPath, StandardOpenOption.APPEND)) {
												writer.write(def == null ? "?" : def.getName());
												writer.write(" (");
												writer.write("" + expected[0]);
												writer.write(") ");
												writer.write(" : ");
												writer.write("" + Misc.format(playerTotals.get(character.replace(".txt", ""))));
												writer.newLine();
												writer.flush(); // flush the
																// buffers
												// contents to disk
											}
										}
									}
								}
							} else if (scanType == ScanType.SECONDARY_ITEM_SCAN) {
								if (key.startsWith("character-item") || key.startsWith("bank-tab") || key.startsWith("character-equip")) {
									int id = Integer.valueOf(values[1]);
									int amount = Integer.valueOf(values[2]);

									if (key.startsWith("character-item") || key.startsWith("bank-tab")) {
										id = id - 1;
									}
									ItemDefinitions def = ItemDefinitions.forId(id);

									if (def != null && def.getShopValue() >= 2_000_000) {
										totals.put(id, (long) (totals.get(id) == null ? 0 : totals.get(id)) + amount);
										playerTotals.put(character.replace(".txt", ""), amount);

										Path writePath = Files.createDirectories(Paths.get("data/items"));
										Path logPath = writePath.resolve(character.substring(character.lastIndexOf("\\") + 1));

										Files.deleteIfExists(logPath);
										Files.createFile(logPath);

										try (BufferedWriter writer = Files.newBufferedWriter(logPath, StandardOpenOption.APPEND)) {
											writer.write(def == null ? "?" : def.getName());
											writer.write(" (");
											writer.write("" + def.getId());
											writer.write(") ");
											writer.write(" : ");
											writer.write("" + Misc.format(playerTotals.get(character.replace(".txt", ""))));
											writer.newLine();
											writer.flush(); // flush the
															// buffers
											// contents to disk
										}
									}
								}
							} else if (scanType == ScanType.MONEYPOUCH_SCAN) {
								if (key.startsWith("money-pouch")) {
									long amount = Long.parseLong(values[0]);

									if (amount >= MONEY_POUCH_TOTAL) {
										System.out.println(character + " has a money pouch total of " + Misc.format(amount) + ".");

									}
								}
							} else if (scanType == ScanType.ACHIEVEMENT_SCAN) {
								if (key.startsWith("achievement-points")) {
									int amount2 = Integer.parseInt(values[0]);

									if (amount2 >= ACHIEVEMENT_POINT_TOTAL) {
										System.out.println(character + " has a total of " + Misc.format(amount2) + " achievement points.");
									}
								}
							} else if (scanType == ScanType.PK_POINT_SCAN) {
								if (key.startsWith("pvp-points")) {
									int amount2 = Integer.parseInt(values[0]);

									if (amount2 >= PK_POINT_TOTAL) {
										System.out.println(character + " has a total of " + Misc.format(amount2) + " pk points.");

									}
								}
							} else if (scanType == ScanType.VOTE_POINT_SCAN) {
								if (key.startsWith("totalVotes")) {
									int amount2 = Integer.parseInt(values[0]);

									if (amount2 >= VOTE_POINT_TOTAL) {
										System.out.println(character + " has a total of " + Misc.format(amount2) + " vote points.");
									}
								}
							}
						}
					}
				}

				return FileVisitResult.CONTINUE;
			}
		});

		if (scanType == ScanType.ITEM_SCAN)

		{
			File file = Paths.get("data/item_info.txt").toFile();
			if (!file.exists()) {
				file.createNewFile();
			}
			try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("data/item_info.txt"), StandardOpenOption.APPEND)) {
				for (Entry<Integer, Long> entry : totals.entrySet()) {
					ItemDefinitions def = ItemDefinitions.forId(entry.getKey());

					writer.write(def.getName());
					writer.write(" (");
					writer.write("" + entry.getKey());
					writer.write(")");
					writer.write(" : ");
					writer.write("" + Misc.format(entry.getValue()));
					writer.newLine();
				}

				writer.flush(); // flush the buffers contents to disk
			}

			if (totals.isEmpty()) {
				System.out.println("No items were found.");
			} else {
				for (int[] items : ITEMS) {
					ItemDefinitions definition = ItemDefinitions.forId(items[0]);
					String name = definition != null ? definition.getName() : Integer.toString(items[0]);
					long found = totals.get(items[0]) != null ? totals.get(items[0]).longValue() : 0;
					System.out.println("Item: " + name + ", amount found: " + Misc.format(found));
				}
			}
		}

		System.out.println("Finishing searching a total of " + Misc.format(totalAccounts) + " account logs...");
	}

}