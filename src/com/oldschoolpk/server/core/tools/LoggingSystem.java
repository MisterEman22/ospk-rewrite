package com.oldschoolpk.server.core.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LoggingSystem {

	private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

	public static void writeOutput(String username, String directory, String output) {
		EXECUTOR.execute(() -> {
			try {
				File path = new File("./data/logs/" + directory + "/");
				if (!path.exists()) {
					path.mkdirs();
				}
				File file = new File("./" + path.getPath() + "/" + username + ".txt");
				BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date date = new Date();
				writer.write("[" + dateFormat.format(date) + "] " + output);
				writer.newLine();
				writer.write("----------------------------------------------");
				writer.newLine();
				writer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

}
