package com.oldschoolpk.server.core.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.oldschoolpk.server.model.item.ItemDefinitions;
import com.oldschoolpk.server.model.item.equipment.EquipmentBonuses;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.Sun14ReflectionProvider;

public class XStreamUtil {
	private static XStreamUtil instance = new XStreamUtil();
	private static XStream xStream = new XStream(new Sun14ReflectionProvider());

	public static XStreamUtil getInstance() {
		return instance;
	}

	public static XStream getxStream() {
		return xStream;
	}

	static {
		xStream.alias("item", ItemDefinitions.class);
		xStream.alias("equip", EquipmentBonuses.class);
	}

	public static void loadAllFiles() throws IOException {
		try {
			ItemDefinitions.init();
			EquipmentBonuses.init();
		} catch (ClassCastException e) {

		}
	}

	public static void writeXML(Object object, File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		try {
			xStream.toXML(object, out);
			out.flush();
		} finally {
			out.close();
		}
	}
}
