package com.oldschoolpk.server.core.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class BankTransferUtility {
	
	public static void main(String[] args) {
		File directory = new File("./data/characters/");
		
		if (!directory.exists()) {
			System.out.println("There is no character directory.");
			return;
		}
		
		File[] files = directory.listFiles();
		
		if (files.length < 1) {
			System.out.println("There are no character files.");
			return;
		}
		
		System.out.println("Starting the scan.");
		
		for (File file : files) {
			
			List<String> lines = new LinkedList<>();
			try (BufferedReader reader = Files.newBufferedReader(Paths.get(file.getPath()))) {
				String line = null;
				
				while ((line = reader.readLine()) != null) {
					if (line.startsWith("character-bank")) {
						String[] values = line.split("\t");
							lines.add("bank-tab = 0\t" + values[1] + "\t" + values[2]);
					} else {
						lines.add(line);
					}
				}
				reader.close();
				
				try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(file.getPath()))) {
					
					for (String s : lines) {
						writer.write(s);
						writer.newLine();
					}
					writer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Finished the scan.");
		
	}

}
